if exists("b:current_syntax")
	finish
endif
syn sync fromstart

syn keyword spec xprt mulx agnl agnh add fldw fsdw cos sin tan movll movlh movhl movhh
syn match num /r\@![0-9.]/
syn match dec0 /%r[0-9]d/
syn match dec1 /%r[0-9]d\@!/
hi num ctermfg=31 guifg=#0087af "rgb=0,135,175
hi dec0 ctermfg=203 guifg=#ff5f5f "rgb=255,95,95
hi dec1 ctermfg=131 guifg=#af5f5f "rgb=175,95,95
hi spec ctermfg=215 guifg=#ffaf5f "rgb=255,175,95
