#include "im.h"
#include "strf.h"
#include "io.h"
#include "m_alloc.h"
#include "pixels.h"
char static *bp;
_64_u static extract_num(_int_u *len) {
	char numbuf[64];
	_int_u i;
	i = 0;
	char c;
	while(1) {
		c = *(bp++);
		if (!(c>='0' && c<='9')) {
			break;
		}
		numbuf[i++] = c;
	}
	*len = i;
	return _ffly_dsn(numbuf,i);
}

void static pixels0(struct y_img *__im, _8_u *__buf) {
	_8_u *rgb = m_alloc(__im->width*__im->height*3);
	im_pread(__im->fd,rgb,__im->width*__im->height*3,__im->start);
	rgb_to_rgba(__buf,rgb,__im->width,__im->height);
	m_free(rgb);
}

void static pixels1(struct y_img *__im, float *__buf) {
	_8_u *rgb = m_alloc(__im->width*__im->height*3);
	im_pread(__im->fd,rgb,__im->width*__im->height*3,__im->start);
	rgb_to_rgba_float(__buf,rgb,__im->width,__im->height);
	m_free(rgb);
}

void static pixels2(struct y_img *__im, float *__buf) {
	_8_u *rgb = m_alloc(__im->width*__im->height*4);
	im_pread(__im->fd,rgb,__im->width*__im->height*4,__im->start);
	rgba_to_rgba_float(__buf,rgb,__im->width,__im->height);
	m_free(rgb);
}

void static rgb_8_8_8(struct y_img *__im, _8_u *__buf) {
	im_pread(__im->fd,__buf,__im->width*__im->height*3,__im->start);
}

void ppm_imload(struct y_img *__im, int fd) {
	printf("loading PPM-image.\n");
	__im->rgb_8_8_8 = rgb_8_8_8;
	__im->rgba_8_8_8_8 = pixels0;
	__im->rgba_rgba_float = pixels2;
	__im->rgb_rgba_float = pixels1;
	char buf[256];
	im_read(fd,buf,256);
	bp = buf;
	_int_u lc = 0;
	while(lc != 2) {
		if (*bp == '\n')
			lc++;	
		bp++;
	}
	_int_u w,h;
	_int_u k,j;
	w = extract_num(&k);
	h = extract_num(&j);
	bp--;

	__im->start = 54+k+j;
	ffly_fprintf(ffly_err, "IM: width: %u, height: %u.\n",w,h);
	__im->width = w;
	__im->height = h;
}



