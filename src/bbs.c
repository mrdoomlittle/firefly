# include "bbs.h"
# include "m_alloc.h"
# include "string.h"
#include "assert.h"
#define LIMIT 0x1000
struct bbs_node* bbsn_new(void) {
	struct bbs_node *n;
	n = (struct bbs_node*)m_alloc(sizeof(struct bbs_node));
	n->src = (_8_u*)m_alloc(LIMIT);
	mem_set(n->src, 0, LIMIT);
	n->ptr = 0;
	n->ptr0 = 0;
	return n;
}

void bbsn_destroy(struct bbs_node *__n) {
	m_free(__n->src);
	m_free(__n);
}

_32_u static
_read(struct bbs_node *__n, void *__buf, _int_u __size, _8_s *__error) {
	mem_cpy(__buf, __n->src+__n->ptr0, __size);
	__n->ptr0+=__size;
}


_32_u static
_write(struct bbs_node *__n, void *__buf, _int_u __size, _8_s *__error) {
	assert(__n->ptr+__size < LIMIT);
	mem_cpy(__n->src+__n->ptr, __buf, __size);
	__n->ptr+=__size;
}

void static
_seek(struct bbs_node *__n, _64_u __pos) {
	__n->ptr = __pos;
}

void static
_pwrite(struct bbs_node *__n, void *__buf, _int_u __size, _64_u __offset) {
	mem_cpy(__n->src+__offset, __buf, __size);
}
struct f_rw_funcs ith0 = {
	NULL,NULL,
	(void*)_read,
	(void*)_write,
};

struct bbs_ops ith = {
	(void*)_read,
	(void*)_write,
	(void*)_pwrite,
	(void*)_seek

};
