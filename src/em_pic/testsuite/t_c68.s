.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
_start:
;stput
clrw
clrw
movwi j[$-8
movwi j[$-7
movwi j[$-6
movwi j[$-5
movlw $239
movwi j[$-4
movlw $190
movwi j[$-3
movlw $173
movwi j[$-2
movlw $222
movwi j[$-1
;eq
;stpos
movlw $8
call subfsr1w
movlb $2
clrf $48
clrf $49
clrf $50
clrf $51
movlw 239
movwf $52
movlw 190
movwf $53
movlw 173
movwf $54
movlw 222
movwf $55
call cmpq
btfsic $56,$0
goto .L0
;eq
movlb $2
movlw 239
movwf $48
movlw 190
movwf $49
movlw 173
movwf $50
movlw 222
movwf $51
clrf $52
clrf $53
clrf $54
clrf $55
call cmpq
btfsis $56,$0
goto .L0
movlw $33
movlb $255
movwf $24
goto .L1
.L0
movlw $0
movlb $255
movwf $24
.L1
;add16
movlw $8
addwf j[$6
clrw
addwfc j[$7
ret
