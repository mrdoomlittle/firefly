.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
.comm a,1
.comm b,1
.comm c,1
.comm d,1
.comm e,1
.comm f,1
_start:
movlw a`l
movwf 4
movlw a`h
movwf 5
movlw $1
movwi $0
movlw b`l
movwf 4
movlw b`h
movwf 5
movlw $2
movwi $0
movlw c`l
movwf 4
movlw c`h
movwf 5
movlw $4
movwi $0
movlw d`l
movwf 4
movlw d`h
movwf 5
movlw $8
movwi $0
movlw e`l
movwf 4
movlw e`h
movwf 5
movlw $16
movwi $0
movlw f`l
movwf 4
movlw f`h
movwf 5
movlw $32
movwi $0
;stput
clrw
clrw
movwi j[$-2
movlw a`l
movwf 4
movlw a`h
movwf 5
;stpos
movlw $1
call subfsr1w
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
movlw b`l
movwf 4
movlw b`h
movwf 5
;stpos
addfsr j[$1
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
movlw c`l
movwf 4
movlw c`h
movwf 5
;stpos
addfsr j[$1
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
movlw d`l
movwf 4
movlw d`h
movwf 5
;stpos
addfsr j[$1
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
movlw e`l
movwf 4
movlw e`h
movwf 5
;stpos
addfsr j[$1
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
movlw f`l
movwf 4
movlw f`h
movwf 5
;stpos
addfsr j[$1
movf $0
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
movlw $1
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
;stput
;bo
movlw $30
subwf $1
movwf $1
;cassign
movf $1
movlb $255
movwf $24
;add16
movlw $2
addwf j[$6
clrw
addwfc j[$7
ret
