#include "vat.h"
#include "m_alloc.h"
#include "ffly_def.h"
#include "msg.h"
#include "io.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_VAT)
static mlock m = MUTEX_INIT;
struct y_vat *vt_top = NULL;
static struct y_vat vats[20];
static struct y_vat *vat_new = vats;
static struct y_vat *bin = NULL;
struct y_vat *vt_new(void) {
	struct y_vat *v;
	mt_lock(&m);
	if (bin != NULL) {
		v = bin;
		bin = bin->link;
		goto _sk;
	}

	v = vat_new++;
	MSG(INFO, "new vat.\n")
_sk:	
	v->next = vt_top;
	vt_top = v;
	mt_unlock(&m);

	v->flags = 0x00;
	v->in = NULL;
	v->in_end = NULL;
	v->in_m = MUTEX_INIT;
	v->cv = clockval;
	return v;
}

void vt_destroy(struct y_vat *__v) {
	mt_lock(&m);
	if (__v == vat_new-1) {
		vat_new--;
		goto _sk;
	}
	__v->link = bin;
	bin = __v;
_sk:
	mt_unlock(&m);
}
# include "system/nanosleep.h"
void vt_tick(struct y_vat *__v) {
	struct vt_task *cur,*nx;
	struct y_clockval now;
	_8_s res;
	if (__v->flags&VT_RAIB)
		goto _atmp;
	if (__v->in != NULL) {
		now = clockval;

	//	printf("---> %u.\n", now.val-__v->cv.val);
		if ((now.val-__v->cv.val)>TIME_FMS(1000)) {
		_atmp:
			res = mt_trylock(&__v->in_m);
			if (res == -1) {
				__v->flags |= VT_RAIB;
				goto _sk;
			}
			__v->in_end->next = __v->top;
			__v->top = __v->in;
			__v->in_end = NULL;
			__v->in = NULL;
			mt_unlock(&__v->in_m);
			__v->cv = clockval;
			__v->flags &= ~VT_RAIB;
		}
	}
_sk:
	cur = __v->top;
	
	while(cur != NULL) {
		nx = cur->next;
		cur->func(cur);
		cur = nx;
	}
}
/*
	HAVE lock ????
*/
struct y_vat* vt_desired(void) {
	struct y_vat *pref;
	struct y_vat *cur;
	while(!vt_top);
	mt_lock(&m);
	pref = vt_top;
	cur = pref->next;
	while(cur != NULL) {
		if (cur->n>pref->n) {
			pref = cur;
		}
		cur = cur->next;
	}
	mt_unlock(&m);
	while(!pref) {
		printf("VAT NULL.\n");
	}
	return pref;
}

struct vt_task* vt_task_add(void(*__func)(struct vt_task*), _ulonglong __arg) {
	if (!vt_top) {
		printf("no vats.\n");
		return NULL;
	}
	struct vt_task *t;
	t = (struct vt_task*)m_alloc(sizeof(struct vt_task));

	t->func = __func;
	t->arg = __arg;

	struct y_vat *pref;
	pref = vt_desired();
	

	mlock *m;
	m = &pref->in_m;
	mt_lock(m);
	if (!pref->in)
		pref->in = t;

	if (pref->in_end != NULL)
			pref->in_end->next = t;
	t->next = NULL;
	pref->in_end = t;
	mt_unlock(m);
	return t;
}

void vt_task_rm(struct vt_task *__tsk) {
	m_free(__tsk);
}

