rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd tools
ffly_objs="$ffly_objs"
gcc $cc_flags -o pic_dis pic_raw_dis.c $ffly_objs -nostdlib
gcc $cc_flags -o pic_slam_dis pic_slam_dis.c $ffly_objs -nostdlib
