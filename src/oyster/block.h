# ifndef __ffly__db__block__h
# define __ffly__db__block__h
# include "../y_int.h"
# include "../types.h"
# include "../oyster.h"
#define blkd_size sizeof(struct ffdb_blkd)
#define PAGE_SHIFT 6
#define PAGE_SIZE (1<<PAGE_SHIFT)
/*
	i was going to use slabs decided for this way as slabs can use this.
	and then it can be an option to the user what to use.
*/

// block
struct ffdb_blkd {
	_int_u size;
	_32_u end, off;
	struct ffdb_blkd *fd, *bk, *p;
	_32_u prev, next;
	_8_u inuse;
};


// later blocks should be alligned to page size
/*
	balloc returns an offset in the db file,
	later reads and writes will load that block into memory
	if not used for an x period of time then the block will be 
	loaded backinto the file and memory will be freed <- larger
	blocks will be loaded page by page
*/

typedef struct ffdb_blkd* ffdb_blkdp;

_32_u extern ffdb_btop;
ffdb_blkdp extern ffdb_bbin;
// block-alloc
_int_u ffdb_balloc(ffdbp, _int_u);

// block-free
void ffdb_bfree(ffdbp, _int_u);
void ffdb_blkd_write(ffdbp, ffdb_blkdp, _f_off_t);
void ffdb_blkd_read(ffdbp, ffdb_blkdp, _f_off_t);
# endif /*__ffly__db__block__h*/
