# include "../y_int.h"
# include "../ffly_def.h"
# include "../system/mutex.h"
/*
	should make this automatic
*/
#define NO_SLOTS 20
/*
	so we dont need to give the pointers to clients
*/
mlock static lock = FFLY_MUTEX_INIT;

void static *slot[NO_SLOTS];
void static **fresh = slot;

_int_u static vacant[NO_SLOTS];
_int_u static *next = vacant;

_int_u acquire_slot() {
	mt_lock(&lock);
	_int_u ret;
	if (next>vacant) {
		ret = *(--next);
		goto _end;
	}
	if (fresh>=slot+NO_SLOTS) {
		//err
		ret = 0;
		goto _end;
	}
	ret = (fresh++)-slot;
_end:
	mt_unlock(&lock);
	return ret;
}

void scrap_slot(_int_u __no) {
	mt_lock(&lock);
	if (__no>=NO_SLOTS)
		goto _end;
	void **p = slot+__no;
	if (p+1 == fresh)
		fresh--;
	else
		*(next++) = __no;
_end:
	mt_unlock(&lock);
}

void *slotget(_int_u __no) {
	mt_lock(&lock);
	void *ret;
	if (__no>=NO_SLOTS) {
		ret = NULL;
		goto _end;
	}
	ret = *(slot+__no);
_end:
	mt_unlock(&lock);
	return ret;
}

void slotput(_int_u __no, void *__p) {
	mt_lock(&lock);
	if (__no>=NO_SLOTS)
		goto _end;
	*(slot+__no) = __p;
_end:
	mt_unlock(&lock);
}
