#include "hungbuf.h"
#include "m_alloc.h"
#define HUNK_SHFT 8
#define HUNK_SIZE (1<<HUNK_SHFT)
#define HUNK_MASK (HUNK_SIZE-1)
void y_hbinit(struct y_hungbuf *__h, _int_u __bsz) {
	__h->hks = m_alloc(sizeof(struct hb_hunk));
	__h->nhks = 1;
	__h->bsz = __bsz;
	__h->hks->ptr = m_alloc(__bsz<<HUNK_SHFT);
	__h->off = 0;
}

void* y_hungbuf_alloc(struct y_hungbuf *__h) {
	_64_u hunk = __h->off>>HUNK_SHFT;
	_64_u hoff = __h->off&HUNK_MASK;

	if(hunk>=__h->nhks) {
		__h->nhks++;
		__h->hks = m_realloc(__h->hks,__h->nhks*sizeof(struct hb_hunk));
		__h->hks[hunk].ptr = m_alloc(__h->bsz<<HUNK_SHFT);
	}

	__h->off++;
	return ((_8_u*)__h->hks[hunk].ptr)+(hoff*__h->bsz);
}

void* y_hungbuf_at(struct y_hungbuf *__h, _64_u __off) {
	_64_u hunk = __off>>HUNK_SHFT;
	_64_u hoff = __off&HUNK_MASK;
	if(hunk>=__h->nhks)return NULL;
	return ((_8_u*)__h->hks[hunk].ptr)+(hoff*__h->bsz);
}
