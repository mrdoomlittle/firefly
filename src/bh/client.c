# include "../bh.h"
# include "../stdio.h"
# include "../string.h"
# include "../brick.h"
# include "../system/util/ff5.h"
# include "../dep/mem_cmp.h"
# include "../dep/mem_cpy.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
//static struct ff_bh bh;

/*
	ignore file, it for debuging
*/
/*
	TODO:
		add "load" command, load contents of file into bricks 

	rename - i dont care about naming but 
*/
struct wedge {
	void *p, *pp;
	struct wedge *next, **bk;
};

struct wedge *mem_top = NULL;

struct arg {
	_8_u *data;
	_int_u len;
};

struct cmd {
	_8_u *id;
	_int_u idlen;
	struct arg *args[20];
	_int_u argc;
};

void static* mem_alloc(_int_u __n) {
	_8_u *p;
	struct wedge *w;
	w = (struct wedge*)__f_mem_alloc(sizeof(struct wedge));
	p = (_8_u*)__f_mem_alloc(__n+sizeof(void*));
	void **wp;
	wp = (void**)p;
	p+=sizeof(void*);
	*wp = w;

	if (mem_top != NULL)
		mem_top->bk = &w->next;
	w->next = mem_top;
	mem_top = w;
	w->bk = &mem_top;

	return  w->pp = (void*)p;
}

void static mem_free(void *__p) {
	struct wedge *w;

	_8_u *p;
	p = ((_8_u*)__p)-sizeof(struct wedge*);
	w = *(void**)p;


	*w->bk = w->next;
	if (w->next != NULL)
		w->next->bk = w->bk;

	__f_mem_free(w);
	__f_mem_free(p);
}

void static* _memdup(void *__p, _int_u __n) {
	void *p;
	p = mem_alloc(__n);
	f_mem_cpy(p, __p, __n);
	return p;
}

struct cmd cur_command;

void static read_comd(char const *__line, _int_u __n) {
	char buf[2048];

	char *le;
	le = __line+__n+1; // add one to use only need to use '<' and not '<='
	char *lp, *bfp;
	_int_u ac;
	ac = 0;
	lp = __line;

	struct arg **ap, *a;

	ap = cur_command.args;
	bfp = buf;
	char c;
	c = *lp;
	while(c != ' ' && lp<le) {
		*(bfp++) = c;
		c = *(++lp);
	}

	_int_u idlen;
	idlen = bfp-buf;
	cur_command.id = (_8_u*)_memdup(buf, idlen);
	cur_command.idlen = idlen;
_again:
	if (lp<le) {
		bfp = buf;
		c = *(++lp);
		while(c != ' ' && lp<le) {
			*(bfp++) = c;
			c = *(++lp);
		}

		a = *(ap++) = mem_alloc(sizeof(struct arg));
		_int_u len;
		len = bfp-buf;
		a->data = (_8_u*)_memdup(buf, len);
		a->len = len;
		ac++;
		goto _again;
	}

	cur_command.argc = ac;
}

void static freeall(void) {
	struct wedge *cur, *bk;
	cur = mem_top;
	while(cur != NULL) {
		bk = cur;
		cur = cur->next;
		mem_free(bk->pp);
	}
}

_f_err_t ffmain(int __argc, char const *__argv[]) {

	ff_bh_open(&bh);
	ff_bh_connect(&bh, "127.0.0.1", 40960);	
	_f_err_t err;

/*	
	_int_u const n = 8;
	_32_u b[n];

	_int_u i;

	i = 0;
	while(i != n) {
		b[i++] = ff_bh_bnew(&bh, _ff_brick_256, &err);
	}

	i = 0;
	while(i != n) {
		ff_bh_brid(&bh, b[i++]);
	}
*/
//	ff_bh_bnewm(&bh, _ff_brick_256, b, n);
//	char buf[1024];

//	*(buf+ffly_ff5_enc(b, buf, n*sizeof(_32_u))) = '\0';
//	printf("brick key: %s\n", buf);
//	ff_bh_bridm(&bh, b, n);

	char line[8192];

	char buf[2048];
	_32_u b;
	_int_u n;
	_int_u i;
	struct arg *a0, *a1, *a2, *a3;
_again:
	printf("~: ");
	ffly_fdrain(_ffly_out);
	n = ffly_rdline(line, sizeof(line), _ffly_in);
	read_comd(line, n-1);

	printf("line: %s : %u\n", line, n);
	f_mem_cpy(buf, cur_command.id, cur_command.idlen);
	*(buf+cur_command.idlen) = '\0';
	printf("command : %s : %u\n", buf, cur_command.idlen);
	i = 0;
	while(i != cur_command.argc) {
		a0 = *(cur_command.args+i);
		f_mem_cpy(buf, a0->data, a0->len);
		*(buf+a0->len) = '\0';
		printf("%u arg : %s\n", i, buf);
		i++;
	}

	switch(cur_command.idlen) {
		case 3:
			// new <x num>
			if (!f_mem_cmp(cur_command.id, "new", 3)) {
				a0 = *cur_command.args;
				_int_u n;
				n = ffly_sntno(a0->data, a0->len);
				_32_u *b;
				b = (_32_u*)__f_mem_alloc(n*sizeof(_32_u));
				ff_bh_bnewm(&bh, _ff_brick_256, b, n);
				char bkbuf[2048];
				*(bkbuf+ffly_ff5_enc(b, bkbuf, n*sizeof(_32_u))) = '\0';
				__f_mem_free(b);
				printf("%s\n", bkbuf);
			// rid <x num> <key>
			} else if (!f_mem_cmp(cur_command.id, "rid", 3)) {
				a0 = *cur_command.args;
				a1 = *(cur_command.args+1);
				_int_u n;
				n = ffly_sntno(a0->data, a0->len);
				_32_u *b;
				b = (_32_u*)__f_mem_alloc(n*sizeof(_32_u));
				ffly_ff5_dec(a1->data, b, a1->len);
				ff_bh_bridm(&bh, b, n);
				__f_mem_free(b);
			}
		break;
		case 4:
			if (!f_mem_cmp(cur_command.id, "exit", 4)) {
				goto _esc;
			} else if (!f_mem_cmp(cur_command.id, "load", 4)) {
				a0 = *cur_command.args;
				a1 = *(cur_command.args+1);
				a2 = *(cur_command.args+2);
				char file[86];
				f_mem_cpy(file, a0->data, a0->len);
				*(file+a0->len) = '\0';
				_int_u n;
				n = ffly_sntno(a1->data, a1->len);
				_32_u *b;
				b = (_32_u*)__f_mem_alloc(n*sizeof(_32_u));
				ffly_ff5_dec(a2->data, b, a2->len);
			
				int fd;
				fd = open(file, O_RDONLY, 0);
				if (fd == -1) {
					printf("error file could not be opened.\n");
				}
				struct stat st;
				fstat(fd, &st);

				char buf[(1<<_ff_brick_256)];
				_int_u bc, i;
				bc = st.st_size>>_ff_brick_256;
				printf("BC: %u, SZ: %u\n", bc, st.st_size);
				i = 0;
				while(i != bc) {	
					read(fd, buf, 1<<_ff_brick_256);
					ff_bh_bwrite(&bh, *(b+i), buf, 1<<_ff_brick_256, 0);
					i++;
				}	

				_int_u left;
			
				if ((left = (st.st_size-(bc<<_ff_brick_256)))>0) {
					read(fd, buf, left);
					ff_bh_bwrite(&bh, *(b+i), buf, left, 0);
				}

				close(fd);			
				__f_mem_free(b);
			}
			break;
		case 5:
			if (!f_mem_cmp(cur_command.id, "store", 5)) {
				a0 = *cur_command.args;
				a1 = *(cur_command.args+1);
				a2 = *(cur_command.args+2);
				char file[86];
				f_mem_cpy(file, a0->data, a0->len);
				*(file+a0->len) = '\0';
				_int_u n;
				n = ffly_sntno(a1->data, a1->len);
				_32_u *b;
				b = (_32_u*)__f_mem_alloc(n*sizeof(_32_u));
				ffly_ff5_dec(a2->data, b, a2->len);
			
				int fd;
				fd = open(file, O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR);
				if (fd == -1) {
					printf("error file could not be opened.\n");
				}			

				char buf[(1<<_ff_brick_256)];
				_int_u i;
				i = 0;
				while(i != n) {	
					ff_bh_bread(&bh, *(b+i), buf, 1<<_ff_brick_256, 0);
					write(fd, buf, 1<<_ff_brick_256);
					i++;
				}	

				close(fd);			
				__f_mem_free(b);			

			}
		break;
	}

	freeall();
	goto _again;
_esc:
	freeall();
/*
	if (*__argv[1] == '#') {
		ffly_ff5_dec(__argv[2], &b, strlen(__argv[2]));
		if (!ff_bh_bexist(&bh, b, &err)) {
			ff_bh_bopen(&bh, b);
			ff_bh_bread(&bh, b, buf0, 256, 0);
			ff_bh_bclose(&bh, b);
			printf("%s\n", buf0);
		} else
			printf("brick/s do not exist.\n");
	} else {
		strcpy(buf0, __argv[2]);
		b = ff_bh_bnew(&bh, _ff_brick_256, &err);
		char buf[1024];
		*(buf+ffly_ff5_enc(&b, buf, sizeof(_32_u))) = '\0';
		printf("brick key: %s\n", buf);
		ff_bh_bopen(&bh, b);
		ff_bh_bwrite(&bh, b, buf0, 100, 0);
		ff_bh_bclose(&bh, b);
	}
*/
	ff_bh_disconnect(&bh);
	ff_bh_close(&bh);
}
