# ifndef __ffly__malloc__h
# define __ffly__malloc__h
#ifdef __fflib
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "memory/mem_realloc.h"
#define malloc(__bc) \
	__f_mem_alloc(__bc)
#define free(__p) \
	__f_mem_free(__p)
#define realloc(__p, __bc) \
	__f_mem_realloc(__p, __bc)
#else
#include <malloc.h>
#endif
# endif /*__ffly__malloc__h*/
