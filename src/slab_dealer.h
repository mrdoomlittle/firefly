#ifndef __slab__dealer__h
#define __slab__dealer__h
#include "y_int.h"
#include "types.h"

#define SD_SSHFT (9)
#define SD_SSIZE (1<<SD_SSHFT)
#define SD_SMASK (SD_SSIZE-1)
struct sd_slab{
	_64_u offset;
	struct sd_slab *next;
};

struct slab_dealer;

struct sd_area{
	_64_u *slabs;
	_int_u n_slabs;
	struct slab_dealer *dealer;
};
struct slab_dealer{
	struct sd_slab *sb_bin;
	_int_u n_inbin;
	_64_u offset;
	void(*read)(_ulonglong,void*,_64_u,_64_u);
	void(*write)(_ulonglong,void*,_64_u,_64_u);
	_ulonglong arg;
};
void sd_realloc(struct sd_area *__a,_64_u __size);
void sb_free(struct slab_dealer *__sd, _64_u __offset);
void sd_init(struct slab_dealer*);
struct sd_area* sd_alloc(struct slab_dealer*,_64_u);
void sd_free(struct sd_area*);

void sd_read(struct sd_area*,void*,_64_u,_64_u);
void sd_write(struct sd_area*,void*,_64_u,_64_u);
#endif/*__slab__dealer__h*/
