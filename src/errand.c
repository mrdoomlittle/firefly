#include "errand.h"
#include "mutex.h"
#include "ffly_def.h"
static mlock lock = MUTEX_INIT;
static struct y_errand *errand = NULL;
void y_errand_submit(struct y_errand *__er) {
	while(errand != NULL);
	mt_lock(&lock);
	errand = __er;
	mt_unlock(&lock);
}

void y_errand_tick(struct pcore *__p) {
	struct y_errand *er = NULL;
	if(!mt_trylock(&lock)) {
		er = errand;
		errand = NULL;
		mt_unlock(&lock);
	}
	if(er != NULL) {
		er->func(er);
	}
}
