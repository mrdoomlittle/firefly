# ifndef __linux__fb__h
# define __linux__fb__h
# include "../y_int.h"

#define FBIOGET_VSCREENINFO 0x4600
#define FBIOPUT_VSCREENINFO 0x4601
#define FBIOGET_FSCREENINFO 0x4602
#define FBIOPAN_DISPLAY     0x4606
struct fb_bitfield {
	_32_u offset;		   /* beginning of bitfield	*/
	_32_u length;		   /* length of bitfield	   */
	_32_u msb_right;		/* != 0 : Most significant bit is */
					/* right */
};

struct fb_var_screeninfo {
	_32_u xres;		 /* visible resolution	   */
	_32_u yres;
	_32_u xres_virtual;	 /* virtual resolution	   */
	_32_u yres_virtual;
	_32_u xoffset;		  /* offset from virtual to visible */
	_32_u yoffset;		  /* resolution		   */

	_32_u bits_per_pixel;	   /* guess what		   */
	_32_u grayscale;		/* 0 = color, 1 = grayscale,	*/
					/* >1 = FOURCC		  */
	struct fb_bitfield red;	 /* bitfield in fb mem if true color, */
	struct fb_bitfield green;   /* else only length is significant */
	struct fb_bitfield blue;
	struct fb_bitfield transp;  /* transparency		 */

	_32_u nonstd;		   /* != 0 Non standard pixel format */

	_32_u activate;		 /* see FB_ACTIVATE_*		*/

	_32_u height;		   /* height of picture in mm	*/
	_32_u width;			/* width of picture in mm	 */

	_32_u accel_flags;	  /* (OBSOLETE) see fb_info.flags */

	/* Timing: All values in pixclocks, except pixclock (of course) */
	_32_u pixclock;		 /* pixel clock in ps (pico seconds) */
	_32_u left_margin;	  /* time from sync to picture	*/
	_32_u right_margin;	 /* time from picture to sync	*/
	_32_u upper_margin;	 /* time from sync to picture	*/
	_32_u lower_margin;
	_32_u hsync_len;		/* length of horizontal sync	*/
	_32_u vsync_len;		/* length of vertical sync  */
	_32_u sync;		 /* see FB_SYNC_*		*/
	_32_u vmode;			/* see FB_VMODE_*	   */
	_32_u rotate;		   /* angle we rotate counter clockwise */
	_32_u colorspace;	   /* colorspace for FOURCC-based modes */
	_32_u reserved[4];	  /* Reserved for future compatibility */
};

struct fb_fix_screeninfo {
    char id[16];            /* identification string eg "TT Builtin" */
    unsigned long smem_start;   /* Start of frame buffer mem */
                    /* (physical address) */
    _32_u smem_len;         /* Length of frame buffer mem */
    _32_u type;         /* see FB_TYPE_*        */
    _32_u type_aux;         /* Interleave for interleaved Planes */
    _32_u visual;           /* see FB_VISUAL_*      */
    _16_u xpanstep;         /* zero if no hardware panning  */
    _16_u ypanstep;         /* zero if no hardware panning  */
    _16_u ywrapstep;        /* zero if no hardware ywrap    */
    _32_u line_length;      /* length of a line in bytes    */
    unsigned long mmio_start;   /* Start of Memory Mapped I/O   */
                    /* (physical address) */
    _32_u mmio_len;         /* Length of Memory Mapped I/O  */
    _32_u accel;            /* Indicate to driver which */
                    /*  specific chip/card we have  */
    _16_u capabilities;     /* see FB_CAP_*         */
    _16_u reserved[2];      /* Reserved for future compatibility */
};

# endif /*__linux__fb__h*/
