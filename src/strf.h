# ifndef __ffly__strf__h
# define __ffly__strf__h
# include "y_int.h"
# include <stdarg.h>

/*
	codename: string formating

	for low level non IO stuff

	as IOprintf has buffers that the number ie result is placed directly into

*/
enum {
    _scnb_h2,
    _scnb_h4,
    _scnb_i
};
struct scn_block {
    _32_u type;
    _16_u start, len;
    void *dst;
};
void ffly_scnf(_int_u, char*, struct scn_block*);

_int_u ffly_strf(char*, _int_u, char const*, ...);
_int_u ffly_strfa(char*, _int_u, char const*, va_list);
_64_u _ffly_hxti(char*, _int_u);
_int_u _ffly_nds(char*, _64_u);
_int_u _ffly_ithx(char*, _64_u, _int_u);
_64_u _ffly_dsn(char*, _int_u);
# endif /*__ffly__strf__h*/
