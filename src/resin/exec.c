# include "../resin.h"
# include "../stdio.h"
# include "../linux/unistd.h"
# include "../linux/stat.h"
# include "../remf.h"
# include "mm.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../memory/mem_realloc.h"
# include "exec.h"
# include "../dep/mem_cpy.h"
struct f_exc_ctx static *exc_ctx;
static _8_u *stack;

struct arg_s {
	void *p;
	_64_u src;
	_32_u size;
	_8_u what;
	_64_u init_arg;
} __attribute__((packed));

# include "../dep/mem_cpy.h"
# include "../init.h"
static struct ffly_resin ctx;

// needs cleaning
void* ring(_8_u __no, void *__arg_p) {
	struct arg_s *arg;
	if (__arg_p != NULL) {
		printf("this error is nothing to panic about, resin/exec.c.\n");
		arg = (struct arg_s*)ff_resin_resolv_adr(&ctx, *(_f_addr_t*)__arg_p);
	} else
		arg = NULL;
	switch(__no) {
		case 0x0: {// set stack pointer
			_64_u sp = ctx.stack_size;
			ff_resin_sst(&ctx, &sp, 0, sizeof(_64_u));
			ctx.sp = ctx.stack_size;
			break;
		}
		case 0x1: {
			// mem_read
			f_mem_cpy(ff_resin_resolv_adr(&ctx, arg->src), arg->p, arg->size);
			break;
		}
		case 0x2: {
			// mem_write
			f_mem_cpy(arg->p, ff_resin_resolv_adr(&ctx, arg->src), arg->size);
			break;
		}
		case 0x3: {
			// mem_mmap
			arg->p = ff_resin_mmap(arg->size);	
			break;
		}
		case 0x4: {
			ff_resin_munmap(*(void**)__arg_p);
			// mem_munmap
			break;
		}
		case 0x5: {
			//init	
			if (arg != NULL) {
				__init_arg__ = (struct init_arg*)ff_resin_resolv_adr(&ctx, arg->init_arg);
				printf("init what? %u\n", arg->what);

				ffly_init(arg->what);
			}
			break;
		}
	}
	printf("ring ring hello?, %u, %p\n", __no, __arg_p);
}

static struct ffly_resin ctx = {
	.stack_size = 700,
	.rin = ring
};
void static init(void) {
	stack = __f_mem_alloc(ctx.stack_size);
	ctx.stack = stack;
}

void static de_init(void) {
	__f_mem_free(stack);
}

void f_rsexc(struct f_exc_ctx *__ctx, _32_u __end, _32_u __entry) {
	exc_ctx = __ctx;
	ctx.e = __end;
	ctx.code = __ctx->p;
	ctx.ip = __entry;
	init();
	ff_resin_init(&ctx);
	_f_err_t exit_code;
	ff_resin_exec(&ctx, &exit_code);
	printf("exit code: %d\n", exit_code);
	ff_resin_de_init(&ctx);
	de_init();
}

void ffres_exec(void(*__get)(_int_u, _int_u, void*), _32_u __end, void(*__prep)(void*, void*), void *__hdr, _32_u __entry) {
}
