# include "msg.h"
# include "config.h"
# include "io.h"
# include "log.h"
# include "mutex.h"
# include "m_alloc.h"
# include "string.h"
void(*ffly_msgout)(void*, _int_u) = NULL;
struct domain_s dummy_domain = {"",0};
/*
	dont ask i dont know why i dont just use log.c
	but i think its too close to user space and we want to do things are way not the users way
*/
#define DEF(__str, __len)\
	{" `" __str, __len+2}
struct domain_s doms[N_DOMS] = {
	DEF("nowhere", 7),			//0
	DEF("sys/thread", 10),		//1
	DEF("sys/lat", 7),			//2
	DEF("core", 4),				//3
	DEF("net", 3),				//4
	DEF("tcp", 3),				//5
	DEF("udp", 3),				//6
	DEF("sys/config", 10),		//7
	DEF("firefly", 7),			//8
	DEF("tc", 2),				//9
	DEF("rs", 2),				//10
	DEF("sys/sched", 9),		//11
	DEF("cache", 5),				//12
	DEF("bog", 3),				//13
	DEF("engine", 6),			//14
	DEF("event", 5),				//15
	DEF("sys/queue", 9),		//16
	DEF("sys/pipe", 8),			//17
	DEF("nought", 6),			//18
	DEF("grf/job", 7),			//19
	DEF("vat", 3),				//20
	DEF("flue", 4),		//21
	DEF("pulse", 5),				//22
	DEF("billet", 6),			//23
	DEF("warehouse",9),			//24
	DEF("sysio", 5),				//25
	DEF("bole",	4),				//26
	DEF("sys/buff", 8),			//27
	DEF("fssm", 4),				//28
	DEF("fs/m", 4),				//29
	DEF("tmh", 3),				//30
	DEF("pms", 3),				//31
	DEF("ic", 2),				//32
	DEF("ihc", 3),
	DEF("havoc",5)
};

char const static *level_str[] = {
	"`II ", "`WW ", "`DB ", "`EE ", "`CC "
};
static char *buf;
static _int_u off;
void static _prep(_int_u __size) {
	buf = (char*)m_alloc(__size+1+off);
}

void static _write(void *__buf, _int_u __size) {
	mem_cpy(buf+off, __buf, __size);
	off+=__size;
}

mlock static lock = FFLY_MUTEX_INIT;
#include "clock.h"
#include "strf.h"
#include "tmu.h"
void ffly_msg(struct domain_s *__doma, struct domain_s *__domb, _64_u __info, char const *__format, ...) {
	if (!ffly_msgout) return;
	mt_lock(&lock);
	_64_u level;
	level = (__info&FFLY_MSG_LEVEL)>>FFLY_MSG_DOM_BO;
	if (level == DEBUG && !ffis_mode(_ff_mod_debug)) {
		return;
	}
#ifndef __fflib
	void *p;
#endif
	va_list args;
	va_start(args, __format);
	off = __doma->len+__domb->len+4;
	char timestamp[64];
	_int_u stamplen;
	off+=stamplen = _ffly_nds(timestamp,clockval.val/CLICK_BASE7);
#ifdef __fflib
	ffly_printin(__format, args, _write, _prep);	
#else
	_int_u size;
	size = vasprintf(&p, __format, args);
	buf = (char*)m_alloc(size+1+off);
	mem_cpy(buf+off, p, size);
	off+=size;
	free(p);
#endif
	char *bufp = buf;
	mem_cpy(bufp,timestamp,stamplen);
	bufp+=stamplen;
	mem_cpy(bufp, __domb->str, __domb->len);
	mem_cpy(bufp+__domb->len, __doma->str, __doma->len);
	*(_32_u*)(bufp+__doma->len+__domb->len) = *(_32_u*)level_str[level];

	buf[off] = '\0';
	ffly_msgout(buf, off);

	m_free(buf);

	va_end(args);
	mt_unlock(&lock);
}

void f_msgraw(void *__buf, _int_u __size) {
	mt_lock(&lock);
	ffly_msgout(__buf, __size);
	mt_unlock(&lock);
}
