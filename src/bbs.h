# ifndef __bbs__h
# define __bbs__h
# include "y_int.h"
#include "rws.h"
/*
	interlevate place/buffer zone?
	
EXAMPLE:
	ycompiler -> this -> assembler
*/
struct bbs_ops {
	struct {
		void(*read)(_ulonglong, void*, _int_u);
	} in;

	struct {
		void(*write)(_ulonglong, void*, _int_u);
		void(*pwrite)(_ulonglong, void*, _int_u, _64_u);
	} out;
	void(*seek)(_ulonglong, _64_u);
};

struct bbs_node {
	_8_u *src;
	_int_u limit;
	_64_u ptr;
	_64_u ptr0;
};

struct bbs_node* bbsn_new(void);
void bbsn_destroy(struct bbs_node*);
extern struct f_rw_funcs ith0;
extern struct bbs_ops ith;

# endif /*__bbs__h*/
