# include "hash.h"
# include "../string.h"
# include "../m_alloc.h"
# include "../crypt.h"
# include "../io.h"
void f_lhash_init(struct f_lhash *__hash) {
	__hash->table = (struct f_lhash_entry**)m_alloc(0x100*sizeof(struct f_lhash_entry*));
	struct f_lhash_entry **p = __hash->table;
	struct f_lhash_entry **end = p+0x100;
	while(p != end)
		*(p++) = NULL;
	__hash->top = NULL;
	__hash->n = 0;
}

void f_lhash_destroy(struct f_lhash *__hash) {
	struct f_lhash_entry *cur = __hash->top, *bk;
	while(cur != NULL) {
		bk = cur;
		cur = cur->fd;
		m_free(bk->key);
		m_free(bk);
	}
	m_free(__hash->table);
}

void
f_lhash_put(struct f_lhash *__hash,
	_8_u const *__key, _int_u __len, void *__p)
{
	_64_u sum = y_hash64(__key, __len);
	struct f_lhash_entry *entry = (struct f_lhash_entry*)m_alloc(sizeof(struct f_lhash_entry));
	struct f_lhash_entry **table = __hash->table+(sum&0xff);

	entry->next = *table;
	*table = entry;
	mem_dup((void**)&entry->key, (void*)__key, __len);
	entry->p = __p;
	entry->len = __len;
	entry->fd = __hash->top;
	__hash->top = entry;
	__hash->n++;
}

void*
f_lhash_get(struct f_lhash *__hash,
	_8_u const *__key, _int_u __len)
{
	_64_u sum = y_hash64(__key, __len);
	struct f_lhash_entry *cur = *(__hash->table+(sum&0xff));
	while(cur != NULL) {
		if (cur->len == __len) {
			if (!mem_cmp(cur->key, __key, __len))
				return cur->p;
		}
		cur = cur->next;
	}
	return NULL;
}
