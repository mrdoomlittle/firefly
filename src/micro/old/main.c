# include "micro.h"
# include <avr/io.h>
void static _setmod(_8_u __pin, _8_u __mode) {
	if (__pin>=0&&__pin<8) {
		DDRD = (DDRD&~(1<<__pin))|((__mode&0x01)<<__pin);
	} else if (__pin>=8&&__pin<14) {
		__pin-=8;
		DDRB = (DDRB&~(1<<__pin))|((__mode&0x01)<<__pin);
	}
}

void static _setstate(_8_u __pin, _8_u __state) {
	if (__pin>=0&&__pin<8) {
		PORTD = (PORTD&~(1<<__pin))|((__state&0x01)<<__pin);
	} else if (__pin>=8&&__pin<14) {
		__pin-=8;
		PORTB = (PORTB&~(1<<__pin))|((__state&0x01)<<__pin);
	}
}

_8_u static _getstate(_8_u __pin) {
	_8_u rval;
	if (__pin>=0&&__pin<8) {
		rval = (PIND>>__pin)&0x01;
	} else if (__pin>=8&&__pin<14) {
		rval = (PINB>>__pin)&0x01;
	}
	return rval;
}

#define I0_SER 2
#define I0_CLK 3
#define I0_LTH 4
#define I1_SERI 5
#define I1_CLK 6
#define I1_LTH 7
#define I1_SERO 8
#define I1_OE 13
_8_u i0ps[8];
_8_u i1ps_out[8];
_8_u i1ps_in[8];
void _i0out(void);
void _i1out(void);
void _i1in(void);
void _seti0p(_8_u __pin, _8_u __state) {
	i0ps[7-__pin] = __state;
	_i0out();
}

void _seti1p(_8_u __pin, _8_u __state) {
	_setstate(I1_OE, LOW);
	i1ps_out[7-__pin] = __state;
	_i1out();
}

_8_u _geti1p(_8_u __pin) {
	_setstate(I1_OE, HIGH);
	_i1in();
	return i1ps_in[7-__pin];
}

#define DWAY 100
void _i0out(void) {
	_int_u i;
	for(i = 0;i != 8;i++){
		_setstate(I0_SER, i0ps[i]);
		_setstate(I0_CLK, HIGH);
		_delay_us(DWAY);
		_setstate(I0_CLK, LOW);
		_delay_us(DWAY);
	}
	_delay_us(DWAY);
	_setstate(I0_LTH, HIGH);
	_delay_us(DWAY);
	_setstate(I0_LTH, LOW);
	_delay_us(DWAY);

}

void _i1out(void) {
	_int_u i;
	for(i = 0;i != 8;i++){
		_setstate(I1_SERO, i1ps_out[i]);
		_setstate(I1_CLK, HIGH);
		_delay_us(DWAY);
		_setstate(I1_CLK, LOW);
		_delay_us(DWAY);
	}
	_delay_us(DWAY);
	_setstate(I1_LTH, HIGH);
	_delay_us(DWAY);
	_setstate(I1_LTH, LOW);
	_delay_us(DWAY);

}

void _i1in(void) {
	_setstate(I1_LTH, HIGH);
	_delay_us(DWAY);
	_setstate(I1_LTH, LOW);
	_delay_us(DWAY);

	*i1ps_in = _getstate(I1_SERI);
	_int_u i;
	for(i = 1;i != 8;i++){
		_setstate(I1_CLK, HIGH);
		_delay_us(DWAY);
		_setstate(I1_CLK, LOW);
		_delay_us(DWAY);
		i1ps_in[i] = _getstate(I1_SERI);
	}
	_delay_us(DWAY);
}

void static _memset(void *__dst, _8_u __val, _int_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++)
		((_8_u*)__dst)[i] = __n;
}

void static _init(void) {
	_setmod(I0_SER, OUT);//serial
	_setmod(I0_CLK, OUT);//clock
	_setmod(I0_LTH, OUT);//latch-clear
	_setmod(I1_SERI, IN);
	_setmod(I1_SERO, OUT);
	_setmod(I1_CLK, OUT);
	_setmod(I1_LTH, OUT);
	_setmod(I1_OE, OUT);
	_setstate(I1_OE, LOW);
	_setstate(I0_SER, LOW);
	_setstate(I0_CLK, LOW);
	_setstate(I0_LTH, LOW);
	_setstate(I1_CLK, LOW);
	_setstate(I1_LTH, LOW);
	_setstate(I1_SERO, LOW);
	_memset(i0ps, 0x00, 8);
	_memset(i1ps_out, 0x00, 8);
	_delay_ms(1);
}

#define PS_N 3
struct pin_set { 
	void(*setstate)(_8_u, _8_u);
	_8_u(*getstate)(_8_u);
	void(*setmod)(_8_u, _8_u);
	_8_u prs, pre, off;

};
#define NULL ((void*)0)
struct pin_set sets[PS_N] = {
	{
		.setstate = _setstate,
		.getstate = _getstate,
		.setmod = _setmod,
		0, 14, 9 
	},
	{
		.setstate = _seti0p,
		.getstate = NULL,
		.setmod = NULL,
		14, 22, 0
	},
	{
		.setstate = _seti1p,
		.getstate = _geti1p,
		.setmod = NULL,
		22, 30, 0
	}
};

struct pin_set* ps_lookup(_8_u __pin) {
	struct pin_set *ps;
	_int_u i;
	i = 0;
	for(;i != PS_N;i++) {
		ps = sets+i;
		if (__pin>=ps->prs && __pin<ps->pre) {
			return ps;
		}
	}
	return NULL;
}

void
_setastate(_8_u __pin, _8_u __state) {
	struct pin_set *ps;
	ps = ps_lookup(__pin);
	ps->setstate((__pin-ps->prs)+ps->off, __state);
}

_8_u
_getastate(_8_u __pin) {
	struct pin_set *ps;
	ps = ps_lookup(__pin);
	return ps->getstate((__pin-ps->prs)+ps->off);
}

void
_setamod(_8_u __pin, _8_u __mode) {
	struct pin_set *ps;
	ps = ps_lookup(__pin);
	ps->setmod((__pin-ps->prs)+ps->off, __mode);
}

#define B0 22
#define B1 23
#define B2 24
#define B3 25
#define B4 26
#define B5 27
#define B6 28
#define B7 29

int main(void) {

	f_micro_uart_init();
	_init();
	_setmod(11, OUT);
/*
	_setmod(10, OUT);
	while(1) {
		_setstate(10, 1);
		_delay_us(10);
		_setstate(10, 0);
		_delay_us(10);
	}
	*/
/*
	_8_u state = 0x00;
while(1) {
	_setastate(B1, state);
//	_setastate(B1, 1);
//	_setastate(B2, 1);
//	_setastate(B3, 1);
//	_setastate(B4, 1);
//	_setastate(B5, 1);
//	_setastate(B6, 1);
//	_setastate(B7, 1);
	_delay_us(100);
	state = ~state;
}
*/

	at_prog_start();
}

