# include "micro.h"
# include "at_prog.h"
#define RDYBSY 0
#define XTAL1 1
#define RST 2
#define PWR 3
#define OE 14
#define WR 15
#define BS1 16
#define XA0 17
#define XA1 18
#define PGL 19
#define BS2 20

#define B0 22
#define B1 23
#define B2 24
#define B3 25
#define B4 26
#define B5 27
#define B6 28
#define B7 29
void static
_wrbyte(_8_u __byte) {
	_setastate(B0, __byte&0x01);
	_setastate(B1, (__byte>>1)&0x01);
	_setastate(B2, (__byte>>2)&0x01);
	_setastate(B3, (__byte>>3)&0x01);
	_setastate(B4, (__byte>>4)&0x01);
	_setastate(B5, (__byte>>5)&0x01);
	_setastate(B6, (__byte>>6)&0x01);
	_setastate(B7, (__byte>>7)&0x01);
}

_8_u static
_rdbyte(void) {
	_8_u byte = 0x00;
	byte |= _getastate(B0);
	byte |= _getastate(B1)<<1;
	byte |= _getastate(B2)<<2;
	byte |= _getastate(B3)<<3;
	byte |= _getastate(B4)<<4;
	byte |= _getastate(B5)<<5;
	byte |= _getastate(B6)<<6;
	byte |= _getastate(B7)<<7;
	return byte;
}

void static
_xtal1_pulse(_int_u __n) {
_another:
	_setastate(XTAL1, 1);
	_delay_us(100);
	_setastate(XTAL1, 0);
	_delay_us(100);
	if (--__n>0) {
		goto _another;
	}
}

void static
_read_eeprom(void) {
	_setastate(XA1, 1);
	_setastate(XA0, 0);
	_setastate(BS1, 0);
	_wrbyte(0b00000011);
	_xtal1_pulse(1);

	_16_u addr;
	f_micro_uart_recv(&addr, 2);
	_setastate(XA1, 0);
	_wrbyte(addr&0xff);
	_xtal1_pulse(1);

	_setastate(BS1, 1);
	_wrbyte(addr>>8);
	_xtal1_pulse(1);

	_setastate(OE, 0);
	_setastate(BS1, 0);
	_delay_us(1);
	f_micro_uart_send_byte(_rdbyte());
	_setastate(OE, 1);
}

void static
_read_flash(void) {
	_setastate(XA1, 1);
	_setastate(XA0, 0);
	_setastate(BS1, 0);
	_wrbyte(0b00000010);
	_xtal1_pulse(1);

	_16_u addr;
	f_micro_uart_recv(&addr, 2);
	_setastate(XA1, 0);
	_wrbyte(addr&0xff);
	_xtal1_pulse(1);

	_setastate(BS1, 1);
	_wrbyte(addr>>8);
	_xtal1_pulse(1);

	_16_u data;
	_setastate(OE, 0);
	_setastate(BS1, 0);
	_delay_us(1);

	data = _rdbyte();
	_setastate(BS1, 1);
	_delay_us(1);
	data |= _rdbyte()<<8;
	f_micro_uart_send(&data, 2);
	_setastate(OE, 1);
}

void static
_read_sig(void) {
	_setastate(XA1, 1);
	_setastate(XA0, 0);
	_setastate(BS1, 0);
	_wrbyte(0b00001000);
	_xtal1_pulse(1);

	_setastate(XA1, 0);
	_wrbyte(0x00);
	_xtal1_pulse(1);

	_setastate(OE, 0);
	f_micro_uart_send_byte(_rdbyte());
	_setastate(OE, 1);

}
void static
_enter_pgm(void) {
	_setastate(PWR, 0);
	_setastate(PGL, 0);
	_setastate(XA1, 0);
	_setastate(XA0, 0);
	_setastate(BS1, 0);
	_setastate(BS2, 0);
	_setastate(RST, 1);
	_delay_ms(1);

	_setastate(PWR, 1);
	_delay_us(30);

	_setastate(RST, 0);
	_delay_us(14);
	
	f_micro_uart_send_byte(_rdbyte());

	_delay_us(300);
}

void static
_exit_pgm(void) {
	_setastate(PWR, 0);
	_setastate(RST, 1);
}

void static
_cmdin(void) {
	_8_u cmd;
	cmd = 0x00;
	f_micro_uart_recv_byte(&cmd);
	
	switch(cmd) {
		case atpg_cmd_rmem:
			_read_eeprom();
		break;
		case atpg_cmd_rdata:
			_read_flash();
		break;
		case atpg_cmd_echo:
			{
				_64_u io;
				f_micro_uart_recv(&io, 8);
				f_micro_uart_send(&io, 8);
			}
		break;
		case atpg_cmd_entpm:
			_enter_pgm();			
		break;
		case atpg_cmd_extpm:
			_exit_pgm();
		break;
		case atpg_cmd_rsig:
			_read_sig();
		break;
		case atpg_cmd_dead:
		default:
		break;
	}
}

void at_prog_start(void) {
	_setamod(RDYBSY, IN);
	_setamod(XTAL1, OUT);
	_setamod(RST, OUT);
	_setamod(PWR, OUT);
	_setastate(RST, 1);
	_setastate(PWR, 0);
	_setastate(OE, 1);
	_setastate(WR, 1);
	_8_u state = 0x00;
	while(1) {
//		_wrbyte(0xff);
		
//		_cmdin();


//		ack();

	//	_8_u in;
	//	f_micro_uart_recv(&in, 1);
	//	_16_u data;
	//	data = 21299;
	//	f_micro_uart_send(&data, 2);
	
//		_wrbyte(~0b01000000);
//		_delay_us(10);
//		_wrbyte(~0b00100000);
//		_delay_us(10);
//

		_setastate(B7, _getastate(B7));
		state = ~state;
		_delay_us(10);

//		f_micro_uart_send_byte(_rdbyte());

//		_setastate(B6, 1);	
	}
}
