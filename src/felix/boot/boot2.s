# include "boot0.h"
#define VIDMEMORY 0xb8000
#define APEX 0x10000
#define CR0_PG (1<<31)
#define CR0_PE 0x00000001
#define PG 0x10000
#define _p_4 (PG)			//0x10000-0x11000
#define _p_3 (PG+0x1000)	//0x11000-0x12000
#define _p_2 (PG+0x2000)	//0x12000-0x13000
#define _p_1 (PG+0x3000)	//0x13000-0x14000
.globl _boot0
// page is present+read+write
#define PAGE_PRW (0x00000003)
.code16
.section .data
GDT32:
.quad 0x0000000000000000
GDT32_cs:
.byte 0xff, 0xff, 0x00, 0x00,   0x00, 0b10011011, 0b01001111, 0x00
GDT32_ds:// ds and cs are the same segment for now
.byte 0xff, 0xff, 0x00, 0x00,   0x00, 0b10010011, 0b01001111, 0x00
//.byte 0xff, 0xff, 0xff, 0xff,   0x00, 0b10010011, 0b11010000, 0x00
.set GDT32_size, (. - GDT32)
.set GDT32_cs_offset, (GDT32_cs - GDT32)
.set GDT32_ds_offset, (GDT32_ds - GDT32)
GDT32_ptr:
.word GDT32_size
.long GDT32
IDT32_ptr:
.word 0
.long 0
GDT64:
.quad 0x0000000000000000
GDT64_cs:
.byte 0xff, 0xff, 0x00, 0x00,   0x00, 0b10011011, 0b00101111, 0x00
GDT64_ds:
.byte 0xff, 0xff, 0x00, 0x00,   0x00, 0b10010011, 0b00101111, 0x00
.set GDT64_size, (. - GDT64)
.set GDT64_cs_offset, (GDT64_cs - GDT64)
.set GDT64_ds_offset, (GDT64_ds - GDT64)
GDT64_ptr:
.word GDT64_size
.quad GDT64
IDT64_ptr:
.word 0
.quad 0
//.bss
//.lcomm LDT32, 0x200
.text
_boot: 
	cli
	lgdt GDT32_ptr
	lidt IDT32_ptr

	//enable protection
	movl $CR0_PE, %eax
	movl %eax, %cr0
	// offset is to be 8 to index 1
	ljmp $GDT32_cs_offset, $0f
.code32
0:
	movw $GDT32_ds_offset, %ax
	movw %ax, %ds
	movw %ax, %es
	movw %ax, %fs
	movw %ax, %gs
	movw %ax, %ss

	movl $STACK_ADDR, %esp
	movl %esp, %ebp
	
	movl $msg, %esi
	movl $msglen, %edx
	call _new_printf
	/*
		page map to page directory ptr table
	*/
	movl $(_p_3|PAGE_PRW), %eax //PML ENTRY
	movl %eax, (_p_4)
	movl $0, (_p_4+4)//clear higher bits

	/*
		page directory ptr entry to page direcrory table
	*/
	movl $(_p_2|PAGE_PRW), %eax
	movl %eax, (_p_3)
	movl $0, (_p_3+4)

	/*
		page directory to physical
	*/
	movl $(_p_1|PAGE_PRW), %eax
	movl %eax, (_p_2)
	movl $0, (_p_2+4)

	movl $PAGE_PRW, %eax
	movl $_p_1, %edi
0:
	movl %eax, (%edi)
	movl $0, 4(%edi)
	addl $0x1000, %eax
	addl $8, %edi
	cmpl $(0x200000|PAGE_PRW), %eax
	jb 0b

	// enable page address extention
	movl %cr4, %eax
	btsl $5, %eax
	movl %eax, %cr4

	//load PML4 ptr to cr3 control register
	//specificly load cr3 with page map
	movl $_p_4, %eax
	movl %eax, %cr3

	movl $0xc0000080, %ecx
	rdmsr//read register

	// set LME bit to enable long mode
	btsl $8, %eax
	//update
	wrmsr

	// enable paging
	movl %cr0, %eax
	orl $CR0_PG, %eax
	movl %eax, %cr0
	lgdt GDT64_ptr
	ljmp $GDT32_cs_offset, $0f
.code64
0:
	movw $GDT64_ds_offset, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss

	jmp _boot0
.code64
_felix_printf:
	movq $VIDMEMORY, %rbx
	movq %rsi, %rdx
	movq %rdi, %rsi
0:
	xorq %rax, %rax
	lodsb
	orq $0x200, %rax
	movw %ax, (%rbx)
	addq $2, %rbx
	decq %rdx
	cmpq $0, %rdx
	jne 0b
	ret

.code32
_new_printf:
	movl $VIDMEMORY, %ebx
0:
	xorl %eax, %eax
	lodsb
	orl $0x300, %eax
	movw %ax, (%ebx)
	addl $2, %ebx
	decl %edx
	cmpl $0, %edx
	jne 0b
	ret
.section .rodata
msg:.ascii "WELCOME TO FELIX V-0.00"
.set msglen, . - msg
