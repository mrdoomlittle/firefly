	.file "boot0.s"
	.code16
# include "boot0.h"
.globl _start
.globl _printf
_start:
	xorw %ax, %ax
	movw %ax, %ss
	movw %ax, %ds
	movw $STACK_ADDR, %bp
	movw %bp, %sp

	movb %dl, boot_drv

	movw $BOOT1_SEGMENT, %ax
	movw %ax, %es

	movb $0x02, %ah
	movb $BOOT1_NB_SECTORS, %al
	movw $BOOT1_START_SECTOR, %cx
	movb $BOOT1_HEAD, %dh
	movb boot_drv, %dl
	movw $BOOT1_START_ADDR, %bx
	int $0x13
	jc _fatal

	movw $msg, %si
	movw $msglen, %dx
	call _printf

	jmpl $BOOT1_SEGMENT, $BOOT1_START_ADDR
_exit:
	jmp _exit

_fatal:
	jmp _exit
_printf:
	movb $0x0e, %ah
0:
	lodsb
	movw $0x0000, %bx
	int $0x10

	decw %dx
	cmpw $0, %dx
	je 1f
	jmp 0b
1:
	ret
.section .rodata
msg:.ascii "STAGE ONE PASS\n\r"
.set msglen, . - msg
.data
boot_drv:.byte 0x00
