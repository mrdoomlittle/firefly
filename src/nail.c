# include "nail.h"
# include "m_alloc.h"
/*
	we want to track structures like sys/vec, sys/map so in debugging we can list all of them that have been attached
*/
static struct f_nail *ward[] = {
	NULL, NULL, NULL
};
// should just make ward gloabal
struct f_nail *f_nailat(_int_u __ward) {
	return ward[__ward];
}

struct f_nail *f_nail_attach(_int_u __ward, void *__p) {
	struct f_nail *n;

	n = (struct f_nail*)m_alloc(sizeof(struct f_nail));
	struct f_nail **w;
	w = ward+__ward;
	n->ptr = __p;
	n->next = *w;
	if (n->next != NULL) {
		n->next->bk = &n->next;
	}
	n->bk = w;
	*w = n;
}

void f_nail_detach(struct f_nail *__n) {
	*__n->bk = __n->next;
	if (__n->next != NULL) {
		__n->next->bk = __n->bk;
	}
	m_free(__n);
}
