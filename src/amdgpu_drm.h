# ifndef __ffly__amdgpu__drm__h
# define __ffly__amdgpu__drm__h
# include "y_int.h"
# include "drm.h"
#define DRM_AMDGPU_GEM_CREATE 0x00
#define DRM_AMDGPU_GEM_MMAP	 0x01
#define DRM_AMDGPU_CTX 0x02
#define DRM_AMDGPU_BO_LIST	  0x03
#define DRM_AMDGPU_INFO 0x05
#define DRM_AMDGPU_GEM_VA	   0x08
#define DRM_AMDGPU_CS		   0x04
#define DRM_AMDGPU_WAIT_CS	  0x09
#define DRM_AMDGPU_WAIT_FENCES      0x12
#define DRM_AMDGPU_GEM_WAIT_IDLE    0x07

#define DRM_IOCTL_AMDGPU_GEM_CREATE DRM_IOWR(DRM_COMMAND_BASE+DRM_AMDGPU_GEM_CREATE, union drm_amdgpu_gem_create)
#define DRM_IOCTL_AMDGPU_GEM_MMAP DRM_IOWR(DRM_COMMAND_BASE+DRM_AMDGPU_GEM_MMAP, union drm_amdgpu_gem_mmap)
#define DRM_IOCTL_AMDGPU_CTX DRM_IOWR(DRM_COMMAND_BASE+DRM_AMDGPU_CTX, union drm_amdgpu_ctx)
#define DRM_IOCTL_AMDGPU_INFO DRM_IOW(DRM_COMMAND_BASE+DRM_AMDGPU_INFO, struct drm_amdgpu_info)
#define DRM_IOCTL_AMDGPU_WAIT_FENCES    DRM_IOWR(DRM_COMMAND_BASE + DRM_AMDGPU_WAIT_FENCES, union drm_amdgpu_wait_fences)
#define DRM_IOCTL_AMDGPU_GEM_WAIT_IDLE  DRM_IOWR(DRM_COMMAND_BASE + DRM_AMDGPU_GEM_WAIT_IDLE, union drm_amdgpu_gem_wait_idle)

/**
 * DOC: memory domains
 *
 * %AMDGPU_GEM_DOMAIN_CPU   System memory that is not GPU accessible.
 * Memory in this pool could be swapped out to disk if there is pressure.
 *
 * %AMDGPU_GEM_DOMAIN_GTT   GPU accessible system memory, mapped into the
 * GPU's virtual address space via gart. Gart memory linearizes non-contiguous
 * pages of system memory, allows GPU access system memory in a linezrized
 * fashion.
 *
 * %AMDGPU_GEM_DOMAIN_VRAM  Local video memory. For APUs, it is memory
 * carved out by the BIOS.
 *
 * %AMDGPU_GEM_DOMAIN_GDS   Global on-chip data storage used to share data
 * across shader threads.
 *
 * %AMDGPU_GEM_DOMAIN_GWS   Global wave sync, used to synchronize the
 * execution of all the waves on a device.
 *
 * %AMDGPU_GEM_DOMAIN_OA    Ordered append, used by 3D or Compute engines
 * for appending data.
 */
#define AMDGPU_GEM_DOMAIN_CPU 0x1
#define AMDGPU_GEM_DOMAIN_GTT 0x2
#define AMDGPU_GEM_DOMAIN_VRAM 0x4
#define AMDGPU_GEM_DOMAIN_GDS 0x8
#define AMDGPU_GEM_DOMAIN_GWS 0x10
#define AMDGPU_GEM_DOMAIN_OA 0x20

/* Flag that CPU access will be required for the case of VRAM domain */
#define AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED (1<<0)
/* Flag that CPU access will not work, this VRAM domain is invisible */
#define AMDGPU_GEM_CREATE_NO_CPU_ACCESS (1<<1)
/* Flag that USWC attributes should be used for GTT */
#define AMDGPU_GEM_CREATE_CPU_GTT_USWC (1<<2)
/* Flag that the memory should be in VRAM and cleared */
#define AMDGPU_GEM_CREATE_VRAM_CLEARED (1<<3)
/* Flag that create shadow bo(GTT) while allocating vram bo */
#define AMDGPU_GEM_CREATE_SHADOW (1<<4)
/* Flag that allocating the BO should use linear VRAM */
#define AMDGPU_GEM_CREATE_VRAM_CONTIGUOUS (1<<5)
/* Flag that BO is always valid in this VM */
#define AMDGPU_GEM_CREATE_VM_ALWAYS_VALID (1<<6)
/* Flag that BO sharing will be explicitly synchronized */
#define AMDGPU_GEM_CREATE_EXPLICIT_SYNC (1<<7)

struct drm_amdgpu_info_device {
	/** PCI Device ID */
	_32_u device_id;
	/** Internal chip revision: A0, A1, etc.) */
	_32_u chip_rev;
	_32_u external_rev;
	/** Revision id in PCI Config space */
	_32_u pci_rev;
	_32_u family;
	_32_u num_shader_engines;
	_32_u num_shader_arrays_per_engine;
	/* in KHz */
	_32_u gpu_counter_freq;
	_64_u max_engine_clock;
	_64_u max_memory_clock;
	/* cu information */
	_32_u cu_active_number;
	/* NOTE: cu_ao_mask is INVALID, DON'T use it */
	_32_u cu_ao_mask;
	_32_u cu_bitmap[4][4];
	/** Render backend pipe mask. One render backend is CB+DB. */
	_32_u enabled_rb_pipes_mask;
	_32_u num_rb_pipes;
	_32_u num_hw_gfx_contexts;
	_32_u _pad;
	_64_u ids_flags;
	/** Starting virtual address for UMDs. */
	_64_u virtual_address_offset;
	/** The maximum virtual address */
	_64_u virtual_address_max;
	/** Required alignment of virtual addresses. */
	_32_u virtual_address_alignment;
	/** Page table entry - fragment size */
	_32_u pte_fragment_size;
	_32_u gart_page_size;
	/** constant engine ram size*/
	_32_u ce_ram_size;
	/** video memory type info*/
	_32_u vram_type;
	/** video memory bit width*/
	_32_u vram_bit_width;
	/* vce harvesting instance */
	_32_u vce_harvest_config;
	/* gfx double offchip LDS buffers */
	_32_u gc_double_offchip_lds_buf;
	/* NGG Primitive Buffer */
	_64_u prim_buf_gpu_addr;
	/* NGG Position Buffer */
	_64_u pos_buf_gpu_addr;
	/* NGG Control Sideband */
	_64_u cntl_sb_buf_gpu_addr;
	/* NGG Parameter Cache */
	_64_u param_buf_gpu_addr;
	_32_u prim_buf_size;
	_32_u pos_buf_size;
	_32_u cntl_sb_buf_size;
	_32_u param_buf_size;
	/* wavefront size*/
	_32_u wave_front_size;
	/* shader visible vgprs*/
	_32_u num_shader_visible_vgprs;
	/* CU per shader array*/
	_32_u num_cu_per_sh;
	/* number of tcc blocks*/
	_32_u num_tcc_blocks;
	/* gs vgt table depth*/
	_32_u gs_vgt_table_depth;
	/* gs primitive buffer depth*/
	_32_u gs_prim_buffer_depth;
	/* max gs wavefront per vgt*/
	_32_u max_gs_waves_per_vgt;
	_32_u _pad1;
	/* always on cu bitmap */
	_32_u cu_ao_bitmap[4][4];
	/** Starting high virtual address for UMDs. */
	_64_u high_va_offset;
	/** The maximum high virtual address */
	_64_u high_va_max;
};

struct drm_amdgpu_wait_cs_in {
	/* Command submission handle
		 * handle equals 0 means none to wait for
		 * handle equals ~0ull means wait for the latest sequence number
		 */
	_64_u handle;
	/** Absolute timeout to wait */
	_64_u timeout;
	_32_u ip_type;
	_32_u ip_instance;
	_32_u ring;
	_32_u ctx_id;
};

struct drm_amdgpu_wait_cs_out {
	/** CS status:  0 - CS completed, 1 - CS still busy */
	_64_u status;
};

union drm_amdgpu_wait_cs {
	struct drm_amdgpu_wait_cs_in in;
	struct drm_amdgpu_wait_cs_out out;
};

struct drm_amdgpu_ctx_in {
	_32_u op;
	_32_u flags;
	_32_u ctx_id;
	_32_s priority;
};

struct drm_amdgpu_sched_in {
	_32_u op;
	_32_u fd;
	_32_s priority;
	_32_u flags;
};

union drm_amdgpu_ctx_out {
	struct {
		_32_u ctx_id;
		_32_u __pad;
	} alloc;

	struct {
		_64_u flags;
		_32_u hangs;
		_32_u reset_status;
	} state;
};

struct drm_amdgpu_query_fw {
	_32_u fw_type;
	_32_u ip_inst;
	_32_u index;
	_32_u __pad;
};

struct drm_amdgpu_heap_info {
	_64_u total_heap_size;
	_64_u usable_heap_size;
	_64_u heap_usage;
	_64_u max_allocation;
};

struct drm_amdgpu_memory_info {
	struct drm_amdgpu_heap_info vram;
	struct drm_amdgpu_heap_info cpu_accessible_vram;
	struct drm_amdgpu_heap_info gtt;
};

struct drm_amdgpu_info {
	_64_u ret_p;
	_32_u ret_size;
	_32_u query;
	union {	
		struct {
			_32_u id;
			_32_u __pad;
		} mode_crtc;

		struct {
			_32_u type;
			_32_u ip_inst;
		} query_hw_ip;

		struct {
			_32_u dword_offset;
			_32_u count;
			_32_u inst;
			_32_u flags;
		} read_mmr_reg;

		struct drm_amdgpu_query_fw query_fw;

		struct {
			_32_u type;
			_32_u offset;
		} vbios_info;

		struct {
			_32_u type;
		} sensor_info;
	};
};

union drm_amdgpu_ctx {
	struct drm_amdgpu_ctx_in in;
	union drm_amdgpu_ctx_out out;
};

union drm_amdgpu_sched {
	struct drm_amdgpu_sched_in in;
};

struct drm_amdgpu_gem_create_in  {
	_64_u bo_size;
	_64_u alignment;
	_64_u domains;
	_64_u domain_flags;
};

struct drm_amdgpu_gem_create_out  {
	_32_u handle;
	_32_u __pad;
};

union drm_amdgpu_gem_create {
	struct drm_amdgpu_gem_create_in in;
	struct drm_amdgpu_gem_create_out out;
};

struct drm_amdgpu_gem_mmap_in {
	_32_u handle;
	_32_u __pad;
};

struct drm_amdgpu_gem_mmap_out {
	_64_u addr_ptr;
};

union drm_amdgpu_gem_mmap {
	struct drm_amdgpu_gem_mmap_in in;
	struct drm_amdgpu_gem_mmap_out out;
};

struct drm_amdgpu_gem_wait_idle_in {
    /** GEM object handle */
    _32_u handle;
    /** For future use, no flags defined so far */
    _32_u flags;
    /** Absolute timeout to wait */
    _64_u timeout;
};

struct drm_amdgpu_gem_wait_idle_out {
    /** BO status:  0 - BO is idle, 1 - BO is busy */
    _32_u status;
    /** Returned current memory domain */
    _32_u domain;
};

union drm_amdgpu_gem_wait_idle {
    struct drm_amdgpu_gem_wait_idle_in  in;
    struct drm_amdgpu_gem_wait_idle_out out;
};


#define AMDGPU_HW_IP_GFX		  0
#define AMDGPU_HW_IP_COMPUTE	  1
#define AMDGPU_HW_IP_DMA		  2
#define AMDGPU_HW_IP_UVD		  3
#define AMDGPU_HW_IP_VCE		  4
#define AMDGPU_HW_IP_UVD_ENC	  5
#define AMDGPU_HW_IP_VCN_DEC	  6
#define AMDGPU_HW_IP_VCN_ENC	  7
#define AMDGPU_HW_IP_VCN_JPEG	 8
#define AMDGPU_HW_IP_NUM		  9

/* Specify flags to be used for IB */

/* This IB should be submitted to CE */
#define AMDGPU_IB_FLAG_CE   (1<<0)

/* Preamble flag, which means the IB could be dropped if no context switch */
#define AMDGPU_IB_FLAG_PREAMBLE (1<<1)

/* Preempt flag, IB should set Pre_enb bit if PREEMPT flag detected */
#define AMDGPU_IB_FLAG_PREEMPT (1<<2)

/* The IB fence should do the L2 writeback but not invalidate any shader
 * caches (L2/vL1/sL1/I$). */
#define AMDGPU_IB_FLAG_TC_WB_NOT_INVALIDATE (1 << 3)

struct drm_amdgpu_cs_chunk_ib {
	_32_u _pad;
	/** AMDGPU_IB_FLAG_* */
	_32_u flags;
	/** Virtual address to begin IB execution */
	_64_u va_start;
	/** Size of submission */
	_32_u ib_bytes;
	/** HW IP to submit to */
	_32_u ip_type;
	/** HW IP index of the same type to submit to  */
	_32_u ip_instance;
	/** Ring index to submit to */
	_32_u ring;
};

struct drm_amdgpu_cs_chunk_fence {
	_32_u handle;
	_32_u offset;
};

struct drm_amdgpu_cs_chunk_data {
	union {
		struct drm_amdgpu_cs_chunk_ib	   ib_data;
		struct drm_amdgpu_cs_chunk_fence	fence_data;
	};
};

struct drm_amdgpu_fence {
	_32_u ctx_id;
	_32_u ip_type;
	_32_u ip_inst;
	_32_u ring;
	_64_u seq_no;
};

struct drm_amdgpu_wait_fences_in {
	/** This points to uint64_t * which points to fences */
	_64_u fences;
	_32_u fence_count;
	_32_u wait_all;
	_64_u timeout_ns;
};

struct drm_amdgpu_wait_fences_out {
	_32_u status;
	_32_u first_signaled;
};

union drm_amdgpu_wait_fences {
	struct drm_amdgpu_wait_fences_in in;
	struct drm_amdgpu_wait_fences_out out;
};


#define AMDGPU_CHUNK_ID_IB	  0x01
#define AMDGPU_CHUNK_ID_FENCE	   0x02
#define AMDGPU_CHUNK_ID_DEPENDENCIES	0x03
#define AMDGPU_CHUNK_ID_SYNCOBJ_IN	  0x04
#define AMDGPU_CHUNK_ID_SYNCOBJ_OUT	 0x05
#define AMDGPU_CHUNK_ID_BO_HANDLES	  0x06

struct drm_amdgpu_cs_chunk {
	_32_u chunk_id;
	_32_u length_dw;
	/*
		cpu ptr - dont know why they have it as a u64 but ??? yea
		
		- cpu ptr to chunk data struct
	*/
	_64_u chunk_data;
};

struct drm_amdgpu_cs_in {
	_32_u ctx_id;
	_32_u bo_list_handle;
	_32_u num_chunks;
	_32_u _pad;
	_64_u chunks;
};

struct drm_amdgpu_cs_out {
	_64_u handle;
};

union drm_amdgpu_cs {
	struct drm_amdgpu_cs_in in;
	struct drm_amdgpu_cs_out out;
};

/** Opcode to create new residency list.  */
#define AMDGPU_BO_LIST_OP_CREATE	0
/** Opcode to destroy previously created residency list */
#define AMDGPU_BO_LIST_OP_DESTROY   1
/** Opcode to update resource information in the list */
#define AMDGPU_BO_LIST_OP_UPDATE	2

struct drm_amdgpu_bo_list_in {
	/** Type of operation */
	_32_u operation;
	/** Handle of list or 0 if we want to create one */
	_32_u list_handle;
	/** Number of BOs in list  */
	_32_u bo_number;
	/** Size of each element describing BO */
	_32_u bo_info_size;
	/** Pointer to array describing BOs */
	_64_u bo_info_ptr;
};

struct drm_amdgpu_bo_list_entry {
	/** Handle of BO */
	_32_u bo_handle;
	/** New (if specified) BO priority to be used during migration */
	_32_u bo_priority;
};

struct drm_amdgpu_bo_list_out {
	/** Handle of resource list  */
	_32_u list_handle;
	_32_u _pad;
};

union drm_amdgpu_bo_list {
	struct drm_amdgpu_bo_list_in in;
	struct drm_amdgpu_bo_list_out out;
};


#define AMDGPU_VA_OP_MAP			1
#define AMDGPU_VA_OP_UNMAP		  2
#define AMDGPU_VA_OP_CLEAR		  3
#define AMDGPU_VA_OP_REPLACE			4
/* readable mapping */
#define AMDGPU_VM_PAGE_READABLE	 (1 << 1)
/* writable mapping */
#define AMDGPU_VM_PAGE_WRITEABLE	(1 << 2)
/* executable mapping, new for VI */
#define AMDGPU_VM_PAGE_EXECUTABLE   (1 << 3)
/* partially resident texture */
#define AMDGPU_VM_PAGE_PRT	  (1 << 4)
/* MTYPE flags use bit 5 to 8 */
#define AMDGPU_VM_MTYPE_MASK		(0xf << 5)
/* Default MTYPE. Pre-AI must use this.  Recommended for newer ASICs. */
#define AMDGPU_VM_MTYPE_DEFAULT	 (0 << 5)
/* Use NC MTYPE instead of default MTYPE */
#define AMDGPU_VM_MTYPE_NC	  (1 << 5)
/* Use WC MTYPE instead of default MTYPE */
#define AMDGPU_VM_MTYPE_WC	  (2 << 5)
/* Use CC MTYPE instead of default MTYPE */
#define AMDGPU_VM_MTYPE_CC	  (3 << 5)
/* Use UC MTYPE instead of default MTYPE */
#define AMDGPU_VM_MTYPE_UC	  (4 << 5)

struct drm_amdgpu_gem_va {
	_32_u handle;
	_32_u _pad;
	_32_u op;
	_32_u flags;
	_64_u va_addr;
	_64_u bo_offset;
	_64_u map_size;
};

// context operations
#define AMDGPU_CTX_OP_ALLOC_CTX 1
#define AMDGPU_CTX_OP_FREE_CTX  2
#define AMDGPU_CTX_OP_QUERY_STATE   3
#define AMDGPU_CTX_OP_QUERY_STATE2  4

/**
 *  Query h/w info: Flag that this is integrated (a.h.a. fusion) GPU
 *
 */
#define AMDGPU_IDS_FLAGS_FUSION		 0x1
#define AMDGPU_IDS_FLAGS_PREEMPTION	 0x2

/* indicate if acceleration can be working */
#define AMDGPU_INFO_ACCEL_WORKING		0x00
/* get the crtc_id from the mode object id? */
#define AMDGPU_INFO_CRTC_FROM_ID		0x01
/* query hw IP info */
#define AMDGPU_INFO_HW_IP_INFO			0x02
/* query hw IP instance count for the specified type */
#define AMDGPU_INFO_HW_IP_COUNT			0x03
/* timestamp for GL_ARB_timer_query */
#define AMDGPU_INFO_TIMESTAMP			0x05
/* Query the firmware version */
#define AMDGPU_INFO_FW_VERSION			0x0e
	/* Subquery id: Query VCE firmware version */
	#define AMDGPU_INFO_FW_VCE		0x1
	/* Subquery id: Query UVD firmware version */
	#define AMDGPU_INFO_FW_UVD		0x2
	/* Subquery id: Query GMC firmware version */
	#define AMDGPU_INFO_FW_GMC		0x03
	/* Subquery id: Query GFX ME firmware version */
	#define AMDGPU_INFO_FW_GFX_ME		0x04
	/* Subquery id: Query GFX PFP firmware version */
	#define AMDGPU_INFO_FW_GFX_PFP		0x05
	/* Subquery id: Query GFX CE firmware version */
	#define AMDGPU_INFO_FW_GFX_CE		0x06
	/* Subquery id: Query GFX RLC firmware version */
	#define AMDGPU_INFO_FW_GFX_RLC		0x07
	/* Subquery id: Query GFX MEC firmware version */
	#define AMDGPU_INFO_FW_GFX_MEC		0x08
	/* Subquery id: Query SMC firmware version */
	#define AMDGPU_INFO_FW_SMC		0x0a
	/* Subquery id: Query SDMA firmware version */
	#define AMDGPU_INFO_FW_SDMA		0x0b
	/* Subquery id: Query PSP SOS firmware version */
	#define AMDGPU_INFO_FW_SOS		0x0c
	/* Subquery id: Query PSP ASD firmware version */
	#define AMDGPU_INFO_FW_ASD		0x0d
	/* Subquery id: Query VCN firmware version */
	#define AMDGPU_INFO_FW_VCN		0x0e
/* number of bytes moved for TTM migration */
#define AMDGPU_INFO_NUM_BYTES_MOVED		0x0f
/* the used VRAM size */
#define AMDGPU_INFO_VRAM_USAGE			0x10
/* the used GTT size */
#define AMDGPU_INFO_GTT_USAGE			0x11
/* Information about GDS, etc. resource configuration */
#define AMDGPU_INFO_GDS_CONFIG			0x13
/* Query information about VRAM and GTT domains */
#define AMDGPU_INFO_VRAM_GTT			0x14
/* Query information about register in MMR address space*/
#define AMDGPU_INFO_READ_MMR_REG		0x15
/* Query information about device: rev id, family, etc. */
#define AMDGPU_INFO_DEV_INFO			0x16
/* visible vram usage */
#define AMDGPU_INFO_VIS_VRAM_USAGE		0x17
/* number of TTM buffer evictions */
#define AMDGPU_INFO_NUM_EVICTIONS		0x18
/* Query memory about VRAM and GTT domains */
#define AMDGPU_INFO_MEMORY			0x19
/* Query vce clock table */
#define AMDGPU_INFO_VCE_CLOCK_TABLE		0x1A
/* Query vbios related information */
#define AMDGPU_INFO_VBIOS			0x1B
	/* Subquery id: Query vbios size */
	#define AMDGPU_INFO_VBIOS_SIZE		0x1
	/* Subquery id: Query vbios image */
	#define AMDGPU_INFO_VBIOS_IMAGE		0x2
/* Query UVD handles */
#define AMDGPU_INFO_NUM_HANDLES			0x1C
/* Query sensor related information */
#define AMDGPU_INFO_SENSOR			0x1D
	/* Subquery id: Query GPU shader clock */
	#define AMDGPU_INFO_SENSOR_GFX_SCLK		0x1
	/* Subquery id: Query GPU memory clock */
	#define AMDGPU_INFO_SENSOR_GFX_MCLK		0x2
	/* Subquery id: Query GPU temperature */
	#define AMDGPU_INFO_SENSOR_GPU_TEMP		0x3
	/* Subquery id: Query GPU load */
	#define AMDGPU_INFO_SENSOR_GPU_LOAD		0x4
	/* Subquery id: Query average GPU power	*/
	#define AMDGPU_INFO_SENSOR_GPU_AVG_POWER	0x5
	/* Subquery id: Query northbridge voltage */
	#define AMDGPU_INFO_SENSOR_VDDNB		0x6
	/* Subquery id: Query graphics voltage */
	#define AMDGPU_INFO_SENSOR_VDDGFX		0x7
	/* Subquery id: Query GPU stable pstate shader clock */
	#define AMDGPU_INFO_SENSOR_STABLE_PSTATE_GFX_SCLK		0x8
	/* Subquery id: Query GPU stable pstate memory clock */
	#define AMDGPU_INFO_SENSOR_STABLE_PSTATE_GFX_MCLK		0x9
/* Number of VRAM page faults on CPU access. */
#define AMDGPU_INFO_NUM_VRAM_CPU_PAGE_FAULTS	0x1E
#define AMDGPU_INFO_VRAM_LOST_COUNTER		0x1F

#define AMDGPU_INFO_MMR_SE_INDEX_SHIFT	0
#define AMDGPU_INFO_MMR_SE_INDEX_MASK	0xff
#define AMDGPU_INFO_MMR_SH_INDEX_SHIFT	8
#define AMDGPU_INFO_MMR_SH_INDEX_MASK	0xff

# endif /*__ffly__amdgpu__drm__h*/
