# include "../../y_int.h"
# include "../../types.h"
# include "../../linux/unistd.h"
# include "../../linux/fcntl.h"
# include "../../linux/stat.h"
# include "../../io.h"
# include "as.h"
# include "../../flue/common.h"
_err_t main(int __argc, char const *__argv[]) {
	flue_prime();
	flue_sweep();

	void *ctx = flue_ctx_new();
	struct flue_prog *pg;
	pg = flue_prog();
	struct flu_plarg arg;
	arg.p = "test.ysa";
	ysa_loader(ctx,&arg, pg);
	return 0;
}
