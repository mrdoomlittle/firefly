# include "as.h"
# include "../../io.h"
# include "../../assert.h"
# include "../../m_alloc.h"
struct reg {
	struct reg_sh sh;
	char const *name;
	_int_u nme_len;
	// selector
	_64_u sel;
};


struct op_body {
	_16_u code[4];
	_int_u len;
	_64_u bits[8];
	_64_u opandpos[4];
	_int_u noa;
	_8_u form;
};

struct op_tray {
	char const *name;
	_int_u nme_len;
	struct op_body *bods;
	_int_u n;	
_64_u bits;
};


#define NOPS 15
static struct op_body bods[] = {
	{{_ys_addfq}, 1, {_o_reg64, _o_reg64, _o_reg64, 0, 0}, {},3,0},
	{{_ys_addfq}, 1, {_o_reg64, _o_reg64, _o_reg64, 0, 0}, {},3,0},
	{{_ys_xprt}, 1, {_o_reg128, 0, 0, 0, 0}, {0},1,0},
	{{_ys_agnl}, 1, {_o_reg64, _o_imm32, 0, 0, 0}, {0,1},2,0},
	{{_ys_mul}, 1, {_o_reg128, _o_reg128, 0, 0, 0}, {0,1},2,1},
	{{_ys_add}, 1, {_o_reg128, _o_reg128, 0, 0, 0}, {0,1},2,1},
	{{_ys_cos}, 1, {_o_reg128, _o_reg128, 0, 0, 0}, {0,1},2,1},
	{{_ys_sin}, 1, {_o_reg128, _o_reg128, 0, 0, 0}, {0,1},2,1},
	{{_ys_fldw}, 1, {_o_reg64, _o_reg64, 0, 0, 0}, {0,1},2,0},
	{{_ys_fsdw}, 1, {_o_reg64, _o_reg64, 0, 0, 0}, {0,1},2,0},
	{{_ys_movll}, 1, {_o_reg64, _o_reg64, 0, 0, 0}, {0,1},2,0},
	{{_ys_movhl}, 1, {_o_reg64, _o_reg64, 0, 0, 0}, {0,1},2,0},
	{{_ys_dist}, 1, {_o_reg64, _o_reg128, _o_reg128, 0, 0}, {0,1,2},3,0},
	{{_ys_clmp}, 2, {_o_reg128, _o_imm32, _o_imm32, 0, 0}, {0,1,2},3,0},
	{{_ys_ld}, 2, {_o_reg64, _o_ifds, _o_imm32, _o_imm32, 0, 0}, {},4,0},
	{{_ys_st}, 2, {_o_reg64, _o_ifds, _o_imm32, _o_imm32, 0, 0}, {},4,0}
};

static struct op_tray ops[NOPS] = {
	{"mov", 3, bods+1, 0},
	// export
	{"xprt", 4, bods+2, 1},
	{"agn", 3, bods+3, 1},
	{"mulx", 4, bods+4, 1},
	{"add", 3, bods+5, 1},
	{"cos", 3, bods+6, 1},
	{"sin", 3, bods+7, 1},
	{"fldw", 4, bods+8, 1},
	{"fsdw", 4, bods+9, 1},
	{"movl", 4, bods+10, 1},
	{"movh", 4, bods+11, 1},
	{"dist", 4, bods+12, 1},
	{"clmp", 4, bods+13, 1},
	{"ld", 2, bods+14, 1},
	{"st", 2, bods+15, 1}
};

#define NREG 24
static struct reg rtab[NREG] = {
	{{_o_reg64},"r0", 2, 0},
	{{_o_reg64},"r1", 2, 1},
	{{_o_reg64},"r2", 2, 2},
	{{_o_reg64},"r3", 2, 3},
	{{_o_reg64},"r4", 2, 4},
	{{_o_reg64},"r5", 2, 5},
	{{_o_reg64},"r6", 2, 6},
	{{_o_reg64},"r7", 2, 7},
	{{_o_reg64},"r8", 2, 8},
	{{_o_reg64},"r9", 2, 9},
	{{_o_reg64},"r10", 3, 10},
	{{_o_reg64},"r11", 3, 11},
	{{_o_reg64},"r12", 3, 12},
	{{_o_reg64},"r13", 3, 13},
	{{_o_reg64},"r14", 3, 14},
	{{_o_reg64},"r15", 3, 15},
	{{_o_reg128},"r0d", 0},//0-1
	{{_o_reg128},"r1d", 3, 1},
	{{_o_reg128},"r2d", 3, 2},
	{{_o_reg128},"r3d", 3, 3},
	{{_o_reg128},"r4d", 3, 4},
	{{_o_reg128},"r5d", 3, 5},
	{{_o_reg128},"r6d", 3, 6},
	{{_o_reg128},"r7d", 3, 7}
};

void yss_out(struct ys_state *__st) {
	struct op_body *b, *op;
	op = NULL;
	_int_u i;
	i = 0;
	struct op_tray *tray = as.tray;
_nope:
	while(i != tray->n) {
		b = tray->bods+(i++);
		if (b->noa == as.noa) {
			_int_u ii;
			ii = 0;
			for(;ii != as.noa;ii++) {
				_64_u a, _b;
				a = as.oa_bits[ii];
				_b = b->bits[ii];
				printf("probing: %u ? %u.\n", a, _b);
				if (!(a&_b)) {
					goto _nope;
				}
			}
			op = b;
			goto _yes;
		} else {
			printf("skipping body-%u operand count does not match, %u != %u\n", i, b->noa, as.noa);
		}
	}
   
	printf("no operation found that meets requirements.\n");
	return;
_yes:
	__st->in_c++;
	printf("looks like we found somthing.\n");
	struct yss_op _op;
	_op.op = op->code[0]+(as.specal>>1);

	i = 0;
	struct yss_operand *opand;
	while(i != as.noa) {
		_64_u bits = as.oa_bits[i];
		opand = _op.opand+op->opandpos[i];
		if (bits&(_o_reg64|_o_reg128)) {
			struct reg *r;
			r = as.oa_p[i];
			opand->reg = r->sel;
		}
		if (bits&(_o_imm32)) {
			opand->i = as.oa_p[i];
		}
		i++;
	}
	_op.form = op->form;
	YSA_write(&_op, sizeof(struct yss_op));
}

void yss_init(void) {
	_int_u i; 
	i = 0; 
	for(;i != NOPS;i++) { 
		struct op_tray *t; 
		t = ops+i; 
		printf("placing OP-%s in table.\n", t->name); 
		f_lhash_put(&as.tab, t->name, t->nme_len, t); 
	} 
	i = 0; 
	for(;i != NREG;i++) { 
		struct reg *r; 
		r = rtab+i; 
		printf("placing REG-%s in table.\n", r->name); 
		f_lhash_put(&as.reg, r->name, r->nme_len, r); 
	} 
}

