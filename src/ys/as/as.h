# ifndef __ys__as__h
# define __ys__as__h
# include "../../lib/hash.h"
# include "../../ffly_def.h"
# include "../../y_int.h"
# include "../ysm.h"
#include "../../rws.h"
#ifndef DUMM_ERROR
static _8_u dummy_error;
#endif
#define YSA_read(...) as.rw_ops.read(as.in_ctx,__VA_ARGS__,&dummy_error)
#define YSA_write(...) as.rw_ops.write(as.out_ctx,__VA_ARGS__,&dummy_error)
struct op_tray;
struct bbs_node;
struct ys_as {
	struct f_rw_funcs rw_ops;
	_ulonglong in_ctx;
	_ulonglong out_ctx;
	_8_u specal;
	void *tray;
	struct f_lhash env;
	struct f_lhash tab;
	struct f_lhash reg;
	_int_u limit;
	_int_u off;
	_8_s selhelp;
	_int_u noa;
	_64_u oa_bits[8];
	_64_u oa_en;
	char prop[64];
	_int_u prop_len;
	char sel[64];
	_int_u sel_len;
	
	_ulonglong oa_p0[8];
	_ulonglong oa_p[8];
	_ulonglong oa_at[8];
	char sfx;
};

struct reg_sh {
	_64_u bits;
};

struct ys_sso {
	_64_u binding;
};

extern _64_u(*ys_findattr)(char*,_int_u);
extern void*(*ys_prop)(char*,_int_u);
#define LINE_MAX 86
struct ys_line {
	_8_u *p;
	_int_u len;
};

enum {
	_ys_r0,
	_ys_r1,
	_ys_r2,
	_ys_r3
};
#define _o_reg (_o_reg32|_o_reg64|_o_reg128)
#define _o_imm (_o_imm8|_o_imm16|_o_imm32|_o_imm64)
#define _o_imm8		(1<<0)
#define _o_imm16	(1<<2)
#define _o_imm32	(1<<3)
#define _o_imm64	(1<<4)
#define _o_reg32	(1<<5)
#define _o_reg64	(1<<6)
#define _o_reg128	(1<<7)
#define _o_f32		(1<<8)
#define _o_f64		(1<<9)
/*
	instruction form dependent selector
*/
#define _o_ifds		(1<<8)
#define SY_INT 0
#define SY_REG 1
#define SY_IDENT 2
#define SY_FLOAT 3
/*
	a number thats confined to X-bits
	ie max dec 32
*/
#define SY_SEL 3
struct ys_symb {
	_8_u id;
	_ulonglong _0;
	_ulonglong _1;
	_ulonglong _2;
	_ulonglong _3;
	_64_u attr;
	_64_u i;
	struct ys_symb *next;
};

#define BLB_INS 0
#define BLB_DIR 1
struct ys_blob {
	_8_u id, specal;
	struct ys_symb *sy;
	void *tray;
};
struct ys_state {
	_int_u in_c;
};

struct ys_mech {
	void(*onwards)(struct ys_state*);
	void(*init)(void);
};
#define ys_software (ys_machine+0)
#define ys_radeon	(ys_machine+1)
#define ys_MACHINE(__x) ys_msel = __x;
extern struct ys_mech *ys_msel;
extern struct ys_mech ys_machine[2];

struct ys_symb* ys_eval(struct ys_line*, _int_u);
struct ys_line* ys_line_next(void);
void ysa_assemble(struct ys_state*);
struct ys_blob* ys_parser(void);
void ysa_ihc_readout(void);
void ys_init(void);
void ys_de_init(void);
extern struct ys_as as;
# endif /*__ys__as__h*/
