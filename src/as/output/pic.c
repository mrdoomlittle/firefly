#include "../malk.h"
#include "../as.h"
#include "../../m_alloc.h"
#include "../../string.h"
#include "../../io.h"
#include "../../strf.h"
#include "../../assert.h"
#define _ 0xff
static struct m_op *__op;

static _8_u leadmap[0x100*4] = {
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,  'b',  'c',  _,_, 'f',  _,_,  'i',  _,_,  'l',  _,_,_,
	'p',  _,  'r',  's',  _,_,_, 'w',  _,_,'z',_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,

	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,'c',_,_, 'f',_, _,'i',  _,_,  'l',  _,_,_,
	_,_,_,  's',  _,_,_,  'w',  _,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,


	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_, 'f',  _,_,_,_,_,_,_,_,_,
	_,_,  'r',  _,_,_,_,  'w',  _,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,  _
};


_16_u static
outs(struct m_inst *__in) {
	_16_u opc;
	struct m_opbody *b = __in->_op;
	opc = b->opcode;
	
	opc |= (0-(__in->mods&1))&b->mask;
	opc |= (__in->mods>>8&0x3)<<b->pos;
#ifdef debug
	printf("OPAND: %x&%x, %x&%x, OPCODE: %x\n", __in->opandv[0],b->opandmask[0], __in->opandv[1],b->opandmask[1], opc);
#endif
	_8_u opn;
	_64_u v;
	v = (__in->opandv[0]&b->opandmask[0])<<b->opandpos[0];
	if ((__in->opandv[0]&~b->opandmask[0])>0) {
		printf("WARNING: operand value has been trimmed, on OP-%x{%u,%u}\n",b->opcode, __in->opandv[0], __in->opandv[1]);
	}
		
	opc |= v;

	if (b->n_opands>1) {
		v = (__in->opandv[1]&b->opandmask[1])<<b->opandpos[1];
		if ((__in->opandv[1]&~b->opandmask[1])>0) {
			printf("WARNING: operand value has been trimmed, on OP-%x{%u,%u}\n",b->opcode, __in->opandv[0], __in->opandv[1]);
		}
		opc |= v;
	}

	printf("OP: %x.\n", opc&0x3fff);
	return opc&0x3fff;
}


_32_u static
emouts(struct m_inst *__in) {
	struct m_opbody *b = __in->_op;
	_32_u r;
	r = b->opcode0;
	printf("OPCODE: %u.\n", b->opcode0);
	_8_u opn;
	_64_u v;
	if (b->n_opands>0) {
	v = (__in->opandv[0]&0xff)<<b->opandpos0[0];
	if ((__in->opandv[0]&~0xff)>0) {
		printf("WARNING: operand value has been trimmed, on OP-%x{%u,%u}\n",b->opcode, __in->opandv[0], __in->opandv[1]);
	}
		
	r |= v;
	}
	if (b->n_opands>1) {
		v = (__in->opandv[1]&0xff)<<b->opandpos0[1];
		if ((__in->opandv[1]&~0xff)>0) {
			printf("WARNING: operand value has been trimmed, on OP-%x{%u,%u}\n",b->opcode, __in->opandv[0], __in->opandv[1]);
		}
		r |= v;
	}

	printf("#### %u, %u.\n",__in->opandv[0],__in->opandv[1]);
	//j/k
	r |= (__in->mods&1)<<24;
	assert((r&0xff) == b->opcode0);
	return r;
}
struct m_op static* opnow(struct m_inst *__in, char *__name, _int_u __len) {
	_int_u c = 0;
	struct m_op *o;
	char name[24];
	mem_cpy(name, __name, __len);
_again:
	o = (struct m_op*)tun_hash_get(&m_strc.optab, __name, __len);
	if (!o) {
		/*
			see if removing trailing char will change anything
		*/
		if (__in->chc != 0 && c != __in->chc) {
			name[__len] = __in->charac[(__in->chc-c)-1];
			__in->charac[(__in->chc-c)-1] = 0;
		
			__len++;
			__name = name;
			c++;
			goto _again;
		}
		char buf[24];
		mem_cpy(buf, __name, __len);
		buf[__len] = '\0';
		printf("no such instruction exists. '%s':%u\n", buf, __len);
		assert(1 == 0);
		return;
	}else {
#ifdef debug
		printf("INS- %s: %u.\n", o->name, o->n_bodies);
#endif
	}

	return o;
}
void static oplook(struct m_inst *__in, struct m_op *o) {
	_int_u i;
	for(i = 0;i != o->n_bodies;i++) {
		struct m_opbody *b = o->bodies+i;
#ifdef debug
		printf("checking, {%x:%x:%x:%x}\n", b->charac[0],b->charac[1],b->charac[2],b->charac[3]);
#endif
		if (b->n_opands == __in->n_opands) {
			if (!(__in->mod&b->mods)) {
			if (*(_32_u*)b->charac == *(_32_u*)__in->charac) {
				_8_u pass = 0;
				printf("%x, %x.\n", b->opands[0], __in->opands[0]);
				if ((b->opands[0]&__in->opands[0])>0) {
					pass++;
				}
				if (pass>0 || b->n_opands == __in->n_opands) {
					printf("found instruction.\n");
					__in->_op = b;
					return;
				}
			}
			} else {
				printf("illegal mods.\n");
			}
		} else {
			printf("unacceptable number of operands, {%u~%u}.\n",b->n_opands,__in->n_opands);
		}
	}
#ifdef debug
	printf("cant find, {%x:%x:%x:%x}\n",
		__in->charac[0],__in->charac[1],__in->charac[2],__in->charac[3]);
#endif
	assert(1 == 0);
}

void m_pic_load(void) {
	m_strc.leadmap = leadmap;
}

static _32_u *buf;
static _int_u bp;
void m_pic_fix(void) {
	struct m_frag *f, *sub;
	f = m_strc.ft;
	_int_u si;
	while(f != NULL) {
		si = 0;
		for(;si != f->sub_n;si++) {
		sub = f->sub[si];
		if (sub->in != NULL) {
			sub->in->opandv[0] = sub->attend;
			_64_u word = outs(sub->in);
			printf("HERE: %x.\n", sub->underlap>>(64-sub->btc));
			sub->underlap = sub->underlap>>14|word<<(64-14);
			sub->btc+=14;
			printf("attend = %u.\n", sub->attend);
		}
		}
		f = f->next;
	}
}
char const *microcher_op[] = {
	"ADD",
	"SHL",
	"SHR",
	"OR",
	"AND"
};

void m_pic_jmp(struct m_frag *__f) {
	__f->ft->reloc = m_strc.rlvalmap[M_R_JMP];
}

void m_pic_call(struct m_frag *__f) {
	__f->ft->reloc = m_strc.rlvalmap[M_R_CALL];
}

void m_pic_dummy(void*__dummy) {
}

/*
	flush remaining bits - non byte alignment bits
*/
void static flush_frag0(
	struct m_frag *sub,
	_64_u wc
){
	if(bp>0) {
		sub->over = bp*4;
		sub->part = buf;
	}
	sub->words+=wc;
}

void static flush_frag(
	struct m_frag *sub,
	_64_u bts,
	_64_u c,	
	_64_u wc
) {
	if (bp>0) {
#ifdef debug
		_int_u i;
		i = 0;
		for(;i != bp;i++) {
			printf("---# %x.\n", buf[i]);
		}
#endif
		sub->over = bp*4;
		sub->part = buf;
	}
#ifdef debug
	printf("leftovers: %x:%u\n", bts>>c, 64-c);
#endif
	sub->words+=wc;

	/*
		flush_frag0() ^ 
	*/

	if (c<64) {
		sub->underlap = bts;
		sub->btc = c;
	}
}
static struct m_frag *sub;
static _8_s crunch;
static struct m_smdata *sm0;
static struct m_inst *in;
static struct m_reloc *rl;
_int_u static
read_opands(void) {
		struct m_symb *sb, *_s;
		sb = in->yum;
		_int_u i = 0;
		while(sb != NULL) {
			_64_u val = 0;
			_16_u id = 0xffff;
			_64_u swp = 0;
			_s = sb;
			struct sb_ext *ex;
			_64_u v = 0;
			_int_u ii;
			ii = 0;
			_8_u op = m_ADD;
#ifdef debug
			printf("EXT### %u.\n", sb->exn);
#endif
			goto _j0;
	
			for(;ii != sb->exn;) {
				ex = sb->ex+ii++;
				_s = ex->sb;
				op = ex->op;
			
			_j0:
			if (_s->type == _m_smb_imm) {
				v = _s->val;
 			} else if (sb->type == _m_smb_lbl) {
				if (__op->bits&_m_op_odd) {
					rl = m_strc.reloc_new();
					rl->from = sub->ft;
					rl->sm = sb->_sm0;
					rl->where = (bp*sizeof(_32_u))+((sub->pc-1)*MF_PG_SIZE);
					if (sb->bits&_syb_high) {
						rl->flags = RLFLAG(M_R_BH);
					}
					if (!sb->dis)
						rl->dis = sb->ex->sb->val;
					else
						rl->dis = 0;
					rl->sel = NULL;
				}else
				if (sb->_sm0->flags == m_strc.symflags[M_SYB_UD]) {
					sm0 = sb->_sm0;	
				} else {
					struct m_tostrc *to = sb->_sm0->_to+sb->_sm0->tn;
					to->sel = NULL;
					if(sb->_0.p != NULL) {
						struct m_symb0 *s = tun_hash_get(&m_strc.labels, sb->_0.p, sb->_0.len);
						if (!s) {
							printf("lookup failed for %w.\n",sb->_0.p, sb->_0.len);
							assert(1 == 0);
						}
						to->sel = s->data;
					}
					sb->_sm0->to[sb->_sm0->tn++] = sub->ft;
				}
				v = 0;//value not yet known
				id = _m_imm8;
				crunch = 0;
				//exclude instruction and do another round
				//breaking will flush current instructions to frag
				break;
			}
				if (op == m_COMP) {
					swp = val;
					val = 0;
				} else
				if (op&m_COMP) {
					v = val;
					val = swp;	
				}

				switch(op&~m_COMP) {
					case m_ADD:
						val+=v;
					break;
					case m_SHL:
						val<<=v;
					break;
					case m_SHR:
						val>>=v;
					break;
					case m_OR:
						val|=v;
					break;
					case m_AND:
						val&=v;
					break;
				}
#ifdef debug
				printf("OPERATION: VAL: %u, V: %u, SWP: %u, %s\n", val, v, swp, microcher_op[op&~m_COMP]);
#endif	
				v = 0;
			}
#ifdef debug
			printf("FINAL-VALUE: %u.\n", val);
#endif
			if (!(val&~0x7f)) {
				id = _m_imm7;
			} else if (!(val&0xffffffffffffff00)) {
				id = _m_imm8;
			} else if (!(val&0xffffffffffff0000)) {
				id = _m_imm16;
			} else if (!(val&0xffffffff00000000)) {
				id = _m_imm32;
			} else {
				id = _m_imm64;
			}
#ifdef debug
			printf("OPAND-VALUE: %u.\n", val);
#endif
			in->opands[i] = id;
			in->opandv[i] = val;
			i++;
			sb = sb->next;
		}

	return i;
}

static _int_u inn;
static _64_u bts;
static _int_u c;
static _64_u wc;
static _int_u ibn;
static struct m_instblock *_b;
static struct m_in *ib;
static struct m_frag *f;
static struct m_iyield *__y;


_8_s static brnul(void) {
	/*
	this part of the code should only run when changing 
*/
	ibn++;
	printf("NNN: %u.\n", __y->n);
	if (ibn>=__y->n) {
		__y = __y->next;
		printf("NEXT YEILD, %p\n", __y);
		ffly_fdrain(_ffly_out);
		if (!__y)
			return 0;//all that could be done is now done
		ibn = 0;
	}
	inn = 0;	
	return -1;
}

_8_s static in_star(void) {
	in = _b->inst+inn++;
	__op = opnow(in,in->p, in->len);
	_int_u i;
	i = read_opands();
	printf("OPERANDS: %u.\n", i);
	oplook(in,__op);
	if (!in->_op) {
		//skim passed it
		printf("no such instruction exists.\n");
		return 0;
	}
	in->_op->special(sub);
	/*
		crunch it set by read operands indecating a fragment split due to call,jmp instruction
	*/
	return -1;
}

void static crunchit(void) {
	if (sm0 != NULL) {
		struct m_reloc *rl;	
		rl = m_strc.reloc_new();
		rl->sel = NULL;
		rl->dis = 0;
		rl->where = 0;
		rl->to = sm0->w;
		rl->from = sub->ft;
		rl->value = sub->ft->reloc;
		rl->flags = RLFLAG(M_R_UD);
		sm0 = NULL;
	}
	sub->in = in;
	sub = m_subfrag(f);		
}

void static emit_real(void) {
	goto _skthis;
_nb:
	_b = _b->next;
	if (!_b) {
		flush_frag(sub,bts,c,wc);
		if(!brnul())
			return;
		goto _skthis;
	}
	inn = 0;
	goto _nodo;
_skthis:
	ib = __y->in+ibn;
	_b = ib->top;
	f = ib->f;
	sub = f;
	if (!f)
		return;		
_rnd://round and round we go
	/*
		ensure fragment is valid by linking it to underlieing
	*/
	m_strc.fat_new(sub->ft);
	wc = 0;
	crunch = -1;
	buf = (_32_u*)m_alloc(MF_PG_SIZE);
	bp = 0;
	bts = 0;
	c = 64;
_nodo:
	printf("NUMBER OF INSTRUCTION: %u.\n", _b->n);
	while(inn != _b->n) {
		if(!in_star())
			continue;
		if (!crunch) {
	/*
		we need to flush the fragment
	*/
			flush_frag(sub,bts,c,wc);
			crunchit();
			goto _rnd;
		}


		bts = (bts&~0x3fff)|outs(in);
#ifdef debug
		printf("operation # %x.\n", bts&0x3fff);
#endif
		__asm__("rorq $14, %0" : "=m"(bts));
#ifdef debug
		printf("OPOUT: ### %x: %x.\n", bts>>32, bts&0xffffffff);
#endif
		c-=14;
		wc++;
		if(c<=32) {
			_32_u w = bts>>c;
			buf[bp] = w;
			bp++;
			if (bp == MF_PG_SIZE/4) {
				m_frag_pgpush(sub, buf);
				bp = 0;
				buf = m_alloc(MF_PG_SIZE);
			}
			c+=32;
		}
#ifdef debug
		printf("OP-char{%x,%x,%x}.\n", in->charac[0], in->charac[1], in->charac[2]);
#endif
	}
	/*
		call test
		jmp test
		instructions cause fragment split

		nop	_|	-	FRAG-0
		nop _|
		call test <- skip passed
		nop _|	-	FRAG-1
		nop _|

	*/
	goto _nb;
	
}


void static
emit_em(void) {
	goto _skthis;
_nb:
	_b = _b->next;
	if (!_b) {
		if (sub != NULL) {
			flush_frag0(sub,wc);
		}
		if(!brnul())
			return;
		goto _skthis;
	}
	inn = 0;
	goto _nodo;
_skthis:
	ib = __y->in+ibn;
	_b = ib->top;
	f = ib->f;
	sub = f;
	if (!f)
		return;		
_rnd://round and round we go
	/*
		ensure fragment is valid by linking it to underlieing
	*/
	m_strc.fat_new(sub->ft);
	crunch = -1;
	buf = (_32_u*)m_alloc(MF_PG_SIZE);
	bp = 0;
	wc = 0;
_nodo:
	printf("NUMBER OF INSTRUCTION: %u.\n", _b->n);
	while(inn != _b->n) {
		rl = NULL;
		if(!in_star())
			continue;

		if (!crunch && !(in->_op->bits&_in_nocrunch)) {
			if (sub != NULL) {
				flush_frag0(sub,wc);
			}
			crunchit();
			goto _rnd;
		}
	
		if (!crunch && in->_op->bits&_in_nocrunch) {
			assert(rl != NULL);
			rl->where+=1;
			rl->value = RLVALU(M_R_BSPACE);
			rl->flags |= RLFLAG(M_R_8);
			crunch  = -1;
			printf("crunch blocked.\n");
		}
		_32_u w;
		w = emouts(in);
		wc++;
		buf[bp] = w;
		bp++;

		if (bp == MF_PG_SIZE/4) {
			m_frag_pgpush(sub, buf);
			bp = 0;
			buf = m_alloc(MF_PG_SIZE);
		}
	}
	goto _nb;
	
}
struct m_opnsel pic18_real = {emit_real,0};
struct m_opnsel pic18_em = {emit_em,1};
void m_pic(struct m_iyield *y) {		
	__y = y;
	if (!__y)return;
	struct m_pry *py;
	py = m_press[_sb_imm];
	m_press[_sb_imm] = NULL;
	while(py != NULL) {
		py->sb->val = _ffly_dsn(py->p, py->len);
		if (!py->sb->neg) {
			// could use somthing like ((0-i)^x)+i to remove if statment but then we wont have access to full 64bits
			py->sb->val = -(_64_s)py->sb->val;
			printf("negative number: %d.\n", py->sb->val);
		}
#ifdef debug
		printf("string evaluated to %x: %u.\n", py->sb->val, py->len);
#endif
		py = py->next;
	}
	
	py = m_press[_sb_hex];
	m_press[_sb_hex] = NULL;
	while(py != NULL) {

		py->sb->val = _ffly_hxti(py->p, py->len);
		py = py->next;
	}

	py = m_press[_sb_llu];
	m_press[_sb_llu] = NULL;
	while(py != NULL) {
		struct m_symb0 *sb;
		sb = tun_hash_get(&m_strc.labels, py->p, py->len);
		if (!sb) {
			struct m_symb *v = tun_hash_get(&m_strc.mac, py->p, py->len);
			if (v != NULL) {
				struct m_symb *_s = py->sb;
#ifdef debug
				printf("found to be MAC-%u.\n", v->exn);
#endif
				if (v->exn>0) {
					mem_cpy(_s->ex+_s->exn, v->ex, v->exn*sizeof(struct sb_ext));
				}
				py->sb->exn+= v->exn;
				py->sb->type = v->type;
				py->sb->val = v->val;
#ifdef debug	
				printf("evaluated to-%c: %u, %u.\n", *py->p, v->val, v->type);
#endif
			}else
				printf("no such label exists: '%c'%c'.\n", *py->p, py->p[py->len-1]);
		}else{
			py->sb->_sm0 = sb->data;
		}
		py = py->next;
	}

	sm0 = NULL;
	inn = 0;
	ibn = 0;
	m_strc.opn.emit();
	return;
}

