#include "turn.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../io.h"
struct turn_struc turn_strc;
#define OUTBUF turn_strc.outbuf
void turn_init(void) {
}

void
turn_drain(void) {
	printf("draining to %u, size: %u\n", OUTBUF.dst, OUTBUF.off);
	if (OUTBUF.off>0) {
		TURN_pwrite(OUTBUF.p, OUTBUF.off, OUTBUF.dst);
		turn_strc.out.offset+=OUTBUF.off;
	}
	OUTBUF.off = 0;
	OUTBUF.dst = turn_strc.out.offset;
}

_8_u
turn_read(void *__buf, _int_u __size) {
	if (turn_strc.in.offset>=turn_strc.in.limit) {
		return TURN_RDEND;
	}

	TURN_read(__buf, __size);
	turn_strc.in.offset+=__size;

	return 0;
}

void
turn_oust(_8_u *__p, _16_u __n) {
	while(__n>TURN_OBSIZE) {
		turn_oust(__p, TURN_OBSIZE);
		__n-=TURN_OBSIZE;
		__p+=TURN_OBSIZE;
	}

	if (!__n) {
		printf("nothing left.\n");
		return;
	}

	/* if offset has been changed by an alternative means drain it
	*/
	if (OUTBUF.dst+OUTBUF.off != turn_strc.out.offset) {
		printf("draining buffer as its no longer on track.\n");
		turn_drain();
	}

	_int overflow;
	if ((overflow = (_int)(OUTBUF.off+__n)-(_int)((TURN_OBSIZE)-1))>0)
		__n-=overflow;

//  printf("filling in buffer at : %u : %u\n", OUTBUF.off, __n);
	mem_cpy(OUTBUF.p+OUTBUF.off, __p, __n);
	OUTBUF.off+=__n;

	if (OUTBUF.off == TURN_OBSIZE-1)
		turn_drain();

	if (overflow>0) {
		_8_u *p;
		p = __p+__n;
		_8_u *end;
		end = p+overflow;// do somthing about this
		while(p != end) {
			turn_oust(p++, 1);
		}
	}
}

void* turn_smemdup(void *__p, _int_u __bc) {
    void *ret = turn_strc.sp;
    turn_strc.sp+=__bc;
    _8_u *p = (_8_u*)ret;
    _8_u *end = p+__bc;
    while(p != end) {
        *p = *((_8_u*)__p+(p-(_8_u*)ret));
        p++;
    }
    return ret;
}

void* turn_memdup(void *__p, _int_u __bc) {
    void *ret = turn_al(__bc);
    _8_u *p = (_8_u*)ret;
    _8_u *end = p+__bc;
    while(p != end) {
        *p = *((_8_u*)__p+(p-(_8_u*)ret));
        p++;
    }
    return ret;
}
