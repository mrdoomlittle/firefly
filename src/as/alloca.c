# include "as.h"
# include "../m_alloc.h"
typedef struct hdr {
	struct hdr *next, **bk;
} *hdrp;


hdrp static top = NULL;
# define hdrsize sizeof(struct hdr)

void turn_al_cu() {
	if (!top) return;
	hdrp cur = top, bk;
	while(cur != NULL) {
		bk = cur;
		cur = cur->next;
		m_free(bk);
	}
}
# define link(__h) \
	__h->bk = &top;	\
	if (top != NULL) {	\
		top->bk = &__h->next;	\
	}				\
	__h->next = top;	\
	top = h;

void *turn_al(_int_u __size) {
	_8_u *p = (_8_u*)m_alloc(hdrsize+__size);
	hdrp h = (hdrp)p;
	link(h);
	return p+hdrsize;
}

# define delink(__h) \
	*__h->bk = __h->next;	\
	if (__h->next != NULL) {	\
		__h->next->bk = __h->bk; \
	}

void turn_fr(void *__p) {
	hdrp h = ((_8_u*)__p)-hdrsize;
	delink(h);
	m_free(h);
}

void *turn_ral(void *__p, _int_u __size) {
	hdrp h = ((_8_u*)__p)-hdrsize;
	delink(h);
	
	h = (hdrp)m_realloc(h, hdrsize+__size);
	link(h);
	return (_8_u*)h+hdrsize;
}
