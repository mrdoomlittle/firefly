# include "bd.h"
#define BZ(__nopds) (sizeof(struct f_bd_blob)+(__nopds*sizeof(struct f_bd_opd)))
struct f_bd_blob_desc f_bd_blob_dsc[F_BD_NBLOB] = {
	{BZ(2)},
	{BZ(1)},
	{BZ(2)},
	{BZ(2)},
	{BZ(1)},
	{BZ(0)},
	{BZ(2)},
	{BZ(2)}
};
