# ifndef __f__as__mp__h
# define __f__as__mp__h
# include "../y_int.h"
# include "as.h"
/*
	rename
*/
/*
	max ident length

	this is for very specific lookups
	with idents that have a limit on length as
	using a traditional hash table would be wastful
*/
#define _F_as_shp_1 0
#define _F_as_shp_2 1
#define _F_as_shp_4 2
#define _F_as_shp_8 3
#define F_AS_MP_MAX 24
/*
	in shift
*/
#define F_AS_SHARP _F_as_shp_1
#define F_AS_MP_TZ (F_AS_MP_MAX>>F_AS_SHARP)
struct f_as_mpent;
struct f_as_mp {
	struct f_as_mpent *tbl[F_AS_MP_TZ];
};
struct f_as_mpent {
	struct f_as_mpent *next;
	_8_u *ident;
	_int_u len;
	_ulonglong data;
};

struct f_as_mpent_s {
	_8_u *p;
	_int_u len;
	_ulonglong data;
};

void f_as_mp_init(void);
void f_as_mp_xput(struct f_as_mpent_s*, _int_u);
void f_as_mp_put(_8_u*, _int_u, _ulonglong);
_ulonglong f_as_mp_get(_8_u*, _int_u);
# endif /*__f__as__mp__h*/
