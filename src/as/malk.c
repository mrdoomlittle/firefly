#include "as.h"
#include "malk.h"
#include "opcodes/pic.h"
#include "../string.h"
#include "../io.h"
#include "../strf.h"
#include "../assert.h"
#include "../m_alloc.h"
struct m_section *m_sect;

struct malk_struc m_strc = {
	.ft = NULL,
	.fcur = NULL
};

/*
	symbol does not carry any name and only exists in WA structs
*/
struct m_smdata*
m_dummy_symb(void) {
	struct m_smdata *sb;
	sb = m_alloc(sizeof(struct m_smdata));
	sb->tn = 0;
	sb->flags  = 0;
	return sb;
}

/*
	globl symbols dont need fragment dislocation
*/
void *__smd;
_8_s didsym_exist;
struct m_smdata*
m_new_symb_non_exist(char *__name,_int_u __len,_64_u __x) {
	struct hep_struc h;
	tun_hash_pgi(&m_strc.labels,__name,__len,&h);
	struct m_symb0 *sb;
	didsym_exist = 0;
	if (h.exist == -1) {
		sb = m_strc.symb_new();
		mem_set(sb->sm_inited,-1,16);
		sb->data = m_strc.sm_data_alloc(16);
		h.d->p = sb;
		mem_cpy(sb->name, __name, __len);
		sb->len = __len;
		sb->n = 0;
		sb->top = NULL;
		didsym_exist = -1;
	} else
		sb = h.d->p;
	assert(__x<16);
	struct m_smdata *sm = M_SMDATA_AT(sb->data,__x);
	if (sb->sm_inited[__x] == -1) {
		sm->smd = __smd;
		sm->w = NULL;	
		sm->value = 0;
		sm->dom = 45;
		sm->tn = 0;
		sm->x = __x;
		sm->sec_idx = 0;
		sb->sm_inited[__x] = 0;
		sb->n+=1;
		sm->next = sb->top;
		sb->top = sm;
	}
	sm->sec = m_sect;
	return sm;
}
_int_u
m_read_ident(char *__buf, char **__src, _int_u __len) {
    _int_u register len = 0;
    char *p = *__src;
    char c;
    c = *++p;
    while((c>='a'&&c<='z')||(c>='0'&&c<='9')||c == '_') {
        __buf[len++] = c;
        printf(">>/%c.\n",c);
        c = *++p;
    }
    *__src = p;
    return len;
}

void malk_init(void) {
	tun_hash_init(&m_strc.labels);
}
