# ifndef __ffly__as__symbol__h
# define __ffly__as__symbol__h
# include "../y_int.h"

# define SY_STR 0x1
# define SY_MAC 0x2
# define SY_CHR 0x3
# define SY_INT 0x4
# define SY_LABEL 0x5
# define SY_DIR 0x6
# define SY_REG 0x7
# define SY_UNKNOWN 0x8
# define SY_LL 0x9
# define is_syreg(__sy) \
	((__sy)->sort==SY_REG)
# define is_symac(__sy) \
	((__sy)->sort==SY_MAC)
# define is_systr(__sy) \
    ((__sy)->sort==SY_STR)
# define is_sychr(__sy) \
    ((__sy)->sort==SY_CHR)
# define is_syint(__sy) \
    ((__sy)->sort==SY_INT)
# define is_sylabel(__sy) \
	((__sy)->sort==SY_LABEL)
# define is_sydir(__sy) \
	((__sy)->sort==SY_DIR)
# define is_syll(__sy) \
	((__sy)->sort==SY_LL)

#define F_AS_SY_HID 0x04
/*
	symbol needs to be resolved
	as it does not exists here
*/
#define F_AS_SY_RS 0x01
/*
	symbol was found within file
*/
#define F_AS_SY_FND 0x02
/*
	handled here
*/
#define F_AS_SY_HH 0x04
#include "symb_struc.h"
symbolp extern sy_head;
_64_u extern syt_dst;
struct hash extern symbols;
void f_as_sbi(f_as_symbolp, _8_u*, _int_u);
f_as_symbolp f_as_symb(_8_u*, _int_u, struct hash*);
f_as_symbolp f_as_symb_n(_8_u*, _int_u, struct hash*, void*);
f_as_symbolp f_as_symb_new(_8_u*, _int_u);
void ff_as_putsymbol(symbolp);
symbolp ff_as_getsymbol(char const*);
f_as_symbolp ff_as_syt(_8_u*, _int_u);

# endif /*__ffly__as__symbol__h*/
