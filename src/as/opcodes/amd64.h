# ifndef __ffly__opcodes__amd64__h
# define __ffly__opcodes__amd64__h
# include "../../y_int.h"
# include "../as.h"
#define NOPS 9
#define REGC 22
#define ot(__t0, __t1, __t2, __t3) \
	{__t0, __t1, __t2, __t3}
#define get_ot(__op, __n) \
	((__op)->ots[__n])
#define modrm 0x01
// same size as dst
#define ssad 0x02
#define noprefix 0x04
#define _d8s	0
#define _d16s	1
#define _d32s	2
#define _d64s	3
#define _d8		0x01
#define _d16	0x02
#define _d32	0x04
#define _d64 	0x08
#define osof16 0x08
struct f_as_opbody;
struct f_as_optray {
	char const *name;
	_8_u nme_len;
	_int_u n_bodies;
	struct f_as_opbody *b;
};

struct f_as_opbody {
	_8_u opcode[8];
	// length of opcode(bytes)
	_8_u l;
	_8_u flags;
	_8_u ext;
	_16_u ots[4];
	_8_u oc;
	_16_u ad;
};

typedef struct f_as_regbody {
	struct f_as_reginfo info;
	_8_u enc, mrm;
	struct f_as_reg const *head;
} *f_as_regbodyp;

extern struct f_as_optray const amd64_optab[NOPS];
extern struct f_as_reg const amd64_regtab[REGC];
# endif /*__ffly__opcodes__amd64__h*/
