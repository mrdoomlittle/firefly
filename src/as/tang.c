#include "urek.h"
#include "../tools.h"
#include "../io.h"
struct f_as_tang* f_as_tang_new(void) {
	struct f_as_tang *t;
	t = (struct f_as_tang*)turn_al(sizeof(struct f_as_tang));
	if (!f_ass.tg_top)
		f_ass.tg_top = t;
	if (f_ass.tg_end != NULL)
		f_ass.tg_end->next = t;
	f_ass.tg_end = t;
//	t->tx = f_ass.tfx_new(f_ass._locl);
//	t->tx->t = t;
	t->size = 0;
	t->space = 0;
	t->adr = 0;
	t->k = 0;
	t->w = NULL;
	t->b = NULL;
	t->bs = 0;
	t->bits = F_asts_DW;
	return t;	

}

char const static* stbl[] = {
	"(OKAY)",
	"(DWINDLING)"
};

void f_as_tout(void) {
	f_ass.tg_end->next = NULL;

	_64_u adr = 0;
	struct f_as_tb *b;
	struct f_as_tang *t;
	t = f_ass.tg_top;
	while(t != NULL) {
		printf("OUTPUT-TANG, state: %s, size: %u\n", stbl[F_AST_BGS(t->bits)], t->size);
		t->dst = turn_strc.out.offset;
		t->adr = adr+t->k;
		struct f_as_tbc *c;
		c = t->b;
		while(c != NULL) {
			_int_u i;
			for(i = 0;i != c->n;i++) {
				b = c->b+i;
				printf("b: %p, %u.\n", b->w, b->size);
				ffly_hexdump(b->w, b->size);
				TURN_write(b->w, b->size);
			}
			c = c->next;
		}
		if (t->bs>0) {
			TURN_write(t->buf, t->bs);
		}
		turn_strc.out.offset+=t->space;
		adr+=t->space;
		t = t->next;
	}
}
