# ifndef __ffly__mould__h
# define __ffly__mould__h
# include "../y_int.h"
extern _8_u *p;
# define incrp p++
# define decrp p--
typedef struct bucket {
	struct bucket *next, *fd;
	void *src, *dst;
	_8_u sort:4;
	_8_u val;
	void *p;
	_16_u len;
} *bucketp;

enum {
	_op_cp,
	_op_exit,
	_op_jump,
	_op_echo,
	_op_end,
	_op_shell,
	_op_jobs_wait
};

struct hash_entry {
	struct hash_entry *fd, *next;	
	_8_u const *key;
	_int_u len;
	void *p;
};

struct hash {
	struct hash_entry **table;
	struct hash_entry *top;
};

struct frag {
	void *p;
	_int_u len;
	struct frag *next;
};

extern struct hash env;
struct shell {
	char *base;
	char *args[20];
	_int_u argc;
};

struct var {
	void *p;
	_int_u l;
};

typedef struct obj {
	struct obj *next;
	_8_u op;
	void *src, *dst, *p;
	struct obj *to;
} *objp;

enum {
	_eq,
	_colon,
	_comma,
	_circumflex,
	_semicolon,
	_period,
	_keywd_entry,
	_keywd_cp,
	_keywd_exit,
	_keywd_end,
	_keywd_echo,
	_keywd_waitall
};

enum {
	_label,
	_cp,
	_exit,
	_end,
	_jump,
	_shell,
	_echo,
	_waitall
};

enum {
	_unknown,
	_ident,
	_chr,
	_keywd,
	_str
};

_8_u is_keywd(bucketp, _8_u);
bucketp nexttok();
void maybe_keyword(bucketp);
_8_u tokbuf_size();
_8_u expect_token(_8_u, _8_u);
bucketp peektok();
bucketp lex();
void ulex(bucketp);
void parse(bucketp*);
_8_u at_eof();
void lexer_cleanup();
void mould_exec(objp);
void gen(bucketp, objp*);
void oust(bucketp, _8_u);
void to_free(void*);

// hash.c
void mld_hash_init(struct hash*);
void mld_hash_destroy(struct hash*);
void mld_hash_put(struct hash*, _8_u const*, _int_u, void*);
void* mld_hash_get(struct hash*, _8_u const*, _int_u);
# endif /*__ffly__mould__h*/
