# ifndef __f__def__h
# define __f__def__h
/*
	some standards need to be set
*/
#define F_SEEK_SET 0
#define F_SEEK_END 1
#define F_SEEK_CUR 2

#define F_SHUT_RD 0
#define F_SHUT_WR 1
#define F_SHUT_RDWR 2

struct f_iov {
	void *p;
	_64_u offset;
	_int_u size;
};
// io vector cluster
struct f_iovc {
	_int_u size;
	_int_u n;
	struct f_iov *vec;
};

# endif /*__f__def__h*/
