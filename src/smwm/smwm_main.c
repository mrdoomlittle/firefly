#include "smwm_common.h"
#include "../iu/iu.h"
#include "../citric/citric.h"
#include "../vekas/common.h"
#include "../vekas/maj.h"
#include "../flue/mjx.h"
#include "../iu/iu_window.h"
#include "../iu/iu_btn.h"
#include "../flue/common.h"
#include "../pixels.h"
#include "../clay.h"
static struct iu_window decor;
static struct yarn_texture icons;
static struct iu_btn close_button;
static struct iu_icon icon_close;
void static load_icons(void) {
  struct clay clay;
  clay_init(&clay);

  clay_load(&clay, "icons.clay");
  clay_read(&clay);

  void *ic, *p;
  ic = clay_get("icons",&clay);
 _64_u *arr;
 
	p = clay_tget("exit",ic);
  arr = clay_array(p);
  icon_close.uv[0] = *(float*)(arr+0);
  icon_close.uv[1] = *(float*)(arr+1);
  icon_close.uv[2] = *(float*)(arr+2);
  icon_close.uv[3] = *(float*)(arr+3);
 
 float mat[] = {
    0,1,0,0,0,
    0,1,0,0,0,
    0,1,0,0,0,
    0,1,0,0,0
  };
  mu_mat = mat;

  ct_tex_new(ct_com.g,&icons);
  struct yarn_fmtdesc fmt = {.depth=4*sizeof(float),.nfmt=YAR_NF_FLOAT,.dfmt=YAR_DF_32_32_32_32};
  ct_tex_data(ct_com.g,&icons,128,128,&fmt);
  ct_tex_fileload(ct_com.g,&icons,"icons.ppm");

  clay_de_init(&clay);
}
void iu_message(_ulonglong __arg, struct ct_msg *m);
static _int_s xdis,ydis;


void static mesg(void*_0,struct ct_msg *m){
	if(m->code == CY_BTN_LEFT && m->value == CT_RELEASE) {
		iu_com.message = iu_message;
		struct maj_patch *pt;
		pt = decor.surf->userdata;
		maj_ptungrab(ct_com.conn,pt);
	}
}


void _iu_postmsg(struct ct_msg *m){
	if(m->code == CY_BTN_LEFT && m->value == CT_PRESS) {
		iu_com.message = mesg;
		struct maj_patch *pt;
		pt = decor.surf->userdata;

		xdis = m->x-pt->x;
		ydis = m->y-pt->y;
		maj_ptgrab(ct_com.conn,pt,xdis,ydis);
	}
}
_8_s _iu_msg(struct ct_msg *m){
}
void _iu_init(void){
	maj_ptconfig(ct_com.conn,2);
	load_icons();
}
void _iu_deinit(void){
}

void _iu_tick(void){
	void *priv[1] = {decor.surf->render};
	flue_clear(priv,1,.8,.8,.8,1);
}

static _int_u tab_for_exit[] = {
	[IU_BTN_PRESS]  =   0
};


void button_events(struct iu_widget*a,struct iu_widget*b,struct ct_msg*m) {
	iu_com.running = -1;
}

void controlmsg(struct vk_msg *m){
	_32_u u;
	u = m->type&~0xff;
	if(u == _VK_MSG_NEW_PATCH){
		printf("got new patch message.\n");
		printf("patch, x: %u, y: %u, width: %u, height: %u, patch: %u\n",
			m->newpatch.x,m->newpatch.y,m->newpatch.width,m->newpatch.height,m->newpatch.patch
		);
		ffly_fdrain(_ffly_out);

		iu_window_new(&decor);
		iu_window_map(&decor,m->newpatch.x,m->newpatch.y-32,m->newpatch.width,32);
		iu_load(&decor);
		iu_btn_init(&close_button);
		close_button._w.w = 32;
		close_button._w.h = 32;
		iu_btn_with_icon(&close_button);
		close_button.icon = &icon_close;
		close_button.tex = &icons;
		struct iu_render *rn;
		rn = iu_render(&decor._b.comp);
		rn->func = iu_wgdraw;
		rn->priv = &close_button;
		iu_wgmove(0,&close_button,m->newpatch.width-32,0);
		iu_wgtrajection(&close_button);
		struct maj_patch *pt;
		pt = decor.surf->userdata;

		maj_ptconjoin(ct_com.conn,pt->adr,m->newpatch.patch);
		close_button._w.codetab = tab_for_exit;
		close_button._w.output = button_events;

	}else{
		ct_com.foward2(m);
	}
}

_err_t main(int __argc, char const *__argv[]){
	ct_com.foward = controlmsg;
	iu_start();
}

