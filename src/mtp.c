# include "mtp.h"
# include <linux/limits.h>
# include "dep/str_cpy.h"
# include "dep/bzero.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/err.h"
# include "linux/fcntl.h"
# include "linux/unistd.h"
# include "linux/stat.h"
# include "system/io.h"
# include "malloc.h"
# include "dep/mem_cpy.h"
# include "dep/mem_set.h"
FF_SOCKET static *sock;

# define FILE_MAX 20

typedef struct tape {
	void *text;
	_int_u len;
} *tapep;

tapep tape_new(_int_u __sz) {
	tapep t;
	t = (tapep)malloc(sizeof(struct tape));
	t->text = malloc(__sz);
	t->len = __sz;
	return t;
}

void tape_destroy(tapep __t) {
	free(__t->text);
	free(__t);
}

static char *dir[PATH_MAX];
char const* ffly_mtp_errst(_8_u __err) {
	switch(__err) {
		case _ff_mtp_err_ptl: return "path too large";
		case _ff_mtp_err_mst: return "missing string terminator";
	}
	return "unknown";
}
/*
_f_err_t
ffly_mtp_snd_report(FF_SOCKET *__sock, ffly_mtp_repp __report) {
	_f_err_t err;
	ff_net_send(__sock, __report, sizeof(struct ffly_mtp_rep), 0, &err);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to send report.\n");
		_ret;
	}
	retok;
}	

_f_err_t
ffly_mtp_rcv_report(FF_SOCKET *__sock, ffly_mtp_repp __report) {
	_f_err_t err;
	ff_net_recv(__sock, __report, sizeof(struct ffly_mtp_rep), 0, &err);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to recv report.\n");
		_ret;
	}
	retok;
}

_f_err_t
ffly_mtp_snderr(_f_err_t __err, FF_SOCKET *__sock) {
	_f_err_t err;
	ff_net_send(__sock, &__err, sizeof(_f_err_t), 0, &err);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to send error.\n");
		_ret;
	}
	retok;
}

_f_err_t
ffly_mtp_rcverr(_f_err_t *__err, FF_SOCKET *__sock) {
	_f_err_t err;
	ff_net_recv(__sock, __err, sizeof(_f_err_t), 0, &err);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to recv error.\n");
		_ret;
	}
	retok;
}
*/
_f_err_t
ffly_mtp_snd(FF_SOCKET *__sock, void *__p, _int_u __size) {
	_f_err_t err;
	ff_net_send(__sock, &__size, sizeof(_int_u), 0, &err);
	if (_err(err))
		_ret;
	ff_net_send(__sock, __p, __size, 0, &err);
	if (_err(err))
		_ret;
	retok;
}

void*
ffly_mtp_rcv(FF_SOCKET *__sock, _int_u *__size, _f_err_t *__err) {
	*__err = FFLY_SUCCESS;
	ff_net_recv(__sock, __size, sizeof(_int_u), 0, __err);
	if (_err(*__err))
		return NULL;
	void *p = __f_mem_alloc(*__size);
	ff_net_recv(__sock, p, *__size, 0, __err);
	if (_err(*__err)) {
		ffly_fprintf(ffly_out, "failed to recv.\n");
		__f_mem_free(p);
		return NULL;
	}
	return p;
}

_f_err_t ffly_mtpd_prepare(char const *__dir) {
	f_str_cpy(dir, __dir);
	retok;
}

_f_err_t ffly_mtpd_open() {
	_f_err_t err;
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(21299);

	sock = ff_net_creat(&err, _NET_PROT_TCP);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to create socket.\n");
		_ret;
	}

	if (_err(err = ff_net_bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)))) {
		ff_net_close(sock);
		_ret;
	}
	retok;
}

// illegal charset
_8_i static
illegal(char const *__s){
	char const *p = __s;
	char c;
	while((c = *(p++)) != '\0') {
		if ((c<'a'||c>'z') && c != '.')
			return -1;
	}
	return 0;
}

#define STACKSZ 256
_8_u static stack[STACKSZ];
#define stackat(__ad) \
	(stack+(__ad))

_8_u static *cc;
FF_SOCKET static *peer;
void static
get_rq(void) {
	_f_err_t err;
	_16_u fa;
	fa = *(_16_u*)cc;
	_16_u len;
	len = *(_16_u*)(cc+2);

	char file[128];
	f_mem_cpy(file, stackat(fa), len);
	*(file+len) = '\0';

	if (illegal(file) == -1) {
		return;
	}

	char buf[PATH_MAX];
	char *p = buf;
	p+=f_str_cpy(p, dir);
	*(p++) = '/';
	f_str_cpy(p, file);

	if (access(buf, F_OK) == -1) {
		ffly_fprintf(ffly_err, "file at '%s' doesn't exist or is inaccessible.\n", buf);
		return;
	}

	int fd;
	if ((fd = open(buf, O_RDONLY, 0)) == -1) {
		printf("failed to open file.\n");
		return;
	}

	struct stat st;
	fstat(fd, &st);

	void *exch = __f_mem_alloc(st.st_size);
	read(fd, exch, st.st_size);
	close(fd);

	if (_err(err = ffly_mtp_snd(peer, exch, st.st_size))) {
		__f_mem_free(exch);
		return;
	}
	__f_mem_free(exch);	

	printf("file: %s\n", buf);
}

void static
store(void) {
	_16_u dst, n;
	dst = *(_16_u*)cc;
	n = *(_16_u*)(cc+2);

	if (dst+n>STACKSZ)
		return;
	_f_err_t err;
	ff_net_recv(peer, stackat(dst), n, 0, &err);
}

void static
load(void) {
	_16_u src, n;
	src = *(_16_u*)cc;
	n = *(_16_u*)(cc+2);

	if (src+n>STACKSZ)
		return;
	_f_err_t err;
	ff_net_send(peer, stackat(src), n, 0, &err);
}

void static
report(void) {

}

static void(*op[])(void) = {
	get_rq,
	store,
	load,
	report
};

_8_u static osz[] = {
	4,
	4,
	4
};

static _8_i run = -1;
void static sig(int __no) {
    run = 0;
}


_f_err_t ffly_mtpd_start() {
	struct sigaction sa;
	f_mem_set(&sa, 0, sizeof(struct sigaction));
	sa.sa_handler = sig;
	sigaction(SIGINT, &sa, NULL);

	struct sockaddr_in cl;
	f_bzero(&cl, sizeof(struct sockaddr_in));

	_f_err_t err;
	socklen_t len = sizeof(struct sockaddr_in);
	tapep t;
	_32_u tsz;
_back:
	if (_err(err = ff_net_listen(sock))) {
		ffly_fprintf(ffly_err, "failed to listen.\n");
		_ret;
	}

	peer = ff_net_accept(sock, (struct sockaddr*)&cl, &len, &err);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to accept.\n");
		_ret;
	}

	ff_net_recv(peer, &tsz, sizeof(_32_u), 0, &err);
	t = tape_new(tsz);
	ff_net_recv(peer, t->text, len, 0, &err);
	_8_u *end;
	cc = (_8_u*)t->text;
	end = cc+tsz;
	_8_u on;
_again:
	on = *(cc++);
	printf("op: %u\n", on);
	op[on]();
	cc+=osz[on];
	if (cc<end)
		goto _again;
	tape_destroy(t);
	ff_net_close(peer);

	if (run == -1)
		goto _back;
_end:
	if (_err(err))
		_ret;
	retok;
}

_f_err_t ffly_mtpd_close() {
	ff_net_close(sock);
	retok;
}

_f_err_t ffly_mtpd_cleanup() {
	retok;

}
