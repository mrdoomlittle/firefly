#ifndef __y__opt__h
#define __y__opt__h
#include "y_int.h"
#include "lib/hash.h"
#define OPT_FRONTLINE 1
struct y_opt{
	char const *str;
	_int_u len;
	char const *val;
	_64_u bits;
};

struct y_opt_comm {
	_32_u *charmap;
	struct f_lhash hm;
	struct y_opt *opts;
	struct y_opt *list[32];
	_int_u ln;
};

void y_opts_prep(struct y_opt_comm*,struct y_opt*,_int_u);

void y_opts(struct y_opt_comm*,char const*[],_int_u);

#endif/*__y__opt__h*/
