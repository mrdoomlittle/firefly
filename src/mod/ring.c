# include "ring.h"
# include "../call.h"
# include "port.h"
# include "../system/port.h"
# include "../ffly_def.h"
# include "../types.h"
void static
ring_printf(void *__ret, void *__params) {
	ffly_port_send(PORT_BAND, __params, sizeof(_64_u), ffmod_portno());
}

void static
ring_malloc(void *__ret, void *__params) {
	_f_err_t err;
	ffly_port_send(PORT_BAND, __params, sizeof(_int_u), ffmod_portno());
	ffly_port_recv(PORT_BAND, __ret, sizeof(void*), ffmod_portno());
}

void static
ring_free(void *__ret, void *__params) {
	ffly_port_send(PORT_BAND, __params, sizeof(void*), ffmod_portno());
}


void static
ring_dcp(void *__ret, void *__params) {
	void *dst = *(void**)__params;
	void *src = *(void**)((_8_u*)__params+8);
	_int_u *n = (_int_u*)((_8_u*)__params+16);

	ffly_port_send(PORT_BAND, (void*)&src, sizeof(void*), ffmod_portno());
	ffly_port_send(PORT_BAND, n, sizeof(_int_u), ffmod_portno());
	ffly_port_recv(PORT_BAND, dst, *n, ffmod_portno());
}

void static
ring_scp(void *__ret, void *__params) {
	void *dst = *(void**)__params;
	void *src = *(void**)((_8_u*)__params+8);
	_int_u *n = (_int_u*)((_8_u*)__params+16);

	ffly_port_send(PORT_BAND, (void*)&dst, sizeof(void*), ffmod_portno());
	ffly_port_send(PORT_BAND, n, sizeof(_int_u), ffmod_portno());
	ffly_port_send(PORT_BAND, src, *n, ffmod_portno());
}

static void(*ring[])(void*, void*) = {
	ring_printf,
	NULL,
	ring_malloc,
	ring_free,
	NULL,
	ring_dcp,
	ring_scp
};

void ffmod_ring(_8_u __no, void *__ret, void *__params) {
	ffly_port_send(PORT_BAND, &__no, 1, ffmod_portno());
	ring[__no](__ret, __params);
}
