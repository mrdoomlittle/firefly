# ifndef __ffly__maths__h
# define __ffly__maths__h
# include "ffly_def.h"
# include "maths/max.h"
# include "maths/min.h"
# include "maths/rotate.h"
# include "maths/abs.h"
double ceil(double);
double cos(double);
double floor(double);
double sin(double);
double round(double);
double sqr(double);
double tan(double);
#define PI 3.14159
# define min3 ffly_min3
# define max3 ffly_max3
#define max(__x,__y) ((__x)>(__y)?(__x):(__y))
#define min(__x,__y) ((__x)<(__y)?(__x):(__y))
#define _abs(__x) (((__x)^((__x)>>32))-((__x)>>32))
#define abs(__val) ((__val)<0?-(__val):(__val))
#define trad(__val) (((__val)/180.)*PI)
# define barycentric ffly_barycentric
#define vec_sub(__dst, __v0, __v1)\
	(__dst).x = (__v0).x-(__v1).x;\
	(__dst).y = (__v0).y-(__v1).y;\
	(__dst).z = (__v0).z-(__v1).z;
#define dotv3(__v0, __v1)\
	(((__v0).x*(__v1).x)+((__v0).y*(__v1).y)+((__v0).z*(__v1).z))
# endif /*__ffly__maths__h*/
