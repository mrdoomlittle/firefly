#include "resin.h"
#include "system/errno.h"
#include "io.h"
#include "string.h"
#include "m_alloc.h"
#define copymem(__src, __dst, __size) \
	mem_cpy(__dst, __src, __size)
#define is_flag(__flags, __flag) \
	((__flags&__flag)==__flag)
/*
	make stack put and get better somehow
	also more error detection

	TODO: COULDDO
		add lock memory - a range from X to X within stack where memory can be locked
		and can only be unlocked by an external source
*/
/*
	TODO:
	stack_put and stack_get to asm not c
*/

/*
	TODO:
		use function pointer for get functions e.g. get_8l
		as if flag not set we will use a direct method to get a byte else will go thru getbits
		as to allow for compact code

	THINK:
		store 8 operations in one 64bit int?????
		or compact it to smaller range or max operations 0-64??
		so to only take up smaller xn of bits
	TODO:
		allow for selection for multi operation mode

		so operation num will be stored in one 64int

		example: <- only example
		op0		op1		op2
		0000 - 0000 - 0000....

		this mode will be user defined so src/as wont be able to use(might)

	TODO:
		allow for user minor operations like db but minor operations are user only
		and wont be used internaly like the rin instruction but more direct.
*/
_y_err static
stack_put(ffly_resinp __res, _8_u *__src, _int_u __bc, _f_addr_t __addr) {
	_8_u *p = __src;
	_8_u *end = p+__bc;
	_f_addr_t dst;

	while(p != end) {
		if ((dst = __addr+(p-__src)) >= __res->stack_size) {
			printf("put error.\n");
			return FFLY_FAILURE;
		}

		*(__res->stack+dst) = *(p++);
	}
	return FFLY_SUCCESS;
}

_y_err static
stack_get(ffly_resinp __res, _8_u *__dst, _int_u __bc, _f_addr_t __addr) {
	_8_u *p = __dst;
	_8_u *end = p+__bc;
	_f_addr_t src;

	while(p != end) {
		if ((src = __addr+(p-__dst)) >= __res->stack_size) {
			printf("get error.\n");
			return FFLY_FAILURE;
		}

		*(p++) = *(__res->stack+src);	
	}
	return FFLY_SUCCESS;
}

void ff_resin_sst(ffly_resinp __res, void *__p, _f_addr_t __adr, _int_u __n) {
	mem_cpy(__res->stack+__adr, __p, __n);
}

void ff_resin_sld(ffly_resinp __res, _f_addr_t __adr, void *__p, _int_u __n) {
	mem_cpy(__p, __res->stack+__adr, __n);
}

void ff_resin_rset(ffly_resinp __res, void *__p, _8_u __r, _int_u __n) {
	mem_cpy(__res->r[__r], __p, __n);
}

void ff_resin_rget(ffly_resinp __res, _8_u __r, void *__p, _int_u __n) {
	mem_cpy(__p, __res->r[__r], __n);
}

_y_err ff_resin_init(ffly_resinp __res) {
	__res->retto = __res->rtbuf;

	_64_u **r = __res->r;
	*r = &__res->r0;
	*(r+1) = &__res->r1;
	*(r+2) = &__res->r2;
	*(r+3) = &__res->r3;
	return FFLY_SUCCESS;
}

_y_err ff_resin_de_init(ffly_resinp __res) {
	return FFLY_SUCCESS;
}
/*
	TODO:
		prestore instruction ^
		so we dont need to read byte per byte thru instruction
		also could do this a few steps ahead
*/
#define __ADDR_TS sizeof(_f_addr_t)
void*
ff_resin_resolv_adr(ffly_resinp __res, _f_addr_t __adr) {
	if (__adr>=__res->stack_size) {
		printf("address out of rage, got: %u\n", __adr);
		return NULL;
	}
	return (void*)(__res->stack+__adr);
}

void _res_exit0();

void _res_as0b();
void _res_as0w();
void _res_as0d();
void _res_as0q();

void _res_jmp0();

void _res_st0b();
void _res_st0w();
void _res_st0d();
void _res_st0q();

void _res_ld0b();
void _res_ld0w();
void _res_ld0d();
void _res_ld0q();

void _res_out0b();
void _res_out0w();
void _res_out0d();
void _res_out0q();

void _res_mov0b();
void _res_mov0w();
void _res_mov0d();
void _res_mov0q();

void _res_rin0();

void _res_div0b();
void _res_div0w();
void _res_div0d();
void _res_div0q();

void _res_mul0b();
void _res_mul0w();
void _res_mul0d();
void _res_mul0q();

void _res_sub0b();
void _res_sub0w();
void _res_sub0d();
void _res_sub0q();

void _res_add0b();
void _res_add0w();
void _res_add0d();
void _res_add0q();

void _res_inc0b();
void _res_inc0w();
void _res_inc0d();
void _res_inc0q();

void _res_dec0b();
void _res_dec0w();
void _res_dec0d();
void _res_dec0q();

void _res_cmp0b();
void _res_cmp0w();
void _res_cmp0d();
void _res_cmp0q();

void _res_cjmp0();
void _res_call0();
void _res_ret();

void _res_as1br0();
void _res_as1wr0();
void _res_as1dr0();
void _res_as1qr0();

void _res_as1br1();
void _res_as1wr1();
void _res_as1dr1();
void _res_as1qr1();

void _res_mov1br0();
void _res_mov1wr0();
void _res_mov1dr0();
void _res_mov1qr0();

void _res_mov1br1();
void _res_mov1wr1();
void _res_mov1dr1();
void _res_mov1qr1();

void _res_mov2br0();
void _res_mov2wr0();
void _res_mov2dr0();
void _res_mov2qr0();

void _res_mov2br1();
void _res_mov2wr1();
void _res_mov2dr1();
void _res_mov2qr1();

void _res_mov3br0r1();
void _res_mov3wr0r1();
void _res_mov3dr0r1();
void _res_mov3qr0r1();

void _res_mov3br1r0();
void _res_mov3wr1r0();
void _res_mov3dr1r0();
void _res_mov3qr1r0();

void _res_out1br0();
void _res_out1wr0();
void _res_out1dr0();
void _res_out1qr0();

void _res_out1br1();
void _res_out1wr1();
void _res_out1dr1();
void _res_out1qr1();

void _res_inc1br0();
void _res_inc1wr0();
void _res_inc1dr0();
void _res_inc1qr0();

void _res_inc1br1();
void _res_inc1wr1();
void _res_inc1dr1();
void _res_inc1qr1();

void _res_inc1br2();
void _res_inc1wr2();
void _res_inc1dr2();
void _res_inc1qr2();

void _res_inc1br3();
void _res_inc1wr3();
void _res_inc1dr3();
void _res_inc1qr3();

void _res_dec1br0();
void _res_dec1wr0();
void _res_dec1dr0();
void _res_dec1qr0();

void _res_dec1br1();
void _res_dec1wr1();
void _res_dec1dr1();
void _res_dec1qr1();

void _res_dec1br2();
void _res_dec1wr2();
void _res_dec1dr2();
void _res_dec1qr2();

void _res_dec1br3();
void _res_dec1wr3();
void _res_dec1dr3();
void _res_dec1qr3();

void _res_jmp1r0();
void _res_jmp1r1();

void _res_add1br0r1();
void _res_add1wr0r1();
void _res_add1dr0r1();
void _res_add1qr0r1();

void _res_add1br1r0();
void _res_add1wr1r0();
void _res_add1dr1r0();
void _res_add1qr1r0();

void _res_r0r0();
void _res_r1r0();
void _res_r2r0();
void _res_r3r0();

void _res_r0r1();
void _res_r1r1();
void _res_r2r1();
void _res_r3r1();

void _res_push0b();
void _res_push0w();
void _res_push0d();
void _res_push0q();

void _res_pop0b();
void _res_pop0w();
void _res_pop0d();
void _res_pop0q();

void _res_push1br0();
void _res_push1wr0();
void _res_push1dr0();
void _res_push1qr0();

void _res_push1br1();
void _res_push1wr1();
void _res_push1dr1();
void _res_push1qr1();

void _res_pop1br0();
void _res_pop1wr0();
void _res_pop1dr0();
void _res_pop1qr0();

void _res_pop1br1();
void _res_pop1wr1();
void _res_pop1dr1();
void _res_pop1qr1();

void _res_lbpr0();
void _res_lbpr1();
void _res_lspr0();
void _res_lspr1();

void _res_exit1();
void _res_exit2r0();
void _res_exit2r1();
struct f_rs_op rs_ops[] = {
	{_res_exit0, __ADDR_TS},
	// assign
	{_res_as0b, __ADDR_TS+1},
	{_res_as0w, __ADDR_TS+2},
	{_res_as0d, __ADDR_TS+4},
	{_res_as0q, __ADDR_TS+8},	
	// jump
	{_res_jmp0, __ADDR_TS},
	// store
	{_res_st0b, __ADDR_TS*2},
	{_res_st0w, __ADDR_TS*2},
	{_res_st0d, __ADDR_TS*2},
	{_res_st0q, __ADDR_TS*2},
	// load
	{_res_ld0b, __ADDR_TS*2},
	{_res_ld0w, __ADDR_TS*2},
	{_res_ld0d, __ADDR_TS*2},
	{_res_ld0q, __ADDR_TS*2},
	// out
	{_res_out0b, __ADDR_TS},
	{_res_out0w, __ADDR_TS},
	{_res_out0d, __ADDR_TS},
	{_res_out0q, __ADDR_TS},
	// move
	{_res_mov0b, __ADDR_TS*2},
	{_res_mov0w, __ADDR_TS*2},
	{_res_mov0d, __ADDR_TS*2},
	{_res_mov0q, __ADDR_TS*2},
	// ring
	{_res_rin0, __ADDR_TS+1},
	// dev
	{_res_div0b, __ADDR_TS*2},
	{_res_div0w, __ADDR_TS*2},
	{_res_div0d, __ADDR_TS*2},
	{_res_div0q, __ADDR_TS*2},
	// mul
	{_res_mul0b, __ADDR_TS*2},
	{_res_mul0w, __ADDR_TS*2},
	{_res_mul0d, __ADDR_TS*2},
	{_res_mul0q, __ADDR_TS*2},
	// sub
	{_res_sub0b, __ADDR_TS*2},
	{_res_sub0w, __ADDR_TS*2},
	{_res_sub0d, __ADDR_TS*2},
	{_res_sub0q, __ADDR_TS*2},
	// add
	{_res_add0b, __ADDR_TS*2},
	{_res_add0w, __ADDR_TS*2},
	{_res_add0d, __ADDR_TS*2},
	{_res_add0q, __ADDR_TS*2},
	// incr
	{_res_inc0b, __ADDR_TS},
	{_res_inc0w, __ADDR_TS},
	{_res_inc0d, __ADDR_TS},
	{_res_inc0q, __ADDR_TS},
	// decr
	{_res_dec0b, __ADDR_TS},
	{_res_dec0w, __ADDR_TS},
	{_res_dec0d, __ADDR_TS},
	{_res_dec0q, __ADDR_TS},
	// compare
	{_res_cmp0b, __ADDR_TS*2},
	{_res_cmp0w, __ADDR_TS*2},
	{_res_cmp0d, __ADDR_TS*2},
	{_res_cmp0q, __ADDR_TS*2},
	// je
	{_res_cjmp0, __ADDR_TS},
	// jne
	{_res_cjmp0, __ADDR_TS},
	// jg
	{_res_cjmp0, __ADDR_TS},
	// jl
	{_res_cjmp0, __ADDR_TS},

	{_res_call0, __ADDR_TS},
	{_res_ret, 0},

	{_res_as1br0, 0},
	{_res_as1wr0, 0},
	{_res_as1dr0, 0},
	{_res_as1qr0, 0},

	{_res_as1br1, 0},
	{_res_as1wr1, 0},
	{_res_as1dr1, 0},
	{_res_as1qr1, 0},

	{_res_mov1br0, 0},
	{_res_mov1wr0, 0},
	{_res_mov1dr0, 0},
	{_res_mov1qr0, 0},

	{_res_mov1br1, 0},
	{_res_mov1wr1, 0},
	{_res_mov1dr1, 0},
	{_res_mov1qr1, 0},

	{_res_mov2br0, 0},
	{_res_mov2wr0, 0},
	{_res_mov2dr0, 0},
	{_res_mov2qr0, 0},
	
	{_res_mov2br1, 0},
	{_res_mov2wr1, 0},
	{_res_mov2dr1, 0},
	{_res_mov2qr1, 0},

	{_res_mov3br0r1, 0},
	{_res_mov3wr0r1, 0},
	{_res_mov3dr0r1, 0},
	{_res_mov3qr0r1, 0},

	{_res_mov3br1r0, 0},
	{_res_mov3wr1r0, 0},
	{_res_mov3dr1r0, 0},
	{_res_mov3qr1r0, 0},

	{_res_out1br0, 0},
	{_res_out1wr0, 0},
	{_res_out1dr0, 0},
	{_res_out1qr0, 0},

	{_res_out1br1, 0},
	{_res_out1wr1, 0},
	{_res_out1dr1, 0},
	{_res_out1qr1, 0},

	{_res_inc1br0, 0},
	{_res_inc1wr0, 0},
	{_res_inc1dr0, 0},
	{_res_inc1qr0, 0},

	{_res_inc1br1, 0},
	{_res_inc1wr1, 0},
	{_res_inc1dr1, 0},
	{_res_inc1qr1, 0},

	{_res_dec1br0, 0},
	{_res_dec1wr0, 0},
	{_res_dec1dr0, 0},
	{_res_dec1qr0, 0},

	{_res_dec1br1, 0},
	{_res_dec1wr1, 0},
	{_res_dec1dr1, 0},
	{_res_dec1qr1, 0},

	{_res_jmp1r0, 0},
	{_res_jmp1r1, 0},

	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},
	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},
	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},
	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},
	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},
	{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0},

	{_res_add1br0r1, 0},
	{_res_add1wr0r1, 0},
	{_res_add1dr0r1, 0},
	{_res_add1qr0r1, 0},

 	{_res_add1br1r0, 0},
 	{_res_add1wr1r0, 0},
 	{_res_add1dr1r0, 0},
	{_res_add1qr1r0, 0},
	
	{_res_lbpr0, 0},
	{_res_lbpr1, 0},
	{_res_lspr0, 0},
	{_res_lspr1, 0},

	{_res_r0r0, 0},
	{_res_r1r0, 0},
	{_res_r2r0, 0},
	{_res_r3r0, 0},

	{_res_r0r1, 0},
	{_res_r1r1, 0},
	{_res_r2r1, 0},
	{_res_r3r1, 0},

	{_res_push0b, 0},
	{_res_push0w, 0},
	{_res_push0d, 0},
	{_res_push0q, 0},
	{_res_pop0b, 0},
	{_res_pop0w, 0},
	{_res_pop0d, 0},
	{_res_pop0q, 0},

	{_res_push1br0, 0},
	{_res_push1wr0, 0},
	{_res_push1dr0, 0},
	{_res_push1qr0, 0},

	{_res_push1br1, 0},
	{_res_push1wr1, 0},
	{_res_push1dr1, 0},
	{_res_push1qr1, 0},

	{_res_pop1br0, 0},
	{_res_pop1wr0, 0},
	{_res_pop1dr0, 0},
	{_res_pop1qr0, 0},

	{_res_pop1br1, 0},
	{_res_pop1wr1, 0},
	{_res_pop1dr1, 0},
	{_res_pop1qr1, 0},

	{_res_exit1, 0},
	{_res_exit2r0, 0},
	{_res_exit2r1, 0}
};

_8_u ff_resin_ops(_8_u __op) {
	return rs_ops[__op].l;
}



/*
	as im using asm labels it may cause issues
	ive checked the assembly output and it allocates
	memory for variables on function entry so i dont think
	it should cause any problems if it does re will have to 
	we think how we deal with variables.

	also registers might cause issues with all the jmps 
	i dont know how gcc would react so could break.
	
	using registers r8, r9, ... im hoping that gcc does not
	use them, it seems to be okay for now...
	its so we dont need to use push and pop to make sure the contents
	of the register stay the same and wont cause issues
*/

/*
	a lot of repeating of same code, why? 
	to reduce number of machine instructions executed.
	i dont like it but thats how it is.

	if we did it diffrently it would be slower and speed
	is the most important thing for this.

	TODO:
		error detection
*/

// for debug
#define MAX 0xff
#define fi			__asm__("jmp _res_fi"); // finished
#define next		__asm__("jmp _res_next")
#define end			__asm__("jmp _res_end")
#define jmpto(__p)	__asm__("jmp *%0" : : "r"(__p))
#define errjmp if (_err(err)) jmpend
# include "system/nanosleep.h"
__asm__("\t.globl _res_fi\n"
		"\t.globl _res_end");
/*
	register pointers 
	4 registers 2 pointers
*/
_64_u *res_r0, *res_r1;
_64_u *res_bp, *res_sp;
_8_u *res_stack;
/*
	rename opno to just on for opn or whatever fits

	TODO:
		"allow" for copy of all code so we can append finish at end
*/
_8_u static *ob;
_y_err ff_resin_exec(ffly_resinp __res, _y_err *__exit_code) {
	_y_err err;
	_16_u opno;
	_8_u l;
	_y_err code;

	// not used directly
	// gets put in res_r0 or res_r1
	_64_u *r0 = &__res->r0;
	_64_u *r1 = &__res->r1;
	_64_u *r2 = &__res->r2;
	_64_u *r3 = &__res->r3;
	_64_u *bp = &__res->bp;
	_64_u *sp = &__res->sp;
	_8_u *cflags = &__res->cflags;
	res_stack = __res->stack;
	res_bp = bp;
	res_sp = sp;
	*sp = 100;//ignore only for testing

	_64_u *r;
	_8_u *ip;
	struct f_rs_op *op;
	__asm__("_res_next:\n\t");
	__res->ip_off = 0;
	/*
		TODO:
			put if statment within define so we dont need to compile with it,
			only for debugging to remove unneed shit
	*/
	if (__res->ip>=__res->e) {
		return FFLY_FAILURE;
	}
	if ((opno = *(ip = (__res->code+__res->ip)))>MAX) {
		printf("opno invalid, got: %u\n", opno);
		return FFLY_FAILURE;
	}
	op = rs_ops+opno;
	__res->ip_off = op->l+1;
	ob = ip+1;
/*
	{
		_int_u i;
		i = 0;
		for(;i != ops[opno];i++) {
			printf("%u\n", ob[i]);
		}
	}
*/
	jmpto(op->p);

	__asm__("_res_push0b:			\n\t"
			"movb $1, %0			\n\t"
			"jmp _res_push0			\n"
			"_res_push0w:			\n\t"
			"movb $2, %0            \n\t"
			"jmp _res_push0         \n"
			"_res_push0d:			\n\t"
			"movb $4, %0            \n\t"
			"jmp _res_push0         \n"
			"_res_push0q:			\n\t"
			"movb $8, %0			\n"
			"_res_push0:			\n\t" : "=m"(l));
	{
		_f_addr_t dst, src;
		dst = (*sp = (*sp-l));
		src = *(_f_addr_t*)ob;
		_8_u tmp[8];
		stack_get(__res, (_8_u*)tmp, l, src);
		stack_put(__res, (_8_u*)tmp, l, dst);		
	}
	fi;

	__asm__("_res_pop0b:			\n\t"
			"movb $1, %0			\n\t"
			"jmp _res_pop0 			\n"
			"_res_pop0w:			\n\t"
			"movb $2, %0			\n\t"
			"jmp _res_pop0			\n"
			"_res_pop0d:			\n\t"
			"movb $4, %0			\n\t"
			"jmp _res_pop0			\n"
			"_res_pop0q:			\n\t"
			"movb $8, %0			\n"
			"_res_pop0:				\n\t" : "=m"(l));
	{
		_f_addr_t dst, src;
		dst = *(_f_addr_t*)ob;
		src = *sp;
		*sp+=l;
		_8_u tmp[8];
		stack_get(__res, (_8_u*)tmp, l, src);
		stack_put(__res, (_8_u*)tmp, l, dst);
	}
	fi;

	__asm__("_res_div0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_div0		\n"
			"_res_div0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_div0		\n"
			"_res_div0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_div0		\n"
			"_res_div0q:		\n\t"
			"movb $8, %0		\n"
			"_res_div0:			\n\t" : "=m"(l));
	{
		_64_u lt, rt;
		_f_addr_t lt_adr, rt_adr;
		lt_adr = *(_f_addr_t*)ob;
		rt_adr = *(_f_addr_t*)(ob+__ADDR_TS);

		stack_get(__res, (_8_u*)&lt, l, lt_adr);
		stack_get(__res, (_8_u*)&rt, l, rt_adr);
		_64_u dst;

		_64_u mask = 0xffffffffffffffff>>(64-(l*8));
		lt &= mask;
		rt &= mask;

		dst = rt/lt;
		stack_put(__res, (_8_u*)&dst, l, rt_adr);
	}
	fi;

	__asm__("_res_mul0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mul0		\n"
			"_res_mul0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mul0		\n"
			"_res_mul0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mul0		\n"
			"_res_mul0q:		\n\t"
			"movb $8, %0		\n"
			"_res_mul0:			\n\t" : "=m"(l));
	{
		_64_u lt, rt;
		_f_addr_t lt_adr, rt_adr;
		lt_adr = *(_f_addr_t*)ob;
		rt_adr = *(_f_addr_t*)(ob+__ADDR_TS);

		stack_get(__res, (_8_u*)&lt, l, lt_adr);
		stack_get(__res, (_8_u*)&rt, l, rt_adr);
		_64_u dst;

		_64_u mask = 0xffffffffffffffff>>(64-(l*8));
		lt &= mask;
		rt &= mask;

		dst = rt*lt;
		stack_put(__res, (_8_u*)&dst, l, rt_adr);
	}
	fi;

	__asm__("_res_sub0b:		\n\t"
			"movb $1, %0		\n\t"		
			"jmp _res_sub0		\n"
			"_res_sub0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_sub0		\n"
			"_res_sub0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_sub0		\n"
			"_res_sub0q:		\n\t"
			"movb $8, %0		\n"
			"_res_sub0:			\n\t" : "=m"(l));
	{
		_64_u lt, rt;
		_f_addr_t lt_adr, rt_adr;
		lt_adr = *(_f_addr_t*)ob;
		rt_adr = *(_f_addr_t*)(ob+__ADDR_TS);

		stack_get(__res, (_8_u*)&lt, l, lt_adr);
		stack_get(__res, (_8_u*)&rt, l, rt_adr);
		_64_u dst;

		_64_u mask = 0xffffffffffffffff>>(64-(l*8));
		lt &= mask;
		rt &= mask;

		dst = rt-lt;
		stack_put(__res, (_8_u*)&dst, l, rt_adr);
	}
	fi;

	__asm__("_res_add0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_add0		\n"
			"_res_add0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_add0		\n"
			"_res_add0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_add0		\n"
			"_res_add0q:		\n\t"
			"movb $8, %0		\n"
			"_res_add0:			\n\t" : "=m"(l));
	{
		_64_u lt, rt;
		_f_addr_t lt_adr, rt_adr;
		lt_adr = *(_f_addr_t*)ob;
		rt_adr = *(_f_addr_t*)(ob+__ADDR_TS);

		stack_get(__res, (_8_u*)&lt, l, lt_adr);
		stack_get(__res, (_8_u*)&rt, l, rt_adr);
		_64_u dst;

		_64_u mask = 0xffffffffffffffff>>(64-(l*8));
		lt &= mask;
		rt &= mask;

		dst = rt+lt;
		stack_put(__res, (_8_u*)&dst, l, rt_adr);
//		printf("%u, %u, %u\n", lt, rt, dst);
	}
	fi;

	__asm__("_res_inc0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_inc0		\n"
			"_res_inc0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_inc0		\n"
			"_res_inc0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_inc0		\n"
			"_res_inc0q:		\n\t"
			"movb $8, %0		\n"
			"_res_inc0:			\n\t" : "=m"(l));
	{
		_f_addr_t adr = *(_f_addr_t*)ob;
		_64_u tmp = 0;
		stack_get(__res, (_8_u*)&tmp, l, adr);
		tmp++;
		stack_put(__res, (_8_u*)&tmp, l, adr);
	}
	fi;

	__asm__("_res_dec0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_dec0		\n"
			"_res_dec0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_dec0		\n"
			"_res_dec0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_dec0		\n"
			"_res_dec0q:		\n\t"
			"movb $8, %0		\n"
			"_res_dec0:			\n\t" : "=m"(l));
	{
		_f_addr_t adr = *(_f_addr_t*)ob;
		_64_u tmp = 0;
		stack_get(__res, (_8_u*)&tmp, l, adr);
		tmp--;
		stack_put(__res, (_8_u*)&tmp, l, adr);
	}
	fi;

	__asm__("_res_cmp0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_cmp0		\n"
			"_res_cmp0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_cmp0		\n"
			"_res_cmp0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_cmp0		\n"
			"_res_cmp0q:		\n\t"
			"movb $8, %0		\n"
			"_res_cmp0:			\n\t" : "=m"(l));
	{
		_f_addr_t la, ra;
		la = *(_f_addr_t*)ob;
		ra = *(_f_addr_t*)(ob+__ADDR_TS);

		_64_u lv = 0, rv = 0;
		stack_get(__res, (_8_u*)&lv, l, la);
		stack_get(__res, (_8_u*)&rv, l, ra);

		_8_u flags = 0x00;
		if (lv>rv) flags |= _gt;
		if (lv<rv) flags |= _lt;
		if (lv==rv) flags |= _eq;
		*cflags = flags;
	}
	fi;

	__asm__("_res_as1br1:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_as1r1		\n"
			"_res_as1wr1:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_as1r1		\n"
			"_res_as1dr1:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_as1r1		\n"
			"_res_as1qr1:		\n\t"
			"movb $8, %0		\n"
			"_res_as1r1:		\n\t" : "=m"(l));
	copymem(ob, res_r1, l);
	fi;

	__asm__("_res_as1br0:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_as1r0		\n"
			"_res_as1wr0:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_as1r0		\n"
			"_res_as1dr0:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_as1r0		\n"
			"_res_as1qr0:		\n\t"
			"movb $8, %0		\n"
			"_res_as1r0:		\n\t" : "=m"(l));
	copymem(ob, res_r0, l);
	fi;

	__asm__("_res_as0b:			\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_as0		\n"
			"_res_as0w:			\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_as0		\n"
			"_res_as0d:			\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_as0		\n"
			"_res_as0q:			\n\t"
			"movb $8, %0		\n"
			"_res_as0:			\n\t" : "=m"(l));
	{
		_f_addr_t to;
		to = *(_f_addr_t*)ob;	

		_64_u val;
		copymem(ob+__ADDR_TS, &val, l);
		stack_put(__res, (_8_u*)&val, l, to);
	}
	fi;

	__asm__("_res_cjmp0:\n\t"); {
		_f_addr_t adr = *(_f_addr_t*)ob;
		_16_s dst;
		stack_get(__res, (_8_u*)&dst, sizeof(_16_s), adr);

		_8_u flags;
		
		flags = *cflags;
		
		if (is_flag(flags, _gt)) {
			if (opno == _op_jg)
				goto _end;
		}

		if (is_flag(flags, _lt)) {
			if (opno == _op_jl)
				goto _end;
		}

		if (is_flag(flags, _eq)) {
			if (opno == _op_je)
				goto _end;
		} else {
			if (opno == _op_jne)
				goto _end;
		}

		fi;
	_end:
		__res->ip+=dst;
		__res->ip_off = 0;	
	}
	next;

	__asm__("_res_jmp2:		\n\t");
	__res->ip+=*(_16_s*)ob;
	__res->ip_off = 0;
	fi;

	__asm__("_res_jmp1r1:	\n\t");
	__res->ip+=*(_16_s*)res_r1;
	__res->ip_off = 0;
	fi;
	__asm__("_res_jmp1r0:	\n\t");
	__res->ip+=*(_16_s*)res_r0;
	__res->ip_off = 0;
	fi;

	__asm__("_res_jmp8:		\n\t"
			"movb $1, %0	\n\t"
			"jmp _res_jmp0	\n\t"
			"_res_jmp16:	\n\t"
			"movb $2, %0	" : "=m"(l));
	__asm__("_res_jmp0:	\n\t");
	{
		_f_addr_t adr = *(_f_addr_t*)ob;
		_16_s dst;
		stack_get(__res, (_8_u*)&dst, sizeof(_16_s), adr);
//		printf("jump dis: %d, dst: %u\n", dst, __res->get_ip()+dst);
		__res->ip+=dst;
		__res->ip_off = 0;
	}
	next;

	__asm__("_res_ld0b:			\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_ld0		\n"
			"_res_ld0w:			\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_ld0		\n"
			"_res_ld0d:			\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_ld0		\n"
			"_res_ld0q:			\n\t"
			"movb $8, %0		\n"
			"_res_ld0:			\n\t" : "=m"(l));
	{
		_f_addr_t dst = *(_f_addr_t*)ob;
		_f_addr_t src = *(_f_addr_t*)(ob+__ADDR_TS);
		_f_addr_t adr;
		stack_get(__res, (_8_u*)&adr, sizeof(_f_addr_t), dst);

		void *p = ff_resin_resolv_adr(__res, adr);
		if (p != NULL)
			stack_get(__res, (_8_u*)p, l, src);
	}
	fi;

	__asm__("_res_st0b:			\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_st0		\n"
			"_res_st0w:			\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_st0		\n"
			"_res_st0d:			\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_st0		\n"
			"_res_st0q:			\n\t"
			"movb $8, %0		\n"
			"_res_st0:			\n\t" : "=m"(l));
	{
		_f_addr_t src = *(_f_addr_t*)ob;
		_f_addr_t dst = *(_f_addr_t*)(ob+__ADDR_TS);
		_f_addr_t adr;
		stack_get(__res, (_8_u*)&adr, sizeof(_f_addr_t), src);

		void *p = ff_resin_resolv_adr(__res, adr);
		if (p != NULL)
			stack_put(__res, (_8_u*)p, l, dst);
	}
	fi;	

	/*
		memory to reg
	*/
	__asm__("_res_mov1br1:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mov1r1	\n"
			"_res_mov1wr1:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mov1r1	\n"
			"_res_mov1dr1:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mov1r1	\n"
			"_res_mov1qr1:		\n\t"
			"movb $8, %0		\n"
			"_res_mov1r1:		\n\t"
			"movq %3, %%r8		\n\t"
			"movq %%r8, %1		\n\t"
			"jmp _res_mov1		\n"

			"_res_mov1br0:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mov1r0	\n"
			"_res_mov1wr0:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mov1r0	\n"
			"_res_mov1dr0:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mov1r0	\n"
			"_res_mov1qr0:		\n\t"
			"movb $8, %0		\n"
			"_res_mov1r0:		\n\t"
			"movq %2, %%r8		\n\t"
			"movq %%r8, %1		\n"
			"_res_mov1:			\n\t" : "=m"(l), "=m"(r) : "m"(res_r0), "m"(res_r1));
	{
		_f_addr_t s = *(_f_addr_t*)ob;
		stack_get(__res, (_8_u*)r, l, s);
	}
	fi;

	/*
		reg to memory
	*/
	__asm__("_res_mov2br1:      \n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mov2r1    \n"
			"_res_mov2wr1:      \n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mov2r1    \n"
			"_res_mov2dr1:      \n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mov2r1    \n"
			"_res_mov2qr1:      \n\t"
			"movb $8, %0		\n"
			"_res_mov2r1:       \n\t"
			"movq %3, %%r8		\n\t"
			"movq %%r8, %1		\n\t"
			"jmp _res_mov2		\n"

			"_res_mov2br0:      \n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mov2r0	\n"
			"_res_mov2wr0:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mov2r0	\n"
			"_res_mov2dr0:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mov2r0	\n"
			"_res_mov2qr0:		\n\t"
			"movb $8, %0		\n"
			"_res_mov2r0:		\n\t"
			"movq %2, %%r8		\n\t"
			"movq %%r8, %1		\n"
			"_res_mov2:				\n\t" : "=m"(l), "=m"(r) : "m"(res_r0), "m"(res_r1));
	{
		_f_addr_t d = *(_f_addr_t*)ob;
		stack_put(__res, (_8_u*)r, l, d);
	}
	fi;

	__asm__("_res_mov0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_mov0		\n"
			"_res_mov0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_mov0		\n"
			"_res_mov0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_mov0		\n"
			"_res_mov0q:		\n\t"
			"movb $8, %0		\n"
			"_res_mov0:			\n\t" : "=m"(l));
	{
		_f_addr_t src = *(_f_addr_t*)ob;
		_f_addr_t dst = *(_f_addr_t*)(ob+__ADDR_TS);

		_8_u tmp[8];
		stack_get(__res, tmp, l, src);
		stack_put(__res, tmp, l, dst);
	}
	fi;
	
	__asm__("_res_rin0:\n\t");
	{
		_8_u no = *(_8_u*)ob;
		_f_addr_t adr = *(_f_addr_t*)(ob+1);
		__res->rin(no, ff_resin_resolv_adr(__res, adr));
	}
	fi;

	__asm__("_res_out1br1:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_out1r1	\n"
			"_res_out1wr1:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_out1r1	\n"
			"_res_out1dr1:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_out1r1	\n"
			"_res_out1qr1:		\n\t"
			"movb $8, %0		\n"
			"_res_out1r1:		\n\t"
			"movq %3, %%r8		\n\t"
			"movq %%r8, %1		\n\t"
			"jmp _res_out1		\n"

			"_res_out1br0:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_out1r0	\n"
			"_res_out1wr0:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_out1r0	\n"
			"_res_out1dr0:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_out1r0	\n"
			"_res_out1qr0:      \n\t"
			"movb $8, %0		\n"
			"_res_out1r0:		\n\t"
			"movq %2, %%r8		\n\t"
			"movq %%r8, %1		\n\t"
			"_res_out1:			\n\t": "=m"(l), "=m"(r) : "m"(res_r0), "m"(res_r1));
	printf("out: %u\n", *r&(0xffffffffffffffff>>(64-(l*8))));
	fi;

	__asm__("_res_out0b:		\n\t"
			"movb $1, %0		\n\t"
			"jmp _res_out0		\n"
			"_res_out0w:		\n\t"
			"movb $2, %0		\n\t"
			"jmp _res_out0		\n"
			"_res_out0d:		\n\t"
			"movb $4, %0		\n\t"
			"jmp _res_out0		\n"
			"_res_out0q:		\n\t"
			"movb $8, %0		\n"
			"_res_out0:			\n\t" : "=m"(l));
	{
		_f_addr_t addr = *(_f_addr_t*)ob;
		_64_u val = 0;
		stack_get(__res, (_8_u*)&val, l, addr);
		printf("out: %u\n", val);
	}
	fi;

	__asm__("_res_r3r1:		\n\t"
			"movq %5, %%r8	\n\t"
			"movq %%r8, %1	\n\t"
			"jmp _res_fi	\n"

			"_res_r2r1:		\n\t"
			"movq %4, %%r8	\n\t"
			"movq %%r8, %1	\n\t"
			"jmp _res_fi	\n"

			"_res_r1r1:		\n\t"
			"movq %3, %%r8	\n\t"
			"movq %%r8, %1	\n\t"
			"jmp _res_fi	\n"

			"_res_r0r1:		\n\t"
			"movq %2, %%r8	\n\t"
			"movq %%r8, %1	\n\t"
			"jmp _res_fi	\n"

			"_res_r3r0:	\n\t"
			"movq %5, %%r8	\n\t"
			"movq %%r8, %0	\n\t"
			"jmp _res_fi	\n"

			"_res_r2r0:	\n\t"
			"movq %4, %%r8	\n\t"
			"movq %%r8, %0	\n\t"
			"jmp _res_fi	\n"

			"_res_r1r0:	\n\t"
			"movq %3, %%r8	\n\t"
			"movq %%r8, %0	\n\t"
			"jmp _res_fi	\n"

			"_res_r0r0:		\n\t"
			"movq %2, %%r8	\n\t"
			"movq %%r8, %0	\n\t"
			"jmp _res_fi	\n" : "=m"(res_r0), "=m"(res_r1) : "m"(r0), "m"(r1), "m"(r2), "m"(r3));

	__asm__("_res_call0:\n\t");
	{
		_f_addr_t adr = *(_f_addr_t*)ob;
		_16_s dst;
		stack_get(__res, (_8_u*)&dst, sizeof(_f_addr_t), adr);
		*(__res->retto++) = __res->ip+__res->ip_off;
		__res->ip+=dst;
		__res->ip_off = 0;
	}
	next;

	__asm__("_res_ret:\n\t");
	{
		__res->ip = *(--__res->retto);
		__res->ip_off = 0;
	}
	next;

	/*
		exit and use rp?? as the code
	*/
	__asm__("_res_exit2r1:		\n\t"
			"movq %1, %%r8		\n\t"
			"movb (%%r8), %%r8b	\n\t"
			"movb %%r8b, %0		\n\t"
			"jmp _res_end" : "=m"(code) : "m"(res_r1));
	__asm__("_res_exit2r0:		\n\t"
			"movq %1, %%r8		\n\t"
			"movb (%%r8), %%r8b	\n\t" 
			"movb %%r8b, %0		\n\t"
			"jmp _res_end" : "=m"(code) : "m"(res_r0));

	__asm__("_res_exit1:\n\t");
	copymem(ob, &code, 1);
	end;

	__asm__("_res_exit0:\n\t");
	{
		_f_addr_t adr = *(_f_addr_t*)ob;
		stack_get(__res, (_8_u*)&code, sizeof(_y_err), adr);
	}
	end;
	__asm__("_res_fi:\n\t");
	__res->ip+=__res->ip_off;
	next;

	__asm__("_res_end:\n\t");
	printf("goodbye.\n");
	if (__exit_code != NULL)
		*__exit_code = code;
}
