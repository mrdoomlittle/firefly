#ifndef __afac__h
#define __afac__h
#include "../y_int.h"
#include "../hungbuf.h"
#include "../rws.h"
#include "../lib/hash.h"
/*
	tool for importing models in wavefront format(.obj)
*/

/*
	if we have a linear span of vectors they we want to access them as so.
	- this means no creating a pointer list or to store each element as a pointer to said element in linear span.


	NOTE:
		we are going a route that would describe the layout of memory, then are job is to pick
		the things we have been directed to do so.

	NOTE:
		not only does this have to support importing from a modeling program but also a game engine.

	DESIGN-NOTE:
		N-Gons are stupid as they convey no geometric infomation, this applys to quads as well.
		a triangle is the only shape that convey that infomation, as a triangle defines the surface with no discrepancies.
		two vertices define an axis while the other conveys displacement from that axis.

		what im getting at is a triangle no matter the order defines a surface thats expected while N-gones dont.

		we work in N-triangles not N-vertices.

		the underlying make make use of N-GONES for compression how ever that will get translated to triangles
		as to the convey geometric infomation.
*/

//asset facility
#include "../maths/vec3.h"

struct afac_vec3 {
  double x,y,z;
};

struct afac_blob {
	struct _y_vec3 *v;
	_int_u len;
};

typedef struct {
	_int_u pos;//vertex pos
	_int_u norm;//vertex normal
	_int_u uv;//vertex UV mapping
}afac_vert_indx;

typedef struct {
	_16_u verts[3];
}afac_tri_indx;




//composition
#define MAX_FACE_VERTS 8
#define MAX_FACE_COMPRISE 8
/*
	may not reflect real size
*/
struct afac_face{
	afac_vert_indx v[MAX_FACE_VERTS];
	/*
		only here because i dont want to go into the hassle of sorting the verts in to some order,
		i like to do, load in what ever order we have the tell the user the layout.
	*/
	afac_tri_indx tris_comprised[MAX_FACE_COMPRISE];
	_int_u n;//N-tris
	_16_u bits;
};

struct afac_wf_face {
	_64_u v_idx[4];
	_64_u vn_idx[4];
	_int_u n;
};

/*
	NOTE:
		a object comprises of a mesh,
		there could be three objects 
		where each one corresponds to a vertex attribute.

		this is possable, WHY? because a mesh is just a structure with infomation attached.
		
		you could have a mesh that only comprises of normals,
		or a mesh that only comprises of position vectors or a combination of both(a mesh that has position and normals)
		what im getting at is like, you could have a tree with red leafs and a tree with blue leafs or a tree with blue and red leafs(same consept).

		OBJECT-0 = position
		OBJECT-1 = normals
*/
#define AF_OBJ_HAS_POS		1
#define AF_OBJ_HAS_UV			2
#define AF_OBJ_HAS_NORM		4
/*
	a selection of faces that contribute to a part of the object

	NOTE:
		the big diffrence and the main distinguisher between a group
		and a object is that a object is detached from other objects
		while as groups may share vertices with other groups.

	NOTE:
		groups could also be detached as a object may consist of two bodys of unconnected faces.


	TODO:
		make so that groups can be plucked from file without reloading everything.
*/

struct afac_prem{
	struct afac_vec3 near,far;
};
struct afac_group{
	char name[64];
	_int_u n_faces;
	struct y_hungbuf faces;
	_int_u tot;//number of vertcies
	struct afac_group *next;
	void *priv;
	struct afac_prem prem;
};

struct afac_object{	
	struct f_lhash _groups;
	char name[64];
	struct afac_group *groups;
	struct afac_group *curgroup;
	/*
		flags are not included at a face level,
		if a face one off dose not have a uv or normal we assume error.
	*/
	_16_u flags;
	_32_u tot;
	struct afac_object *next;
};

/*
	when reading data of faces the user may want to do some inspections
*/
//#define AF_USER_INVOLVE 1
struct afac{
	/*
		always exists
	*/
	struct y_hungbuf vecs;
	
	//these are attributes or accessories to the mesh
	//could or could not exist

	struct y_hungbuf norms;
	struct y_hungbuf uv;
	
	_int_u n;	
	struct afac_object *obj;
};

/*
	NOTE: we assume for now all vectors,normals
	for every object are congregated into a 
	single place in memory.
*/
struct afac_wf {
	_int_u vdis;
	/*
		list of all vectors
	*/
	struct afac_blob *v;
	_int_u n_v;

	/*
		list of normals
	*/
	struct afac_blob *vn;
	_int_u n_vn;

	/*
		the faces 
	*/
	struct afac_wf_face *vf;
	_int_u n_vf;
};
#define AFAC_OBJDUMP_ONLY_OBJECTS_AND_GROUPS 1
void afac_objdump(struct afac *__af,struct afac_object *__obj,_64_u);
void afac_load(struct afac *__af, struct rws_schematic *sch);
void afac_wf_export(struct afac_wf *__a, char const *__file);
#endif/*__afac__h*/
