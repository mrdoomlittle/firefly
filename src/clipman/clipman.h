#ifndef __clipman__h
#define __clipman__h
#include "../y_int.h"
#define CM_SET 0
#define CM_GET 1
struct cm_msg {
	_int_u type;
	_int_u size;
};
void cm_connect(void);
void cm_set(_8_u *__buf,_int_u __size);
void cm_get(_8_u *__buf,_int_u *__size);
void cm_disconnect(void);

#endif/*__clipman__h*/
