rm -f *.o hw/*.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../../../ && . ./compile.sh && cd flue/test/shader
ffly_objs="$ffly_objs"
gcc $cc_flags -o fsh fsh.c $ffly_objs -nostdlib

