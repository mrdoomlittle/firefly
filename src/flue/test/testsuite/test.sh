#!/bin/bash
results="NONE"
test () {
	input="$(cat $1.input)"
	results=$(../../amd/emu/a.out $1.s $input)
	desired="$(cat $1.desired)"
	if [ "$results" == "$desired" ]; then
		echo "[$1.fsl] success"
	else
		echo "[$1.fsl] failure wanted {$desired} got {$results}"
	fi
}

sh compile.sh
export EMU_MODE="real"
test ts0
test ts1
test ts2
test ts3
