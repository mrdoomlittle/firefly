#include "common.h"
char const static *nfmt[] = {
	"FLOAT",
	"UINT",
	"SRGB",
	"UNORM"
};
char const static *dfmt[] = {
	"32_32_32_32",
	"8_8_8_8",
	"32"
};


char const* flue_dfmtstr(void *__fmt, _64_u __what) {
	struct flue_fmtblock *b = ((_64_u*)__fmt)-(1+__what);
	return dfmt[b->info];
}
char const* flue_nfmtstr(void *__fmt, _64_u __what) {
	struct flue_fmtblock *b = ((_64_u*)__fmt)-(1+__what);
	return nfmt[b->info];
}
