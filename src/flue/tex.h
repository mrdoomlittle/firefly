# ifndef __flue__tex
# define __flue__tex
# include "../y_int.h"
#include "../struc.h"
/*
	texture is memory mapped
*/
#define FLUE_TEX_MMAP 0x01
#define FLUE_TEX_BLEND_EN	1
/*
	take note that the users private data needs to include his_tex

	NOTE: ptr points to the renderable
	in the case of a texture it points to itself.
	this is only used in the case of getting the real texture.
*/
#define FLUE_BACK 0
#define FLUE_FRONT 1
struct flue_tex;
struct flue_framebuffer{
	struct y_his_tex tx;
	void *ptr;

	union{
		struct{
		_16_u xback;
		_16_u xfront;
		};
		_32_u back_front;
	};
	struct flue_tex *attachments[2];
};

typedef struct flue_tex {
	struct y_his_tex tx;
	void *ptr;
	
	_32_u x, y;
	_32_u width, height;
	_32_u totsz;
	/*
		wheres the texture comming from???? its source file/memory????
	*/
	_8_u bits;
	_64_u config;
	struct flue_fmtdesc fmt;
	void *mmap;
	struct flue_teximage *img;
} *flue_texp;

flue_texp flue_tex_new(_32_u,_32_u,struct flue_fmtdesc*,_64_u);
void flue_tex_relinquish(flue_texp);
void flue_tex_destroy(flue_texp);
void flue_texload(struct flue_tex*);

# endif /*__flue__tex*/
