#include "ima.h"
#include "../../../assert.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"
#include "radeon.h"
/*
	PLEASE NOTE:

	ys can be seen as the final produce but just in a text format to make debug and testing more lenient.
*/
//#define REG(__r) ima_regmap[ima_gred[__r>>4]+(__r&0xf)]
#include "r_struc.h"

struct instrdata{
	char const *ident;
};

#define _rcp32f							0
#define _mulu16							1
#define _add32u							2
#define _mov32v							3
#define _add32f							4
#define _sub32f							5
#define _mul32f							6
#define _mac32f							7
#define _cvt_i32f32					8
#define _cvt_pkrtz_f16_f32	9
#define _rsq32f							10
#define _max32f							11
#define _min32f							12
#define _mov32s							13

static struct instrdata ysdata[] = {
	{//rcp32f
		.ident = "rcp32f"
	},
	{//mulu16
		.ident = "mul16ulo"
	},
	{//add32u
		.ident = "add32u"
	},
	{//mov32v
		.ident = "mov32v"
	},
	{//add32f
		.ident = "add32f"
	},
	{//sub32f
		.ident = "sub32f"
	},
	{//mul32f
		.ident = "mul32f"
	},
	{//mac32f
		.ident = "mac32f"
	},
	{//cvt_i32f32
		.ident = "cvt_i32f32"
	},
	{//cvt_pkrtz_f16_f32
		.ident = "cvt_pkrtz_f16_f32"
	},
	{//rsq32f
		.ident = "rsq32f"
	},
	{//max32f
		.ident = "max32f"
	},
	{//min32f
		.ident = "min32f"
	},
	{//mov32s
		.ident = "mov32s"
	}
};

_16_u static instr_personal[] = {
  _rcp32f,//_r_op_rcp32f
  _mulu16,//_r_op_mulu16
  _add32u,//_r_op_add32u
  _mov32v,//_r_op_mov
  _add32f,//_r_op_fadd
  _sub32f,//_r_op_fsub
  _mul32f,//_r_op_fmul
  _mac32f,//_r_op_fmac
  _cvt_i32f32,//_r_op_cvt_i32f32
  _cvt_pkrtz_f16_f32,//_r_op_cvt_pkrtz_f16_f32
  _rsq32f,//_r_op_rsq_f32
  _max32f,//_r_op_max32f
  _min32f,//_r_op_min32f
  -1,//_r_op_imgstore
  -1,//_r_op_imgsample
  -1,//_r_op_imgload
  -1,//_r_op_sldw
  -1,//_r_op_endprog
  -1,//_r_op_waitcnt
  -1,//_r_op_load16
	-1,//_r_op_load
	-1,//_r_op_store
	0,//_r_op_interp
	1,
	_mov32s,//_r_op_mov_s
};
static char const *expl_xfmt[] = {
	"32f4",
	"32u1"
};

static char const *expl_yfmt[] = {
	"xyzw",
	"x"
};

static _16_u xfmt[] = {
	0,
	0,
	1,
	1
};
static _16_u yfmt[] = {
	0,
	1,
	0,
	1
};

void static emitwait(void) {
	char buf[128];
	char *bp = buf;
	_int_u l = str_cpy(bp,"waitcnt 0\n");
	IHC_write(&ima_pd->file,buf,l);
}
void static emitsmem(char const *__name, char const *__dwc, _32_u __offset, _64_u __base, _64_u __data) {
	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,__name);
	bp+=str_cpy(bp,__dwc);
	*(bp++) = ' ';
	bp+=_ffly_nds(bp,__offset);
	*(bp++) = ',';
	bp+=str_cpy(bp,"%s#");
	bp+=_ffly_nds(bp,__data);
	*(bp++) = ',';
	bp+=str_cpy(bp,"%s#");
	bp+=_ffly_nds(bp,__base);
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
	emitwait();
}
/*
	NOTE:	
	SBASE is in pairs of 2
	meaning

	SGPR(0)+SGPR(1) = %sgpr#0
	SGPR(2)+SGPR(3) = %sgpr#1
	SGPR(4)+SGPR(5) = %sgpr#2
	SGPR(6)+SGPR(7) = %sgpr#3
*/
void static _r_instr_sldw(struct r_instr *__r){
	_16_u offset;
	offset = __r->r_reg[0];

	_16_u r0,r1;
	r0 = R_REG_GET(__r->r_reg[1]);
	r1 = R_REG_GET(__r->r_reg[2]);


	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,"s_ldw,4 ");
	bp+=_ffly_nds(bp,offset);
	bp+=str_cpy(bp,",%s#");
	bp+=_ffly_nds(bp,r0);
	bp+=str_cpy(bp,",%s#");
	bp+=_ffly_nds(bp,r1>>1);
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
	emitwait();
}

void static 
emitmtbuf(char const *__name, char const *__xyzw, char const *__fmt, _64_u __offset, _64_u __v0, _64_u __desc, _64_u __bits) {
	char buf[128];
	char *bp = buf;

	bp+=str_cpy(bp,__name);
	*(bp++) = ',';
	bp+=str_cpy(bp,__xyzw);
	*(bp++) = '\x27';
	bp+=str_cpy(bp,__fmt);
	*(bp++) = ' ';
	bp+=_ffly_nds(bp,__offset);
	*(bp++) = ',';
	if (__bits&IMA_PRIM) {
		bp+=str_cpy(bp,"%v#2'idx");
	}else
	if (__bits&IMA_INTR_K) {
		bp+=str_cpy(bp,"%v#0'idx");
	}else{
		bp+=str_cpy(bp,"%v#0");
	}
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,R_REG_GET(__v0));
	*(bp++) = ',';
	bp+=str_cpy(bp,"%s#");
	bp+=_ffly_nds(bp,__desc>>2);
	*(bp++) = ',';
	*(bp++) = '0';
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
	emitwait();
}

void static
emitmubuf(_64_u __offset, _64_u __v0, _64_u __desc) {
	char buf[128];
	char *bp = buf;
// this will have to be delt with later
//	bp+=str_cpy(bp,"s_ldw,4 0,%s#12,%s#2\nwaitcnt 0\n");
	bp+=str_cpy(bp,"u_ldw,4 ");

	bp+=_ffly_nds(bp,__offset);
	*(bp++) = ',';
	*(bp++) = '0';
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,R_REG_GET(__v0));
	*(bp++) = ',';
	bp+=str_cpy(bp,"%s#");
	bp+=_ffly_nds(bp,__desc>>2);
	*(bp++) = ',';
	*(bp++) = '0';
	*bp = '\n';

	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
	emitwait();
}
#define image_load		"image_load "
#define image_store		"image_store "
#define image_sample	"image_sample "
void static
emitimg(char const *__type, _64_u __v0, _64_u __v1, _64_u __desc,_64_u __sampler) {
//	fetch_decriptor_from_tabe(__desc);
	char buf[128];
	char *bp = buf;

	bp+=str_cpy(bp,__type);
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,__v1);
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,R_REG_GET(__v0));
	*(bp++) = ',';
	//source SGPR
//	bp+=str_cpy(bp,"%s#");

	bp+=str_cpy(bp,"%s#");
	bp+=_ffly_nds(bp,__desc>>2);
	//sampler
	bp+=str_cpy(bp,",%s#");
	bp+=_ffly_nds(bp,__sampler>>2);
	bp[0] = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
	emitwait();
}

char static* read_float(char *bp,_64_u _v){
	double v = *(double*)&_v;
	if (v<0) {
		*(bp++) = '-';
		v = -v;
	}

	if (!v) {
		bp+=str_cpy(bp,"0.0");
	}else{
		if (v<1.0) {
			bp+=str_cpy(bp,"0");
		}
		bp+=ffly_floatts(v,bp);
	}
	return bp;
}


void static
_r_instr_vop2(struct r_instr *__i){
	struct instrdata *d;
	d = ysdata+instr_personal[__i->op];

	IHC_write(&ima_pd->file,";VOP2\n",6);
	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,d->ident);
	*(bp++) = ' ';
	
	_16_u rtyp = __i->r_info[0];
	if(rtyp == _R_TYP_PHYREG) {
		_16_u reg = __i->r_reg[0];
		_16_u info = R_REG_GSTMP(reg);
		reg = R_REG_GET(reg);
		str_cpy(bp,"%v#");
		if(info&_R_SGPR) {
			bp[1] = 's';
		}

		bp+=3;
		bp+=_ffly_nds(bp,reg);
	}else
	if(rtyp == _R_TYP_FLOAT) {
		bp = read_float(bp,__i->r_reg[0]);
	}else
	if(rtyp == _R_TYP_IMM) {
		bp+=_ffly_nds(bp,__i->r_reg[0]);
	}else
	if(rtyp == _R_TYP_STR) {
		bp+=str_cpy(bp,__i->r_reg[0]);
	}else{
		/*error*/
		assert(1 == 0);
	}

	_16_u r0,r1;
	r0 = __i->r_reg[1];
	r1 = __i->r_reg[2];
	_16_u cinfo;
	cinfo = R_REG_GSTMP(r0|r1);
	r0 = R_REG_GET(r0);
	r1 = R_REG_GET(r1);
	assert(cinfo == _R_VGPR);
	str_cpy(bp,",%v#");
	bp+=4;
	bp+=_ffly_nds(bp,r0);
	str_cpy(bp,",%v#");
	bp+=4;
	bp+=_ffly_nds(bp,r1);

	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
}

void static
_r_instr_vop1(struct r_instr *__i){
	struct instrdata *d;
	d = ysdata+instr_personal[__i->op];
	IHC_write(&ima_pd->file,";VOP1\n",6);
	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,d->ident);
	*(bp++) = ' ';
	str_cpy(bp,"%v#");
	if(R_REG_GSTMP(__i->r_reg[0]) == _R_M){
		bp[1] = 'm';
	}
	bp+=3;
	bp+=_ffly_nds(bp,R_REG_GET(__i->r_reg[0]));
	*(bp++) = ',';
	
	_16_u rtyp = __i->r_info[1];
	if(rtyp == _R_TYP_PHYREG) {
		_16_u reg = __i->r_reg[1];
		_16_u info = R_REG_GSTMP(reg);
		reg = R_REG_GET(reg);
		str_cpy(bp,"%v#");
		if(info&_R_M){
			bp[1] = 'm';
		}
		if(info&_R_SGPR) {
			bp[1] = 's';
		}
		bp+=3;
		bp+=_ffly_nds(bp,reg);
	}else
	if(rtyp == _R_TYP_FLOAT) {
		bp = read_float(bp,__i->r_reg[1]);
	}else
	if(rtyp == _R_TYP_IMM) {
		bp+=_ffly_nds(bp,__i->r_reg[1]);
	}else
	if(rtyp == _R_TYP_STR) {
		bp+=str_cpy(bp,__i->r_reg[1]);
	}else{
		assert(1 == 0);
	}
	
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
}

void static
_r_instr_vop3a(struct r_instr *__i) {
	struct instrdata *d;
	d = ysdata+instr_personal[__i->op];

	_16_u r0,r1,r2;
	r0 = __i->r_reg[0];
	r1 = __i->r_reg[1];
	r2 = __i->r_reg[2];
	_16_u
	info = R_REG_GSTMP(r0|r1|r2);
	assert(info == _R_VGPR);
	r0 = R_REG_GET(r0);
	r1 = R_REG_GET(r1);
	r2 = R_REG_GET(r2);

	IHC_write(&ima_pd->file,";VOP3A\n",7);
	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,d->ident);
	*(bp++) = ' ';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,r0);
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,r1);
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,r2);
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
}

void static
_r_instr_image_store(struct r_instr *__i) {
	emitimg(image_store,__i->r_reg[0],__i->r_reg[1],__i->r_reg[2],0);
}

void static
_r_instr_image_sample(struct r_instr *__i) {
	emitimg(image_sample,__i->r_reg[0],__i->r_reg[1],__i->r_reg[2],__i->r_reg[3]);
}

void static
_r_instr_image_load(struct r_instr *__i) {
	emitimg(image_load,__i->r_reg[0],__i->r_reg[1],__i->r_reg[2],0);
}
#define getfsy(__fmt) expl_yfmt[yfmt[__fmt]]
#define getfsx(__fmt) expl_xfmt[xfmt[__fmt]]


void static
_r_instr_load(struct r_instr *__i){
	if(!__i->sub){
		emitmubuf(__i->offset,__i->r_reg[0],__i->desc);
	}else{
		emitmtbuf("load",getfsy(__i->fmt),getfsx(__i->fmt),__i->offset,__i->r_reg[0],__i->desc,__i->bits);
	}
}

void static
_r_instr_store(struct r_instr *__i) {
	IHC_write(&ima_pd->file,";STORE\n",7);
	emitmtbuf("store",getfsy(__i->fmt),getfsx(__i->fmt),__i->offset,__i->r_reg[0],__i->desc,__i->bits);
}

void static
_r_instr_const_load(struct r_instr *__i) {
	emitsmem("s_l","dw",__i->offset,0/*const location*/,__i->r_reg[0]);
}

void static
_r_instr_const_store(struct r_instr *__i) {
	emitsmem("s_s","dw",__i->offset,0/*const location*/,__i->r_reg[0]);
}


void static
_r_instr_sload(struct r_instr *__i) {
	emitsmem("s_l","dw",__i->offset,__i->r_reg[1],__i->r_reg[0]);
}
void static
_r_instr_sstore(struct r_instr *__i) {
	emitsmem("s_s","dw",__i->offset,__i->r_reg[1],__i->r_reg[0]);
}

#define intrp_p1f32 "intrp_p1f32'"
#define intrp_p2f32 "intrp_p2f32'"
void static emitinterp(char const *__name, char __comp, _64_u __atb, _64_u __v0, _64_u __v1) {
	char buf[128];
	char *bp = buf;
	bp+=str_cpy(bp,__name);
	*(bp++) = __comp;
	bp+=_ffly_nds(bp,__atb);
	bp+=str_cpy(bp," %v#");
	bp+=_ffly_nds(bp,R_REG_GET(__v0));
	*(bp++) = ',';
	bp+=str_cpy(bp,"%v#");
	bp+=_ffly_nds(bp,R_REG_GET(__v1));
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);
}

static char const *interp_name[] = {intrp_p1f32,intrp_p2f32};
static char component[] = {'x','y','z','w'};
void static
_r_instr_interp(struct r_instr *__i){
	emitinterp(interp_name[instr_personal[__i->op]],component[__i->comp],__i->atb,__i->r_reg[0],__i->r_reg[1]);
}

void static
emitexp(_int_u __dst, _64_u *__v, _64_u __info) {
	char buf[128];
	char *bp;
	bp = buf;
	_int_u rtyp;
	rtyp = R_REG_GSTMP(__dst);
	__dst = R_REG_GET(__dst);
	bp+=str_cpy(bp,"exp");
	*bp++ = __info&1?'w':'d';
	switch(rtyp) {
		case _R_POS:
			bp+=str_cpy(bp," %pos#");
		break;
		case _R_MRT:
			bp+=str_cpy(bp," %mrt#");
		break;
		case _R_PAR:	
			bp+=str_cpy(bp," %par#");
			__info &= ~(32);
		break;
		default:	
			printf("defaulted: %u.\n",rtyp);
			assert(1 == 0);
	}

	bp+=_ffly_nds(bp,__dst);
	*(bp++) = ',';

	if (__info&2) {
		bp+=str_cpy(bp,"%v#");
		bp+=_ffly_nds(bp,__v[0]);
		*(bp++) = ',';
	} else {
		bp+=str_cpy(bp,"!,");
	}

	if (__info&4) {
		bp+=str_cpy(bp,"%v#");
		bp+=_ffly_nds(bp,__v[1]);
		*(bp++) = ',';
	}else {
		bp+=str_cpy(bp,"!,");
	}

	if (__info&8) {
		bp+=str_cpy(bp,"%v#");
		bp+=_ffly_nds(bp,__v[2]);
		*(bp++) = ',';
	} else {
		bp+=str_cpy(bp,"!,");
	}

	if (__info&16) {
		bp+=str_cpy(bp,"%v#");
		bp+=_ffly_nds(bp,__v[3]);
	} else{
		*(bp++) = '!';
	}
	if (__info&32)
		*(bp++) = '`';
	*bp = '\n';
	IHC_write(&ima_pd->file,buf,(bp-buf)+1);

/*
	NOTE:
		i dont like all the waiting but unfortunately we are in the testing stages,
		so as bad as it is im not wasting time on somthing that only matters when 
		rendering large counts.
*/
	emitwait();
}
void static
_r_instr_export(struct r_instr *__i){
	emitexp(__i->r_reg[0],__i->r_reg+1,__i->info);
}

void static
_r_endprog(struct r_instr *__i){
	IHC_write(&ima_pd->file,"end 0\n",6);
}
#define __r_instr_vop1 0
#define __r_instr_vop2 1
#define __r_instr_image_store 2
#define __r_instr_image_sample 3
#define __r_instr_image_load 4
#define __r_instr_sldw 5
#define __r_endprog 6
#define __r_instr_load 7
#define __r_instr_store 8
#define __r_instr_interp 9
#define __r_instr_vop3a 10
#define __r_instr_export 11
struct r_producer r_ys = {
	.ft = {
	_r_instr_vop1,
	_r_instr_vop2,
	_r_instr_image_store,
	_r_instr_image_sample,
	_r_instr_image_load,
	_r_instr_sldw,
	_r_endprog,
	_r_instr_load,
	_r_instr_store,
	_r_instr_interp,
	_r_instr_vop3a,
	_r_instr_export
	},
	.selt = {
	__r_instr_vop1,//_r_op_rcp32f
	__r_instr_vop2,//_r_op_mulu16
	__r_instr_vop2,//_r_op_add32u
	__r_instr_vop1,//_r_op_mov
	__r_instr_vop2,//_r_op_fadd
	__r_instr_vop2,//_r_op_fsub
	__r_instr_vop2,//_r_op_fmul
	__r_instr_vop2,//_r_op_fmac
	__r_instr_vop1,//_r_op_cvt_i32f32
	__r_instr_vop3a,//_r_op_cvt_pkrtz_f16_f32
	__r_instr_vop1,//_r_op_rsq_f32
	__r_instr_vop2,//_r_op_max32f
	__r_instr_vop2,//_r_op_min32f
	__r_instr_image_store,//_r_op_imgstore
	__r_instr_image_sample,//_r_op_imgsample
	__r_instr_image_load,//_r_op_imgload
	__r_instr_sldw,//_r_op_sldw
	__r_endprog,//_r_op_endprog
	-1,//_r_op_waitcnt
	-1,//_r_op_load16
	__r_instr_load,//_r_op_load
	__r_instr_store,//_r_op_store
	__r_instr_interp,//_r_op_intrp_p1f32
	__r_instr_interp,//_r_op_intrp_p2f32
	__r_instr_vop1,//_r_op_mov_s
	__r_instr_export,//_r_op_export
	-1,//
	-1,//
	-1,//
	}
};

