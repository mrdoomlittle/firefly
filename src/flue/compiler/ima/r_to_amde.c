#include "ima.h"
#include "../../../assert.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"
#include "radeon.h"
#include "../../amd/emu/emu.h"
#include "r_struc.h"


#define R0(__x) (tab[__x]<<8)
#define R1(__x) (tab[__x]<<12)
#define R2(__x) (tab[__x]<<16)
#define R3(__x) (tab[__x]<<20)

_32_u static tab[] = {
	AMDE_IMM,
	AMDE_IMM,
	-1,
	-1,
	AMDE_REG,
	-1,
	-1
};

void static
_r_instr_vop2(struct r_instr *__i){
	_32_u buf[32];

	buf[0] = __i->op|R0(__i->r_info[0])|R1(_R_TYP_PHYREG)|R2(_R_TYP_PHYREG);
	_16_u rtyp = __i->r_info[0];
	if(rtyp == _R_TYP_PHYREG) {
		_16_u reg = R_REG_GET(__i->r_reg[0]);
		buf[1] = reg;
	}else
	if(rtyp == _R_TYP_FLOAT) {
		*(float*)&buf[1] = *(double*)&__i->r_reg[0];
	}else
	if(rtyp == _R_TYP_IMM) {
		buf[1] = __i->r_reg[0];
	}else
	if(rtyp == _R_TYP_STR) {
		assert(1 == 0);
	}else{
		/*error*/
		assert(1 == 0);
	}
	_16_u r0,r1;
	r0 = R_REG_GET(__i->r_reg[1]);
	r1 = R_REG_GET(__i->r_reg[2]);
	buf[2] = r0;
	buf[3] = r1;
	IHC_write(&ima_pd->file,buf,4*sizeof(_32_u));
}

void static
_r_instr_vop1(struct r_instr *__i){
	_32_u buf[32];
	buf[0] = __i->op|R0(_R_TYP_PHYREG)|R1(__i->r_info[1]);
	buf[1] = R_REG_GET(__i->r_reg[0]);
	_16_u rtyp = __i->r_info[1];
	if(rtyp == _R_TYP_PHYREG) {
		_16_u reg = R_REG_GET(__i->r_reg[1]);
		buf[2] = reg;
	}else
	if(rtyp == _R_TYP_FLOAT) {
		*(float*)&buf[2] = *(double*)&__i->r_reg[1];
	}else
	if(rtyp == _R_TYP_IMM) {
		buf[2] = __i->r_reg[1]; 
	}else
	if(rtyp == _R_TYP_STR) {
		assert(1 == 0);
	}else{
		assert(1 == 0);
	}

	IHC_write(&ima_pd->file,buf,3*sizeof(_32_u));
}

void static
_r_instr_load(struct r_instr *__i){
 	_32_u buf[32];
	buf[0] = __i->op;
	buf[1] = __i->offset;
	buf[2] = R_REG_GET(__i->r_reg[0]);
	buf[3] = __i->desc+AMDE_SGPR;
	IHC_write(&ima_pd->file,buf,4*sizeof(_32_u));
}

void static
_r_instr_store(struct r_instr *__i) {
	_32_u buf[32];
	buf[0] = __i->op;
	buf[1] = __i->offset;
	buf[2] = R_REG_GET(__i->r_reg[0]);
	buf[3] = __i->desc+AMDE_SGPR;
	IHC_write(&ima_pd->file,buf,4*sizeof(_32_u));
}

void static
_r_instr_sldw(struct r_instr *__i){
	_32_u buf[32];
	buf[0] = __i->op;
	buf[1] = __i->r_reg[0];
	buf[2] = R_REG_GET(__i->r_reg[1])+AMDE_SGPR;
	buf[3] = R_REG_GET(__i->r_reg[3])+AMDE_SGPR;
	IHC_write(&ima_pd->file,buf,4*sizeof(_32_u));
}

#define __r_instr_vop1		0
#define __r_instr_vop2		1
#define __r_instr_load		2
#define __r_instr_store		3
#define __r_instr_sldw		4
struct r_producer r_amde = {
  .ft = {
  _r_instr_vop1,
  _r_instr_vop2,
	_r_instr_load,
	_r_instr_store,
	_r_instr_sldw
  },
  .selt = {
  __r_instr_vop1,//_r_op_rcp32f
  __r_instr_vop2,//_r_op_mulu16
  __r_instr_vop2,//_r_op_add32u
  __r_instr_vop1,//_r_op_mov
  __r_instr_vop2,//_r_op_fadd
  __r_instr_vop2,//_r_op_fsub
  __r_instr_vop2,//_r_op_fmul
  __r_instr_vop2,//_r_op_fmac
  __r_instr_vop1,//_r_op_cvt_i32f32
  -1,//_r_op_cvt_pkrtz_f16_f32
  __r_instr_vop1,//_r_op_rsq_f32
  __r_instr_vop2,//_r_op_max32f
  __r_instr_vop2,//_r_op_min32f
  -1,//_r_op_imgstore
  -1,//_r_op_imgsample
  -1,//_r_op_imgload
  __r_instr_sldw,//_r_op_sldw
  -1,//_r_op_endprog
  -1,//_r_op_waitcnt
  -1,//_r_op_load16
  __r_instr_load,//
  __r_instr_store,//
  -1,//interp
  -1,//interp
  __r_instr_vop1,//
  -1,//
  -1,//
  -1,//
  -1,//
  }
};
