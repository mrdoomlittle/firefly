/*
	how register allocation works in MESA.


	diffrent device manufactures have there own compiler.

	EXAMPLE: broadcom(V3D)

	first the NIR program is translated to VIR.

	NOTE: some other drivers translate to there own intermediate language.

	example NIR->INTERMEDIATE->instructions

	for V3D its from NIR directly to instructions.


	the thing we call to process the NIR program.
	
	- this function all it does is adds a TEMP register to a every growing list of TEMP registers(it accumulate them)
	vir_get_temp();

	ntq_setup_registers(){
		nir_foreach_registerforfunction(){
			- now things here get very wtf? 
			- the way they go about assigning qreg is by a hash map... what?

			struct qreg *qregs = ralloc_array();

			- they use the pointer of the nir_reg structure as the key for the hash lookup
			- is this real? why? this is soo bad.
			_mesa_hash_table_insert(nir_regptr,qregs);

			- NOTE: qreg are 32-bits, and nir_registers may consist of more then one qreg

			foreach_qreg(i){
				qregs[i] = vir_get_temp();
			}
		}
	}


	ntq_emit_impl(){
		- function assignes nir_regs with their corrsponding qreg structures.
		ntq_setup_registers();
	}

	nir_to_vir(){
		- the shader program info is set(constraints)
		...
		
		- here every function is processed and the instructions are emited
		nir_foreach_function(){
			ntq_emit_impl();
		}
	}

	v3d_nir_to_vir(){
		- the thing that will translate are NIR program to VIR
		nir_to_vir();


		while(1){
			- this assigns the real hardware values to registers .
			- this connects the virtual registers to the hardware ones.
			v3d_register_allocate();
			if(success){
				break;
			}
		}

		- takes the VIR code and turnes it into GPU language that the GPU will execute
		v3d_vir_to_qpu();
	}

	# LIVENESS

	this is where my knowledge breaks down. and im not too sure about it.
	
	
	both vir_live_variables_dataflow() and vir_live_variables_defin_defout_dataflow()
	loop though all qblocks.

	- q qblock is a block or group of instructions(qinst).

	each function has some sort of access to BITSET_WORD.
"qblock
        BITSET_WORD *def;
        BITSET_WORD *defin;
        BITSET_WORD *defout;
        BITSET_WORD *use;
        BITSET_WORD *live_in;
        BITSET_WORD *live_out;
"
	what is a qblock:
	{
			then_block,
			after_block,
			else_block,
			loop_block
	}

	for example the contents of a if-statment would be a qblock;

	vir_live_variables_dataflow(){
		vir_for_each_block_rev(){

		}
	}

	vir_live_variables_defin_defout_dataflow(){
		vir_for_each_block_rev(){

		}
	}

	- im not realy sure

- important for reference
#define BITSET_SET(x, b) ((x)[BITSET_BITWORD(b)] |= BITSET_BIT(b))
	- this functions deals with instruction source registers
	vir_setup_use(){

	}

	vir_reg_to_var(qreg){
		return qreg.index;
	}

	- this functions deals with instruction destination registers
	vir_setup_def(){
		int var = vir_reg_to_var(inst->dst);

		BITSET_SET(block->defout, var);
		=> block->defout[BITSET_BITWORD(var)] |= BITSET_BIT(var); 
		=> block->defout[var/BITSET_WORDBITS] |= 1<<(var>>5)
		=> block->defout[var>>5] |= 1<<(var&((1<<5)-1))
	}

	- im going to assume they use bits as its easy to detect collisions?

	what this looks like to me is the conpaction of flags.
	so each register get a flag for defout(either 1 or 0).

	im not too sure about why they dont just group them
	so insted of .
"
				- this tells us if the register is defined for not
        BITSET_WORD *def;
        
				- i dont realy know but im going to assume this is to tell us if this register came from outside of the block or is to be used outside.
				- im assuming this because they say "Propagate defin/defout down the successors"
				BITSET_WORD *defin;
        BITSET_WORD *defout;

				- this tells us if any instruction has used this register
        BITSET_WORD *use;

				- im going to assume this is somthing to do with blocks.
        BITSET_WORD *live_in;
        BITSET_WORD *live_out;

				- defin/defout and live_in and live_out depend on 'use' and 'def' bits
				- they are Propagated values
				i say this because.

				vir_live_variables_defin_defout_dataflow();
				- this function works on only defin and defout meaning only zeros uless someone sets it.
				- the only function that touches this is vir_setup_def(); with its BITSET_SET(block->defout, var);

				live_in and live_out - these never get SET using BITSET_SET. these values depend on 'def' and 'use' bits for their data.
"
use
struct group{
	bool def:1;
	bool defin:1;
	bool defout:1;
};
"
	struct group *groups;
"

- so there must be a meaning behind this.
- i assume bit somthing to do with bit scaning.

"
	BITSET_WORD *use;
"
	- this BITSET is used in the processing of source registers.

	in vir_setup_use(); a BITTEST of the BITSET_WORD *def is made.
	BITSET_TEST() = ((somthing&(BIT)) != 0)

	vir_setup_use(){
		- if this comes back zero then this means the bit is NOT-SET.
		if(!BITSET_TEST(block->def, var)) {
				#if (BITSET_TEST(block->defout, var)) <= i dont realy know
				...
				- here the bit is set to say the register has been used.
				BITSET_SET(block->use, var);
		}
	}


	BITS
	[00000000] instr-0
	[00000000] instr-1
	[00000000] instr-2

	vir_setup_def_use(){
		vir_for_each_block(){
			vir_for_each_inst(){
				vir_for_each_instsrc(){
					vir_setup_use();
				}

				vir_setup_def();
			}
		}
	}

	vir_calculate_live_intervals(){
		- this is very important
		int bitset_words = BITSET_WORDS(c->num_temps);
		
		- simple from: int bitset_words = (c->num_temps+31)>>5; 
		=> int bitset_words = (((c->num_temps) + (sizeof(unsigned int) * 8) - 1) / (sizeof(unsigned int) * 8))

		
		vir_setup_def_use();
		...
		while(vir_live_variables_dataflow(bitset_words));
		while(vir_live_variables_defin_defout_dataflow(bitset_words));
		...
	}

	v3d_register_allocate(){
		- function we want to look at
		vir_calculate_live_intervals();
	}

# (struct v3d_compile*)->num_temps
	struct qreg vir_get_temp(){
		struct qreg reg;
		reg.file = QFILE_TEMP;
		reg.index = c->num_temps++;
	}
*/
