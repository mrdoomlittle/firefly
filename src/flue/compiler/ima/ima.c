#include "ima.h"
#include "../../../m_alloc.h"
#include "../../../string.h"
#include "../../../io.h"
#include "../../../lib.h"
struct rd_reg ima_reg[64];
static _int_u nn = 0;

struct ima_produce *ima_pd;
_64_u ima_getregmap(_64_u __i){
	if(__i>511){
		return 0;
	}
	return ima_regmap[__i];
}
void ima_init(void) {
	ima_instr_head = NULL;
	ima_instr_tail = NULL;
}
void ima_interreg_cvt(_64_u __lf, _64_u __lv, _64_u __rf, _64_u __rv) {
	struct ima_instr *in = ima_instr_new();
	in->l->val = __lv;
	in->l->type = _ima_instr_em_reg;
//	in->l->reg_flavour = __lf;
	
	in->r->val = __rv;
	in->r->type = _ima_instr_em_reg;
//	in->r->reg_flavour = __rf;

	in->op = _ima_op_mov;
	rd_instr_vop1(in);
}
#include "../../../assert.h"
struct ima_instr* ima_instr_new(void) {
  struct ima_instr *i;
	i = m_alloc(sizeof(struct ima_instr));
	if(!ima_instr_head){
		ima_instr_head = i;
	}
	i->next = NULL;
	if(ima_instr_tail != NULL){
		ima_instr_tail->next = i;
	}
	i->info = 0;
	ima_instr_tail = i;
	return i;
}
struct ima_opdetails{
	char const *name;
};
static struct ima_opdetails details[]={
	{
		.name = "rcp32f"
	},
	{
		.name = "mulu16"
	},
	{
		.name = "add32u"
	},
	{
		.name = "mov"
	},
	{
		.name = "fadd"
	},
	{
		.name = "fsub"
	},
	{
		.name = "fmul"
	},
	{
		.name = "fmac"
	},
	{
		.name = "cvt_i32f32"
	},
	{
		.name = "cvt_pkrtz_f16_f32"
	},
	{
		.name = "rsq_f32"
	},
	{
		.name = "max32f"
	},
	{
		.name = "min32f"
	},
	{
		.name = "vecmov"
	},
	{
		.name = "export"
	},
	{
		.name = "load"
	},
	{
		.name = "store"
	},
	{
		.name = "load16"
	},
	{
		.name = "image_load"
	},
	{
		.name = "image_store"
	},
	{
		.name = "image_sample"
	},
	{
		.name = "texture"
	},
	{
		.name = "param"
	},
	{
		.name = "endprog"
	},
	{
		.name = "st"
	}
};

void ima_instr_dump(void){
	struct ima_instr *i;
	i = ima_instr_head;
	_int_u ip;
	ip = 0;
	while(i != NULL){
		printf("%u: %s.\n",ip++,details[i->op].name);
		i = i->next;
	}
}


