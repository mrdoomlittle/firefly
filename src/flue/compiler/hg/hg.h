#ifndef __hg__h
#define __hg__h
/*
  _hg_TEX,
  _hg_ASSIGN_FLOATCONST,
  _hg_ASSIGN_FLOATFLOAT,
  _hg_ASSIGN_FLOATOTHER,
  _hg_ASSIGN_VECCONST,
  _hg_ASSIGN_MATVCONST,
  _hg_ASSIGN_MATREL,
  _hg_ASSIGN_VECINTRP,
  _hg_ASSIGN_VECVCONST,
  _hg_ASSIGN_VECVEC,
  _hg_ASSIGN_VECBUF,
  _hg_ASSIGN_BUFVEC,
  _hg_ASSIGN_VECIMG,
  _hg_ASSIGN_VECIMGV,
  _hg_ASSIGN_IMGVEC,
  _hg_ASSIGN_VECOTHER,
  _hg_ASSIGN_VECREL,
  _hg_ASSIGN_EXPVEC,
  _hg_ARTH_VECMAT,
  _hg_ARTH_VECOTHER,
  _hg_ARTH_CONSTFLOAT,
  _hg_ARTH_CONSTVEC,
  _hg_ARTH_VECIMM,
  _hg_ARTH_VECVEC,
  _hg_ARTH_VECFLOAT,
  _hg_NORMALIZE,
  _hg_DOT_PRODUCT,
  _hg_INVERSE,
  _hg_CLAMP,
  _hg_MAX,
  _hg_MIN,
  _hg_AMALGAMATE
*/

#define _hg_NULL -1
#define _hg_TEX                0
#define _hg_ASSIGN_FLOATCONST  1
#define _hg_ASSIGN_FLOATFLOAT  2
#define _hg_ASSIGN_FLOATOTHER  3
#define _hg_ASSIGN_VECCONST    4
#define _hg_ASSIGN_MATVCONST   5
#define _hg_ASSIGN_MATREL      6
#define _hg_ASSIGN_VECINTRP    7
#define _hg_ASSIGN_VECVCONST   8
#define _hg_ASSIGN_VECVEC      9
#define _hg_ASSIGN_VECBUF      10
#define _hg_ASSIGN_BUFVEC      11
#define _hg_ASSIGN_VECIMG      12
#define _hg_ASSIGN_VECIMGV     13
#define _hg_ASSIGN_IMGVEC      14
#define _hg_ASSIGN_VECOTHER    15
#define _hg_ASSIGN_VECREL      16
#define _hg_ASSIGN_EXPVEC      17


#define _hg_ARTH_VECMAT        18
#define _hg_ARTH_VECOTHER      19
#define _hg_ARTH_CONSTFLOAT    20
#define _hg_ARTH_CONSTVEC      21
#define _hg_ARTH_VECIMM        22
#define _hg_ARTH_VECVEC        23
#define _hg_ARTH_VECFLOAT      24

#define _hg_NORMALIZE          25
#define _hg_DOT_PRODUCT        26
#define _hg_INVERSE            27
#define _hg_CLAMP              28
#define _hg_MAX                29
#define _hg_MIN                30
#define _hg_AMALGAMATE         31
#define _hg_ARTH_FLOATVEC      32
#define _hg_ARTH_MATVEC        33


#include "../../../y_int.h"
#include "../../../ihc/fsb.h"
#include "../../common.h"
#include "../../radeon/common.h"
#include "../../../ihc/struc.h"
#include "../../../lib/hash.h"
#define _hg_32f4       0
#define _hg_32u1       1
#define _hg_xyzw       0
#define _hg_x          1
/*
  realization:

  inorder to swap out the backend we would have to use indirect route.
*/
#define hg_ploped(__name,__x,__y) __name[__x|(__y<<4)]
#define hg_plopfunc(__name,__x,__y,__func) hg_ploped(__name,__x,__y) = __func;

#define _hg_imm 0
#define _hg_vec 1

#define _hg_const 2
#define _hg_exp 2
#define _hg_buf 3
#define _hg_mat 4
#define _hg_float 5
#define _hg_other 6
#define _hg_intrp 9
#define _hg_vconst 10
/*
  and hgge/texture routine but has a param of vec ie not an shader input
*/
#define _hg_imgv 11
#define _hg_pos 0
#define _hg_mrt 1
#define _hg_z 2
#define _hg_col 3
#define _hg_img 12
#define _hg_reg 5

#define _hg_rel 7
#define _hg_param 7
//left
#define __hg_fsv0   0
#define __hg_gv0    1

//right hand side
#define __hg_imm 0
//func spec var
#define __hg_fsv 1
//global var
#define __hg_gv  2 
#define _hg_matindex (1<<8)

typedef struct{
	_64_s a;
	_64_u b;
	double frac;
}hg_pack;

struct hg_val{
	hg_pack pack;
};
struct hg_vec{
	struct hg_val xyzw[4*4];
	_64_u comp;
};

struct hg_component{
	union{
	struct{ 
		_32_u cx:4;
		_32_u cy:4;
		_32_u cz:4;
		_32_u cw:4;
		_32_u cnt:4; 
		_32_u index:4;
	};
	_32_u comp;
	};
};


struct ys_em;
struct ys_emc{
	/*
		this contains are data.
	*/
	struct ys_em *ys;
	struct hg_component comp;
};

struct ys_em {
	struct ys_emc c;
	struct {
	_64_u placement;
	_64_u placement0;
	};

	struct ys_em *ys;
	_64_u op;
	_16_u f;
	_int_u i;
	_8_u id, cnt;
	_64_u val;
	_64_u offset;
	_64_u comp;
	_int_u exp;
	_64_u ident;
	_64_u bits;
	char *str;
	void *reg;
	hg_pack pack;

	struct hg_vec vec;
	struct ys_em *next;

	struct hg_component _comp;
	struct hg_component comp0;
	union{
		struct ys_em *ems[8];
		struct{
			struct ys_em *l;
			struct ys_em *r;
		};
	};
	struct ys_em *il;//interleaveate
	struct ys_em *decl;
};

struct hg_symb {
	union {
		struct ys_em em;
	};
	void *priv;
	char str[64];
	_int_u len;
	_8_s ign;
	struct hg_symb *next;
};

struct hg_produce{
	struct f_lhash symbs;
	struct ys_em *hg_top_node;
	struct ys_em *hg_decl;
	_16_u interp_mode[32];
};

extern _16_u hg_asglhs[32];
extern _16_u hg_asgrhs[32];
extern _16_u hg_asgtab[256];
extern _16_u hg_arthtab[256];
extern struct hg_produce *hg_pd;
//hg.c
struct ys_em* hg_new_node(void);
void hg_decl(struct ys_em*);
void hg_init(void);
double hg_unpack_float(hg_pack*);
void hg_pack_float_str(hg_pack*,char const*);
#endif/*__hg__h*/
