# include "common.h"
# include "../../ffly_def.h"
# include "reg.h"
#include "../../assert.h"
#include "../../string.h"
#include "../../m_alloc.h"
#include "../../io.h"
struct cmd_details {
	_8_s present;
	char const *name;
};
#define _ {-1}
static struct cmd_details cmds[0x100] = {
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//0
	{0, "NOP"}, _, {0, "CLEAR_STATE"}, _, _, _, _, _, _, _, _, _, _, _, _, _,//1
	_, _, _, _, _, _, _, _, {0, "CONTEXT_CONTROL"}, _, _, _, _, _, _, _,//2
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//3
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//4
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//5
	_, _, _, _, _, _, _, _, {0, "SET_CONFIG_REG"}, {0, "SET_CONTEXT_REG"}, _, _, _, _, _, _,//6
	_, _, _, _, _, _, {0, "SET_SH_REG"}, _, _, _, _, _, _, _, _, _,//7
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//8
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//9
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//10
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//11
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//12
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//13
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,//14
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ //15
};

struct addrent {
	_64_u addr;
	struct addrent *next;
};

struct addrtav {
	struct addrent *tab[0x100];
};

static struct addrtav tav;

void static
addrtab_init(void) {
	mem_set(tav.tab,0,sizeof(void*)*0x100);
}


_8_s static
addrput(_64_u __adr) {
	// will do for now
	struct addrent *ent, **tab;
	ent = *(tab = (tav.tab+(__adr&0xff)));

	while(ent != NULL) {
		if (ent->addr == __adr) {
			return -1;
		}
		ent = ent->next;
	}
	ent = m_alloc(sizeof(struct addrent));

	ent->next = *tab;
	*tab = ent;
	ent->addr = __adr;
	return 0;
}
void static
addrtab_cleanup(void) {
	struct addrent *ent, *bk;
	_int_u i;
	i = 0;
	for(;i != 0x100;i++) {
		ent = tav.tab[i];
		
		while(ent != NULL) {
			ent = (bk = ent)->next;
			m_free(bk);
		}
	}
}

#include "../../assert.h"
void rd_cmdbuf_dump(rd_contextp __ctx) {
	assert(__ctx->top != NULL);
	addrtab_init();
	struct rd_dbblock *b;
	b = __ctx->top;
	while(b != NULL) {
	if (b->ident != NULL)
		printf("%s- %u.\n", b->ident,b->ndw);
	_32_u *dw;
	_32_u val;
	dw = b->start;
	struct cmd_details *dt;
	_32_u *end = dw+b->ndw;
	while(dw != end) {
		val = *dw;
		_32_u type = val>>30;
		_32_u cnt = (val>>16)&0xff;
		if (!val) {
			dw+=cnt+2;
		} else if (val == 1) {
			while(1);	
		} else if (val == 2) {
			// filler packet
			dw++;
		} else {
			if ((dw+cnt+2)>end) {
				_int_u i = 0;
				while(dw != end) {
					printf("OOB_DWORD%u: %x.\n",i++,*dw);
					dw++;
				}
				printf("outofbounds break in %s. by %u-dws, %u\n",b->ident,(dw+cnt+2)-end,b->ndw);
				break;
			}
			_32_u op = (val>>8)&0xff;
			dt = cmds+op;
			if (op == SET_CONTEXT_REG) {
				_int_u i;
				i = 0;
				for(;i != cnt;i++) {
				//	assert(addrput(dw[1]+i) != -1);
/*
	might cause issues but to ensure we are not overwriting registers due to stupidity
*/
				}
			}

			printf("_|\tPKT3-%x, +%u, %s\n", op, cnt, !dt->present?dt->name:"UNKNOWN");
			_int_u i;
			i = 0;
			for(;i != cnt+1;i++) {
				printf("_|\tDW-%u: %x\n", i,dw[i+1]);
			}
			dw+=cnt+2;
		}
	}
		b = b->next;
	}
	addrtab_cleanup();
}

struct rd_dbblock* rd_dbblock(rd_contextp __ctx) {
	struct rd_dbblock *b;
	b = m_alloc(sizeof(struct rd_dbblock));

	if (__ctx->bk != NULL)
		__ctx->bk->next = b;
	__ctx->bk = b;
	b->next = NULL;
	if (!__ctx->top)
		__ctx->top = b;

	return b;
}
