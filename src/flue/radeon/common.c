#include "common.h"
#include "../../assert.h"
#include "../../linux/unistd.h"
#include "../../linux/ioctl.h"
#include "../../linux/fcntl.h"
#include "../../linux/mman.h"
#include "../../m_alloc.h"
#include "../../io.h"
#include "../../string.h"
#include "../../linux/fb.h"
#include "reg.h"
void rd_emit_userdata(struct rd_context*,struct flue_shinfo *sh);
void rd_resources(struct rd_context*,struct rd_bo0**,struct flue_resspec*,_int_u,_64_u*);
void rd_textures(struct rd_context*,struct flue_tex**,struct flue_shinfo*,_int_u,_64_u*);
/*
	update function

	if your going insane check RSRC2
*/
void static _rbcopy(struct flue_drawable *__d,struct rd_tex *__s, _32_u __width, _32_u __height) {
	_int_u active = 0;
	_int_u x, y;
	void *rt_ptr =	((struct amdgpu_bo*)__s->bo)->cpu_ptr;
	y = 0; 
	for(;y != __s->height;y++) {
		x = 0; 
		for(;x != __s->width;x++) {
			_8_u *s, *d;
			s = ((_8_u*)rt_ptr)+((x+(y*__s->width))*4);
			d = ((_8_u*)__d->ptr)+((x*4)+(y*__d->line_length));
			*(_32_u*)d = *(_32_u*)s;
			if (*(_32_u*)s >0)
				active++;
		}
	}
	printf("%u, %u > %u\n",__s->width,__s->height,__d->line_length);
	printf("ACTIVE-PIXELS: %u.\n",active);
}

struct rd_common rd_com;
void rd_drv(void) {
	rd_com.dvs = NULL;
}

void rd_drv_deinit(void) {
	// pv = preserved
	struct rd_dev *dv, *pv;
	dv = rd_com.dvs;
	while(dv != NULL) {
		dv = (pv = dv)->next;
		printf("AMDGPU, goodbye to device.\n");
		amdgpu_dev_de_init(pv->dev);
		close(pv->fd);
		m_free(pv);
	}
}
#define COLOUR_UNORM	0
#define COLOUR_SNORM	1
#define COLOUR_USCALED	2
#define COLOUR_SSCALED	3
#define COLOUR_UINT		4
#define COLOUR_SINT		5
#define COLOUR_SRGB		6
#define COLOUR_FLOAT	7

#define NF_UNORM	0
#define NF_SNORM	1
#define NF_USCALED	2
#define NF_SSCALED	3
#define NF_UINT		4
#define NF_SINT		5
#define NF_FLOAT	7
#define NF_SRGB		9
/*
	can be
	NF_8
	NF_8_8
	NF_8_8_8_8
*/
#define NF_SRGB		9

#define DF_32_32_32_32	14
#define DF_8_8_8_8		10
#define DF_32			4

/*
	look for gpu
*/
#include "../../ys/as/as.h"
void static init_dev(struct rd_dev *dv) {
	rd_cs_init(dv);
}
struct flue_dev* rd_drv_dev(struct rd_dev *dv, struct amdgpu_dev *__dev) {
	_int_u npm;
	npm = FLU_PRIO0.ndv++;
	struct flue_dev *d;
	d = FLU_PRIO0.dv+npm;
	d->n_use = 0;
	d->d = dv;
	d->m = ys_radeon;
	d->dv = FLUE_RADEON;
	dv->fd = __dev->fd;
	printf("AMDGPU, hello to device, %d\n", dv->fd);
	if (dv->fd<0) {
		printf("failed to open AMDGPU-device.\n");
		return;
	}

	dv->dev = __dev;
	dv->sh.fd = dv->fd;
	if (!dv->dev) {
		printf("it seems somthing went wong while initing AMDGPU-device.\n");
		return;
	}
	dv->next = rd_com.dvs;
	rd_com.dvs = dv;
	init_dev(dv);
	return d;
}
void rd_cs_init(struct rd_dev*);
void rd_drv_sweep(void) {
	struct rd_dev *dv;
	dv = m_alloc(sizeof(struct rd_dev));
	_int_u npm;
	npm = FLU_PRIO0.ndv++;
	struct flue_dev *d;
	d = FLU_PRIO0.dv+npm;
	d->n_use = 0;
	d->d = dv;
	d->dvid = FLUE_RADID;
	d->m = ys_radeon;
	d->dv = FLUE_RADEON;
	dv->fd = open("/dev/dri/card0", O_RDWR, 0);
	if (flue_comm.auth != NULL)
		flue_comm.auth(flue_comm.conn,dv->fd,1);
	printf("AMDGPU, hello to device, %d\n", dv->fd);
	if (dv->fd<0) {
		printf("failed to open AMDGPU-device.\n");
		return;
	}

	dv->dev = amdgpu_dev_init(dv->sh.fd = dv->fd);
	if (!dv->dev) {
		printf("it seems somthing went wong while initing AMDGPU-device.\n");
		return;
	}
	dv->next = rd_com.dvs;
	rd_com.dvs = dv;
	init_dev(dv);
}
/*
	rdbo does not stores real size only page aligned one?
*/
void rd_bo_init(struct amdgpu_dev *__dv, struct rd_bo *__bo, _64_u __size, _64_u __alignment, _64_u __domains, _64_u __flags, _64_u __vmflags) {
	__size = AMDGPU_GPU_PAGE_ALIGN(__size);
	struct rd_bo *b = __bo;
	struct amdgpu_bo *bo = &b->bo;
	bo->cpu_ptr = NULL;
	amdgpu_bo_alloc(__dv, bo,__size, __alignment, __domains, __flags);
	
	struct amdgpu_va *v;
	v = amdgpu_va_alloc(
		amdgpu_va_range_general(__dv),
		__size,
		0
	);
	printf("rd_bo_create: %x, %u.\n",v->addr,__size);
	amdgpu_bo_va_op(BO_DEVICE(bo), bo, AMDGPU_VA_OP_MAP, __vmflags, 0, v->addr, __size);
	b->v = v;
}

struct rd_bo* rd_bo_create(struct amdgpu_dev *__dv, _64_u __size, _64_u __alignment, _64_u __domains, _64_u __flags, _64_u __vmflags) {
	struct rd_bo *b = m_alloc(sizeof(struct rd_bo));
	rd_bo_init(__dv,b,__size,__alignment,__domains,__flags,__vmflags);
	return b;
}

void rd_bo_map(struct rd_bo *__bo) {
	amdgpu_bo_cpu_map(BO_DEVICE(__bo), (struct amdgpu_bo*)__bo);
}

void rd_bo_unmap(struct rd_bo *__bo) {
	amdgpu_bo_cpu_unmap((struct amdgpu_bo*)__bo);
}

void rd_bo_destroy(struct rd_bo *__bo) {
	struct amdgpu_bo *bo = __bo;
	amdgpu_bo_va_op(BO_DEVICE(bo), bo, AMDGPU_VA_OP_UNMAP, 0, 0, __bo->v->addr, bo->size);
	amdgpu_va_free(amdgpu_va_range_general(BO_DEVICE(bo)), __bo->v);
	amdgpu_bo_free(BO_DEVICE(bo), bo);
	m_free(__bo);
}


void static init_cb(struct rd_tex *__bsc) {	
	_32_u *dw = __bsc->buf;
	struct flue_tex *tx = __bsc;
	struct rd_bo *bo = __bsc->bo;
	// base is multable of 256
	/*
		changelog:
			assert(clear&0xff == 0) removed.
	*/
	assert((bo->v->addr&0xff) == 0);
	_64_u nfmt = RD_FMTGET(__bsc->fmt.nfmt,RD_NF_COLOUR);
	_64_u dfmt = RD_FMTGET(__bsc->fmt.dfmt,RD_DF_COLOUR);
	*dw = bo->v->addr>>8;/*CB_COLOR_BASE*/
	dw[1] = (__bsc->width/8)-1;/*CB_COLOR_PITCH*/
	dw[2] = ((__bsc->width*__bsc->height)/64)-1;/*CB_COLOR_SLICE*/
	dw[3] = 0xffffffff<<13;/*CB_COLOR_VIEW*/
	dw[4] = (dfmt<<2)|(nfmt<<8)|(1<<17)|(1<<20)|(1<<23)|(1<<26)|(!(tx->config&FLUE_TEX_BLEND_EN)?(_MRT_BLEND_BYPASS|_MRT_BLEND_CLAMP):0)/*DISABLE FMASK COMPRESSION*/;/*CB_COLOR_INFO*/
	dw[5] = 8/*linear aligned 16x16 with 4 pipes*/;/*CB_COLOR_ATTRIB*/
	dw[6] = 1;/*CB_COLOR_DCC_CONTROL*/
	dw[7] = *dw;//__bsc->clear>>8;/*CB_COLOR0_CMASK*/
	dw[8] = 0;/*CB_COLOR0_CMASK_SLICE*/
	dw[9] = *dw;/*CB_COLOR0_FMASK*/
	dw[10] = 0;/*CB_COLOR0_FMASK_SLICE*/
	dw[11] = 0;/*CB_COLOR0_CLEAR_WORD0*/
	dw[12] = 0;/*CB_COLOR0_CLEAR_WORD1*/
	flue_printf("COLOUR-BUFFER: address: %x, width: %u, height: %u, format: %s_%s, blend_%s\n", bo->v->addr, __bsc->width, __bsc->height, flue_nfmtstr(__bsc->fmt.nfmt,0),flue_dfmtstr(__bsc->fmt.dfmt,0),!(tx->config&FLUE_TEX_BLEND_EN)?"disabled":"enabled");
}

void static init_zb(struct rd_tex *__z) {
	struct rd_bo *bo = __z->bo;
	_32_u *dw = __z->buf;
	*dw = (2<<4)|(5<<8)|(1<<0x11)|(3<<0x13);/*DB_DEPTH_INFO*/	
	dw[1] = _A010_Z_32_FLOAT|(5<<0x14/*tile mode index*/)|(5<<0xd)|(1<<0x1e/*CLEAR DISALLOWED*/);//__z->fmt->format;/*DB_Z_INFO*/
	dw[2] = 0;/*DB_STENCIL_INFO*/
	
	dw[3] = bo->v->addr>>8;/*DB_Z_READ_BASE*/
		dw[4] = 0x41;/*DB_STENCIL_READ_BASE*/
		dw[5] = bo->v->addr>>8;/*DB_Z_WRITE_BASE*/
		dw[6] = 0x41;/*DB_STENCIL_WRITE_BASE*/	
	dw[7] = ((__z->width/8)-1)|(((__z->height/8)-1)<<11);/*DB_DEPTH_SIZE*/
	dw[8] = ((__z->width*__z->height)/64)-1;/*DB_DEPTH_SLICE*/
	
	ffly_fprintf(ffly_err, "ZBUFFER:\n"
		"- DEPTH_INFO:		 %x\n"
		"- Z_INFO:			 %x\n"
		"- STENCIL_INFO:		 %x\n"	
		"- Z_READ_BASE:		%x\n"
		"- STENCIL_READ_BASE:	%x\n"
		"- Z_WRITE_BASE:		 %x\n"
		"- STENCIL_WRITE_BASE: %x\n"
		"- DEPTH_SIZE:		 %x\n"
		"- DEPTH_SLICE:		%x\n",
		dw[0],
		dw[1],
		dw[2],
		dw[3],
		dw[4],
		dw[5],
		dw[6],
		dw[7],
		dw[8]
	);
	printf("ZBUF- %x.\n", bo->v->addr);
}

/*
	never fucking forget to add DWC to cmdbuffer cpu_ptr
*/
void static _viewport(struct rd_context *__ct, struct flue_viewport *__v, _int_u __offset, _int_u __n) {
	__ct->bits |= RD_VIEWPORT_DIRTY;
	_32_u *dw = __ct->viewport;

	_32_u *dwtop = dw;
	flue_printf("VIEWPORT{translate{%f, %f, %f}, scale{%f, %f, %f}}: %u, %u.\n",
	__v->translate[0],__v->translate[1],__v->translate[2],
		__v->scale[0],__v->scale[1],__v->scale[2]
	,__offset,__n);

	*dw = PKT3(SET_CONTEXT_REG, __n*6);
	dw[1] = CONTEXT_REG(PA_CL_VPORT_XSCALE);
	dw+=(__n*6)+2;

	*dw = PKT3(SET_CONTEXT_REG, __n*2);
	dw[1] = CONTEXT_REG(PA_SC_VPORT_SCISSOR_0_TL);
	dw+=(__n*2)+2;


	*dw = PKT3(SET_CONTEXT_REG, __n*2);
	dw[1] = CONTEXT_REG(PA_SC_VPORT_ZMIN_0);
	
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		_32_u *scale;
		_32_u *scissor;
		_32_u *zmm;

		scale = dwtop+2+(i*6);
		*scale = *(_32_u*)&__v->scale[0];
		scale[1] =	*(_32_u*)&__v->translate[0];
		scale[2] = *(_32_u*)&__v->scale[1];	
		scale[3] = *(_32_u*)&__v->translate[1];
		scale[4] = *(_32_u*)&__v->scale[2];
		scale[5] = *(_32_u*)&__v->translate[2];
		assert(__v->scale[0] != 0 && __v->scale[1] != 0 && __v->scale[2] != 0); 

		scissor = dwtop+4+(__n*6)+(i*2);
		zmm = dwtop+6+(__n*6)+(__n*2)+(i*2);
			
		float z_max = 1, z_min = -1;
		zmm[0] = *(_32_u*)&z_min;
		zmm[1] = *(_32_u*)&z_max;
		ffly_fprintf(ffly_err, "ZMIN: %f, ZMAX: %f.\n",z_min,z_max);
		printf("#################### VIEWPORT_SCALE: %f, %f, %f.\n",__v->scale[0],__v->scale[1],__v->scale[2]);
		__v++;
	}
	_int_u dwc = 6+(__n*6)+(__n*2)+(__n*2);
	__ct->viewport_dwc = dwc;
}

void rd_addtothelist(struct rd_context *__ct, struct rd_bo *__bo) {
	struct rd_bo_list_entry *ent;
	ent = m_alloc(sizeof(struct rd_bo_list_entry));
	ent->nextover = __ct->list_top;
	__ct->list_top = ent;
	__ct->list_size++;
	ent->bo = __bo;
	ent->bo_priority = 0;
}
void rd_cache_(struct rd_context *__ct, struct rd_cmd_buffer *__buf) {
	_32_u *dw;
	dw = __buf->bo->cpu_ptr;
	dw+=__buf->ndw;
}

void static
clear_everything(struct rd_context *__ct) {
	struct rd_bo_list_entry *ent,*bk;
	ent = __ct->list_top;
	while(ent != NULL) {
		ent = (bk = ent)->nextover;
		m_free(bk);
	}
	__ct->list_top = NULL;
	__ct->list_size = 0;

	__ct->cmdbuf->ndw = 0;
	{
		struct rd_dbblock *db, *bk;
		db = __ct->top;
		while(db != NULL) {
			db = (bk = db)->next;
			m_free(bk);
		}
		__ct->top = NULL;
		__ct->bk = NULL;
	}
	FLUE_RSVS->ud_offset = 0;
	FLUE_RSPS->ud_offset = 0;	
	if (__ct->nconsts>0) {
		_int_u i;
		i = 0;
		for(;i != __ct->nconsts;i++) {
			// BAD!
			rd_bo_destroy(__ct->consts[i]);
		}
		__ct->nconsts = 0;
	}

	if (__ct->ndesc>0) {
		_int_u i;
		i = 0;
		for(;i != __ct->ndesc;i++) {
			rd_bo_destroy(__ct->desc[i]);
		}
		__ct->ndesc = 0;
	}
	__ct->f0 = rd_nowhere_func;
	__ct->cache_bits = 0;
}

void static
_rendertgs(struct rd_context *__ct,struct rd_tex **__list, _int_u __n) {
	__ct->bits |= RD_COLOURBUFFER_DIRTY|RD_BLEND_DIRTY;
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct rd_tex *tx = __list[i]->tx.ptr;
		init_cb(tx);
		__ct->cb[i] = tx;
		struct flue_tex *ft = tx;
		__ct->blend[i] = !(ft->config&FLUE_TEX_BLEND_EN)?0:((1<<30)|4|(5<<8));
	}
	__ct->ncb = __n;
}
void rd_cs_buffer_clear(rd_contextp __ctx,
	struct rd_tex *__tex,struct rd_bo*,float, float, float, float
);

void static
_clear(struct rd_context *__ct,struct rd_tex **__list, _int_u __n,float r, float g, float b, float a) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct rd_tex *t = __list[i]->tx.ptr;
		rd_cs_buffer_clear(__ct,t,t->bo,r,g,b,a);
	}
}

/*
	after the state push nothing can be altered only draw calls are to be issued

	to improve CONSTS buffers.

	0. save BO to table and ask the user to provide us with a has this been modified status

	TODO:
		remove mem_cpy part just raw pointer.
	
	this function gets called right before we start pushing draw calls
*/
void rd_load_reg(rd_contextp __ct);
void static _pushstate(struct rd_context *__ct) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	if(__ct->sh.zb != __ct->zb){//quick FIXME:
		__ct->bits |= RD_DEPTH_DIRTY|RD_DPB_DIRTY;
		__ct->zb = __ct->sh.zb;
	}

  _32_u *dw = __cmdbuf->bo->cpu_ptr;
  dw+=__cmdbuf->ndw;
	*dw = PKT3(CONTEXT_CONTROL, 1);
  dw[1] = (1<<31)|3;
  dw[2] = (1<<31)|3;

  //  ensure all defaults are set

  dw[3] = PKT3(CLEAR_STATE, 0);
  dw[4] = 0;
	__cmdbuf->ndw+=5;

	rd_load_reg(__ct);

	/*
		we have two buffers here.

		consts - draw call specific data.
		EXAMPLE:
			draw_rect, CONSTS=red
			draw_rect, CONSTS=blue
		consts is accable from shaders.

		desc - buffer resource constants 
	*/
	struct rd_bo *consts;
	struct rd_bo *desc;
	consts = rd_bo_create(
		__ct->d->dev,
		4096,
		0,
		AMDGPU_GEM_DOMAIN_VRAM,
		AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED,
		AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE
	);
	
	desc = rd_bo_create(
		__ct->d->dev,
		4096,
		0,
		AMDGPU_GEM_DOMAIN_VRAM,
		AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED,
		AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE
	);

	__ct->desc[__ct->ndesc++] = desc;
	__ct->consts[__ct->nconsts++] = consts;
	rd_addtothelist(__ct, &desc->bo);
	rd_addtothelist(__ct, &consts->bo);
	rd_bo_map(desc);
	rd_bo_map(consts);
	/*
		we could store the resource desciptor for this in DESC but we could also do this.
		after looking on how OpenGL does this it seems they construct the descriptor using instructions.
		im pretty sure that the worst way to do this, i dont know why they found this way better but we doing it this way.

		this way should be faster, my hope is this gets cached in L1 and stays there for the duraton of the whole shader.
	
		NOTE:
			caching isent setup as at the time its well time consuming on things that dont matter right of this moment.
	*/


	/*
		how things work here. 
		as im lazy.

		the first 2-QWORDS are the descriptor for the const buffer.

		the descriptor and the data for the const buffer are contained within the same buffer.

	*/
	_64_u *k = consts->bo.cpu_ptr;
	mem_cpy(((_64_u*)consts->bo.cpu_ptr)+2,__ct->sh.const_data,4096-(sizeof(_64_u)*2));
	/*
		NOTE: (consts->v->addr+16) points us to are data location
	*/
	k[0] = BUF_RD_QW0((consts->v->addr+16),(sizeof(float)*4),0,0);
	k[1] = BUF_RD_QW1(
		(4096),
		SEL_R,
		SEL_G,
		SEL_B,
		SEL_A,
		NF_FLOAT,
		DF_32_32_32_32,
		0,/*dont care*/
		0,
		0,/*no add thread ID*/
		0,
		0,
		0,
		0,
		0
	);
	/*
	
		const data starts at base+2(DWORDS)	
	*/
	printf("CONSTANT BUFFER: %x.\n",consts->v->addr);
	mem_cpy(desc->bo.cpu_ptr,__ct->d_dw,RD_DESCMAX*8);
	rd_bo_unmap(desc);
	rd_bo_unmap(consts);

	rd_init_state(__ct,__cmdbuf);
	
	rd_emit_userdata(__ct,FLUE_RSVS);
	rd_emit_userdata(__ct,FLUE_RSPS);
	__ct->d_offset = 0;

	__ct->f0(__ct,__cmdbuf);
}
void rd_printf_load_reg(rd_contextp ct);
#include "../../msg.h"
#define MSG_BITS MSG_DOMAIN(_DOM_FLUE)
void rd_shader_update(rd_contextp);
#include "../../time.h"
void
rd_submit(void) {
	struct flue_context *fc;
	fc = flue_comm.ctx;
	
	flue_blobp b;
	b = fc->b;

	struct rd_context *ct = fc->priv;
	struct rd_cmd_buffer *cmdbuf;
	cmdbuf = ct->cmdbuf;
	rd_addtothelist(ct, ct->shadow);
	rd_addtothelist(ct, cmdbuf->bo);
	_32_u *dw;
	/*
		amdgpu requires us to align the commandbuffer to 8-dwords
	*/
	_int_u i;
	dw = cmdbuf->bo->cpu_ptr;
	_int_u xclear = 0;
	while((cmdbuf->ndw&7) != 0 || cmdbuf->ndw <=256) {
		dw[cmdbuf->ndw] = 0xffff1000;
		cmdbuf->ndw++;
		xclear++;
	}
	assert(!(cmdbuf->ndw&7));

	printf("NUMBER-OF-DWORDS: %u, oscupied: %u.\n", cmdbuf->ndw, cmdbuf->ndw-xclear);

//	clear_everything(ct);
//	return;


	struct amdgpu_ctx *ctx = ct->ctx;
	struct drm_amdgpu_bo_list_entry *ents = m_alloc(sizeof(struct drm_amdgpu_bo_list_entry)*ct->list_size);

	struct rd_bo_list_entry *ent = ct->list_top;
	i = 0;
	while(ent != NULL) {
		ents[i].bo_handle = ((struct amdgpu_bo*)ent->bo)->handle;
		ents[i].bo_priority = 0;
		printf("BO_LIST_ENT: %u, %u.\n", ents[i].bo_handle,ents[i].bo_priority);
		i++;
		ent = ent->nextover;
	}

	struct amdgpu_bo_list *list = amdgpu_bo_list(ct->d->dev, ct->list_size, ents);
	m_free(ents);//no longer needed
	assert(list->handle != 0);
	
	struct amdgpu_cs_request req;
	req.flags = 0;
	req.ip_type = AMDGPU_HW_IP_GFX;
	req.ip_inst = 0;
	req.ring = 0;
	req.ib_n = 1;
	req.list_handle = list->handle;
	struct amdgpu_cs_ib_info ibinfo;
	req.ib = &ibinfo;
	ibinfo.flags = 0;
	ibinfo.addr = cmdbuf->v->addr;
	ibinfo.size = cmdbuf->ndw;
	_64_u handle = amdgpu_cs_submit(ctx, &req);
	struct amdgpu_info info;
	 if (amdgpu_info(ct->d->dev->fd, &info) == -1) {
		printf("failed to get amdgpu info.\n");
	}
	printf("gfx sclk: %u\n", info.gfx_sclk);
	printf("gfx mclk: %u\n", info.gfx_mclk);
	printf("temp: %f-degree\n", ((float)info.temp)*0.001);
	printf("load: %u\n", info.load);
	printf("power: %ld-watts\n", info.avg_power);
		{
		struct drm_amdgpu_memory_info info;
	 		 bzero(&info, sizeof(struct drm_amdgpu_memory_info));
		ffly_amdgpu_memory_info(ct->d->dev->fd,&info);
		MSG(INFO,"radeon{mem_usage-vram}: %u-global, %u-local\n",info.vram.heap_usage, ct->d->dev->vam.alloced)	
	}
	union drm_amdgpu_wait_cs cswait;
	cswait.in.handle = handle;
	cswait.in.ctx_id = ctx->ctx_id;
	cswait.in.ip_type = AMDGPU_HW_IP_GFX;
	cswait.in.ip_instance = 0;
	cswait.in.ring = 0;
	cswait.in.timeout = AMDGPU_TIMEOUT_INFINITE;
	printf("waiting for command buffer to be exec.\n");
	int r;
	r = drm_cmdreadwrite(ct->d->dev->fd, DRM_AMDGPU_WAIT_CS, &cswait, sizeof(union drm_amdgpu_wait_cs));
	if (r == -1) {
		printf("wait failure.\n");
	}
 	printf("status: %s.\n", !cswait.out.status?"completed":"still busy");	
	
	//cleanup
	amdgpu_bo_list_destroy(ct->d->dev,list);
	m_free(list);

	rd_cmdbuf_dump(ct);
	i = 0;
	for(;i != ct->d_offset;i++) {
		printf("-#### %x.\n",ct->d_dw[i+64]);
	}
	clear_everything(ct);
	rd_printf_load_reg(ct);
}

void static
configure(struct rd_context *__ctx) {
}

void static* bo_data(struct rd_dev *__d,_32_u __size)
{
	struct rd_bo0 *bo;
	bo = m_alloc(sizeof(struct rd_bo0));
	bo->bo = rd_bo_create(__d->dev, __size, 0, 
		AMDGPU_GEM_DOMAIN_VRAM,
		AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED,
		AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE);
	bo->_0.size = __size;
	return bo;
}
// if the user loses the ptr then well its there fault not are's :)
void static bo_map(struct rd_bo0 *__buf) {
	rd_bo_map(__buf->bo);
	__buf->_0.cpu_map = ((struct amdgpu_bo*)__buf->bo)->cpu_ptr;
}

void static bo_unmap(struct rd_bo0 *__buf) {
	rd_bo_unmap(__buf->bo);
	__buf->bo->bo.cpu_ptr = NULL;
}
//abdicate
void static bo_destroy(struct rd_bo0 *__buf) {
	if(!__buf)return;	
	if (__buf->bo->bo.cpu_ptr != NULL)
		rd_bo_unmap(__buf->bo);

	rd_bo_destroy(__buf->bo);
	m_free(__buf);
}

void rd_rb_new(struct rd_context *__ctx, struct rd_tex *__rb) {

}
void rd_rb_init(struct rd_context *__ctx, struct rd_tex *__rb, _int_u __width, _int_u __height, struct flue_fmtdesc *__fmt, struct rd_fmtdesc *__fmt0) {
}

void rd_rb_destroy(struct rd_context *__ctx, struct rd_tex *__rb) {
	
}


_32_u static
_srb_init(struct rd_dev *__d, struct rd_context *__ctx, int *__back, int *__front, struct rd_tex *__rb, _int_u __width, _int_u __height, struct flue_fmtdesc *__fmt, struct rd_fmtdesc *__fmt0) {

//	rd_rb_init(__ctx,__rb,__width,__height,__fmt,__fmt0);
	_32_u handle;
	amdgpu_bo_export(__d->dev,__rb->bo,amdgpu_bo_handle_dma_buf_fd,&handle);
	*__back = handle;
/*
	amdgpu_bo_export(__d->dev,__rb->bufs[RD_FRONT],amdgpu_bo_handle_dma_buf_fd,&handle);
	*__front = handle;
	*/
}

void rd_tex__bo(struct rd_tex *__t, struct rd_bo *__back, struct rd_bo *__front,_32_u __width, _32_u __height, struct flue_fmtdesc *__fmt) {
#ifdef _
	__t->tx.tx.ptr = __back->bo.cpu_ptr;
	__t->tx.tx.front_ptr = __front->bo.cpu_ptr;
	__t->tx.tx.front = __front;
	rd_tex_alloc_with_own_bo(__t,__back,__width,__height,__fmt);
#endif
}

void static
drawarray(struct rd_context *__ctx,struct rd_bo0 *idx, _64_u __offset, _int_u __n, _32_u __typ) {
	struct rd_bo *__idx = !idx?NULL:idx->bo;
//	__ctx->bits |= RD_DIRTY;//FRAMEBUFFER_DIRTY|RD_VIEWPORT_DIRTY;
	rd_shader_update(__ctx);
	
	_pushstate(__ctx);
	rd_shader_ps_state_emit(__ctx,__ctx->ps);
	rd_shader_vs_state_emit(__ctx,__ctx->vs);
	struct rd_cmd_buffer *cmdbuf = __ctx->cmdbuf;
	_32_u *dw;
	dw = cmdbuf->bo->cpu_ptr;
	dw+=cmdbuf->ndw;

	if (__typ == FLUE_TRI3) {
		dw[0] = PKT3(SET_UCONFIG_REG, 1);
		dw[1] = UCONFIG_REG(VGT_PRIM_TYPE);
		dw[2] = DI_PT_TRILIST;
		dw[3] = PKT3(INDEX_TYPE, 0);
		dw[4] = VGT_IDX_32;
		dw[5] = PKT3(NUM_INST, 0);
		dw[6] = 1;
		if (__idx != NULL) {
		dw[7] = PKT3(DRAW_INDEX_2, 4);
		dw[8] = 3*__n;
		/*
			docs say word-aligned, as are address space is page aligned we dont need to care about it.
			unless offset is present
		*/
		//make sure its aligned
		assert(!(__offset&3));
		_64_u addr = __idx->v->addr+__offset;
		dw[9] = addr&0xffffffff;
		dw[10] = addr>>32;
		dw[11] = 3*__n/*this stays the same*/;
		dw[12] = DI_SRC_SEL_DMA/*go through DMA*/;
		cmdbuf->ndw+=13;
		}else{
		dw[7] = PKT3(DRAW_INDEX_AUTO, 1);
		dw[8] = 3*__n;
		dw[9] = DI_SRC_SEL_AUTO_INDEX;
		cmdbuf->ndw+=10;	
		}

	}else
	if (__typ == FLUE_RECT) {
		dw[0] = PKT3(SET_UCONFIG_REG, 1);
		dw[1] = UCONFIG_REG(VGT_PRIM_TYPE);
		dw[2] = DI_PT_RECTLIST;
		dw[3] = PKT3(INDEX_TYPE, 0);
		dw[4] = VGT_IDX_32;
		dw[5] = PKT3(NUM_INST, 0);
		dw[6] = 1;
		if (__idx != NULL) {
			dw[7] = PKT3(DRAW_INDEX_2, 4);
			dw[8] = 3*__n;
		
			assert(!(__offset&3));
			_64_u addr = __idx->v->addr+__offset;
			dw[9] = addr&0xffffffff;
			dw[10] = addr>>32;
			dw[11] = 3*__n/*this stays the same*/;
			dw[12] = DI_SRC_SEL_DMA/*go through DMA*/;
			cmdbuf->ndw+=13;
		}else{
			dw[7] = PKT3(DRAW_INDEX_AUTO, 1);
			dw[8] = 3*__n;
			dw[9] = DI_SRC_SEL_AUTO_INDEX;
			cmdbuf->ndw+=10;	
		}
	}else
	if (__typ == FLUE_QUAD) {
		dw[0] = PKT3(SET_UCONFIG_REG, 1);
		dw[1] = UCONFIG_REG(VGT_PRIM_TYPE);
		dw[2] = DI_PT_QUADLIST;
		dw[3] = PKT3(INDEX_TYPE, 0);
		dw[4] = VGT_IDX_32;
		dw[5] = PKT3(NUM_INST, 0);
		dw[6] = 1;
		dw[7] = PKT3(DRAW_INDEX_AUTO, 1);
		dw[8] = 4*__n;
		dw[9] = DI_SRC_SEL_AUTO_INDEX;
		cmdbuf->ndw+=10;	
	}

	/*
		TODO:
			this should be optional
	
		also its here be because i im sure draw commands dont wait on each other to finish,
		this can be fixed with a z-buffer but for now this is okay.\n
	*/
	dw = cmdbuf->bo->cpu_ptr;
	dw+=cmdbuf->ndw;

	dw[0] = PKT3(EVENT_WRITE,0);
	dw[1] = EVENT_TYPE(15)|EVENT_INDEX(4);
	dw[2] = PKT3(EVENT_WRITE,0);
	dw[3] = EVENT_TYPE(16)|EVENT_INDEX(4);
	cmdbuf->ndw+=4;
	rd_submit();
}

void static
_bosync(struct rd_context *__ct,struct rd_bo0 *__bo) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	_32_u *dw;
	dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	dw[0] = PKT3(EVENT_WRITE, 0);
	dw[1] = EVENT_TYPE(0x10)|EVENT_INDEX(4);
	dw+=2;
	dw[0] = PKT3(PFP_SYNC_ME, 0);
	dw[1] = 0;//dummy
	dw+=2;
	dw[0] = PKT3(SURFACE_SYNC, 3);
	dw[1] = (1<<0x17)|(1<<0x16)|(1<<0x12);
	dw[2] = 0xffffffff;
	dw[3] = 0;
	dw[4] = 0xa;
	__cmdbuf->ndw+=9;
}

void static*
_tex_backing(struct rd_context *__ctx, struct rd_tex *__tx){
  struct rd_bo *back;
  _int_u size;
  size = __tx->width*__tx->height*__tx->fmt.depth;
 	size = AMDGPU_GPU_PAGE_ALIGN(size);
	flue_printf("texture_size: %u-bytes.\n",size);
  back = rd_bo_create(__ctx->d->dev, size, 0, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE);
	return back;
}

void rd_tex_alloc_with_own_bo(struct rd_tex *__tx, struct rd_bo *__bo, _32_u __width, _32_u __height, struct flue_fmtdesc *__fmt) {
	assert(__fmt->depth>0);
	__tx->bo = __bo;
	__tx->clear_bo = NULL;
	__tx->fmt = *__fmt;
	__tx->width = __width;
	__tx->height = __height;
	printf("RD-SURFACE_NEW, w{%u}, h{%u}, format{depth: %u}\n", __width,__height,__fmt->depth);

}
void rd_tex_alloc(struct rd_context *__ctx, struct rd_tex *__tx, _32_u __width, _32_u __height, struct flue_fmtdesc *__fmt) {
	_64_u config = __tx->tx.config;
	flue_printf("new texture{config: %x, width: %u, height: %u, depth: %u, format: %s_%s}.\n",config,__width,__height,__fmt->depth,flue_nfmtstr(__fmt->nfmt,0),flue_dfmtstr(__fmt->dfmt,0));
	
	rd_tex_alloc_with_own_bo(__tx,NULL,__width,__height,__fmt);

	struct rd_bo *back;
	back = _tex_backing(__ctx,__tx);
	__tx->bo = back;
	ffly_fprintf(ffly_err,"tex_alloc: %x - %ux%u\n",back->v->addr,__width,__height);

//	__tx->clear_bo = rd_bo_create(__ctx->d->dev, size, 0, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE);
	__tx->clear = 0;//__tx->clear_bo->v->addr;
}

void static*
tex_new(struct rd_context *__ct, _int_u __width, _int_u __height, struct flue_fmtdesc *__fmt,_64_u __config) {
	struct rd_tex *tx;
	tx = m_alloc(sizeof(struct rd_tex));
	tx->tx.ptr = tx;
	tx->tx.config = __config;
	tx->tx.fmt = *__fmt;
	tx->tx.width = __width;
	tx->tx.height = __height;
	tx->tx.totsz = __width*__height*__fmt->depth;

	rd_tex_alloc(__ct,tx,__width,__height,__fmt);
	rd_bo_map(tx->bo);

	tx->fmt = *__fmt;
	tx->tx.tx.ptr = tx->bo->bo.cpu_ptr;
	
	flue_printf("new texture{cpu_ptr: %p}\n",tx->tx.tx.ptr);
	return tx;
}

//void rd_dpb_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf, struct rd_tex *__zb);
void rd_shader_data(rd_contextp __ctx, struct rd_shader *sh, _int_u __size);


void static _framebuffer(rd_contextp __ctx,_int_u __width, _int_u __height) {
	__ctx->bits |= RD_FRAMEBUFFER_DIRTY;
	flue_printf("frame buffer{%u, %u}\n",__width,__height);
	__ctx->fb = (__width)/*WIDTH*/|((__height)<<16/*HEIGHT*/);
}
void rd_tex_relinquish(struct rd_tex*);
void rd_sampler(struct rd_context*,struct flue_sampler*,_int_u,_64_u*);
struct flue_dd rd_drv_struc = {
	.bo_data = bo_data,
	.bo_map = bo_map,
	.bo_unmap = bo_unmap,
	.bo_destroy = bo_destroy,
	.ctx_new = rd_ctx_new,
	.ctx_destroy = rd_ctx_destroy,
	.submit = rd_submit,
	.configure = configure,
	.tex_new = tex_new,
	.tex_destroy = rd_tex_destroy,
	.rb_new = rd_rb_new,
	.rb_init = rd_rb_init,
	.rb_destroy = rd_rb_destroy,
	.scaf = {
		
	},
	.srb_init = _srb_init,
	.rbcopy = _rbcopy,
	.viewport = _viewport,
	.draw_array = drawarray,
	.resources = rd_resources,
	.rendertgs = _rendertgs,
	.initz = init_zb,
	.pushstate = _pushstate,
	.framebuffer = _framebuffer,
	.sh = {
		{{},0,SPI_SHADER_USER_DATA_VS_0},
		{{},0,SPI_SHADER_USER_DATA_PS_0}
	},
	.bo_sync = _bosync,
	.textures = rd_textures,
	.clear = _clear,
	.id = 0,
	.sampler = rd_sampler,
	.tex_relinquish = rd_tex_relinquish
};
