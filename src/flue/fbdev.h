#ifndef __flue__fbdev__h
#define __flue__fbdev__h
#include "common.h"
#include "../linux/fb.h"
/*
	for debugging
*/
struct flue_fbdev {
	struct flue_drawable d;
	flue_texp render;
	_int_u fb_len;
    struct fb_var_screeninfo fb_info;
    struct fb_fix_screeninfo fix;
	int fd;
};
struct flue_fbdev* fbdev_init(char const*);
void fbdev_deinit(struct flue_fbdev*);
struct flue_fbdev* flue_fbdev(void);
void flue_fbdev_cleanup(void);
void fbdev_swapbufs(struct flue_fbdev*);
#endif/*__flue__fbdev__h*/
