#include "common.h"
#include "../assert.h"
#include "../ihc/ihc.h"
#include "../m_alloc.h"
#include "../ihc/fsl/fsl.h"
#include "../flue/compiler/ima/ima.h"
/*
	how should this be delt with?

	what im thinking is this


	ycc shader.o shader.fsl

	shader.o - intermediate plans

	shader.path = "shader.o";
	
void compile_shader(){
	shader.path = "shader.o";
	- this step is made on compiler
	- it takes the object file and converts it to the device code for the device affiliated with the context
	flue_makeshader(shader);
}

void draw(){
	flue_useshader(shader);
	draw_shit();
}

void cleanup(){
	flue_shader_destory(shader);
}

	the question here is how to deal with other device arches?
	what happens if the device is switched????

	should we just on context change do a complete renovation?
	for every "shader in context"{
		recompile_to_device();
	}

	if you look to mesa, see v3d driver.

	we have procedures like.

	; v3d_shader_state_create
	; v3d_shader_state_delete

	and
	; v3d_vp_state_bind

	- the place where all the magic happens
	v3d_get_compiled_shader(){
		- setup and prep word

		- translating NIR to driver specific language
		v3d_compile();

		- shader is then uploaded to GPU memory using u_upload_data
		- other states are set and things like spill buffers are erected
	}

	v3d_shader_precompile(){
		if(FRAGMENT){
			v3d_get_compiled_shader();
		}else if (VERTEX){

		}else if (...)
		...
	}

	v3d_uncompiled_shader_create(){
		- empty function nothing special

		- just precompile of shader
		if(precompile){
			v3d_shader_precompile();
		}
	}

	- on shader state creation no compileing happens unless 'precompile' option is given
	v3d_shader_state_create(){
		v3d_uncompiled_shader_create();
	}


	- other then precompile its seen in function like v3d_update_compiled_vs();
	v3d_get_compiled_shader();


	- im also going to assume that active shader code manipulation takes place
	- im assuming this as doing such changing wouldent harm performance too much
	- for example low end graphics cards might/may define shader inputs in shader code???
	- or may do this to compress the shader code by using the same memory for diffrent functions???
	- as this would remove things such as pointers so there would be no "void(*func)()"
	- what im getting at is things such as jmp instructions would be very impactfull on performance
	- so you would be better off generating a new shader for this specific purpose

	void shaderA(){
		... some preshader stuff
		if(constant)
			algorithmB();
		}{
			algorithmA();
		}
		.. some postshader stuff
	}

	- so instead of this we could just generate the respective shader depending on these constant states.
	- for example diffrent lighting algorithm. NOTE: this is just me speculating on what could be done.


	v3d_update_compiled_vs(){
		if("if any states are not in need of updating"){
			- nothing to update; dont care
			return;
		}

		....
		v3d_get_compiled_shader();
		....
	}

	v3d_update_compiled_(vs,gs,fs) is called by v3d_update_compiled_shaders.

	v3d_update_compiled_shaders is then called apon by v3d_draw_vbo.

	v3d_draw_vbo(){
		v3d_update_compiled_shaders();
		v3d**_emit_state();

		...
		-submit commands to GPU
	}



	functions like v3d_*p_state_bind are used to set the shader backing(NIR?)

# SHADER
	- v3d_context->dirty bits can cause a shader compile.
	- shader compiling wont take place if shader key exists.

	v3d_get_compiled_shader() touches on struct v3d_uncompiled_shader

	this structure comes from the v3d_shader_state_create()

#RADEONSI

	- function that compiles the shader into its final form
	si_compile_shader(){
		struct nir_shader *nir;
		nir = si_get_nir_shader();
		si_llvm_compile_shader(nir);
	}

	si_compile_shader() is called by si_create_shader_variant()

	si_create_shader_variant(){
		...
		si_compile_shader();
		...
	}


	si_create_shader_variant() called apon by si_build_shader_variant() 

	si_build_shader_variant(){
		...
		si_create_shader_variant();
		...
	}

	and si_shader_select_with_key() calls si_build_shader_variant()

	si_shader_select() calls si_shader_select_with_key();


	and finaly si_update_shaders() calls si_shader_select();
	
	si_update_shaders()[
		...
		si_shader_select();
		...
	}

	and just like V3D we end are hunt at si_draw_vbo()
	si_draw_vbo(){
		...
		si_update_shaders();
		...
	}
CONCLUSION: shader compilation is state driven and user has not direct control over it. only from shader-lang to intermediate step

VERY IMPORTENT:
	- when it comes to compile the shaders linking may be done on shaders.
	- shaders are compiled apon vbo drawing, meaning shaders are subject to tweking
	- this tweking is dependent on context state, and what other shader stages are active.
	- for example, changes to shader code may be done to adjust shader outputs depending on next shader stage?

	- at what stage does the underlieing IML get translated to raw GPU code. ctx->Driver.LinkShader();


*/


static struct flue_yard yard;

void flue_prog_init(struct flue_prog *pg) {
	pg->secondary = NULL;
	pg->primary = NULL;
//	pg->bits = 0;
}

struct flue_prog* flue_prog(void) {
	struct flue_prog *pg;
	pg = m_alloc(sizeof(struct flue_prog));
	flue_prog_init(pg);
	return pg;
}

void flue_placement(void *__ptr, _int_u __comp, _64_u __placement) {
	struct fsl_symb *s = __ptr;

	_64_u *p = &s->s.em.placement;
	p[__comp] = __placement;
}

void* flue_allot(struct flue_prog *__pgm, char const *__ident, _int_u __len, _64_u __type, _64_u __offset, _64_u __placement) {
	struct fsl_symb *s;
	s = fsl_symb(__ident,__len);
	s->s.ign = -1;
	s->s.em.placement = __placement;
	s->s.em.id = 0;
	s->s.em.offset = __offset;
	s->s.em.val = 4;
	s->s.em.comp = 0xf;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->bits = _fsl_symb_buf;
	if (__type == 3) {
		s->s.em.bits |= IMA_PRIM; 
		__type = 1;
	}

	if (__type == 1) {
		s->s.em.bits |= IMA_INTR_K;
	}
	if(__type == 4) {
		s->s.em.ident = _ima_buf;
		s->s.em.bits |= IMA_SAMPLE;
	}else{
	if (__type == 0 || __type == 1) {
		s->s.em.ident = _ima_buf;
	}else {
		s->s.em.placement = 0;
		//constants
		s->s.em.ident = _ima_rel;
	}
	}
	return s;
}

void flue_prog_delete(struct flue_prog *__pgm) {
	m_free(__pgm->primary);
	if (__pgm->secondary != NULL) {
//		m_free(__pgm->secondary);
	}
	m_free(__pgm);
}
#include "../assert.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../io.h"
#include "../ys/as/as.h"
#include "../string.h"
#include "../tools.h"
void flue_prog_ready(struct flue_prog *__pgm) {
	FLUE_CTX->d->shader_init(FLUE_CTX->priv, __pgm);
}
void flue_prog_data(void *__shader, _int_u __size) {
	FLUE_CTX->d->shader_data(FLUE_CTX->priv,__shader,__size);
}
int static fd;
_32_u static _read(_ulonglong __ctx, void *__buf, _int_u __size, _8_s *__error) {
	read(fd, __buf, __size);
	as.off+=__size;
}
#include "../assert.h"
static _8_u *buf;
static _int_u off = 0,limit;
_32_u static _write(_ulonglong __ctx, void *__buf, _int_u __size, _8_s *__error) {
	assert(off<limit);
	mem_cpy(buf+off, __buf, __size);
	off+=__size;
}
/*
	NOTE: this function requires the context to be passed.

	purpose: final step into getting device specific code
*/
void ysa_loader(struct ys_mech *__m, struct flu_plarg *__arg, struct flue_shader *__pgm) {
	as.rw_ops.read = _read;
	as.rw_ops.write = _write;
	fd = open(__arg->p, O_RDONLY, 0);
	if (fd<0) {
		printf("failed to open source file, %s\n", __arg->p);
		return;
	}
	off = 0;
	__pgm->ps = 512*8;
	__pgm->primary = buf = m_alloc(__pgm->ps);
  	limit = __pgm->ps;
	struct stat st;
	fstat(fd, &st);
	as.off = 0;
	as.limit = st.st_size;
	ys_MACHINE(__m);
	ys_init();
	struct ys_state state;
	ysa_assemble(&state);
	ys_de_init();
	close(fd);
	__pgm->ps = off;//state.in_c;
	_int_u i;
	i = 0;
	for(;i != off/4;i++) {
		printf("DW%u: %x\n",i,((_32_u*)buf)[i]);
	}
	/*
		to root out errors

		ysa is error prone
	*/
	assert(off<4096);
}
void fsl_load(struct flue_prog *__shader, char const *__in) {
	struct fsl_symb *s;
	s = fsl_symb("pos",3);
	s->s.ign = -1;
	s->s.em.id = _ima_pos;
	s->s.em.val = 0;
	s->s.em.ident = _ima_exp;
	s->s.em.comp = 0xf;
	s->s.em.reg = NULL;

	s = fsl_symb("col",3);
	s->s.ign = -1;
	s->s.em.id = _ima_col;
	s->s.em.val = 0;
	s->s.em.comp = 0xf;
	s->s.em.reg = NULL;
	s->s.em.ident = _ima_exp;

	struct rd_reg r0 = {.rval=2};
    s = fsl_symb("fragpos",7);
    s->s.ign = -1;
    s->s.em.id = 0;
    s->s.em.val = 0;
    s->s.em.comp = 0x7;
    s->s.em.reg = &r0;
    s->s.em.ident = _ima_vec;
    s->s.em.placement = 0;

    s = fsl_symb("innorm",6);
    s->s.ign = -1;
    s->s.em.offset = 0;
    s->s.em.id = 0;
	s->s.em.val = 0;
    s->s.em.comp = 0xf;
    s->s.em.reg = NULL;
    s->s.em.ident = _ima_intrp;
    s->s.em.placement = 0;

	flue_printf("IMA_SH = %p.\n",__shader);
	rd_sh = __shader->shader;
	ihc_defout(&ihc_comm);
	fsl_compile(__in);
}

/*
	TODO:
	 this isent for FLUE, some high level is for this.
*/
void flue_file_shader(struct flue_prog *pg, char const *__path, _int_u __pathlen) {
    char ext[64];
    char c;
    _int_u i = __pathlen,j = 0;
    while((c = __path[i-1]) != '.') {
        ext[j++] = c;
        i--;
    }

    if (!j)
        return;//some error

    struct flu_plarg arg;
    ffly_fprintf(ffly_err, "compiling shader: %p.\n",pg->shader);
    fsl_load(pg,__path);

    char buf[128];
    mem_cpy(buf,__path,i);
    mem_cpy(buf+i,"s\0",2);
    arg.p = buf;
    ysa_loader(FLUE_CTX,&arg,pg);
}

