# ifndef __flue__slag__h
# define __flue__slag__h
# include "../y_int.h"
struct flue_slag {
	void *prog;

	_8_u *s, *d;
};

struct flue_slag* flue_slag(void*);
void flue_slagrun(struct flue_slag*);
# endif /*__flue__slag__h*/
