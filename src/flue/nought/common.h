# ifndef __flue__nought__common
# define __flue__nought__common
# include "../common.h"
# include "../types.h"
# include "../dd.h"
# include "../struc.h"
#define NT_TILE_SHFT 4
#define NT_TILE_SIZE (1<<NT_TILE_SHFT)
#define NT_TILE_MASK (NT_TILE_SIZE-1)
struct nt_rb;
#define NT_TR3_X0 0
#define NT_TR3_Y0 1
#define NT_TR3_Z0 2
#define NT_TR3_X1 3
#define NT_TR3_Y1 4
#define NT_TR3_Z1 5
#define NT_TR3_X2 6
#define NT_TR3_Y2 7
#define NT_TR3_Z2 8
#define NT_TR3_U0 0
#define NT_TR3_V0 1
#define NT_TR3_U1 2
#define NT_TR3_V1 3
#define NT_TR3_U2 4
#define NT_TR3_V2 5
struct nt_tri3 {
	_flue_float *buf;
	_flue_float *uv;
};

struct nt_shader {
	union {
		struct {
 		_8_u *secondary;
		_int_u ss;
		_8_u ram[1024];
		_64_u reg[256];
		_8_u *out;

		};
		_8_u tmp[SHADER_CSS];
	};
};

struct nt_dev {
	struct flue_dv sh;
};
struct nt_tex;
typedef struct nt_context {
    struct flue_ctx sh;
    struct ad_funcs *ad;

	struct nt_shader *ps;
	struct nt_shader *vs;
	struct flue_prog **pgm;
	struct nt_rb **render;
	struct nt_dev *d;
	struct nt_tex **tx;
	_flue_float *m;
} *nt_contextp;
#define NT_SHFROMPGM(__pgm)\
	((void*)0)
/*
	(((_64_u*)nt_shtab)+((__pgm)->shader_id))
*/
extern struct nt_shader nt_shtab[8];
extern nt_contextp flue_ctx;
nt_contextp nt_ctx_new(struct nt_dev*);
void nt_ctx_destroy(nt_contextp);

struct nt_bo {
	struct flue_bo sh;
};

#define clamp(__x, __to)\
	if (__x>__to){__x=__to;}
#define tatv(__t)((__t)->av = 0)
#define tav(__t)(!(__t)->av)
#define iftav(__t) if (tav(__t))
#define iftnav(__t) if (!tav(__t))
#define talter(__sf, __t)\
	iftnav(__t){tatv(__t);(__sf)->tb[(__sf)->_tb++].tp = __t;}
#define sfreset(__sf)\
	(__sf)->_tb = 0;
// not mine
#define NT_TILE_NM 0x01
#define NT_TILE_AV 0x00
struct nt_tile {
	_8_u *map0;
	_8_u *map1;
	_8_u bits;
	_8_s av;
	_32_u x, y;
};

struct nt_tb {
	struct nt_tile *tp;
};

struct nt_vtx {

};

typedef struct nt_tex {
	struct flue_tex sh;
	struct nt_surf *sf;
	void *map;
} *nt_texp;

struct nt_surf {
	_32_u w, h;
	_32_u width, height;
	_int_u tw, th;
	_int_u tn;
	struct nt_tile *tiles;
	struct nt_tb *tb;
	_32_u _tb;
	_8_u *map;
};

#define DEFINE_CONTEXT(__name)\
	nt_contextp __name = (nt_contextp)flue_comm.ctx->priv;
struct nt_rb {
	union {
		struct {
			struct nt_surf *sf;
			_flu_float *dpb;
		};
		_8_u tmp[0x100];
	};
	struct flue_rb0 sh;
};
void nt_vertex(nt_contextp, _flu_float*, _int_u);
void _nt_setpix(nt_contextp, _8_u*, _8_u*, _flu_float*);
void nt_line(nt_contextp, _flue_float*, _int_u);
void nt_lpcm(nt_contextp, _flue_float*);
void nt_ilpcm(nt_contextp, _flue_float*);
//tile.c
void nt_tile_ww(struct nt_tile*, _8_u*, _32_u, _32_u, _32_u, _32_u);
void nt_tile_wr(struct nt_tile*, _8_u*, _32_u, _32_u, _32_u, _32_u);
void nt_taltered(struct nt_surf*, struct nt_tile*);
//renderbuff.c
void nt_rb_new(struct nt_context*,struct nt_rb*);
void nt_rb_init(struct nt_context*,struct nt_rb*,_int_u,_int_u,struct flue_fmtdesc*);
void nt_rb_destroy(struct nt_context*,struct nt_rb*);

//bo.h
struct nt_bo* nt_bo_new(void);
void nt_bo_map(struct nt_bo*);
void nt_bo_unmap(struct nt_bo*);
void nt_bo_destroy(struct nt_bo*);

//shader.c
void nt_shader_ready(struct flue_prog*, struct nt_shader*);
void nt_shader_init(struct nt_context*, struct flue_prog*);
void nt_shader_run(struct nt_shader*);

//surface.c
struct nt_surf *nt_surf_new(_32_u, _32_u);
void nt_surf_destroy(struct nt_surf*);
void nt_surf_copy(struct nt_surf*, struct nt_surf*, _32_u, _32_u);
void nt_surf_read(struct nt_surf*, void*, _32_u, _32_u, _32_u, _32_u, _32_u);
//tri.c
void nt_raster_tgl(nt_contextp, struct nt_tri3*, _32_u);
extern struct flue_dd nt_drv_struc;
void nt_drv(void);
void nt_drv_deinit(void);
nt_texp nt_tex_new(void);
void nt_tex_destroy(nt_texp);
struct nt_dev *nt_dev_add(int);
# endif /*__flue__nought__common*/
