# ifndef __flue__nt__macros
# define __flue__nt__macros
#define tileat(__sf, __x, __y)\
	((__sf)->tiles+(__x)+((__y)*(__sf)->tw))
#define tilepx(__t, __x, __y)\
	((__t)->map0+(((__x)+((__y)<<NT_TILE_SHFT))*4))
#define dwmov(__dst, __src)\
	(*(_32_u*)(__dst) = *(_32_u*)(__src))
# endif /*__flue__nt__macros*/
