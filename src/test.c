# include "y_int.h"
# include "types.h"
# include "string.h"
# include "io.h"
//# include "havoc.h"
# include "wh.h"
# include "resource.h"
# include "linux/time.h"
# include "ffly_def.h"
# include "sched.h"
void _h_init(void){}
void _h_tick(void){}
void _h_deinit(void){}
void _h_msg(void){}
# include "maths.h"
# include "amdgpu.h"

void show_meminfo(int __fd) {
	struct drm_amdgpu_memory_info info;
	bzero(&info, sizeof(struct drm_amdgpu_memory_info));
	if (ffly_amdgpu_memory_info(__fd, &info) == -1) {
		printf("failed to get memory info.\n");
	}

	struct drm_amdgpu_heap_info *heap;

	heap = &info.vram;
	printf("vram:\n");
	printf("\ttotal heap size: %u\n", heap->total_heap_size);
	printf("\tusable heap size: %u\n", heap->usable_heap_size);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->max_allocation);

	heap = &info.cpu_accessible_vram;
	printf("vram:\n");
	printf("\ttotal heap size: %u\n", heap->total_heap_size);
	printf("\tusable heap size: %u\n", heap->usable_heap_size);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->max_allocation);

	heap = &info.gtt;
	printf("gtt:\n");
	printf("\ttotal heap size: %u\n", heap->total_heap_size);
	printf("\tusable heap size: %u\n", heap->usable_heap_size);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->max_allocation);

}
#define FORMAT(__fmt)(__fmt<<2)
#define COLOR_INVALID	FORMAT(0)
#define COLOR_8			FORMAT(1)
#define COLOR_16		FORMAT(2)
#define COLOR_8_8		FORMAT(3)
#define COLOR_32		FORMAT(4)
#define COLOR_8_8_8_8	FORMAT(10)
#define NTYP_SRGB		(6<<8)
#define DRAW_INDEX_IMMED	0x2e
#define DRAW_INDEX_AUTO		0x2d
#define SET_SH_REG			0x76
#define NUM_INST	0x2f
#define SPI_SHADER_PGM_LO_PS ((0xb020-0xb000)>>2)
#define SPI_SHADER_PGM_HI_PS ((0xb024-0xb000)>>2)
#define SPI_SHADER_PGM_LO_VS ((0xb120-0xb000)>>2)
#define SPI_SHADER_PGM_HI_VS ((0xb124-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC1_PS ((0xb028-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC1_VS ((0xb128-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC2_PS ((0xb02c-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC2_VS ((0xb12c-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC3_PS ((0xb01c-0xb000)>>2)
#define SPI_SHADER_PGM_RSRC3_VS ((0xb118-0xb000)>>2)
#define SPI_SHADER_LATE_ALLOC_VS ((0xb11c-0xb000)>>2)
#define CONFIG_BASE 0x30000
#define CONTEXT_BASE 0x28000
#define SET_CONFIG_REG 0x79
#define SET_CONTEXT_REG 0x69
#define VGT_PRIM_TYPE   (((0xc242<<2)-CONFIG_BASE)>>2)
#define SPI_PS_INPUT_ENA    ((0x286cc-CONTEXT_BASE)>>2)
#define SPI_SHADER_POS_FORMAT		((0x2870c-CONTEXT_BASE)>>2)
#define DI_PT_TRILIST 4
#define PKT3(__op, __n) ((3<<30)|(__op<<8)|(__n<<16))
#define PKT0(__reg, __cnt)\
	(__cnt<<16)|(__reg>>2)/*dw only*/
#define SPI_SHADER_USER_DATA_PS0 ((0xb030-0xb000)>>2)
#define SPI_SHADER_USER_DATA_PS1 (((0xb030-0xb000)+4)>>2)
#define SPI_SHADER_USER_DATA_PS2 (((0xb030-0xb000)+8)>>2)
#define SPI_SHADER_USER_DATA_PS3 (((0xb030-0xb000)+12)>>2)

#define SPI_SHADER_USER_DATA_VS0 ((0xb130-0xb000)>>2)
#define SPI_SHADER_USER_DATA_VS1 (((0xb130-0xb000)+4)>>2)
#define SPI_SHADER_USER_DATA_VS2 (((0xb130-0xb000)+8)>>2)
#define SPI_SHADER_USER_DATA_VS3 (((0xb130-0xb000)+12)>>2)

#define VGT_IDX_32	1
#define VGT_IDX_16	0
#define INDEX_TYPE 0x2a
#define CONTEXT_CONTROL 0x28
#define CB_COLOR0_BASE	((0x28c60-CONTEXT_BASE)>>2)
#define CB_COLOR0_INFO	((0x28c70-CONTEXT_BASE)>>2)
#define CB_COLOR0_PITCH	((0x28c64-CONTEXT_BASE)>>2)
#define CB_COLOR0_SLICE	((0x28c68-CONTEXT_BASE)>>2)
#define CB_COLOR0_VIEW	((0x28c6c-CONTEXT_BASE)>>2)
#define CB_COLOR0_ATTR	((0x28c74-CONTEXT_BASE)>>2)
#define CB_COLOR0_CMASK  ((0x28e20-CONTEXT_BASE)>>2)
#define CB_COLOR0_FMASK  ((0x28c84-CONTEXT_BASE)>>2)
#define CB_COLOR0_CMASK_SLICE ((0x28c80-CONTEXT_BASE)>>2)
#define CB_COLOR0_FMASK_SLICE ((0x28c88-CONTEXT_BASE)>>2)
#define CB_COLOR0_CLEAR_WORD0  ((0x28c8c-CONTEXT_BASE)>>2)
#define CB_COLOR0_CLEAR_WORD1  ((0x28c90-CONTEXT_BASE)>>2)
#define CB_TARGET_MASK ((0x28238-CONTEXT_BASE)>>2)
#define CB_COLOR_CONTROL ((0x28808-CONTEXT_BASE)>>2)
#define CB_SHADER_MASK ((0x2823c-CONTEXT_BASE)>>2)
#define CB_BLEND0_CONTROL ((0x28780-CONTEXT_BASE)>>2)
#define SURFACE_SYNC 0x43
#define RASTER_CONFIG ((0x28350-CONTEXT_BASE)>>2)
#define RASTER_CONFIG_1 ((0x28354-CONTEXT_BASE)>>2)
#define VGT_PRIMITIVEID_EN ((0x28a84-CONTEXT_BASE)>>2)
#define VGT_SHADER_STAGES_EN ((0x28b54-CONTEXT_BASE)>>2)
#define VGT_STRMOUT_CONFIG ((0x28b94-CONTEXT_BASE)>>2)
#define VGT_PRIMITIVEID_RESET ((0x28a8c-CONTEXT_BASE)>>2)
#define DB_DEPTH_CONTROL ((0x28800-CONTEXT_BASE)>>2)
#define DB_STENCIL_CONTROL ((0x2842c-CONTEXT_BASE)>>2)
#define DB_RENDER_CONTROL ((0x28000-CONTEXT_BASE)>>2)
#define DB_RENDER_OVERRIDE ((0x2800c-CONTEXT_BASE)>>2)
#define DB_RENDER_OVERRIDE2  ((0x28010-CONTEXT_BASE)>>2)
#define DB_STENCILREFMASK ((0x28430-CONTEXT_BASE)>>2)
#define DB_STENCILREFMASK_BF ((0x28434-CONTEXT_BASE)>>2)
#define PA_CL_CLIP_CNTL ((0x28810-CONTEXT_BASE)>>2)
#define SPI_INTERP_CONTROL_0 ((0x286d4-CONTEXT_BASE)>>2)
#define PA_SU_VTX_CNTL ((0x28be4-CONTEXT_BASE)>>2)
#define PA_SU_SC_MODE_CNTL ((0x28814-CONTEXT_BASE)>>2)
#define PA_SC_MODE_CNTL_0 ((0x28a48-CONTEXT_BASE)>>2)
#define PA_SC_MODE_CNTL_1 ((0x28a4c-CONTEXT_BASE)>>2) 
#define PA_SU_PRIM_FILTER_CNTL ((0x2882c-CONTEXT_BASE)>>2)
#define PA_CL_VTE_CNTL ((0x28818-CONTEXT_BASE)>>2)
#define PA_CL_VS_OUT_CNTL ((0x2881c-CONTEXT_BASE)>>2)
#define VGT_REUSE_OFF ((0x28ab4-CONTEXT_BASE)>>2)
#define SPI_PS_INPUT_CNTL_0 ((0x28644-CONTEXT_BASE)>>2)
#define SPI_PS_INPUT_CNTL_1 (((0x28644+4)-CONTEXT_BASE)>>2)
#define SPI_PS_INPUT_CNTL_2 (((0x28644+8)-CONTEXT_BASE)>>2)
#define DB_SHADER_CONTROL ((0x2880c-CONTEXT_BASE)>>2)
#define PA_SC_LINE_CNTL ((0x28bdc-CONTEXT_BASE)>>2)
#define SPI_VS_OUT_CONFIG ((0x286c4-CONTEXT_BASE)>>2)
#define SPI_PS_IN_CONTROL ((0x286d8-CONTEXT_BASE)>>2)
#define SPI_PS_INPUT_ADDR ((0x286d0-CONTEXT_BASE)>>2)
#define PA_SC_EDGERULE	((0x28230-CONTEXT_BASE)>>2)
#define VGT_GS_MODE	((0x28a40-CONTEXT_BASE)>>2)
#define PA_SU_POINT_SIZE ((0x28a00-CONTEXT_BASE)>>2)
#define PA_SU_POINT_MINMAX ((0x28a04-CONTEXT_BASE)>>2)
#define VGT_INSTANCE_STEP_RATE_0 ((0x28aa0-CONTEXT_BASE)>>2)
#define VGT_INSTANCE_STEP_RATE_1 ((0x28aa4-CONTEXT_BASE)>>2)
#define SPI_SHADER_COL_FORMAT ((0x28714-CONTEXT_BASE)>>2)
#define CB_BLEND_RED ((0x28414-CONTEXT_BASE)>>2)
#define CB_DCC_CONTROL ((0x28424-CONTEXT_BASE)>>2)
#define PA_CL_GB_VERT_CLIP_ADJ ((0x28be8-CONTEXT_BASE)>>2)
#define PA_SC_AA_MASK_X0Y0_X1Y0 ((0x28c38-CONTEXT_BASE)>>2)
#define PA_SC_AA_MASK_X0Y1_X1Y1 ((0x28c3c-CONTEXT_BASE)>>2)
#define DB_STENCIL_CLEAR ((0x28028-CONTEXT_BASE)>>2)
#define DB_DEPTH_CLEAR ((0x2802c-CONTEXT_BASE)>>2)
#define DB_ALPHA_TO_MASK ((0x28b70-CONTEXT_BASE)>>2)
#define DB_COUNT_CONTROL ((0x28004-CONTEXT_BASE)>>2)
#define PA_SC_AA_CONFIG ((0x28be0-CONTEXT_BASE)>>2)
#define PA_SC_SCREEN_SCISSOR_TL ((0x28030-CONTEXT_BASE)>>2)
#define PA_SC_SCREEN_SCISSOR_BR ((0x28034-CONTEXT_BASE)>>2)
#define PA_SC_WINDOW_SCISSOR_BR ((0x28208-CONTEXT_BASE)>>2)
#define PA_SC_WINDOW_SCISSOR_TL ((0x28204-CONTEXT_BASE)>>2)
#define PA_SC_GENERIC_SCISSOR_BR ((0x28244-CONTEXT_BASE)>>2)
#define PA_SC_GENERIC_SCISSOR_TL ((0x28240-CONTEXT_BASE)>>2)
#define PA_SC_VPORT_SCISSOR_BR0 ((0x28254-CONTEXT_BASE)>>2)
#define PA_SC_VPORT_SCISSOR_TL0 ((0x28250-CONTEXT_BASE)>>2)
#define PA_CL_NANINF_CNTL ((0x28820-CONTEXT_BASE)>>2)
#define PA_SC_VPORT_ZMAX ((0x282d4-CONTEXT_BASE)>>2)
#define PA_SC_VPORT_ZMIN ((0x282d0-CONTEXT_BASE)>>2)
#define PA_SC_HARDWARE_SCREEN_OFFSET ((0x28234-CONTEXT_BASE)>>2)
#define SPI_BARYC_CNTL ((0x286e0-CONTEXT_BASE)>>2)
#define DB_STENCILREFMASK ((0x28430-CONTEXT_BASE)>>2)
#define DB_DEPTH_BOUNDS_MAX ((0x28024-CONTEXT_BASE)>>2)
#define DB_DEPTH_BOUNDS_MIN ((0x28020-CONTEXT_BASE)>>2)
#define PA_SU_POLY_OFFSET_CLAMP ((0x28b7c-CONTEXT_BASE)>>2)
#define DB_Z_INFO ((0x28040-CONTEXT_BASE)>>2)
#define DB_DEPTH_INFO ((0x2803c-CONTEXT_BASE)>>2)
#define DB_STENCIL_INFO ((0x28044-CONTEXT_BASE)>>2)
#define SPI_SHADER_Z_FORMAT ((0x28710-CONTEXT_BASE)>>2)
#define DB_Z_READ_BASE ((0x28048-CONTEXT_BASE)>>2)
#define DB_Z_WRITE_BASE ((0x28050-CONTEXT_BASE)>>2)
#define DB_DEPTH_SIZE ((0x28058-CONTEXT_BASE)>>2)
#define DB_DEPTH_SLIZE ((0x2805c-CONTEXT_BASE)>>2)
#define DB_DEPTH_VIEW ((0x28008-CONTEXT_BASE)>>2)
#define SPI_TMPRING_SIZE ((0x286e8-CONTEXT_BASE)>>2)
#define PA_CL_VPORT_XSCALE ((0x2843c-CONTEXT_BASE)>>2)
#define VGT_MULTI_PRIM_IB_RESET_EN ((0x028a94-CONTEXT_BASE)>>2)
#define VGT_MULTI_PRIM_IB_RESET_INDX ((0x02840c-CONTEXT_BASE)>>2)
#define VGT_MAX_VTX_INDX ((0x028400-CONTEXT_BASE)>>2)
#define VGT_MIN_VTX_INDX ((0x028404-CONTEXT_BASE)>>2)
#define VGT_INDX_OFFSET ((0x028408-CONTEXT_BASE)>>2)
#define VGT_VTX_CNT_EN ((0x028ab8-CONTEXT_BASE)>>2)
#define RB_MAP_PKR0(x)              ((x) << 0)
#define RB_MAP_PKR0_MASK            (0x3 << 0)
#define RB_MAP_PKR1(x)              ((x) << 2)
#define RB_MAP_PKR1_MASK            (0x3 << 2)
#define RB_XSEL2(x)             ((x) << 4)
#define RB_XSEL2_MASK               (0x3 << 4)
#define RB_XSEL                 (1 << 6)
#define RB_YSEL                 (1 << 7)
#define PKR_MAP(x)              ((x) << 8)
#define PKR_MAP_MASK                (0x3 << 8)
#define PKR_XSEL(x)             ((x) << 10)
#define PKR_XSEL_MASK               (0x3 << 10)
#define PKR_YSEL(x)             ((x) << 12)
#define PKR_YSEL_MASK               (0x3 << 12)
#define SC_MAP(x)               ((x) << 16)
#define SC_MAP_MASK             (0x3 << 16)
#define SC_XSEL(x)              ((x) << 18)
#define SC_XSEL_MASK                (0x3 << 18)
#define SC_YSEL(x)              ((x) << 20)
#define SC_YSEL_MASK                (0x3 << 20)
#define SE_MAP(x)               ((x) << 24)
#define SE_MAP_MASK             (0x3 << 24)
#define SE_XSEL(x)              ((x) << 26)
#define SE_XSEL_MASK                (0x3 << 26)
#define SE_YSEL(x)              ((x) << 28)
#define SE_YSEL_MASK                (0x3 << 28)

/* mmPA_SC_RASTER_CONFIG_1 mask */
#define SE_PAIR_MAP(x)              ((x) << 0)
#define SE_PAIR_MAP_MASK            (0x3 << 0)
#define SE_PAIR_XSEL(x)             ((x) << 2)
#define SE_PAIR_XSEL_MASK           (0x3 << 2)
#define SE_PAIR_YSEL(x)             ((x) << 4)
#define SE_PAIR_YSEL_MASK           (0x3 << 4)

#define INDEX_BASE              0x26
#define INDEX_BUFFER_SIZE           0x13
#define CLEAR_STATE 0x12
//in blocks ie 1 = 4-rgba
#define EVENT_WRITE 0x46
#define EVENT_WRITE_EOP 0x47
#define PFP_SYNC_ME 0x42
#define W 512
#define H 512
#define EXP_MRT0 0
#define ENDPGM (1<<16)


#define EXP_DW0(__en, __tgt, __compr, __done, __vm)\
	((__en)|((__tgt)<<4)|((__compr)<<10)|((__done)<<11)|((__vm)<<12)|(0x31<<26))
#define EXP_DW1(__vs0, __vs1, __vs2, __vs3)\
	((__vs0)|((__vs1)<<8)|((__vs2)<<16)|((__vs3)<<24))


#define SOPP(__op)\
	(__op|(0x17f<<23))
#define S_BUF_LOAD_DW 0x08
#define S_BUF_LOAD_DWX4 0x0a
#define MUBUF_DW0(__offset, __offen, __idxen, __glc, __lds, __slc, __op)\
	((__offset)|((__offen)<<12)|((__idxen)<<13)|((__glc)<<14)|((__lds)<<16)|((__slc)<<17)|((__op)<<18)|(0x38<<26))
#define MUBUF_DW1(__vadr, __vdat, __srsrc, __tfe, __soffset)\
	((__vadr)|((__vdat)<<8)|((__srsrc)<<16)|((__tfe)<<23)|((__soffset)<<24))

#define MTBUF_DW0(__offset, __offen, __idxen, __glc, __op, __df, __nf)\
	((__offset)|((__offen)<<12)|((__idxen)<<13)|((__glc)<<14)|((__op)<<15)|((__df)<<19)|((__nf)<<23)|(0x3a<<26))
#define MTBUF_DW1(__vadr, __vdat, __srsrc, __slc, __tfe, __soffset)\
	((__vadr)|((__vdat)<<8)|((__srsrc)<<16)|((__slc)<<22)|((__tfe)<<23)|((__soffset)<<24))

#define SMEM_DW0(__sb, __sd, __glc, __imm, __op)\
	((__sb)|((__sd)<<6)|((__glc)<<16)|((__imm)<<17)|((__op)<<18)|(0x30<<26))
#define SMEM_DW1(__offset)\
	(__offset)
#define VOP1_DW(__src0, __op, __vdst)\
	((__src0)|((__op)<<9)|((__vdst)<<17)|(0x3f<<25))
#define VOP2_DW(__src0, __vsrc1, __vdst, __op)\
	((__src0)|((__vsrc1)<<9)|((__vdst)<<17)|((__op)<<25))
#define SOP2_DW(__ssrc0, __ssrc1, __sdst, __op)\
	((__ssrc0)|((__ssrc1)<<8)|((__sdst)<<16)|((__op)<<23)|(0x02<<30))
#define V_MOV_B32 0x01
#define S_XOR_B32 16
#define V_AND_B32 19
#define BUF_ATOM_ADD 66
#define UT_BUF_LOAD_DW 0x14
#define S_BUF_STORE_DW 0x18
#define UT_BUF_STORE_DW4 0x1f
#define UT_BUF_STORE_DW 0x1c
#define UT_BUF_LOAD_DW4 0x17
#define S_LOAD_DW4 0x02
#define S_STORE_DW 0
#define S_MOV_B32 0
#define TB_LOAD_FMT_XYZW 3
#define TB_STORE_FMT_XYZW 7
#define FLAT_DW0(__glc, __slc, __op)\
	(((__glc)<<16)|((__slc)<<17)|((__op)<<18)|(0x37<<26))
#define FLAT_DW1(__addr, __data, __tfe, __vdst)\
	((__addr)|((__data)<<8)|((__tfe)<<23)|((__vdst)<<24))
#define S_NOP(__x)\
	((__x)|(0x17f<<23))
#define S_WAITCNT(__x)\
	((__x)|(0x0c<<16)|(0x17f<<23))
struct amdgpu_bo *shader_bo;
void *shader_ptr;
_64_u shader_addr;
#define SHADER_PS 0
#define SHADER_VS 512
void shader_init(struct amdgpu_dev *dv) {
	shader_bo = amdgpu_bo_alloc(dv, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_cpu_map(dv, shader_bo);
	shader_ptr = shader_bo->cpu_ptr;
	_32_u *dw = shader_ptr;
	
//	*dw = SMEM_DW0(0/*base sgpr??(SRC)*/, 4/*sgpr(DST)*/, 1, 1, S_BUF_LOAD_DW);
//	dw[1] = SMEM_DW1(0/*offset from sgpr*/);
//	dw[2] = VOP1_DW(0/*vgpr-1(DST)*/, V_MOV_B32, 4/*sgpr-0(SRC)*/);
//	dw[3] = VOP1_DW(1/*vgpr-1(DST)*/, V_MOV_B32, 5/*sgpr-0(SRC)*/);
//	dw[4] = VOP1_DW(2/*vgpr-1(DST)*/, V_MOV_B32, 6/*sgpr-0(SRC)*/);
//	dw[5] = VOP1_DW(3/*vgpr-1(DST)*/, V_MOV_B32, 7/*sgpr-0(SRC)*/);
	dw[0] = VOP1_DW(242/*sgpr(SRC)*/, V_MOV_B32, 0/*vgpr(DST)*/);

	dw[1] = EXP_DW0(0x1, 0/*MRT-0*/, 0, 1, 0);
	dw[2] = EXP_DW1(0, 1, 2, 3);
//	dw[8] = EXP_DW0(0x1, 8/*Z-0*/, 0, 1, 0);
//	dw[9] = EXP_DW1(0, 1, 2, 3);
	dw[3] = SOPP(ENDPGM);

	{
		_int_u i;
		i = 0;
		for(;i != 4;i++) {
			printf("PS-DW%u: %x.\n", i,dw[i]);
		}
	}
	dw = dw+SHADER_VS;

//	*dw = SMEM_DW0(0/*(SRC)*/, 4/*sgpr(DST)*/, 0, 1, S_BUF_LOAD_DWX4);
//	dw[1] = SMEM_DW1(0);

//	dw[2] = VOP1_DW(129/*sgpr(SRC)*/, V_MOV_B32, 4/*vgpr(DST)*/);
//	dw[3] = VOP1_DW(5/*sgpr(SRC)*/, V_MOV_B32, 5/*vgpr(DST)*/);
//	dw[4] = VOP1_DW(6/*sgpr(SRC)*/, V_MOV_B32, 6/*vgpr(DST)*/);
//	dw[0] = VOP1_DW(240/*sgpr(SRC)*/, V_MOV_B32, 0/*vgpr(DST)*/);

//	dw[0] = SOP2_DW(8, 8, 8, S_XOR_B32);

//	dw[1] = VOP2_DW(8, 8, 8, V_AND_B32);
//	dw[8] = MUBUF_DW0(4*4*3, 0, 1, 0, 0, 0, BUF_ATOM_ADD);
//	dw[9] = MUBUF_DW1(4, 1/*SRC*/, 0/*DST(RESOURCE CONSTANT)*/, 0, 128);	
//	dw[2] = MUBUF_DW0(4*4*3, 0, 0, 0, 0, 0, UT_BUF_STORE_DW);
//	dw[3] = MUBUF_DW1(8, 0/*SRC*/, 0/*DST(RESOURCE CONSTANT)*/, 0, 128);
 
//	dw[0] = VOP1_DW(6/*sgpr(SRC)*/, V_MOV_B32, 0/*vgpr(DST)*/);
//	dw[0] = SOP2_DW(8, 8, 8, S_XOR_B32);
//	dw[1] = VOP2_DW(8, 8, 8, V_AND_B32);
//	dw[2] = MUBUF_DW0(4*4*3, 0, 0, 0, 0, 0, UT_BUF_STORE_DW4);
//	dw[3] = MUBUF_DW1(8, 0/*SRC*/, 0/*DST(RESOURCE CONSTANT)*/, 0, 128);
//	dw+=4;

//	dw[2] = FLAT_DW0(0, 0, 28);
//	dw[3] = FLAT_DW1(0, 0, 0, 8);
//	dw[0] = SOP2_DW(8, 8, 8, S_XOR_B32);
//	dw[1] = VOP2_DW(8, 8, 8, V_AND_B32);

//	dw[2] = MTBUF_DW0(0, 0, 0, 0, TB_STORE_FMT_XYZW, 4, 7);
//	dw[3] = MTBUF_DW1(8, 0, 0/*DST(CONSTANT)*/, 0, 0, 128);
//	dw[0] = SOP2_DW(8, 8, 8, S_XOR_B32);
//	dw[1] = VOP2_DW(8, 8, 8, V_AND_B32);

//	dw[0] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 10/*vgpr(DST)*/);
//	dw[1] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 11/*vgpr(DST)*/);
//	dw[2] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 12/*vgpr(DST)*/);
//	dw[0] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 10/*vgpr(DST)*/);
//	dw[1] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 11/*vgpr(DST)*/);
//	dw[2] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 12/*vgpr(DST)*/);
//	dw[3] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 13/*vgpr(DST)*/);
//	dw+=4;
/*
#define MTBUF_DW0(__offset, __offen, __idxen, __glc, __op, __df, __nf)\
    ((__offset)|((__offen)<<12)|((__idxen)<<13)|((__glc)<<14)|((__op)<<15)|((__df)<<19)|((__nf)<<23)|(0x3a<<26))
#define MTBUF_DW1(__vadr, __vdat, __srsrc, __slc, __tfe, __soffset)\
    ((__vadr)|((__vdat)<<8)|((__srsrc)<<16)|((__slc)<<22)|((__tfe)<<23)|((__soffset)<<24))

*/

//	dw[0] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 9/*vgpr(DST)*/);
//	dw[1] = MTBUF_DW0(0, 0, 0, 0/*miss L1 go for L2*/, TB_LOAD_FMT_XYZW, 14, 7);
//	dw[2] = MTBUF_DW1(9, 4, 0/*DST(CONSTANT)*/, 0, 0, 128);
//	dw[3] = S_WAITCNT(0);
//	dw[4] = VOP1_DW(128/*sgpr(SRC)*/, V_MOV_B32, 9/*vgpr(DST)*/);
//	dw[5] = MTBUF_DW0(4*4*4, 0, 0, 0, TB_STORE_FMT_XYZW, 14, 7);
//	dw[6] = MTBUF_DW1(9, 0, 0/*DST(CONSTANT)*/, 0, 0, 128);
//	dw[7] = S_WAITCNT(0);
//dw[9] = MUBUF_DW0(4*4*4, 0, 0, 0, 0, 0, UT_BUF_STORE_DW);
//	dw[10] = MUBUF_DW1(8, 0/*SRC*/, 0/*DST(RESOURCE CONSTANT)*/, 0, 128);
//	dw[11] = S_WAITCNT(0);
//	dw+=8;
//	*dw = EXP_DW0(0xf, 12, 0, 1, 0);
//	dw[1] = EXP_DW1(4, 5, 6, 7);
	/*
		export
		vgpr-0,1,2,3
	*/
//	dw[2] = SOPP(ENDPGM);
	dw[0] = 0xebf18000;
	dw[1] = 0x80000409;
	dw[2] = 0xbf8c0000;
	dw[3] = 0xebf38040;
	dw[4] = 0x80000409;
	dw[5] = 0xbf8c0000;
	dw[6] = 0xc40008cf;
	dw[7] = 0x7060504;
	dw[8] = 0xbf810000;

	shader_addr = amdgpu_va_alloc(dv->vam, AMDGPU_GPU_PAGE_SIZE, 0)->addr;
	amdgpu_bo_va_op(dv,shader_bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, shader_addr, AMDGPU_GPU_PAGE_ALIGN(shader_bo->size));
	printf("pixel shader: %lx.\nvertex shader: %lx.\n", shader_addr, shader_addr+SHADER_VS);
	printf("PS: %s, VS: %s.\n", !((shader_addr+SHADER_PS)&0xff)?"YES":"NO", !((shader_addr+SHADER_VS)&0xff)?"YES":"NO");
	_int_u i;
	i = 0;
	for(;i != 11;i++) {
		printf("DW%u:	%x\n",i,(dw-8)[i]);
	}
}

void shader_deinit(struct amdgpu_dev *dv) {
	amdgpu_bo_cpu_unmap(shader_bo);
	amdgpu_bo_va_op(dv,shader_bo, AMDGPU_VA_OP_UNMAP, 0, 0, shader_addr, AMDGPU_GPU_PAGE_ALIGN(shader_bo->size));
	amdgpu_bo_free(dv,shader_bo);
}

#define SIZE (W*H*4)
#define RT_SIZE ((SIZE+AMDGPU_GPU_PAGE_MASK)&~AMDGPU_GPU_PAGE_MASK)
struct amdgpu_bo *rt_bo;
void *rt_ptr;
_64_u rt_addr;
void rt_init(struct amdgpu_dev *dv) {
	rt_bo = amdgpu_bo_alloc(dv, RT_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_cpu_map(dv,rt_bo);
	rt_ptr = rt_bo->cpu_ptr;
	rt_addr = amdgpu_va_alloc(dv->vam, RT_SIZE, 0)->addr;
	amdgpu_bo_va_op(dv,rt_bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE,
		0, rt_addr, AMDGPU_GPU_PAGE_ALIGN(rt_bo->size));
	mem_set(rt_ptr, 255, rt_bo->size);
	printf("aligned to 256? %s.\n", !(rt_addr&0xff)?"YES":"NO");
	printf("render target: %lx, size: %lu.\n", rt_addr, rt_bo->size);
}

void rt_deinit(struct amdgpu_dev *dv) {
	amdgpu_bo_cpu_unmap(rt_bo);
	amdgpu_bo_va_op(dv,rt_bo, AMDGPU_VA_OP_UNMAP, 0, 0, rt_addr, AMDGPU_GPU_PAGE_ALIGN(rt_bo->size));
	amdgpu_bo_free(dv,rt_bo);
}
# include "linux/fb.h"
# include "linux/unistd.h"
# include "linux/ioctl.h"
# include "linux/fcntl.h"
# include "linux/mman.h"
struct fb_var_screeninfo fb_info;
struct fb_fix_screeninfo fix;
void *fb_pixels;
int static fb;
_int_u fb_len;
void dumprt(void) {
	_int_u x, y;
	y = 0;
	for(;y != H;y++) {
		x = 0;
		for(;x != W;x++) {
			_8_u *s, *d;
			s = ((_8_u*)rt_ptr)+((x+(y*W))*4);
			d = ((_8_u*)fb_pixels)+((x*4)+(y*fix.line_length));
			*(_32_u*)d = *(_32_u*)s;
		}
	}
}

#define ZB_W 512
#define ZB_H 512
#define _ZBS (ZB_W*ZB_H*4)
#define ZBS ((_ZBS+AMDGPU_GPU_PAGE_MASK)&~AMDGPU_GPU_PAGE_MASK)
struct amdgpu_bo *zb_bo;
void *zb_ptr;
_64_u zb_addr;
void zb_init(struct amdgpu_dev *dv) {
	zb_bo = amdgpu_bo_alloc(dv, ZBS, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_cpu_map(dv,zb_bo);
	zb_ptr = zb_bo->cpu_ptr;
	zb_addr = amdgpu_va_alloc(dv->vam, ZBS, 0)->addr;
	amdgpu_bo_va_op(dv,zb_bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, zb_addr, AMDGPU_GPU_PAGE_ALIGN(zb_bo->size));
	mem_set(zb_ptr, 0xff, zb_bo->size);
}

void zb_deinit(struct amdgpu_dev *dv) {
	amdgpu_bo_cpu_unmap(zb_bo);
	amdgpu_bo_va_op(dv,zb_bo, AMDGPU_VA_OP_UNMAP, 0, 0, zb_addr, AMDGPU_GPU_PAGE_ALIGN(zb_bo->size));
	amdgpu_bo_free(dv,zb_bo);
}

// user data
struct amdgpu_bo *ud_bo;
void *ud_ptr;
_64_u ud_addr;
void ud_init(struct amdgpu_dev *dv) {
	ud_bo = amdgpu_bo_alloc(dv, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_VRAM, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_cpu_map(dv,ud_bo);
	ud_ptr = ud_bo->cpu_ptr;
	ud_addr = amdgpu_va_alloc(dv->vam, AMDGPU_GPU_PAGE_SIZE, 0)->addr;
	amdgpu_bo_va_op(dv,ud_bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE,
		0, ud_addr, AMDGPU_GPU_PAGE_ALIGN(ud_bo->size));
	printf("user data: %lx, handle: %u.\n", ud_addr, ud_bo->handle);
	mem_set(ud_ptr, 0xff, ud_bo->size);
}
//#define NODISP
//#define DEBUG
void ud_deinit(struct amdgpu_dev *dv) {
	amdgpu_bo_cpu_unmap(ud_bo);
	amdgpu_bo_va_op(dv,ud_bo, AMDGPU_VA_OP_UNMAP, 0, 0, ud_addr, AMDGPU_GPU_PAGE_ALIGN(ud_bo->size));
	amdgpu_bo_free(dv,ud_bo);
}
# include "assert.h"
_err_t main(int __argc, char const *__argv[]) {
#ifndef NODISP
	fb = open("/dev/fb0", O_RDWR, 0);
	if (fb<0) {
		printf("failed to open FB.\n");
		return -1;
	}
	ioctl(fb, FBIOGET_VSCREENINFO, &fb_info);
	ioctl(fb, FBIOGET_FSCREENINFO, &fix);
	fb_len = fb_info.xres*fb_info.yres*4;
	fb_pixels = mmap(NULL, fb_len, PROT_READ|PROT_WRITE, MAP_SHARED, fb, 0);
	mem_set(fb_pixels, 76, fb_len);
#endif
	struct amdgpu_dev *dv;
	int fd;
	fd = ffly_drm_open("/dev/dri/card0");

	dv = amdgpu_dev_init(fd);
	int r;
	show_meminfo(fd);
#ifdef DEBUG
    struct amdgpu_info info;
    if (amdgpu_info(fd, &info) == -1) {
        printf("failed to get amdgpu info.\n");
    }
    printf("gfx sclk: %u\n", info.gfx_sclk);
    printf("gfx mclk: %u\n", info.gfx_mclk);
    printf("temp: %f-degree\n", ((float)info.temp)*0.001);
    printf("load: %u\n", info.load);
    printf("power: %ld-watts\n", info.avg_power);

	amdgpu_dev_de_init(dv);
	close(fd);
	return 0;
#endif


	struct amdgpu_bo *bo;
	struct amdgpu_ctx *ctx;
	bo = amdgpu_bo_alloc(dv, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	ctx = amdgpu_ctx_create(dv, 0);
	// map buffer object to cpu address space
	amdgpu_bo_cpu_map(dv,bo);
	void *cpu_ptr;

	cpu_ptr = bo->cpu_ptr;

	_64_u addr;
	// allocate virtral address space
	addr = amdgpu_va_alloc(dv->vam, AMDGPU_GPU_PAGE_SIZE, 0)->addr;
	// map that space
	amdgpu_bo_va_op(dv,bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));
	shader_init(dv);
	rt_init(dv);
	ud_init(dv);
	zb_init(dv);
#define N (5+310+36+30+7)
	assert((N*4)<AMDGPU_GPU_PAGE_SIZE);
	_32_u *ptr = (_32_u*)cpu_ptr;//here we work in dwords

	*ptr = PKT3(CONTEXT_CONTROL, 1);
	ptr[1] = 1<<31;
	ptr[2] = 1<<31;
	ptr[3] = PKT3(CLEAR_STATE, 0);
	ptr[4] = 0;
    ptr+=5;

	*ptr = PKT3(SET_CONTEXT_REG, 1);
	ptr[1] = CB_COLOR0_BASE;
	ptr[2] = rt_addr>>8;

	ptr[3] = PKT3(SET_CONTEXT_REG, 1);
	ptr[4] = CB_COLOR0_INFO;
	ptr[5] = (10<<2)|(6<<8)|(1<<7);//(1<<17)|(4<<2)/**color format*/|(7<<8)|(1<<7)|(1<<26)/*disable fmask*/|(1<<16);

	ptr[6] = PKT3(SET_CONTEXT_REG, 1);
	ptr[7] = CB_COLOR0_PITCH;
	ptr[8] = (W/8)-1;

	ptr[9] = PKT3(SET_CONTEXT_REG, 1);
	ptr[10] = CB_COLOR0_SLICE;
	ptr[11] = ((W*H)/64)-1;

	ptr[12] = PKT3(SET_CONTEXT_REG, 1);
	ptr[13] = CB_COLOR0_VIEW;
	ptr[14] = 0xffffffff<<13;

	ptr[15] = PKT3(SET_CONTEXT_REG, 1);
	ptr[16] = CB_COLOR0_ATTR;
	ptr[17] = 0;//(16<<12)/*4 samples*/|1<<15;
	
	ptr[18] = PKT3(SET_CONTEXT_REG, 1);
	ptr[19] = CB_COLOR0_CMASK;
	ptr[20] = 0;//rt_addr>>8;
	
	ptr[21] = PKT3(SET_CONTEXT_REG, 1);
	ptr[22] = CB_COLOR0_FMASK;
	ptr[23] = 0;//rt_addr>>8;
	
	ptr[24] = PKT3(SET_CONTEXT_REG, 1);
	ptr[25] = CB_COLOR0_CMASK_SLICE;
	ptr[26] = 0;
	
	ptr[27] = PKT3(SET_CONTEXT_REG, 1);
	ptr[28] = CB_COLOR0_FMASK_SLICE;
	ptr[29] = 0;
	
	ptr[30] = PKT3(SET_CONTEXT_REG, 1);
	ptr[31] = CB_COLOR0_CLEAR_WORD0;
	ptr[32] = 0;
	
	ptr[33] = PKT3(SET_CONTEXT_REG, 1);
	ptr[34] = CB_COLOR0_CLEAR_WORD1;
	ptr[35] = 0;

	ptr[36] = PKT3(SET_CONTEXT_REG, 1);
	ptr[37] = CB_TARGET_MASK;
	ptr[38] = 0xf;

	ptr[39] = PKT3(SET_CONTEXT_REG, 1);
	ptr[40] = CB_SHADER_MASK;
	ptr[41] = 0xf;

	ptr[42] = PKT3(SET_CONTEXT_REG, 1);
	ptr[43] = CB_BLEND0_CONTROL;
	ptr[44] = 1<<31;

	ptr[45] = PKT3(SET_CONTEXT_REG, 1);
	ptr[46] = VGT_PRIMITIVEID_EN;
	ptr[47] = 0;

	ptr[48] = PKT3(SET_CONTEXT_REG, 1);
	ptr[49] = VGT_PRIMITIVEID_RESET;
	ptr[50] = 0;

	ptr[51] = PKT3(SET_CONTEXT_REG, 1);
	ptr[52] = VGT_SHADER_STAGES_EN;
	ptr[53] = 0;

	ptr[54] = PKT3(SET_CONTEXT_REG, 1);
	ptr[55] = VGT_STRMOUT_CONFIG;
	ptr[56] = 0;

	ptr[57] = PKT3(SET_CONTEXT_REG, 1);
	ptr[58] = PA_SC_WINDOW_SCISSOR_BR;
	ptr[59] = W|(H<<16);



	ptr[60] = PKT3(SET_CONTEXT_REG, 1);
	ptr[61] = DB_DEPTH_CONTROL;
	ptr[62] = 0;

	ptr[63] = PKT3(SET_CONTEXT_REG, 1);
	ptr[64] = DB_STENCIL_CONTROL;
	ptr[65] = 0;

	ptr[66] = PKT3(SET_CONTEXT_REG, 1);
	ptr[67] = DB_RENDER_CONTROL;
	ptr[68] = (1<<5)|(1<<6);

	ptr[69] = PKT3(SET_CONTEXT_REG, 1);
	ptr[70] = DB_RENDER_OVERRIDE;
	ptr[71] = 2|(2<<2)|(2<<4)|(1<<16);

	ptr[72] = PKT3(SET_CONTEXT_REG, 1);
	ptr[73] = DB_RENDER_OVERRIDE2;
	ptr[74] = 0;

	ptr[75] = PKT3(SET_CONTEXT_REG, 1);
	ptr[76] = DB_STENCILREFMASK;
	ptr[77] = 0;

	ptr[78] = PKT3(SET_CONTEXT_REG, 1);
	ptr[79] = DB_STENCILREFMASK_BF;
	ptr[80] = 0;

	ptr[81] = PKT3(SET_CONTEXT_REG, 1);
	ptr[82] = PA_CL_CLIP_CNTL;
	ptr[83] = (1<<16)|(1<<20)|(3<<26)|(1<<27);

	ptr[84] = PKT3(SET_CONTEXT_REG, 1);
	ptr[85] = SPI_INTERP_CONTROL_0;
	ptr[86] = 0;

	ptr[87] = PKT3(SET_CONTEXT_REG, 1);
	ptr[88] = PA_SU_VTX_CNTL;
	ptr[89] = 0;

	ptr[90] = PKT3(SET_CONTEXT_REG, 1);
	ptr[91] = PA_SU_SC_MODE_CNTL;
	ptr[92] = 0;

	ptr[93] = PKT3(SET_CONTEXT_REG, 1);
	ptr[94] = PA_SC_MODE_CNTL_0;
	ptr[95] = 0;

	ptr[96] = PKT3(SET_CONTEXT_REG, 1);
	ptr[97] = PA_SC_MODE_CNTL_1;
	ptr[98] = 0;//(3<<25);

	ptr[99] = PKT3(SET_CONTEXT_REG, 1);
	ptr[100] = PA_SU_PRIM_FILTER_CNTL;
	ptr[101] = 1/*might cause issues???*/;

	/*
	seems to do somthing????????
	*/
	ptr[102] = PKT3(SET_CONTEXT_REG, 1);
	ptr[103] = PA_CL_VTE_CNTL;
	ptr[104] = 1<<8;//0x3f|(7<<8);

	ptr[105] = PKT3(SET_CONTEXT_REG, 1);
	ptr[106] = PA_CL_VS_OUT_CNTL;
	ptr[107] = 0;

	ptr[108] = PKT3(SET_CONTEXT_REG, 1);
	ptr[109] = VGT_REUSE_OFF;
	ptr[110] = 0;

	/*
		dont think this helps,
		but just incase
	*/
	ptr[111] = PKT3(SET_CONTEXT_REG, 1);
	ptr[112] = SPI_PS_INPUT_CNTL_0;
	ptr[113] = 0;//(1<<5)|(0<<8)/*input vertex*/;

	ptr[114] = PKT3(SET_CONTEXT_REG, 1);
	ptr[115] = SPI_PS_INPUT_CNTL_1;
	ptr[116] = 1;//(1<<5)|(1<<8);

	ptr[117] = PKT3(SET_CONTEXT_REG, 1);
	ptr[118] = SPI_PS_INPUT_CNTL_2;
	ptr[119] = 2;//(1<<5)|(3<<8);

	ptr[120] = PKT3(SET_CONTEXT_REG, 1);
	ptr[121] = DB_SHADER_CONTROL;
	ptr[122] = 1<<11;

	ptr[123] = PKT3(SET_CONTEXT_REG, 1);
	ptr[124] = PA_SC_LINE_CNTL;
	ptr[125] = 0;

	ptr[126] = PKT3(SET_CONTEXT_REG, 1);
	ptr[127] = CB_COLOR_CONTROL;
	ptr[128] = 1<<4;/*normal rendering mode*/

	ptr[129] = PKT3(SET_CONTEXT_REG, 1);
	ptr[130] = SPI_VS_OUT_CONFIG;
	ptr[131] = 0;/*one vertex export per vertex shader?*/

	ptr[132] = PKT3(SET_CONTEXT_REG, 1);
	ptr[133] = SPI_PS_IN_CONTROL;
	ptr[134] = 0/*N-params*/;

	ptr[135] = PKT3(SET_CONTEXT_REG, 1);
	ptr[136] = SPI_PS_INPUT_ENA;
	ptr[137] = 1<<4/*perspective*/;

	ptr[138] = PKT3(SET_CONTEXT_REG, 1);
	ptr[139] = SPI_SHADER_POS_FORMAT;
	ptr[140] = 4/*4-components x 32*/;

	ptr[141] = PKT3(SET_CONTEXT_REG, 1);
	ptr[142] = SPI_PS_INPUT_ADDR;
	ptr[143] = 1<<4;

	ptr[144] = PKT3(SET_CONTEXT_REG, 1);
	ptr[145] = PA_SC_EDGERULE;
	ptr[146] = 0xaaaaaaaa;

	ptr[147] = PKT3(SET_CONTEXT_REG, 1);
	ptr[148] = VGT_GS_MODE;
	ptr[149] = 0;

	unsigned tmp = (unsigned)(1.0*8.0);
	ptr[150] = PKT3(SET_CONTEXT_REG, 1);
	ptr[151] = PA_SU_POINT_SIZE;
	ptr[152] = tmp|(tmp<<16);

	ptr[153] = PKT3(SET_CONTEXT_REG, 1);
	ptr[154] = PA_SU_POINT_MINMAX;
	ptr[155] = 0|0xffff<<16;

	ptr[156] = PKT3(SET_CONTEXT_REG, 1);
	ptr[157] = VGT_INSTANCE_STEP_RATE_0;
	ptr[158] = 0;

	ptr[159] = PKT3(SET_CONTEXT_REG, 1);
	ptr[160] = VGT_INSTANCE_STEP_RATE_1;
	ptr[161] = 0;

	ptr[162] = PKT3(SET_CONTEXT_REG, 1);
	ptr[163] = SPI_SHADER_COL_FORMAT;
	ptr[164] = 9;

	ptr[165] = PKT3(SET_CONTEXT_REG, 4);
	ptr[166] = CB_BLEND_RED;
	ptr[167] = 0;
	ptr[168] = 0;
	ptr[169] = 0;
	ptr[170] = 0;

	ptr[171] = PKT3(SET_CONTEXT_REG, 1);
	ptr[172] = CB_DCC_CONTROL;
	ptr[173] = 3;

	ptr[174] = PKT3(SET_CONTEXT_REG, 4);
	ptr[175] = PA_CL_GB_VERT_CLIP_ADJ;
	float def = 1;

	ptr[176] = *(_32_u*)&def;
	ptr[177] = *(_32_u*)&def;
	ptr[178] = *(_32_u*)&def;
	ptr[179] = *(_32_u*)&def;

	ptr[180] = PKT3(SET_CONTEXT_REG, 1);
	ptr[181] = PA_SC_AA_MASK_X0Y0_X1Y0;
	ptr[182] = 0xffffffff;

	ptr[183] = PKT3(SET_CONTEXT_REG, 1);
	ptr[184] = PA_SC_AA_MASK_X0Y1_X1Y1;
	ptr[185] = 0xffffffff;
	
	ptr[186] = PKT3(SET_CONTEXT_REG, 1);
	ptr[187] = DB_STENCIL_CLEAR;
	ptr[188] = 0;

	ptr[189] = PKT3(SET_CONTEXT_REG, 1);
	ptr[190] = DB_DEPTH_CLEAR;
	ptr[191] = 0;

	ptr[192] = PKT3(SET_CONTEXT_REG, 1);
	ptr[193] = DB_ALPHA_TO_MASK;
	ptr[194] = (2<<8)|(2<<10)|(2<<12)|(2<<14);

	ptr[195] = PKT3(SET_CONTEXT_REG, 1);
	ptr[196] = DB_COUNT_CONTROL;
	ptr[197] = 0;

	ptr[198] = PKT3(SET_CONTEXT_REG, 1);
	ptr[199] = PA_SC_AA_CONFIG;
	ptr[200] = 0;

	ptr[201] = PKT3(SET_CONTEXT_REG, 1);
	ptr[202] = PA_SC_SCREEN_SCISSOR_TL;
	ptr[203] = 1<<31;

	ptr[204] = PKT3(SET_CONTEXT_REG, 1);
	ptr[205] = PA_SC_SCREEN_SCISSOR_BR;
	ptr[206] = 16384|(16384<<16);

	ptr[207] = PKT3(SET_CONTEXT_REG, 1);
	ptr[208] = PA_SC_WINDOW_SCISSOR_TL;
	ptr[209] = 1<<31;

	ptr[210] = PKT3(SET_CONTEXT_REG, 1);
	ptr[211] = PA_SC_GENERIC_SCISSOR_BR;
	ptr[212] = 16384|(16384<<16);

	ptr[213] = PKT3(SET_CONTEXT_REG, 1);
	ptr[214] = PA_SC_GENERIC_SCISSOR_TL;
	ptr[215] = 1<<31;

	ptr[216] = PKT3(SET_CONTEXT_REG, 1);
	ptr[217] = PA_SC_VPORT_SCISSOR_BR0;
	ptr[218] = 16384|(16384<<16);

	ptr[219] = PKT3(SET_CONTEXT_REG, 1);
	ptr[220] = PA_SC_VPORT_SCISSOR_TL0;
	ptr[221] = 1<<31;

	ptr[222] = PKT3(SET_CONTEXT_REG, 1);
	ptr[223] = PA_CL_NANINF_CNTL;
	ptr[224] = 0;

	float z_max = 1, z_min = -1;
	ptr[225] = PKT3(SET_CONTEXT_REG, 1);
	ptr[226] = PA_SC_VPORT_ZMAX;
	ptr[227] = *(_32_u*)&z_max;

	ptr[228] = PKT3(SET_CONTEXT_REG, 1);
	ptr[229] = PA_SC_VPORT_ZMIN;
	ptr[230] = *(_32_u*)&z_min;

	ptr[231] = PKT3(SET_CONTEXT_REG, 1);
	ptr[232] = PA_SC_HARDWARE_SCREEN_OFFSET;
	ptr[233] = 0;

	ptr[234] = PKT3(SET_CONTEXT_REG, 1);
	ptr[235] = SPI_BARYC_CNTL;
	ptr[236] = 0;

	ptr[237] = PKT3(SET_CONTEXT_REG, 2);
	ptr[238] = DB_STENCILREFMASK;
	ptr[239] = 0;
	ptr[240] = 0;

	ptr[241] = PKT3(SET_CONTEXT_REG, 1);
	ptr[242] = DB_DEPTH_BOUNDS_MAX;
	ptr[243] = 0;

	ptr[244] = PKT3(SET_CONTEXT_REG, 1);
	ptr[245] = DB_DEPTH_BOUNDS_MIN;
	ptr[246] = 0;

	ptr[247] = PKT3(SET_CONTEXT_REG, 5);
	ptr[248] = PA_SU_POLY_OFFSET_CLAMP;
	ptr[249] = 0;
	ptr[250] = 0;
	ptr[251] = 0;
	ptr[252] = 0;
	ptr[253] = 0;

	ptr[254] = PKT3(SET_CONTEXT_REG, 1);
	ptr[255] = DB_Z_INFO;
	ptr[256] = 0;

	ptr[257] = PKT3(SET_CONTEXT_REG, 1);
	ptr[258] = DB_DEPTH_INFO;
	ptr[259] = 0;

	ptr[260] = PKT3(SET_CONTEXT_REG, 1);
	ptr[261] = DB_STENCIL_INFO;
	ptr[262] = 0;

	ptr[263] = PKT3(SET_CONTEXT_REG, 1);
	ptr[264] = SPI_SHADER_Z_FORMAT;
	ptr[265] = 0;/*no such export*/

	ptr[266] = PKT3(SET_CONTEXT_REG, 1);
	ptr[267] = DB_Z_READ_BASE;
	ptr[268] = zb_addr>>8;

	ptr[269] = PKT3(SET_CONTEXT_REG, 1);
	ptr[270] = DB_Z_WRITE_BASE;
	ptr[271] = zb_addr>>8;

	ptr[272] = PKT3(SET_CONTEXT_REG, 1);
	ptr[273] = DB_DEPTH_SIZE;
	ptr[274] = ((ZB_W/8)-1)|(((ZB_H/8)-1)<<11);

	ptr[275] = PKT3(SET_CONTEXT_REG, 1);
	ptr[276] = DB_DEPTH_SLIZE;
	ptr[277] = ((ZB_W*ZB_H)/64)-1;

	ptr[278] = PKT3(SET_CONTEXT_REG, 1);
	ptr[279] = DB_DEPTH_VIEW;
	ptr[280] = 0;

	ptr[281] = PKT3(SET_CONTEXT_REG, 1);
	ptr[282] = SPI_TMPRING_SIZE;
	ptr[283] = 0;//32|(0xff<<12);

	float x_scale, y_scale, z_scale;
	x_scale = 1;
	y_scale = 1;
	z_scale = 1;
	float x_offset, y_offset, z_offset;
	x_offset = 0;
	y_offset = 0;
	z_offset = 0;
	ptr[284] = PKT3(SET_CONTEXT_REG, 6);
	ptr[285] = PA_CL_VPORT_XSCALE;
	ptr[286] = *(_32_u*)&x_scale;
	ptr[287] = *(_32_u*)&x_offset;
	ptr[288] = *(_32_u*)&y_scale;
	ptr[289] = *(_32_u*)&y_offset;
	ptr[290] = *(_32_u*)&z_scale;
	ptr[291] = *(_32_u*)&z_offset;
	
	ptr[292] = PKT3(SET_CONTEXT_REG, 1);
	ptr[293] = VGT_MULTI_PRIM_IB_RESET_EN;
	ptr[294] = 0;

	ptr[295] = PKT3(SET_CONTEXT_REG, 1);
	ptr[296] = VGT_MULTI_PRIM_IB_RESET_INDX;
	ptr[297] = 0;

	ptr[298] = PKT3(SET_CONTEXT_REG, 1);
	ptr[299] = VGT_MAX_VTX_INDX;
	ptr[300] = ~0;

	ptr[301] = PKT3(SET_CONTEXT_REG, 1);
	ptr[302] = VGT_MIN_VTX_INDX;
	ptr[303] = 0;

	ptr[304] = PKT3(SET_CONTEXT_REG, 1);
	ptr[305] = VGT_INDX_OFFSET;
	ptr[306] = 0;

	ptr[307] = PKT3(SET_CONTEXT_REG, 1);
	ptr[308] = VGT_VTX_CNT_EN;
	ptr[309] = 0;

	ptr+=310;


#define QW _64_u
#define BUF_RD_QW0(__badr, __stride, __cs, __se)\
	((QW)(__badr)|((QW)(__stride)<<48)|((QW)(__cs)<<62)|((QW)(__se)<<63))
#define BUF_RD_QW1(__nr, __dsx, __dsy, __dsz, __dsw, __nf, __df, __es, __is, __ate, __atc, __he, __h, __mt, __ty)\
	((QW)(__nr)|((QW)(__dsx)<<32)|((QW)(__dsy)<<35)|((QW)(__dsz)<<38)|((QW)(__dsw)<<41)|((QW)(__nf)<<44)\
	|((QW)(__df)<<47)|((QW)(__es)<<51)|((QW)(__is)<<53)|((QW)(__ate)<<55)|((QW)(__atc)<<56)|((QW)(__he)<<57)\
	|((QW)(__h)<<58)|((QW)(__mt)<<59)|((QW)(__ty)<<62))

	_64_u qw2_0[2], qw2_1[2];
	*qw2_0 = BUF_RD_QW0(ud_addr, 4*4, 0, 0);
	qw2_0[1] = BUF_RD_QW1(128, 4, 5, 6, 7, 7/*nf*/, 14, 0, 0, 1/*add tid*/, 0, 0, 0, 0, 0);
	*qw2_1 = BUF_RD_QW0(ud_addr+512, 4*4, 0, 0);
	qw2_1[1] = BUF_RD_QW1(0xffffffff, 0, 0, 0, 0, 7/*nf*/, 4, 0, 0, 0, 0, 0, 0, 0, 0);
	float *vb;
	vb = (_8_u*)ud_ptr;
	*vb = 128;
	vb[1] = 128;
	vb[2] = 0;
	vb[3] = 0;

	//
	vb[4] = 512;
	/*
		the &qw2 is just a way to produce ransdomness 
	*/
	vb[5] = ((_64_u)&qw2_0)&0xff;
	vb[6] = 0;
	vb[7] = 0;
	//
	vb[8] = ((_64_u)&qw2_0)&0xff;
	vb[9] = 512;
	vb[10] = 0;
	vb[11] = 0;

	vb[12] = 0;
	vb[13] = 0;
	vb[14] = 0;
	vb[15] = 0;
	vb[16] = 0;
	vb[17] = 0;
	vb[18] = 0;
	vb[19] = 0;
	float *color;
	color = ((_8_u*)ud_ptr)+512;
	*color = 255;
	color[1] = 0;
	color[2] = 0;
	color[3] = 255;

	//
	*ptr = PKT3(SET_SH_REG, 1);
	ptr[1] = SPI_SHADER_PGM_LO_PS;
	ptr[2] = (shader_addr+SHADER_PS)>>8;

	ptr[3] = PKT3(SET_SH_REG, 1);
	ptr[4] = SPI_SHADER_PGM_HI_PS;
	ptr[5] = (shader_addr+SHADER_PS)>>40;

	ptr[6] = PKT3(SET_SH_REG, 1);
	ptr[7] = SPI_SHADER_PGM_LO_VS;
	ptr[8] = (shader_addr+SHADER_VS)>>8;

	ptr[9] = PKT3(SET_SH_REG, 1);
	ptr[10] = SPI_SHADER_PGM_HI_VS;
	ptr[11] = (shader_addr+SHADER_VS)>>40;
	printf("SHADER- low: %x, high: %x.\n", ptr[2], ptr[5]);
	//
	ptr[12] = PKT3(SET_SH_REG, 1);
	ptr[13] = SPI_SHADER_USER_DATA_PS0;
	ptr[14] = *(_32_u*)qw2_1;

	ptr[15] = PKT3(SET_SH_REG, 1);
	ptr[16] = SPI_SHADER_USER_DATA_PS1;
	ptr[17] = *(((_32_u*)qw2_1)+1);

	ptr[18] = PKT3(SET_SH_REG, 1);
	ptr[19] = SPI_SHADER_USER_DATA_PS2;
	ptr[20] = *(((_32_u*)qw2_1)+2);

	ptr[21] = PKT3(SET_SH_REG, 1);
	ptr[22] = SPI_SHADER_USER_DATA_PS3;
	ptr[23] = *(((_32_u*)qw2_1)+3);
	
	ptr[24] = PKT3(SET_SH_REG, 1);
	ptr[25] = SPI_SHADER_USER_DATA_VS0;
	ptr[26] = *(_32_u*)qw2_0;

	ptr[27] = PKT3(SET_SH_REG, 1);
	ptr[28] = SPI_SHADER_USER_DATA_VS1;
	ptr[29] = *(((_32_u*)qw2_0)+1);

	ptr[30] = PKT3(SET_SH_REG, 1);
	ptr[31] = SPI_SHADER_USER_DATA_VS2;
	ptr[32] = *(((_32_u*)qw2_0)+2);

	ptr[33] = PKT3(SET_SH_REG, 1);
	ptr[34] = SPI_SHADER_USER_DATA_VS3;
	ptr[35] = *(((_32_u*)qw2_0)+3);

//	assert(!((ud_addr+512)>>32));
	ptr+=36;

	*ptr = PKT3(SET_SH_REG, 1);
	ptr[1] = SPI_SHADER_PGM_RSRC1_PS;
	ptr[2] = 63|(8<<6);

	ptr[3] = PKT3(SET_SH_REG, 1);
	ptr[4] = SPI_SHADER_PGM_RSRC1_VS;
	ptr[5] = 63|(8<<6)|(3<<24);

	ptr[6] = PKT3(SET_SH_REG, 1);
	ptr[7] = SPI_SHADER_PGM_RSRC2_PS;
	ptr[8] = 4<<1;

	ptr[9] = PKT3(SET_SH_REG, 1);
	ptr[10] = SPI_SHADER_PGM_RSRC2_VS;
	ptr[11] = 4<<1/*1-userdata term*/;

	ptr[12] = PKT3(SET_SH_REG, 1);
	ptr[13] = SPI_SHADER_PGM_RSRC3_PS;
	ptr[14] = 0xffff|(0x3f<<16);

	ptr[15] = PKT3(SET_SH_REG, 1);
	ptr[16] = SPI_SHADER_PGM_RSRC3_VS;
	ptr[17] = 0xffff|(0x3f<<16);
	
	ptr[18] = PKT3(SET_SH_REG, 1);
	ptr[19] = SPI_SHADER_LATE_ALLOC_VS;
	ptr[20] = 0;

	ptr[21] = PKT3(SET_CONFIG_REG, 1);
	ptr[22] = VGT_PRIM_TYPE;
	ptr[23] = DI_PT_TRILIST;

	ptr[24] = PKT3(SET_CONTEXT_REG, 1);
	ptr[25] = RASTER_CONFIG;
	ptr[26] = RB_MAP_PKR0(2) | RB_XSEL2(1) | SE_MAP(2) |
              SE_XSEL(1) | SE_YSEL(1);

	ptr[27] = PKT3(SET_CONTEXT_REG, 1);
	ptr[28] = RASTER_CONFIG_1;
	ptr[29] = 0;

	ptr+=30;

	*ptr = PKT3(INDEX_TYPE, 0);
	ptr[1] = VGT_IDX_32;
	
	ptr[2] = PKT3(NUM_INST, 0);
	ptr[3] = 1;

//	ptr[4] = PKT3(EVENT_WRITE, 0);
//	ptr[5] = 36;

//	ptr[4] = PKT3(PFP_SYNC_ME, 0);
//	ptr[5] = 0;
/*
	ptr[8] = PKT3(SURFACE_SYNC, 3);
	ptr[9] = (1<<27)|(1<<23);
	ptr[10] = 0xffffffff;
	ptr[11] = 0;
	ptr[12] = 10;
*/
//	ptr[6] = PKT3(SURFACE_SYNC, 3);
//	ptr[7] = (1<<23)|(1<<27)|(1<<22);
//	ptr[8] = 0xffffffff;
//	ptr[9] = 0;
//	ptr[10] = 10;

	ptr[4] = PKT3(DRAW_INDEX_AUTO, 1);
	ptr[5] = 3;
	ptr[6] = 2;
	ptr+=7;
	/*
		FLUSH_AND_INV_CB_DATA_TS
	*/
	_int_u ndws = N;
	while((ndws&7) != 0) {
		*(ptr++) = 0xffff1000;
		ndws++;
	}

	printf("N-DWORDS: %u, %u\n", ndws, ndws&7);
/*
	ptr = PKT3(EVENT_WRITE_EOP, 4);
	ptr[1] = 45|(5<<8);
	ptr[2] = 0;
	ptr[3] = 0;
	ptr[4] = 0;
	ptr[5] = 0;

	ptr[6] = PKT3(EVENT_WRITE_EOP, 4);
	ptr[7] = 45|(5<<8);
	ptr[8] = 0;
	ptr[9] = 0;
	ptr[10] = 0;
	ptr[11] = 0;
*/
	struct drm_amdgpu_bo_list_entry list[4];
	list->bo_handle = rt_bo->handle;
	list->bo_priority = 0;
	list[1].bo_handle = shader_bo->handle;
	list[1].bo_priority = 0;
	list[2].bo_handle = ud_bo->handle;
	list[2].bo_priority = 0;
	list[3].bo_handle = bo->handle;
	list[3].bo_priority = 0;
	union drm_amdgpu_bo_list args;
	mem_set(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_CREATE;
	args.in.bo_number = 4;
	args.in.bo_info_size = sizeof(struct drm_amdgpu_bo_list_entry);
	args.in.bo_info_ptr = (_64_u)list;
	r = drm_cmdreadwrite(dv->fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));
	if (r == -1) {
		printf("failed to create list.\n");
	}
	
//	vb = vb+12;

	_int_u i = 0;
	for(;i != 1;i++) {
	struct amdgpu_cs_request req;
	req.flags = 0;
	req.ip_type = AMDGPU_HW_IP_GFX;
	req.ip_inst = 0;
	req.ring = 0;
	req.ib_n = 1;
	req.list_handle = args.out.list_handle;
	struct amdgpu_cs_ib_info ibinfo;
	req.ib = &ibinfo;
	ibinfo.flags = 0;
	ibinfo.addr = addr;
	ibinfo.size = ndws;//N dwords 
	
	_64_u handle = amdgpu_cs_submit(ctx, &req);
	union drm_amdgpu_wait_cs cswait;
	cswait.in.handle = handle;
	cswait.in.ctx_id = ctx->ctx_id;
	cswait.in.ip_type = AMDGPU_HW_IP_GFX;
	cswait.in.ip_instance = 0;
	cswait.in.ring = 0;
	cswait.in.timeout = AMDGPU_TIMEOUT_INFINITE;

	r = drm_cmdreadwrite(dv->fd, DRM_AMDGPU_WAIT_CS, &cswait, sizeof(union drm_amdgpu_wait_cs));
	if (r == -1) {
		printf("wait failure.\n");
	}
	printf("status: %s.\n", !cswait.out.status?"completed":"still busy");
	struct amdgpu_info info;
	if (amdgpu_info(fd, &info) == -1) {
		printf("failed to get amdgpu info.\n");
	}
	printf("gfx sclk: %u\n", info.gfx_sclk);
	printf("gfx mclk: %u\n", info.gfx_mclk);
	printf("temp: %f-degree\n", ((float)info.temp)*0.001);
	printf("load: %u\n", info.load);
	printf("power: %ld-watts\n", info.avg_power);


	printf("cswait: %u\n", cswait.out.status);
	}

	printf("%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n",
		vb[0], vb[1], vb[2], vb[3], vb[4], vb[5], vb[6], vb[7], vb[8], vb[9], vb[10], vb[11]);
	printf("%x, %x, %x, %x\n%x, %x, %x, %x\n%x, %x, %x, %x\n",
		*(_32_u*)&vb[0], *(_32_u*)&vb[1], *(_32_u*)&vb[2], *(_32_u*)&vb[3],
		*(_32_u*)&vb[4], *(_32_u*)&vb[5], *(_32_u*)&vb[6], *(_32_u*)&vb[7],
		*(_32_u*)&vb[8], *(_32_u*)&vb[9], *(_32_u*)&vb[10], *(_32_u*)&vb[11]);

	printf("============================\n");
	vb = vb+12;
	i = 0;
	for(;i != 9;i++) {
//	printf("%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n",
//		vb[0], vb[1], vb[2], vb[3], vb[4], vb[5], vb[6], vb[7], vb[8], vb[9], vb[10], vb[11]);
	printf("%x, %x, %x, %x\n%x, %x, %x, %x\n%x, %x, %x, %x\n",
		*(_32_u*)&vb[0], *(_32_u*)&vb[1], *(_32_u*)&vb[2], *(_32_u*)&vb[3],
		*(_32_u*)&vb[4], *(_32_u*)&vb[5], *(_32_u*)&vb[6], *(_32_u*)&vb[7],
		*(_32_u*)&vb[8], *(_32_u*)&vb[9], *(_32_u*)&vb[10], *(_32_u*)&vb[11]);

		vb+=12;
	}
	_64_u list_handle;
	list_handle = args.out.list_handle;
	mem_set(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_DESTROY;
	args.in.list_handle = list_handle;
	drm_cmdreadwrite(fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));
#ifndef NODISP
	dumprt();
#endif
	zb_deinit(dv);
	ud_deinit(dv);
	rt_deinit(dv);
	shader_deinit(dv);
	// unmap
	amdgpu_bo_va_op(dv,bo, AMDGPU_VA_OP_UNMAP, 0, 0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));
	amdgpu_ctx_free(ctx);
	amdgpu_bo_cpu_unmap(bo);
	amdgpu_bo_free(dv,bo);
	amdgpu_dev_de_init(dv);
	close(fd);
#ifndef NODISP
	munmap(fb_pixels, fb_len);
	close(fb);
#endif
}
