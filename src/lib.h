# ifndef __ffly__lib__h
# define __ffly__lib__h
# include "y_int.h"
# include <stdarg.h>
#include "io.h"
#define NULLSTR	""
_8_u rnd_pow2table[256];
_32_u pw2_ranking(_32_u);
_32_s ff_mktemp(char*);

void filesave(char const*,void*,_int_u);
void fileload(char const*,void*,_int_u);


void abort(void);
_int_u y4_enc(void const*, char*, _int_u);
_int_u y4_dec(char const*, void*, _int_u);
_int_u snumext(char*, char*);
_int_u sextract(char*, char*);
_int_u extractupto(char*,char*,char);
_int_u trekbackto(char*,char);
_int_u extractpast(char*,char);
_int_u locofchar(char*,_int_u*,char);
_int_u locofchar_l(char*,char);
_int_u harvest(char*,char*,_16_u*,_16_u);
char* harvest_s(char*,char*,_16_u*,_16_u,_int_u*);
void str_cat(char*,char*);
void str_rmc(char*,char*,char*,_int_u);
void str_rplc(char*,char,char);
#define BIT_TEST(__bits,__bit) (((__bits)&(__bit)) != 0)
void* cmalloc(_int_u,_16_u,_64_u);
//TODO: rename
#define checkmate(...)\
	printf(__VA_ARGS__);\
	_assert(__FILE__,__LINE__,__func__,"");
# endif /*__ffly__lib__h*/
