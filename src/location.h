# ifndef __ffly__location__h
# define __ffly__location__h
#ifdef __ffly_debug
#define _location_push	ff_location_push
#define _location_pop	ff_location_pop
#else
#define _location_push(...)
#define _location_pop(...)
#endif
#include "y_int.h"

/*
	might need to rethink


	...

	okay so what it is

	operation tree or list of whats happend within function call
	who the function called and what said function returned or info 

	only to be displayed apon error
	only for debug ie wont be compiled 

	using stack of calling function to store structure?????

	e.g.
	#define f_loc_push()\
		struct node;
		ff_location_push(&node);
*/

enum {
	_ff_loc_mem_alloc,
	_ff_loc_mem_free,
	_ff_loc_mem_realloc,
	_ff_loc_stores_connect,
	_ff_loc_stores_login,
	_ff_loc_stores_logout,
	_ff_loc_stores_disconnect,
	_ff_loc_db_ctr,
	_ff_loc_net_creat,
	_ff_loc_net_connect,
	_ff_loc_db_ctr_login
};

void ff_location_init(void);
void ff_location_list(void);
void ff_location_show(void);
void ff_location_push(_int_u);
void ff_location_pop(void);
# endif /*__ffly__location__h*/
