# include "dus.h"
# include "../ffly_def.h"
# include "../io.h"
# include "../m_alloc.h"
#include "../assert.h"
void static emit(dus_nodep);
void static emitthis(dus_nodep);
dus_objp static top = NULL;
dus_objp static end = NULL;
static _32_u _loc;
static dus_valuep newval(void){
	dus_valuep v;
	v = m_alloc(sizeof(struct dus_value));
	v->p = NULL;
	return v;
}
dus_objp static
next_obj(void) {
	dus_objp o = dus_obj_alloc();
	if (!top)
		top = o;
	if (end != NULL)
		end->next = o;
	o->next = NULL;
	end = o;
	return o;
}

void static
op_copy(dus_objp __src, dus_objp __dst, _int_u __len) {
	dus_objp o = next_obj();
	o->op = _dus_op_copy;
	o->dst = __dst;
	o->src = __src;
	o->len = __len;
}

void static
op_movreg(_32_u dst,_32_u src){
	dus_objp o = next_obj();
	o->op = _dus_op_movreg;
	o->reg[0] = dst;
	o->reg[1] = src;
}

dus_objp static
op_fresh(dus_objp __obj, _int_u __size) {
	dus_objp o = next_obj();
	o->op = _dus_op_fresh;
	o->size = __size;
	o->to = __obj;
	return o;
}

void static
op_free(dus_objp __obj) {
	dus_objp o = next_obj();
	o->op = _dus_op_free;
}

void static
op_push(dus_objp __obj) {
	dus_objp o = next_obj();
	o->op = _dus_op_push;
	o->p = __obj;
}

void static
op_pop(dus_objp **__obj) {
	dus_objp o = next_obj();
	o->op = _dus_op_pop;
	*__obj = (dus_objp*)&o->p;
}

void static
op_cas(dus_objp __obj, dus_objp __m) {
	dus_objp o = next_obj();
	o->op = _dus_op_cas;
	o->p = __obj;
	o->dst = __m;
}

void static
op_set(dus_objp __src, dus_objp __dst) {
	dus_objp o = next_obj();
	o->op = _dus_op_set;
	o->src = __src;
	o->dst = __dst;
}
void static
op_load(_32_u __loc, dus_valuep __src) {
	dus_objp o = next_obj();
	o->op = _dus_op_load;
	o->val = __src;
	o->reg[0] = __loc;
}



void static
op_syput(char *__name, _int_u __len, _32_u __src) {
	dus_objp o = next_obj();
	o->op = _dus_op_syput;
	o->len = __len;
	o->name = __name;
	o->reg[0] = __src;
}

void static
op_shell(_32_u __env,_32_u __path) {
	dus_objp o = next_obj();
	o->op = _dus_op_shell;
	o->reg[0] = __env;
	o->reg[1] = __path;
}

void push(dus_objp __obj) {
	op_push(__obj);
}

dus_objp* pop(void) {
	dus_objp *ret;
	op_pop(&ret);
	return ret;
}

static dus_objp ret;
void static
emit_decl_init(_32_u dst, _32_u src, dus_nodep __node) {
	_loc = src;
	emit(__node);
	dus_objp j;
	j = next_obj();
	j->op = _dus_op_mov;
	j->reg[0] = dst;
	j->reg[1] = src;
}

void static
emit_decl(dus_nodep __node) {
	dus_nodep init = __node->init;
	dus_nodep var = __node->var;
	dus_objp m;
	m = dus_obj_alloc();
	var->_obj = m;
	var->val = newval();
	var->loc = -1;
	var->val->hm = m_alloc(sizeof(struct f_lhash));
	f_lhash_init(var->val->hm);	
	if(init != NULL){
		_loc = 1;
		emit(var);
		emit_decl_init(1,0,init);
	}
}

void static
emit_var(dus_nodep __node) {
	if(__node->loc != -1){
		op_movreg(_loc,__node->loc);
	}else{
		op_load(_loc,__node->val);
	}
}


void static
_cas(_32_u dst,_32_u src) {
	dus_objp j;
	dus_objp o = next_obj();
	o->op = _dus_op_cas;
	o->reg[0] = dst;
	o->reg[1] = src; 
}

void static
emit_out(dus_nodep __node) {
	dus_nodep n = __node->p;
	/*
		this give us somthing to work with
	*/
	_loc = 0;
	emit(n);
	
	dus_valuep val;
	val = newval();
	val->type = _dusc_str;
	op_load(1,val);
 
	/*
		now we apply the translation
	*/
	_cas(1,0);	

	dus_objp j = next_obj();
	j->op = _dus_op_out;
	j->reg[0] = 1;
}

void static
emit_assign(dus_nodep __node) {
	dus_nodep l = __node->l;
	_loc = 1;
	emit(l);
	emit_decl_init(1,0,__node->r);
}

void static
emit_str(dus_nodep __node) {
	printf("STRING_LEN: %u.\n",__node->len);
	dus_valuep val;
	val = newval();
	val->p = __node->p;
	val->size = __node->len;
	val->type = _dusc_str;
	op_load(_loc,val);
}

void static 
emit_syput(dus_nodep __node) {
	_loc = 0;
	emit(__node->p);
	op_syput(__node->name, __node->len,0);	
}

void static
emit_shell(dus_nodep __node) {
	_loc = 0;
	emit(__node->_env);
	_loc = 1;
	emit(__node->p);
	op_shell(0,1);
}

char const *nkstr(_8_u __kind) {
	switch(__kind) {
		case _dus_str:		return "str";
		case _dus_decl:		return "decl";
		case _dus_var:		return "var";
		case _dus_assign:	return "assign";
		case _dus_out:		return "out";
		case _dus_cas:		return "cas";
		case _dus_syput:	return "syput";
		case _dus_shell:	return "shell";
	}
	return "unknown";
}


void static
emit_set(dus_nodep __node) {
	struct dus_ndbean *b;
	b = __node->beans;
	_int_u i = 0;
	printf("EMIT-SET: %u-beans.\n", __node->n_beans+1);
	for(;i <= __node->n_beans;i++,b++) {
		dus_nodep l = b->l;
		dus_nodep r = b->r;
		_loc = 0;
		emit(l);
		_loc = 1;
		emit(r);
		_cas(0,1);
	}
}

void static
emit_cond(dus_nodep __node) {
	_loc = 0;
	emit(__node->l);
	_loc = 1;
	emit(__node->r);

	dus_objp j;
	j = dus_obj_alloc();
	j->op = __node->subop;
	j->reg[0] = 0;
	j->reg[1] = 1;
	ret = j;
}


void static
emit_if(dus_nodep __node) {
	emit_cond(__node->cond);

	dus_objp j;
	j = next_obj();
	j->cond = ret;
	j->op = _dus_op_if;


	dus_objp orig_top, orig_end;
	orig_top = top;
	orig_end = end;
	top = end = NULL;

	if (__node->main != NULL)
		emitthis(__node->main);
	j->_if = top;

	top = end = NULL;
	if (__node->_else != NULL) 
		emitthis(__node->_else);

	j->_else = top;
	top = orig_top;
	end = orig_end;

}

void static
emit_while(dus_nodep __node) {
    emit_cond(__node->cond);

	dus_objp j;
	j = next_obj();
	j->cond = ret;
	j->op = _dus_op_while;

	dus_objp orig_top, orig_end;
	orig_top = top;
	orig_end = end;
	top = end = NULL;
	if (__node->main != NULL)
		emitthis(__node->main);
	j->_if = top;

	top = orig_top;
	end = orig_end;
}

void static
emit_int(dus_nodep __node) {
	dus_objp j;
	j = dus_obj_alloc();
	__node->_obj = j;
	j->in = __node->intval;
	j->type = _dusc_int;
	ret = j;
}

void static
emit_func(dus_nodep __node) {
	__node->val = newval();
	_int_u i;
	i = 0;
	for(;i != __node->npar;i++) {
		__node->params[i]->loc = 4+i;
	}
	dus_objp orig_top, orig_end;
	orig_top = top;
	orig_end = end;
	top = end = NULL;
	emitthis(__node->main);
	__node->_obj = top;
	top = orig_top;
	end = orig_end;	
	
	printf("FUNCTION node: %p.\n",__node);
}


void static
emit_func_call(dus_nodep __node) {
	dus_objp j;
	_int_u i; 
	i = 0;
	for(;i != __node->narg;i++) {
		_loc = 4+i;
		emit(__node->args[i]);
	}
	j = next_obj();
	j->op = _dus_op_call;
	j->func = __node->func->_obj;
}

void static
emit_array(dus_nodep __node) {
	
	dus_objp j;
	j = dus_obj_alloc();
	dus_objp *arr;
	arr = m_alloc(__node->arrlen*sizeof(dus_objp));
	_int_u i;
	i = 0;
	for(;i != __node->arrlen;i++) {
		emit(__node->array[i]);
		arr[i] = ret;
	}
	j->arr = arr;
	ret = j;
}

void emit_arrelem(dus_nodep __node) {
	_32_u st;
	st = _loc;
	_loc = 0;
	emit(__node->idx);

	_loc = 1;
	emit(__node->arr);

	dus_objp j;
	j = next_obj();
	j->op = _dus_op_deref;
	j->reg[0] = 0;
	j->reg[1] = 1;
	j->reg[2] = st;
}

void emitthis(dus_nodep __top) {
	dus_nodep cur = __top;
	while(cur != NULL) {
		emit(cur);
		cur = cur->next;
	}


}

void static
emit_tr(dus_nodep __node){
	_loc = 0;
	emit(__node->p);
	_loc = 1;
	emit(__node->cmd);
	dus_objp j;
	j = next_obj();
	j->op = _dus_op_tr;
	j->reg[0] = 0;
	j->reg[1] = 1;
}


void static
emit_bo(dus_nodep __node) {
	dus_objp l,r;
	emit(__node->l);
	l = ret;
	emit(__node->r);
	r = ret;


	dus_objp j;
	j = next_obj();
	j->op = __node->subop;
	j->l = l;
	j->r = r;

	ret = j;
}


void
emit(dus_nodep __node) {
	if (!__node)return;
	switch(__node->kind) {
		case _dus_set:
			emit_set(__node);
		break;
		case _dus_str:
			emit_str(__node);
		break;
		case _dus_decl:
			emit_decl(__node);
		break;
		case _dus_var:
			emit_var(__node);
		break;
		case _dus_assign:
			emit_assign(__node);
		break;
		case _dus_out:
			emit_out(__node);
		break;
	//	case _dus_cas:
	//		emit_cas(__node);
	//	break;
		case _dus_syput:
			emit_syput(__node);
		break;
		case _dus_shell:
			emit_shell(__node);
		break;
		case _dus_if:
			emit_if(__node);
		break;
		case _dus_func:
			emit_func(__node);
		break;
		case _dus_func_call:
			emit_func_call(__node);
		break;
		case _dus_int:
			emit_int(__node);
		break;
		case _dus_array:
			emit_array(__node);
		break;
		case _dus_deref:
			emit_arrelem(__node);
		break;
		case _dus_while:
			emit_while(__node);
		break;
		case _dus_bo:
			emit_bo(__node);
		break;
		case _dus_tr:
			emit_tr(__node);
		break;
	}
	//printf("%s\n", nkstr(__node->kind));
}
dus_valuep dus_results;
dus_valuep dus_exit_code;
dus_objp dus_gen(dus_nodep __top) {
	emitthis(__top);
	dus_nodep _exit;
	_exit = f_lhash_get(&dus_env, "exit_status",11);
	dus_exit_code = _exit->val;
	dus_exit_code->type = _dusc_int;
	dus_nodep _res;
	_res = f_lhash_get(&dus_env, "results",7);
	dus_results = _res->val;
	dus_results->p = NULL;
	dus_results->type = _dusc_str;
	return top;
}
