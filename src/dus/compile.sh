rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd dus
ffly_objs="$ffly_objs dus.o exec.o exp.o lexer.o parser.o gen.o mm.o"
#if ! [ -f dus ]; then
gcc $cc_flags -c -o dus.o dus.c
gcc $cc_flags -c -o exec.o exec.c
gcc $cc_flags -c -o exp.o exp.c
gcc $cc_flags -c -o lexer.o lexer.c
gcc $cc_flags -c -o parser.o parser.c
gcc	$cc_flags -c -o gen.o gen.c
gcc $cc_flags -c -o mm.o mm.c
#else
#	./dus compile.dus "/usr/bin/gcc" "$cc_flags"
#fi
gcc $cc_flags -o dus main.c $ffly_objs -nostdlib
