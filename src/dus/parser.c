# include "dus.h"
# include "../m_alloc.h"
# include "../io.h"
void static
parser_decl(dus_nodep *__node) {
	dus_tokenp name = dus_nexttok();
	printf("DECL.\n");
	dus_nodep init = NULL, var;
	dus_nodep nod = (*__node = new_node);
	var = new_node;
	var->kind = _dus_var;
	
	if (dus_nexttokis(_dus_tok_keywd, _dus_eq) == -1) {
		goto _sk;
	}
	printf("DECL INIT\n");
	
	init = dus_exp();
	if (!init) {
		// error
		while(1);
	}
//	printf("decl, %s\n", name->p);
_sk:
	f_lhash_put(&dus_env, name->p, name->l, var);
	nod->init = init;
	nod->var = var;
	nod->kind = _dus_decl;
}

void static
parser_out(dus_nodep *__node) {
	dus_nodep nod = (*__node = new_node);

	nod->kind = _dus_out;
	nod->p = dus_exp();
	printf("PARSER_out.\n");
}

void static
parser_syput(dus_nodep *__node) {
	dus_nodep nod = (*__node = new_node);
	nod->kind = _dus_syput;
	dus_tokenp name = dus_nexttok();
	nod->name = name->p;
	nod->len = name->l;
	nod->p = dus_exp();
}

void static
parser_shell(dus_nodep *__node) {
	dus_nodep nod = (*__node = new_node);
	nod->kind = _dus_shell;
	nod->_env = dus_exp();
	nod->p = dus_exp();
}
#include "../assert.h"
void static
parser_set(dus_nodep *__node) {
	dus_nodep nod;
	nod = (*__node = new_node);
	nod->kind = _dus_set;
	_int_u i = 0;
	struct dus_ndbean *n = nod->beans;
_anoth:
	n->l = dus_exp();
	n->r = dus_exp();
	assert(n->l != NULL && n->r != NULL);
	if (!dus_nexttokis(_dus_tok_keywd, _dus_grave)) {
		i++;
		n++;
		goto _anoth;
	}

	nod->n_beans = i;
}

void static
parser_tr(dus_nodep *__node){
	dus_nodep n;
	n = (*__node = new_node);
	n->kind = _dus_tr;
	n->p = dus_exp();
	n->cmd = dus_exp();
}

void static
parser_while(dus_nodep*);
void static
parser_if(dus_nodep*);
void static
parser_func(dus_nodep*,dus_tokenp);
_8_s static
parser_parse(dus_nodep *p) {
	dus_tokenp tok;
	tok = dus_nexttok();
	if (!tok) return -1;
	if (tok->sort == _dus_tok_ident) {
		dus_tokenp t = dus_nexttok();
		if (t->sort == _dus_tok_keywd && t->val == _dus_keywd_apos) {
			parser_func(p,tok);
			return 0;
		}
		dus_ulex(t);
		dus_ulex(tok);

		if (f_lhash_get(&dus_env, tok->p, tok->l) != NULL) {	
			*p = dus_exp();
		} else {
			parser_decl(p);
		}
	} else if (tok->sort == _dus_tok_keywd) {
		if (tok->val == _dus_keywd_out) {
			parser_out(p);
		} else if (tok->val == _dus_keywd_syput) {
			/*
				construct a string uses diffrent map from are main one
			*/
			parser_syput(p);
		} else if (tok->val == _dus_keywd_shell) {
			parser_shell(p);
		} else if (tok->val == _dus_keywd_set) {
			parser_set(p);
		}else if (tok->val == _dus_keywd_if) {
			parser_if(p);
		}else if (tok->val == _dus_keywd_while) {
			parser_while(p);
		}else if(tok->val == _dus_keywd_tr){
			parser_tr(p);
		}
	}
	return 0;
}

dus_nodep static
compound_stmt(void) {
	dus_tokenp tok;
	dus_nodep p, top = NULL, last = NULL;
	printf("COMPOUND_start.\n");
_again:
	tok = dus_peektok();
	if (!tok)
		goto _escape;
	if (tok->sort == _dus_tok_keywd) {
		switch(tok->val) {
			case _dus_r_brace:
				dus_nexttok();
				goto _escape;
			break;
			case _dus_keywd_else:
				goto _escape;
			break;
		}

	}
	p = NULL;
	parser_parse(&p);
	
	if (!top) top = p;
	if (last != NULL) {
		last->next = p;
	}
	last = p;
	goto _again;
_escape:
	if (!top)
		return NULL;
	last->next = NULL;
	printf("COMPOUND_end.\n");
	return top;
}

extern struct f_lhash *dus_hm;

void
parser_func(dus_nodep *__node, dus_tokenp __name) {
	dus_tokenp name;
	struct f_lhash *hm = m_alloc(sizeof(struct f_lhash));
	f_lhash_init(hm);
	dus_nodep var;
	dus_nodep n;
	*__node = n = new_node;
	printf("FUNCTION: %p : '%s'\n",n,__name->p);
	n->npar = 0;
	f_lhash_put(&dus_env,__name->p,__name->l,n);
_again:
	printf("FUNC_PARAM%u.\n",n->npar);
	name = dus_nexttok();
	var = new_node;
	var->kind = _dus_var;
	f_lhash_put(hm,name->p,name->l,var);
	n->params[n->npar++] = var;
	if (!dus_nexttokis(_dus_tok_keywd,_dus_comma)) {
		goto _again;
	}

	dus_hm = hm;
	n->main = compound_stmt();
	n->kind = _dus_func;
}

dus_nodep static if_body(void) {
	dus_nodep n;
	dus_nodep cond;
	n = new_node;
	n->kind = _dus_if;
	dus_expect_token(_dus_tok_keywd,_dus_l_bracket);
	cond = dus_exp();
	dus_expect_token(_dus_tok_keywd,_dus_r_bracket);

	n->next = NULL;
	n->cond = cond;
	n->_else = NULL;
	n->main = compound_stmt();
	
	if (!dus_nexttokis(_dus_tok_keywd,_dus_keywd_else)) {
		if (!dus_nexttokis(_dus_tok_keywd,_dus_keywd_if)) {
			n->_else = if_body();
		}else
			n->_else = compound_stmt();
	}

	return n;
}

void
parser_if(dus_nodep *__node) {
	if(dus_expect_token(_dus_tok_keywd,_dus_l_brace) == -1) {
		printf("expected left brace at start of statment.\n");
		return NULL;
	}

	*__node = if_body();
}

void
parser_while(dus_nodep *__node) {
	if(dus_expect_token(_dus_tok_keywd,_dus_l_brace) == -1) {
		return NULL;
	}
	dus_nodep n;
	dus_nodep cond;
	n = new_node;
	n->next = NULL;
	n->kind = _dus_while;
	dus_expect_token(_dus_tok_keywd,_dus_l_bracket);
	cond = dus_exp();
	dus_expect_token(_dus_tok_keywd,_dus_r_bracket);

	n->cond = cond;
	n->main = compound_stmt();
	*__node = n;
}

void dus_parse(dus_nodep *__top)
{
	dus_nodep p, end = NULL;
	while(!dus_at_eof()) {
		p = NULL;
		if (parser_parse(&p) == -1)
			goto _end;
		if (p != NULL) {
			if (!*__top)
				*__top = p;
			if (end != NULL)
				end->next = p;
			end = p;
		}
	}
_end:
	if (end != NULL)
		end->next = NULL;
}
