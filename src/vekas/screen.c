#include "common.h"
#include "../string.h"
#include "../m_alloc.h"
void vk_scrninit(struct vk_scrninfo *__s) {
	__s->next = vk_ctx.screens;
	vk_ctx.screens = __s;
	__s->lock = MUTEX_INIT;
}

/*
	vekas screen init
*/
void vk_scrn_init(struct vk_scrn *__s) {
	vk_scrninit(&__s->scrn);
	__s->scrn.width		= __s->d->width;
	__s->scrn.height	= __s->d->height;
}

struct vk_scrn *getscrn(_int_s __x, _int_s __y) {
    struct vk_scrn *r;
    struct vk_scrn *sc;
    r = NULL;
    sc = vk_ctx.screens;
/*
	TODO:
		use a single number for this. so no X,Y and only X or somthing
*/
    while(sc != NULL) {
        _int_s x = sc->scrn.x;
        _int_s y = sc->scrn.y;
        if (__x-x>=0&&__x-(x+(_int_s)sc->scrn.width)<0) {
            if (__y-y>=0&&__y-(y+(_int_s)sc->scrn.height)<0) {
                r = sc;
                break;
            }
        }
        sc = sc->scrn.next;
    }
    if (!r) {
        vk_printf("screen lookup by coords failed, %d, %d\n", __x,__y);
    }
    return r;
}
