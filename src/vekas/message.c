#include "common.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../mutex.h"
void vk_msgfree(struct vk_msg *m){	
	m_free(m);
}

struct vk_msg* vk_msgalloc(void){
	struct vk_msgpers *p;
	p = m_alloc(sizeof(struct vk_msgpers));

	return p;
}

struct vk_msg* vk_msgnew(struct vk_context *__vk_ctx) {
	struct vk_msg *m;
	struct vk_msgpers *p;

	p = m = m_alloc(sizeof(struct vk_msgpers));

	if (!__vk_ctx->m_head)
		__vk_ctx->m_head = m;
	m->next = NULL;
	m->bk = &__vk_ctx->m;
	if (__vk_ctx->m != NULL) {
		m->bk = &__vk_ctx->m->next;
		__vk_ctx->m->next = m;
	}
	__vk_ctx->m = m;

	return m;
}
static char const *codenames[512] = {
	"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
	"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
	"button left",
	"button right",
	"ESC",
	"SPACE",
	"LEFT SHIFT",
	"UP",
	"DOWN",
	"LEFT",
	"RIGHT",
	"F1",
	"F2",
	"F3",
	"F4",
	"F5",
	"F6",
	"F7",
	"F8",
	"F9",
	"F10",
	"F11",
	"F12",
	"BACKSPACE",
	"DOT",
	"FOWARD SLASH"
};

void vk_msgclient(struct vk_client_priv *cl, struct vk_msg *b,_16_u flags){
	struct vk_inbox *in;
	in = &cl->inbox;
	mt_lock(&in->lock);
	if (in->nmsg>16 && !flags) {
		printf("too many messages to takein.\n");
		vk_msgfree(b);
	}else{
		if(!in->top)
			in->top = b;

		struct vk_msgpers *p = b;
		p->next[in->id] = NULL;
		if(in->m != NULL){
			((struct vk_msgpers*)in->m)->next[in->id] = b;
		}
		in->m = b;
		in->nmsg++;
	}
	mt_unlock(&in->lock);		
}

void vk_msg_in(struct vk_msg *b) {
	struct vk_patch *pt;
	pt = !vk_com.grab_pat?pt_here(b->x,b->y):vk_com.grab_pat;
	if (b->value == VK_PTR_M) {
			vk_ctx.cursor_has_moved = 0;
			//s->drv->cursor(s, vk_ctx.cursor->img, b->x-s->x, b->y-s->y);
		} else {
			if (b->x<= 0 && b->y <= 0 && b->value == VK_PRESS && b->code == VK_BTN_LEFT) {
				printf("got exit, cursor: %u,%u.\n",b->x,b->y);
		//		running = -1;
				return;

			}

			if (b->value == VK_PRESS) {
				/*
				printf("%f %f, %u == %u, %u == %u\n",(float)b->x,(float)b->y,b->value,VK_PRESS,b->code,VK_BTN_LEFT);
				ffly_fdrain(_ffly_out);
				struct vk_patch *pt = pt_here(vk_ctx.cursor->s,b->x,b->y);
				if (pt != NULL) {
				//  if (griped == -1)
					griped = 0;
							g_pt = pt;
					printf("CURSOR AT PATCH.\n");
				} else {
					printf("no patch located at cursor position.\n");
				}
				*/
			}
			if (b->value == VK_RELEASE) {
				printf("RELEASED.\n");
	//			griped = -1;

			}
			if (b->code<=VY_SLASH)
				vk_printf("key trigger. %u, %u, code: %s, value: %u\n", b->x, b->y, codenames[b->code], b->value);
		}

	if(!pt){
		vk_printf("DEADEND for message.\n");
		vk_msgfree(b);
	}else{
		b->newpatch.patch = pt->adr;
		vk_msgclient(pt->client,b,0);
	}
}

