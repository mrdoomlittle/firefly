#include "common.h"
#include "../io.h"
#include "../rws.h"
#include "../ioc_struc.h"
#include "../system/file.h"

struct ffly_file *vk_out;
void *vkout;
extern struct f_rw_funcs rws_func;

void vk_log_init(void) {
    _8_s err;
    if (!(vk_out = f_fopen("vklog",
        FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
        FF_S_IRUSR|FF_S_IWUSR, &err))) {
    }
    ffly_fopt(vk_out, FF_STREAM);
    static struct f_ioc_struc ioc_out;

    ioc_out.rws.funcs = &rws_func;
    ioc_out.rws.arg = (_ulonglong)vk_out;
    vk_out->rws = &ioc_out.rws;
    vkout = &ioc_out;
}
