#include "fbdev.h"
# include "../../linux/fb.h"
# include "../../linux/unistd.h"
# include "../../linux/fcntl.h"
# include "../../linux/ioctl.h"
# include "../../linux/mman.h"
# include "../../m_alloc.h"
# include "../../io.h"
# include "../../string.h"
#define DRV vk_drvtab[VK_DRV_FBDEV]
void static update(void) {

}

void fbdev_delete(struct vk_fbdev *fb) {
	printf("FB-DELETE.\n");
	munmap(fb->pixels, fb->len);
	close(fb->fd);
}

struct vk_drawable static *dw_alloc(void) {
	struct fbdev_dw *d;
	d = (struct fbdev_dw*)m_alloc(sizeof(struct fbdev_dw));
	return d;
}

void static dw_free(struct vk_drawable *__dw) {
	struct fbdev_dw *d = __dw;
	m_free(d->map);
	m_free(d);
}

void static
_write(struct fbdev_dw *__d, void *__buf) {
	mem_cpy(__d->map, __buf, __d->d.size);
}

void static
bo_backing(void*d, struct fbdev_dw *__dw, _32_u __width, _32_u __height) {
	__dw->d.width = __width;
	__dw->d.height = __height;
	__dw->map = m_alloc(__dw->d.size = (__width*__height*4));
	mem_set(__dw->map, 255, __dw->d.size);
}

static struct vk_gc g = {
	dw_alloc,
	dw_free,
	_write,
	vk_drawpixs,
	bo_backing
};

void fbdev_cursor(struct vk_fbdev *__fb, struct bitimage *__image, _int_u __x, _int_u __y) {
	_int_u x, y;
	_8_u s, *d, *ss;
#define W 8
#define H 8
	if (__x>__fb->s.d->width-W || __y>__fb->s.d->height-H) {
		return;
	}

	y = 0;
	for(;y != H;y++) {
		x = 0;
		for(;x != W;x++) {
			ss = __fb->cursorbuf+((x+(y*H))*4);
			d = __fb->pixels+(((x+__fb->x)*4)+((y+__fb->y)*__fb->line_length));
			d[0] = ss[0];
			d[1] = ss[1];
			d[2] = ss[2];
			d[3] = ss[3];
		}
	}

	y = 0;
	for(;y != H;y++) {
		x = 0;
		for(;x != W;x++) {
			ss = __fb->cursorbuf+((x+(y*H))*4);
			s = *(__image->bitmap+x+(y*W));
			d = __fb->pixels+(((x+__x)*4)+((y+__y)*__fb->line_length));
			ss[0] = d[0];
			ss[1] = d[1];
			ss[2] = d[2];
			ss[3] = d[3];
			d[0] = s;
			d[1] = s;
			d[2] = s;
			d[3] = s;
		}
	}
	__fb->x = __x;
	__fb->y = __y;
}


void static draw(struct vk_fbdev *__fb, struct fbdev_dw *__d, _int_u __x, _int_u __y) {
	_int_u x, y;
	y = 0;
	_8_u *s, *d;
	for(;y != __d->d.height;y++) {
		x = 0;
		for(;x != __d->d.width;x++) {
			d = __fb->pixels+(((x+__x)*4)+((y+__y)*__fb->line_length));
			s = __d->map+((x+(y*__d->d.width))*4);
			d[0] = s[2];
			d[1] = s[1];
			d[2] = s[0];
			d[3] = s[3];
		}
	}
	printf("DRAW. %u, %u, %u, %u\n", __x, __y, __d->d.width,__d->d.height);
}
extern struct vk_context vk_ctx;
void vk_fbdev_update(void) {
/*	struct vk_fbdev *fb;
	fb = DRV.screens;
	_8_s update;
	while(fb != NULL) {
		mt_lock(&fb->s.lock);
		struct vk_patch *pt;
		pt = fb->s.pt;
		while(pt != NULL) {
			update = -1;
			mt_lock(&pt->drawbuf.lock);
			if (!pt->drawbuf.pending) {
				_write(pt->d, pt->drawbuf.buf);
				m_free(pt->drawbuf.buf);
				pt->drawbuf.pending = -1;
				update = 0;
			}
			mt_unlock(&pt->drawbuf.lock);
			
			if (!update || !pt->update) {
				printf("COMMENCING DRAW.\n");
				draw(fb, pt->d, pt->x, pt->y);
				//redraw the cursor as it gets overwriten by the patch draw function
				fbdev_cursor(fb, vk_ctx.cursor->img,fb->x,fb->y);
				pt->update = -1;
			}
			pt = pt->next;
		}
		mt_unlock(&fb->s.lock);
		fb = fb->next;
	}
*/
}
#define PAGE_SHIFT 12
#define PAGE_SIZE (1UL<<PAGE_SHIFT)
#define PAGE_MASK (PAGE_SIZE-1)

#include "../../assert.h"
struct vk_fbdev* _sweep(struct vk_context *__ctx) {
	printf("FB-CREATE.\n");

	struct vk_fbdev *fb;
	fb = (struct vk_fbdev*)m_alloc(sizeof(struct vk_fbdev));
	int fd;
	fd = open("/dev/fb0", O_RDWR, 0);
	if (fd<0) {
		printf("failed to open fb.\n");
		return;
	}


	_int_u len;
	struct fb_var_screeninfo info;
	struct fb_fix_screeninfo fix;
	ioctl(fd, FBIOGET_VSCREENINFO, &info);
	ioctl(fd, FBIOGET_FSCREENINFO, &fix);
	len = info.yres*fix.line_length;
	printf("bits per pixel: %u, %u, %u.\n", info.bits_per_pixel, info.xres, info.yres);
	assert(info.bits_per_pixel == 32);
	_64_u offset = fix.smem_start&PAGE_MASK;
	fb->pixels = mmap(NULL, fix.smem_len+offset, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	fb->pixels+=offset;
	fb->len = len;
	fb->fd = fd;
	/*
		cant use xres as width because of margins and shit 
		should put xres but diffrent name but usless when drawing stuff
		on screen
	*/
	fb->line_length = fix.line_length;
	fb->s.d->width = info.xres;
	fb->s.d->height = info.yres;
	fb->s.scrn.next = __ctx->screens;
	__ctx->screens = &fb->s;
	fb->s.drv = &DRV;
	fb->s.g = &g;
//	vk_scrn_newcords(&fb->s.x,&fb->s.y,info.xres,info.yres);
	fb->x = 0;
	fb->y = 0;
	fb->s.scrn.lock = MUTEX_INIT;
	fb->s.off = 0;
	fb->next = DRV.screens;
	DRV.screens = fb;
	mem_set(fb->pixels, 255, len);
	return fb;
}
void vk_fbdev_sweep(struct vk_context *__ctx) {
	_sweep(__ctx);
}

struct vk_fbdev* vk_fbdev_open(struct vk_context *__ctx) {
	struct vk_fbdev *f;
	f = _sweep(__ctx);
	return f;
}




