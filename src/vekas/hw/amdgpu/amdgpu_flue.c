#include "../common.h"
#include "amdgpu.h"
#include "../../../flue/radeon/common.h"
void amdgpu_drawable_flue_bo(struct amdgpu_ent *ent, struct amdgpu_dw *__dw, _32_u __width, _32_u __height) {
	flue_texp tex;
	tex = flue_tex_new();
	struct flue_fmtdesc fmt = {.depth=4,.nfmt=FLUE_NFMT(FLUE_NF_UNORM),.dfmt=FLUE_DFMT(FLUE_DF_8_8_8_8)};	
	flue_texconfig(tex,NULL,0,0,__width,__height,&fmt);
	struct rd_tex *rt = (struct rd_tex*)tex;
	tex->config |= FLUE_TEX_DUAL;
	rd_tex__bo(
		rt,
		&__dw->buffer->bo,
		((_64_u*)&__dw->buffer->bo)+1,
		__width,
		__height,
		&fmt
	);
	__dw->vf.tex = tex;
}


void amdgpu_shared_backing_flue(struct amdgpu_ent *ent, struct amdgpu_dw *__dw, int back, int front, _32_u __width, _32_u __height) {
	amdgpu_shared_backing(ent,__dw,back,front,__width,__height);
	amdgpu_drawable_flue_bo(ent,__dw,__width,__height);
}
