#ifndef __vk__fil__h
#define __vk__fil__h
#include "../y_int.h"
#include "../mutex.h"
#include "../list.h"
/*
  filament = a thin wire in a light bulb(a wire which connected two parts)
  needs renaming

	okay the purpose of FIL
	is that we can use a pipe as if it has many streams.


	A---| -- pipe -- |---A
	B---|					   |---B

	so we dont have to create multiple pipes,
	say for example

	A - is a command stream
	B - is used to deal with data
*/

/*
	the issue this fixes is order of data.

	lets say we have a client asking for infomation,
	but we are also getting sending side messages(state info or other)
	to the client parallel with the client(its going on at the same time)


	and example responce message)

	command  = read(pipe)

	if command == COMMAND
		write(pipe) = responce


	now the client also needs to know about internel stuff,
	for example when its allowed access to the screen buffer.

	so in another thread)

	THREAD-ROUTINE{
		if(has_access){
			write(pipe) = "you now have access to the screen"
		}
	}

	we see the issue? the data might come out of order.

	we have a couple of options to deal with this.

	firstly we could append a job-routine the the samethread that commands are being accepted on.
	down side to this is that are data isent going to get back until and command is finished.

	(OR)

	we create dropoff bays(bunks), where is all command responce writes(pipe,bunk-0)
	and in the other thread we do writes(pipe,bunk-1)


*/
#include "../sched.h"
struct fil_pkt;
struct fil_bunk{
	struct y_list inbound;
	struct y_list outbound;
	mlock ib_lock,ob_lock;
};
#define FIL_INTERRUPT 2
struct fil_struc {
  _32_u flags;
	int fds[2];
/*
  fds[0] = read operations
  fds[1] = write operations
*/
  struct fil_bunk bunks[4];
	mlock in_lock,out_lock;
};

struct fil_pkt {
  struct y_list_entry ent;
	// the bunk its destaned for
  _64_u bunk;
	_32_u size;
  // this is not needed or the real packet thats sent(!REMOVE)
  void *data;
 };
_8_s fil_existentdata(struct fil_struc *__stc, _64_u __bunk);
_8_s fil_pollin(struct fil_struc *__stc);
void fil_sndoff(struct fil_struc *__stc);
void fil_start(struct fil_struc *__stc);
/*
	free packet and contents
*/
void fil_pktfree(struct fil_pkt*);
void fil_rcvfromall(struct fil_struc *__stc);
void fil_strcinit(struct fil_struc*);
void fil_sndpkt(struct fil_struc*,struct fil_pkt*);
_8_s fil_rcvpkt(struct fil_struc*,_64_u,struct fil_pkt**,_int_u);
_8_s fil_rcvp(struct fil_struc*,_64_u,void*,_int_u);
#endif/*__vk__fil__h*/
