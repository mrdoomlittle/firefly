#include "flue.h"
#include "../../assert.h"
void vf_rect_shaders(void);
void vf_copyarea(struct vf_drawable *__d,struct vf_drawable *__s,_int_u __x,_int_u __y,_int_u __width,_int_u __height) {
	struct flue_bo *vtxbuf;
	vtxbuf = flue_bo_new(sizeof(float)*24);
	float *vtx_buf;
	vtx_buf = flue_bo_map(vtxbuf);
	vtx_buf[0] = __x;
	vtx_buf[1] = __y;
	vtx_buf[2] = 0;
	vtx_buf[3] = 1.0;

	vtx_buf[4] = __x+__width;
	vtx_buf[5] = __y;
	vtx_buf[6] = 0;
	vtx_buf[7] = 1.0;

	vtx_buf[8] = __x;
	vtx_buf[9] = __y+__height;
	vtx_buf[10] = 0;
	vtx_buf[11] = 1.0;
	
	vtx_buf[12] = 0;
	vtx_buf[13] = 0;
	vtx_buf[14] = 0;
	vtx_buf[15] = 0;

	vtx_buf[16] = __width;
	vtx_buf[17] = 0;
	vtx_buf[18] = 0;
	vtx_buf[19] = 0;

	vtx_buf[20] = 0;
	vtx_buf[21] = __height;
	vtx_buf[22] = 0;
	vtx_buf[23] = 0;

	printf("RECT: {%f, %f}, {%f, %f}, {%f, %f}.\n",
		vtx_buf[0],vtx_buf[1],vtx_buf[4],vtx_buf[5],vtx_buf[8],vtx_buf[9]
	);
	flue_start();
	_64_u placement[] = {0,1};
	void *rs[] = {vtxbuf};
	void *tgs[] = {__d->s.tex};
	flue_zbuffer(NULL);
	flue_rendertgs(tgs,1);
	struct flue_resspec vs_res = {
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)},
		.id = FLUE_RS_VERTEX
	};
	flue_resources(rs,&vs_res,1,placement);

	void *tx[] = {__s->s.tex};
	flue_textures(tx,FLUE_RSPS,1,placement+1);
	vf_rect_shaders();

	struct flue_viewport vp;
	vp.scale[0] = 1;
	vp.scale[1] = 1;
	vp.scale[2] = 1;
	vp.translate[0] = 0;
	vp.translate[1] = 0;
	vp.translate[2] = 0;
	flue_viewport(&vp,0,1);
	printf("FRAMEBUFFER: %u, %u.\n",__d->d.width,__d->d.height);
	flue_framebuffer(__d->d.width,__d->d.height);
	
	flue_pushstate();
	flue_draw_array(NULL,0,1,FLUE_RECT);
	flue_done();

//clean up
	flue_bo_destroy(vtxbuf);
}
