#ifndef __y__vat__h
#define __y__vat__h
#include "y_int.h"
#include "mutex.h"
#include "clock.h"
/*
	might be renamed

	TODO:
		intergrate into sched,
		REMOVE! 
*/
// reattempt inbound
#define VT_RAIB 0x01
struct vt_task {
    void(*func)(struct vt_task*);
    _ulonglong arg;
    struct vt_task *next;
};

struct y_vat {
	struct y_clockval cv;
	mlock in_m;

	/*
		like a buffer will be dumped into vat.top
	*/
	struct vt_task *in, *in_end;
	_8_u flags;
	struct vt_task *top;
	_int_u n;
	struct y_vat *link, *next;
};

struct vt_task* vt_task_add(void(*)(struct vt_task*), _ulonglong);
void vt_task_rm(struct vt_task*);


extern struct y_vat *vt_top;
struct y_vat *vt_new(void);
void vt_destroy(struct y_vat*);
void vt_tick(struct y_vat*);

// pass struct with various stuff
struct y_vat* vt_desired(void);
#endif/*__y__vat__h*/
