# ifndef __f__iis__h
# define __f__iis__h
# include "y_int.h"
/*
	rename and do somthing about this
	BREF:
		us -> (MSG/FOUT) -> (RWS/LOG)

	okay 

	like the rws structure but more higher level
	used in hexdump to dump to MSGLOG

	NAMING: IIS = inter insection struct

	say we want to pipe it to ffly_out, ffly_err, or FF_LOG
	this helps us do just that
*/
struct f_iis {
	void(*out)(void*, _int_u);
};

extern struct f_iis _f_msg;
extern struct f_iis _f_out;
# endif /*__f__iis__h*/
