#include "dawn.h"
#include "../clay.h"
#include "../m_alloc.h"
#include "../clipman/clipman.h"
struct dwn_main _dwn_main;
#define elem _dwn_main

#define MAGSCALE 0.001

static struct yarn_texture icons;
static struct iu_icon icon_expand,icon_collapse;
static struct iu_icon icon_add,icon_exit,icon_file;
static struct iu_icon icon_select;
static _int_u transtab[] = {
	[IU_RECT_PRESS]		= IU_BOARD_GRAB,
	[IU_RECT_RELEASE]	= ~0
};

static _int_u transtab_exit_wd[] = {
	[IU_BTN_PRESS]		= IU_BOARD_HIDE_SHOW
};

static _int_u transtab_wd_to_decor[] = {
	[IU_BOARD_HIDE]	= IU_RECT_HIDE,
	[IU_BOARD_SHOW]	= IU_RECT_SHOW
};

static _int_u transtab_file_menu[] = {
	[IU_BTN_PRESS]	=	IU_MENU_EXTEND
};
static _int_u transtab_file_menu0[] = {
	[IU_BTN_PRESS]	=	IU_MENU_RETRACT
};
static _int_u tab_for_exit[] = {
	[IU_BTN_PRESS]  =   0
};

void static _altoutput(struct iu_text *t,void *ig, struct iu_msgdata *m);

void static export_button(struct iu_widget*w,struct iu_text *__t, struct ct_msg *m) {
	iu_brdhide(&elem.side_board);
	iu_brdhide(&elem.export_board);
}

void static exit_btn_func(struct iu_widget*w,struct iu_text *__t, struct ct_msg *m) {
	if(m->code == 0) {
		iu_com.running = -1;
	}
}
#include "../pixels.h"
void static load_icons(void) {
	struct clay clay;
	clay_init(&clay);

	clay_load(&clay, "icons.clay");
	clay_read(&clay);

	void *ic, *p;
	ic = clay_get("icons",&clay);
	p = clay_tget("add",ic);
	_64_u *arr = clay_array(p);

	icon_add.uv[0] = *(float*)(arr+0);
	icon_add.uv[1] = *(float*)(arr+1);
	icon_add.uv[2] = *(float*)(arr+2);
	icon_add.uv[3] = *(float*)(arr+3);

	p = clay_tget("select",ic);
	arr = clay_array(p);
	icon_select.uv[0] = *(float*)(arr+0);
	icon_select.uv[1] = *(float*)(arr+1);
	icon_select.uv[2] = *(float*)(arr+2);
	icon_select.uv[3] = *(float*)(arr+3);

	p = clay_tget("exit",ic);
	arr = clay_array(p);
	icon_exit.uv[0] = *(float*)(arr+0);
	icon_exit.uv[1] = *(float*)(arr+1);
	icon_exit.uv[2] = *(float*)(arr+2);
	icon_exit.uv[3] = *(float*)(arr+3);

	p = clay_tget("file",ic);
	arr = clay_array(p);
	icon_file.uv[0] = *(float*)(arr+0);
	icon_file.uv[1] = *(float*)(arr+1);
	icon_file.uv[2] = *(float*)(arr+2);
	icon_file.uv[3] = *(float*)(arr+3);

	p = clay_tget("expand",ic);
	arr = clay_array(p);
	icon_expand.uv[0] = *(float*)(arr+0);
	icon_expand.uv[1] = *(float*)(arr+1);
	icon_expand.uv[2] = *(float*)(arr+2);
	icon_expand.uv[3] = *(float*)(arr+3);

	p = clay_tget("collapse",ic);
	arr = clay_array(p);
	icon_collapse.uv[0] = *(float*)(arr+0);
	icon_collapse.uv[1] = *(float*)(arr+1);
	icon_collapse.uv[2] = *(float*)(arr+2);
	icon_collapse.uv[3] = *(float*)(arr+3);

	float mat[] = {
		0,1,0,0,0,
		0,1,0,0,0,
		0,1,0,0,0,
		0,1,0,0,0
	};
	mu_mat = mat;

	ct_tex_new(ct_com.g,&icons);
	struct yarn_fmtdesc fmt = {.depth=4*sizeof(float),.nfmt=YAR_NF_FLOAT,.dfmt=YAR_DF_32_32_32_32};
	ct_tex_data(ct_com.g,&icons,128,128,&fmt);
	ct_tex_fileload(ct_com.g,&icons,"icons.ppm");

	clay_de_init(&clay);
}

void static debug_tests(void) {
	struct iu_render *rn;
	//init the text structure
	iu_text_init(&elem.text);
	
	elem.text.size = 8;
	elem.text.bits = 0;//TX_VALIGN|TX_HALIGN;//pinpoint mode

#define TEXT_TEXT "sets the alignment."
	iu_text_deposit(&elem.text,TEXT_TEXT,sizeof(TEXT_TEXT)-1);

	elem.text._w.wj = 1;
	elem.text._w.hj = 0.1;
	elem.text.c.r = 1;

	iu_wgsize1(&elem.text);
	iu_wgmove(NULL,&elem.text,0,0.2);
	iu_text_config(&elem.text);
	
	//add it to render draw list	
	rn = iu_render(&elem.window._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.text;
}

void static
_file_menu(void) {	
	struct iu_render *rn;
	iu_menu_init(&elem.file_menu);
	elem.file_menu.min_width = 256;
	elem.file_menu.min_height = 10;
	elem.file_menu.max_width = 256*2;	
	
//	elem.file_menu._w.yd = -0.017;//to be admended


	struct iu_caplv *tmp;
	//swap out current capture rectangle for another
	/*
		this means that all UI inits will be added to that capture
	*/
	iu_exccapt(&tmp,&elem.file_menu.capt);
	
	iu_btn_init(&elem.fm_settings_button);
	iu_btn_init(&elem.fm_exit_button);
	iu_btn_init(&elem.fm_export_button);
	iu_text_init(&elem.text_test);	
	
	iu_exccapt(NULL,tmp);//restore

#define TEXT_TEST "alignment"
	iu_text_deposit(&elem.text_test,TEXT_TEST,sizeof(TEXT_TEST)-1);

	elem.text_test.c.r = 1;
	elem.text_test.size = 8;
	elem.text_test.bits = 0;
	elem.text_test._w.output = _altoutput;
	elem.text_test._w.w = 0;
	elem.text_test._w.h = 0;

	iu_text_determin(&elem.text_test);
	elem.text_test.min_reduct = 256;

	elem.fm_settings_button._w.w = 256;
	elem.fm_settings_button._w.h = 20;
	elem.fm_export_button._w.w = 256;
	elem.fm_export_button._w.h = 20;
	elem.fm_exit_button._w.w = 256;
	elem.fm_exit_button._w.h = 20;

	iu_btn_with_text(&elem.fm_settings_button,"Settings");
	iu_btn_with_text(&elem.fm_exit_button,"Exit");
	iu_btn_with_text(&elem.fm_export_button,"Export");
	iu_menu_add(&elem.file_menu,&elem.fm_settings_button);
	iu_menu_add(&elem.file_menu,&elem.text_test);
	iu_menu_add(&elem.file_menu,&elem.fm_export_button);
	iu_menu_add(&elem.file_menu,&elem.fm_exit_button);
	iu_menu_lay(&elem.file_menu);

	elem.fm_exit_button._w.output = exit_btn_func;
	elem.fm_exit_button._w.codetab = tab_for_exit;
	elem.fm_export_button._w.output = export_button;
	elem.fm_export_button._w.codetab = tab_for_exit;

	//  iu_wgmove(0,&file_menu,0.2,0.2);
}

static _int_u tools_transtab[] = {
	/*
		if the toolbar is resized in anyway then adjust its childs to that change in size
	*/
	[TOOLBAR_RESIZE]	= IU_ALWATP_ADJUSTMENT
};

void static _tools_bar(void) {
	struct iu_render *rn;
	iu_toolbar_init(&elem.tools);
	elem.tools.h = 20;
	iu_btn_init(&elem.file_button);

	iu_wgdefault(&elem.file_button);

	elem.file_button._w.w = 50;
	elem.file_button._w.h = 20;

	elem.tools._w.output	= iu_cntl_alwatp;
	elem.tools._w.out		= &elem.file_menu;
	elem.tools._w.codetab	= tools_transtab;
	elem.tools._w.cntl_output = elem.file_menu._w.input;

	iu_btn_with_text(&elem.file_button,"File");

	iu_toolbar_add(&elem.tools,&elem.file_button,0.00);
	iu_toolbar_confine(&elem.tools);
	
	iu_wgchild(&elem.file_menu,&elem.file_button,elem.file_button._w.move_chain);
	iu_wgmove(0,&elem.tools,0.00,0.00);

	rn = iu_render(&elem.window._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.file_menu;

	rn = iu_render(&elem.window._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.tools;

	elem.file_button._w.output = iu_cntl_2press;
	elem.file_button._w.codetab = transtab_file_menu;
	elem.file_button._w.codetab_resv[0] = transtab_file_menu;
	elem.file_button._w.codetab_resv[1] = transtab_file_menu0;
	elem.file_button._w.cntl_output = elem.file_menu._w.input;
	elem.file_button._w.out = &elem.file_menu;
	iu_wgsize1(&elem.tools);
}

void static setshape_pos(float cx, float cy, struct dwn_vec3 *shape) {
	shape[0].x = cx-NODE_RADIUS;
	shape[0].y = cy-NODE_RADIUS;
	shape[0].z = 0;
		
	shape[1].x = cx+NODE_RADIUS;
	shape[1].y = cy-NODE_RADIUS;
	shape[1].z = 0;
	
	shape[2].x = cx;
	shape[2].y = cy+NODE_RADIUS;
	shape[2].z = 0;
}
static struct dwn_vert _startvt[5] = {
	{{-0.1, -0.1,0}},
	{{0.1, -0.1,0}},
	{{-0.1, 0.1,0}},
	{{0.1, 0.1,0}},
	{{0,0,1}}
};


static struct dwn_vert *startvt[5] = {
	_startvt,
	_startvt+1,
	_startvt+2,
	_startvt+3,
	_startvt+3
};

void static config_node(struct dwn_cnode *n, float xd, float yd, struct dwn_vert **vt) { 
	xd*=MAGSCALE;
	yd*=MAGSCALE;
	dwn_printf("PAST_NODE: %f, %f, %f, %f.\n",vt[0]->pos.z,vt[1]->pos.z,vt[2]->pos.z,vt[3]->pos.z);

	n->verts[0]->pos.y = vt[0]->pos.y+yd;
	n->verts[0]->pos.z = vt[0]->pos.z+xd;

	n->verts[1]->pos.y = vt[1]->pos.y+yd;
	n->verts[1]->pos.z = vt[1]->pos.z+xd;

	n->verts[2]->pos.y = vt[2]->pos.y+yd;
	n->verts[2]->pos.z = vt[2]->pos.z+xd;

	n->verts[3]->pos.y = vt[3]->pos.y+yd;
	n->verts[3]->pos.z = vt[3]->pos.z+xd;
}

#include "../maths.h"
struct skin {	
	struct dwn_face *_t,*_b,*_r,*_l;
};
/*
	NOTE:
		this will be removed once indexes are setup, ie too lazy,
		only thing that will stay is normals
*/
#include "../assert.h"
void static fillinskin(struct dwn_vert **__a, struct dwn_vert **__b, struct dwn_face *_t, struct dwn_face *_b, struct dwn_face *_r, struct dwn_face *_l) {
	float x0,y0,z0,x1,y1,z1;
	z0 = __b[0]->pos.z-__a[0]->pos.z;
	y0 = __b[0]->pos.y-__a[0]->pos.y;

	_t->normal.x = 0;
	_t->normal.y = -z0;
	_t->normal.z = y0;

	_b->normal.x = 0;
	_b->normal.y = z0;
	_b->normal.z = -y0;

	_r->normal.x = 1;
	_r->normal.y = 0;
	_r->normal.z = 0;
	
	_l->normal.x = -1;
	_l->normal.y = 0;
	_l->normal.z = 0;
}

void static configskin(struct dwn_vert **__a, struct dwn_vert **__b, struct dwn_face *_t, struct dwn_face *_b, struct dwn_face *_r, struct dwn_face *_l) {
	_t->verts[0] = __a[0];//0;
	_t->verts[1] = __a[1];//1;
	_t->verts[2] = __b[0];
	_t->verts[3] = __b[1];

	_b->verts[0] = __a[2];
	_b->verts[1] = __a[3];
	_b->verts[2] = __b[2];
	_b->verts[3] = __b[3];

	_r->verts[0] = __a[1];
	_r->verts[1] = __a[3];
	_r->verts[2] = __b[1];
	_r->verts[3] = __b[3];

	_l->verts[0] = __a[0];
	_l->verts[1] = __a[2];
	_l->verts[2] = __b[0];
	_l->verts[3] = __b[2];
}

void static new_faces(struct dwn_face *__faces, _int_u __n) {
	struct dwn_bunch *bch = &_dwn_main.mesh.face_table[_dwn_main.mesh.num_bunches];
	bch->faces = __faces;
	bch->n = __n;

	_dwn_main.mesh.num_bunches++;
	_dwn_main.mesh.num_faces+=__n;
}

struct skin static* linkface(struct dwn_vert **__a, struct dwn_vert **__b) {
	struct skin *sk = m_alloc(sizeof(struct skin));
	struct dwn_face *faces = dwn_mempool_bunch(&_dwn_main.mesh.faces,4);
	sk->_t = faces;
	sk->_b = faces+1;
	sk->_r = faces+2;
	sk->_l = faces+3;

	new_faces(faces,4);
	configskin(
		__a,
		__b,
		sk->_t,
		sk->_b,
		sk->_r,
		sk->_l
	);
	fillinskin(
		__a,
		__b,
		sk->_t,
		sk->_b,
		sk->_r,
		sk->_l
	);


	return sk;
}
void static update_skin(struct dwn_cnode *n) {
	/*this is the previus node from where we started*/
	if(n->next != NULL) {
		struct skin *sk = n->next->skin;
		if(sk != NULL)
			fillinskin(n->next->verts,n->verts,sk->_t,sk->_b,sk->_r,sk->_l);
	}else {
		dwn_printf("attempt made to adjust previus node, but has no node of that kind.\n");
	}

	/*
		this means we are fixing the skin of this NODE
	*/
	if(n->prev != NULL) {
		struct skin *sk = n->skin;
		if (sk != NULL) {
			dwn_printf("adjustments made to node skin.\n");
			fillinskin(n->verts,n->prev->verts,sk->_t,sk->_b,sk->_r,sk->_l);
		}else {
			dwn_printf("move attempt made of node but no has no skin.\n");
		}
	}else {
		dwn_printf("attempt made to adjust the next node, but has no node of that kind.\n");
	}	
}

void static update_node(struct dwn_cnode *n, float __x, float __y) {
	struct dwn_vert **vt;
	struct dwn_cnode *pastnode;
	pastnode = n->next;
	float xd,yd;
	if(!pastnode)
		pastnode = n->prev;

	vt = pastnode->verts;

	if(n->trk == n->tracks) {
		xd = __x-pastnode->tracks[0].x;
		yd = __y-pastnode->tracks[0].y;
		config_node(n,xd,yd,vt);
	}else{
		yd = __y-pastnode->tracks[1].y;
		yd*=MAGSCALE;
		n->verts[0]->pos.x = vt[0]->pos.x+yd;
		n->verts[1]->pos.x = vt[1]->pos.x+yd;
		n->verts[2]->pos.x = vt[2]->pos.x+yd;
		n->verts[3]->pos.x = vt[3]->pos.x+yd;
	}

	update_skin(n);
}

void static add_node(struct ct_msg *m) {
	struct dwn_cnode *n;
	n = dwn_node_new();
	n->skin = NULL;
	struct dwn_face *face;
	face = dwn_mempool_alloc(&_dwn_main.mesh.faces);
	n->face = face;
	new_faces(face,1);
	face->normal.x = 0;
	face->normal.y = 0;
	face->normal.z = 1;

	n->verts[0] = dwn_mempool_alloc(&_dwn_main.mesh.vtx);
	n->verts[1] = dwn_mempool_alloc(&_dwn_main.mesh.vtx);
	n->verts[2] = dwn_mempool_alloc(&_dwn_main.mesh.vtx);
	n->verts[3] = dwn_mempool_alloc(&_dwn_main.mesh.vtx);

	face->verts[0] = n->verts[0];
	face->verts[1] = n->verts[1];
	face->verts[2] = n->verts[2];
	face->verts[3] = n->verts[3];

	//cursor location
	float cx,cy;
	struct iu_window *w = iu_grabwd_at(m->ctx);
	cx = w->_b.comp.vpw*(float)m->x;
	cy = w->_b.comp.vph*(float)m->y;
	cx-=_dwn_main.xdis;
	cy-=_dwn_main.ydis;
	cx*=1./_dwn_main.node_scale.value;
	cy*=1./_dwn_main.node_scale.value;

	struct dwn_vert **vt;
	struct dwn_cnode *pastnode;
	pastnode = n->next;
	float xd,yd;
	if (!pastnode) {
		xd = 0;
		yd = 0;
		vt = startvt;
	}else{
		xd = cx-pastnode->tracks[0].x;
		yd = cy-pastnode->tracks[0].y;
		vt = pastnode->verts;
	}

	n->verts[0]->pos.x = vt[0]->pos.x;
	n->verts[1]->pos.x = vt[1]->pos.x;
	n->verts[2]->pos.x = vt[2]->pos.x;
	n->verts[3]->pos.x = vt[3]->pos.x;
	config_node(n,xd,yd,vt);
	if(_dwn_main.front_face != NULL) {
		/*
			the skin, is from the past nodes to us now, not! the other way round.
		*/
		n->next->skin = linkface(_dwn_main.front_face,n->verts);
	}
	_dwn_main.front_face = n->verts;

	setshape_pos(cx,cy+204,n->tracks[1].shape);
	n->tracks[1].x = cx;
	n->tracks[1].y = cy+204;

	setshape_pos(cx,cy,n->tracks[0].shape);
	n->tracks[0].x = cx;
	n->tracks[0].y = cy;
}
struct iu_caprect *main_rect;
struct {
	float x,y,z;
} trans = {0,0,-20};
//prolong
float static _xp = 0,_yp = 0,_zp = 0;
float static rotx = 0, roty = 0;
float static rotonx = 0, rotony = 0;
_8_s static capture_reposition(struct ct_msg *m) {
	if(m->value == CT_RELEASE) {
		rotonx = rotony = 0;
		_xp = _yp = _zp = 0;
	}
	if(m->value != CT_PRESS) return -1;

	switch(m->code){
		case CY_KEY_W:
			_zp = elem.move_scale.value;
		break;
		case CY_KEY_A:
			_xp = -elem.move_scale.value;
		break;
		case CY_KEY_S:
			_zp = -elem.move_scale.value;
		break;
		case CY_KEY_D:
			_xp = elem.move_scale.value;
		break;
		case CY_KEY_UP:
			rotonx = -1;
		break;
		case CY_KEY_DOWN:
			rotonx = 1;
		break;
		case CY_KEY_LEFT:
			rotony = -1;
		break;
		case CY_KEY_RIGHT:
			rotony = 1;
		break;
		default:
			return -1;
	}

	return 0;
}
static _8_s bypass = 0;
void static msgsink(struct iu_msgdata *m);
static struct dwn_cnode *selected_node;
static _8_s dropselection = -1;
static _int_s mode = -1;
_8_s _iu_msg(struct ct_msg *m){}
_8_s _msg(_ulonglong arg,struct ct_msg *m) {	
	if(m->ctx != 0) return -1;
	msgsink(m);
	if(bypass != 0){
	if(m->code == CY_BTN_LEFT && m->value == CT_PRESS) {
		if(mode != -1) {
			if(mode == 1) {
				dwn_printf("add node: %d, %d, ctx: %u.\n",m->x,m->y,m->ctx);
				add_node(m);
			}
		}
	}
	}


	
	if(!capture_reposition(m))
		return 0;
	struct dwn_vec3 point;
	struct dwn_cnode *n = _dwn_main.nodes;

	struct iu_window *w = iu_grabwd_at(0);
	point.x = w->_b.comp.vpw*(float)m->x;
	point.y = w->_b.comp.vph*(float)m->y;
	point.x-=_dwn_main.xdis;
	point.y-=_dwn_main.ydis;
	point.x*=1./_dwn_main.node_scale.value;
	point.y*=1./_dwn_main.node_scale.value;

	if(!dropselection){
		if(m->code == CY_BTN_LEFT && m->value == CT_RELEASE) {
			dropselection = -1;	
			return 0;
		}
		/* selected_node not 'n' you fuckwit*/	
		selected_node->trk->x = point.x;
		selected_node->trk->y = point.y;

		setshape_pos(point.x,point.y,selected_node->trk->shape);
		update_node(selected_node,point.x,point.y);

		return -1;
	}

	if(m->code == CY_BTN_LEFT && m->value == CT_PRESS) {
		while(n != NULL) {
			if(!dwn_tri_intersect(n->tracks[0].shape,&point)) {
				dwn_printf("intersecting with point\n");
				selected_node = n;
				dropselection = 0;
				n->trk = n->tracks;
				return 0;
			}else
			if(!dwn_tri_intersect(n->tracks[1].shape,&point)) {
				dwn_printf("intersecting with point\n");
				selected_node = n;
				dropselection = 0;
				n->trk = n->tracks+1;
				return 0;
			}
			n = n->next;
		}
	}
	return -1;
}

void static _add_node(struct iu_btn *b,void *ig, struct ct_msg *m) {
	if(m->code == 0) {
		bypass = !bypass?-1:0;
		dwn_printf("operation mode: %d.\n",mode);
		if(mode == -1) {
			mode = 1;
			b->c.g = 0;
		}else{
			b->c.g = 1;
			mode = -1;
		}
	}
}
void static _side_tools(void) {
	struct iu_render *rn;
	elem.side_board._w._w = 194;
	elem.side_board._w._h = 410;
	elem.side_board.parent = &elem.window._b.comp;

	iu_board_init(&elem.side_board);
	iu_brdinit(&elem.side_board);
	iu_board_load(&elem.side_board);

	iu_wgmove(0,&elem.side_board,0,102);

	/*
	*	node scale
	*/

	iu_label_init(&elem.node_label);
	iu_label_text(&elem.node_label, "node scale");
	elem.node_label.size = 8;
	elem.node_label._w.xd = 0;
	elem.node_label._w.yd = 20;

	iu_scale_init(&elem.node_scale);
	iu_wgchild(&elem.node_label,&elem.node_scale,elem.node_scale._w.draw_chain);
	iu_wgchild(&elem.node_label,&elem.node_scale,elem.node_scale._w.move_chain);

	float xalg;
	xalg = iu_board_xalign(&elem.side_board,&elem.node_scale);
	iu_wgmove(0,&elem.node_scale,(_int_u)xalg,35);
	elem.node_scale.value = 1;

	struct iu_composer *comp = &elem.side_board.comp;
	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.node_scale;

	/*
	*	move scale
	*/
	iu_label_init(&elem.move_label);
	iu_label_text(&elem.move_label, "move scale");
	elem.move_label.size = 8;
	elem.move_label._w.xd = 0;
	elem.move_label._w.yd = 20;

	iu_scale_init(&elem.move_scale);	
	iu_wgchild(&elem.move_label,&elem.move_scale,elem.move_scale._w.draw_chain);
	iu_wgchild(&elem.move_label,&elem.move_scale,elem.move_scale._w.move_chain);

	xalg = iu_board_xalign(&elem.side_board,&elem.move_scale);
	iu_wgmove(0,&elem.move_scale,(_int_u)xalg,71);
	elem.move_scale.value = 1;

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.move_scale;

	/*
	*	x scale
	*/
	iu_label_init(&elem.xs_label);
	iu_label_text(&elem.xs_label, "x scale");
	elem.xs_label.size = 8;
	elem.xs_label._w.xd = 0;
	elem.xs_label._w.yd = 20;

	iu_scale_init(&elem.x_scale);
	iu_wgchild(&elem.xs_label,&elem.x_scale,elem.x_scale._w.draw_chain);
	iu_wgchild(&elem.xs_label,&elem.x_scale,elem.x_scale._w.move_chain);
	
	xalg = iu_board_xalign(&elem.side_board,&elem.x_scale);
	iu_wgmove(0,&elem.x_scale,(_int_u)xalg,107);
	elem.x_scale.value = 0.5;

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.x_scale;

	/*
	*	y scale
	*/
	iu_label_init(&elem.ys_label);
	iu_label_text(&elem.ys_label, "y scale");
	elem.ys_label.size = 8;
	elem.ys_label._w.xd = 0;
	elem.ys_label._w.yd = 20;

	iu_scale_init(&elem.y_scale);
	iu_wgchild(&elem.ys_label,&elem.y_scale,elem.y_scale._w.draw_chain);
	iu_wgchild(&elem.ys_label,&elem.y_scale,elem.y_scale._w.move_chain);

	xalg = iu_board_xalign(&elem.side_board,&elem.y_scale);
	iu_wgmove(0,&elem.y_scale,(_int_u)xalg,143);
	elem.y_scale.value = 0.5;

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.y_scale;

	iu_toolbar_init(&elem.side_tools);
	elem.side_tools._w.h = 25;	
	iu_btn_init(&elem.add_button);
	iu_btn_init(&elem.sel_button);

	elem.add_button._w.w = 25;
	elem.add_button._w.h = 25;
	
	elem.sel_button._w.w = 25;
	elem.sel_button._w.h = 25;

	elem.add_button._w.output = _add_node;
	elem.add_button._w.out = 0;
	elem.add_button._w.codetab = tab_for_exit;//ignore is dummy

	iu_btn_with_icon(&elem.add_button);
	elem.add_button.icon = &icon_add;
	elem.add_button.tex = &icons;	
	iu_btn_with_icon(&elem.sel_button);
	elem.sel_button.icon = &icon_select;
	elem.sel_button.tex = &icons;
	iu_toolbar_add(&elem.side_tools,&elem.add_button,0);
	iu_toolbar_add(&elem.side_tools,&elem.sel_button,32);
	iu_toolbar_confine(&elem.side_tools);
	xalg = iu_board_xalign(&elem.side_board,&elem.side_tools);
	iu_wgmove(0,&elem.side_tools,(_int_u)xalg,204);	
   	struct iu_tuck *tc = &elem.side_board;
	tc->icon0 = &icon_collapse;
	tc->icon1 = &icon_expand;
	tc->tex = &icons;

	iu_tuck_init(tc);
	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.side_tools;

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.side_board;
	iu_load(&elem.window);

}

void static _export(struct iu_btn *b,void *ig, struct ct_msg *m) {
	char buf[128];
	iu_text_charstr(&elem.export_path,buf);
	buf[elem.export_path.len] = '\0';

	dwn_printf("expoting to '%s'.\n",buf);
	_int_u i;
	i = 0;
	/*
		check the path output sanity
	*/
	for(;i != elem.export_path.len;i++) {
		char c = buf[i];
		if((c<'a' || c>'z') && c != '.') {
			return;
		}
	}
	dwn_wf_export(buf);
}

void msgsink(struct iu_msgdata *m) {
	if(m->m.code == CY_BTN_LEFT && m->m.value == CT_PRESS && m->id != 0) {
		iu_menu_hide(&elem.context_menu);
	}
}


static void *focused_text = NULL;
void _altoutput(struct iu_text *t,void *ig, struct iu_msgdata *m) {
	if (m->m.code == 0) {
		struct iu_window *w = iu_grabwd_at(0);
		float x = w->_b.comp.vpw*(float)m->x;
		float y = w->_b.comp.vph*(float)m->y;
		iu_wgmove(0,&elem.context_menu,x,y);
		focused_text = t;
		iu_menu_show(&elem.context_menu);	
		return;
	}
	t->_w.input(t,ig,m);
}

void static _copy_to_clipboard(struct iu_btn *b,void *ig, struct ct_msg *m) {
	struct iu_text *t = focused_text;
	if(!t) {
		dwn_printf("dead in the water.\n");
		return;
	}
	_int_u len = t->cursor_end;
	_int_u start = t->cursor_pos;
	char buf[128];
	iu_text_charstr_selection(t,buf,start,len);
	buf[len] = '\0';
	dwn_printf("copy selection: '%s'.\n",buf);
	if(*buf != '\0' && len>0) {
		cm_set(buf,len);
	}
}

void _export_borad(void) {
	struct iu_composer *comp = &elem.export_board.comp;
	struct iu_render *rn;
	elem.export_board._w._w = 194;
	elem.export_board._w._h = 256;
	elem.export_board.parent = &elem.window._b.comp;

	iu_board_init(&elem.export_board);
	iu_brdinit(&elem.export_board);
	iu_board_load(&elem.export_board);

	iu_wgmove(0,&elem.export_board,0.0,102);


	iu_btn_init(&elem.export_button);
	elem.export_button._w.w = 25;
	elem.export_button._w.h = 25;

	elem.export_button._w.output = _export;
	elem.export_button._w.out = 0;
	elem.export_button._w.codetab = tab_for_exit;//ignore is dummy

	iu_btn_with_icon(&elem.export_button);
	elem.export_button.icon = &icon_file;
	elem.export_button.tex = &icons;
 	float xalg;
	xalg = iu_board_xalign(&elem.export_board,&elem.export_button);
	iu_wgmove(0,&elem.export_button,(_int_u)xalg,204);

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.export_button;

	iu_shell_init(&elem.entry_shell);

	iu_text_init(&elem.export_path);

#define TEXT_TEST "test.obj"
	iu_text_deposit(&elem.export_path,TEXT_TEST,sizeof(TEXT_TEST)-1);

	elem.export_path.c.r = 0;
	elem.export_path.c.g = 0;
	elem.export_path.c.b = 0;
	elem.export_path.c.a = 1;
	elem.export_path.size = 8;
	elem.export_path.bits = 0;
	elem.export_path._w.parent = NULL;
	elem.export_path._w.output = _altoutput;
	iu_text_determin(&elem.export_path);
	elem.export_path.max = 194;
	elem.export_path._w.w = 194;
	elem.export_path._w.width_limit = 194;
	iu_wgsize1(&elem.export_path);

	iu_wgchild(&elem.entry_shell,&elem.export_path,elem.export_path._w.draw_chain);
	iu_wgchild(&elem.entry_shell,&elem.export_path,elem.export_path._w.move_chain);
	iu_shell_add(&elem.entry_shell,&elem.export_path);

	iu_wgmove(NULL,&elem.export_path,0,0);

	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.export_path;

	
	rn = iu_render(comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.export_board;

	iu_load(&elem.window);
	iu_brdhide(&elem.export_board);
}

void static _context_menu(void) {
	struct iu_render *rn;
	iu_menu_init(&elem.context_menu);
	elem.context_menu.min_width = 128;
	elem.context_menu.min_height = 4*15;
	elem.context_menu.id = 0;
	struct iu_caplv *tmp;
	iu_exccapt(&tmp,&elem.context_menu.capt);
	iu_btn_init(&elem.copy_button);
	iu_exccapt(NULL,tmp);
	
	elem.copy_button._w.w = 128;
	elem.copy_button._w.h = 15;

	iu_btn_with_text(&elem.copy_button,"Copy");
	elem.copy_button._w.output = _copy_to_clipboard;
	elem.copy_button._w.out = 0;
	elem.copy_button._w.codetab = tab_for_exit;//ignore is dummy


	iu_menu_add(&elem.context_menu,&elem.copy_button);
	iu_menu_lay(&elem.context_menu);
	iu_menu_show(&elem.context_menu);

	rn = _iu_render(&elem.window._b.comp,3);
	rn->func = iu_wgdraw;
	rn->priv = &elem.context_menu;
	elem.context_menu._w.rn = rn;
	iu_wgmove(NULL,&elem.context_menu,512,512);
	iu_menu_hide(&elem.context_menu);
}

void static prep_ui(void) {
	struct iu_render *rn;
//	debug_tests();

	_file_menu();
	_tools_bar();
	_side_tools();
	_export_borad();
	_context_menu();

	/*
		window size
	*/
	elem.wd2._b._w._w = 409;
	elem.wd2._b._w._h = 409;

	elem.wd2._b.parent = &elem.window._b.comp;

	
	iu_wdinit(&elem.wd2);
	iu_wdinit0(&elem.wd2);

	iu_load(&elem.wd2);

	iu_rect_init(&elem.wd2_bg);
	elem.wd2_bg._w._w = 204;
	elem.wd2_bg._w._h = 204;

	/*
		exit button for window
	*/
	iu_btn_init(&elem.exit_button);
	elem.exit_button._w.w = 24;
	elem.exit_button._w.h = 24;
	iu_btn_with_icon(&elem.exit_button);
	elem.exit_button.icon = &icon_exit;
	elem.exit_button.tex = &icons;

	elem.exit_button._w.codetab = transtab_exit_wd;
	elem.exit_button._w.output = elem.wd2._b._w.input;
	elem.exit_button._w.out = &elem.wd2;

	iu_load(&elem.window);

//	iu_wgchild(&elem.exit_button,&elem.wd2,elem.wd2._b._w.draw_chain);
//	iu_wgchild(&elem.wd2_bg,&elem.wd2,elem.wd2._b._w.draw_chain);
	rn = iu_render(&elem.wd2._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.exit_button;

	rn = _iu_render(&elem.wd2._b.comp,0);
	rn->func = iu_wgdraw;
	rn->priv = &elem.wd2;

	iu_rect_init(&elem.wd2_decor);
	elem.wd2_decor._w._w = 409-24;
	elem.wd2_decor._w._h = 24;
	elem.wd2_decor._w.xd0 = 0;
	elem.wd2_decor._w.yd0 = 0;
	iu_wgchild(&elem.wd2,&elem.wd2_decor,elem.wd2_decor._w.move_chain);
	iu_wgmove(0, &elem.wd2_decor,409,409);

	elem.wd2_decor._w.output = elem.wd2._b._w.input;
	elem.wd2_decor._w.out = &elem.wd2;
	elem.wd2_decor._w.codetab = transtab;
	rn = iu_render(&elem.window._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.wd2_decor;
	elem.wd2.decor = &elem.wd2_decor;
	elem.wd2.move_decor = iu_wgmove;
	elem.wd2.decor_output = elem.wd2_decor._w.input;
	elem.wd2.decor_codetab = transtab_wd_to_decor; 

	iu_load(&elem.wd2);
	iu_wgmove(0,&elem.wd2_bg,0,0);
	iu_wgmove(0,&elem.exit_button,409-24,0);

	iu_load(&elem.window);

	//debuggin
	elem.fm_settings_button._w.output = elem.wd2._b._w.input;
	elem.fm_settings_button._w.codetab = transtab_exit_wd;
	elem.fm_settings_button._w.out = &elem.wd2;
	iu_wdhide(&elem.wd2);
	iu_wgtrajection(&elem.exit_button);
	iu_wgtrajection(&elem.wd2);
}

#define WWIDTH	768
#define WHEIGHT	768

#include "../y.h"
#include "../yarn/yarn.h"
void dwn_gint(void);
static struct iu_btn exemplar_btn;
static struct iu_btn exemplar_btn2;
void _iu_init(void) {
	dwn_init();

	dwn_gint();

	load_icons();
	iu_window_new(&elem.window);
	iu_window_map(&elem.window,0,64,WWIDTH,WHEIGHT);
	iu_load(&elem.window);
	prep_ui();

	elem.context_menu._w.cap->lv = 0;

	iu_btn_init(&exemplar_btn);
	exemplar_btn._w.w = 80;
	exemplar_btn._w.h = 20;
	iu_btn_with_text(&exemplar_btn,"exemplar-0");

	iu_btn_init(&exemplar_btn2);
	exemplar_btn2._w.w = 80;
	exemplar_btn2._w.h = 20;
	iu_btn_with_text(&exemplar_btn2,"exemplar-1");

	main_rect = iu_captnew();
  main_rect->sub.update = _msg;
	struct iu_hbox *hb = &main_rect->sub.hb;
	hb->x = 0;
	hb->y = 0;
	hb->xfar = WWIDTH;
	hb->yfar = WHEIGHT;
	iu_captattach(main_rect);

	struct iu_render *rn;
	rn = iu_render(&elem.window._b.comp);
	rn->func = iu_wgdraw;
	rn->priv = &elem.tabs_test;

	iu_tab_init(&elem.tabs_test);
	elem.tabs_test._w._w = 400;
	elem.tabs_test._w._h = 400;
	iu_tab_wadd(elem.tabs_test.page,&exemplar_btn);
	iu_tab_wadd(elem.tabs_test.page+1,&exemplar_btn2);

	iu_wgmove(0,&exemplar_btn,10,10);
	iu_wgmove(0,&exemplar_btn2,100,100);
	
	iu_wgmove(0,&elem.tabs_test,0,540);
	iu_wgtrajection(&elem.tabs_test);
	iu_wgtrajection(&exemplar_btn);
	iu_wgtrajection(&exemplar_btn2);

	iu_wgtrajection(&elem.context_menu);
	iu_wgtrajection(&elem.copy_button);

	iu_wgtrajection(&elem.export_button);
	iu_wgtrajection(&elem.export_path);
	iu_wgtrajection(&elem.export_board);
	iu_wgtrajection(&elem.side_board);

	iu_wgtrajection(&elem.node_scale);
	iu_wgtrajection(&elem.move_scale);
	iu_wgtrajection(&elem.x_scale);
	iu_wgtrajection(&elem.y_scale);

	iu_wgtrajection(&elem.side_tools);
	iu_wgtrajection(&elem.add_button);
	iu_wgtrajection(&elem.sel_button);

	iu_wgtrajection(&elem.file_button);
	iu_wgtrajection(&elem.file_menu);
	iu_wgtrajection(&elem.fm_exit_button);
	iu_wgtrajection(&elem.fm_export_button);
	iu_wgtrajection(&elem.fm_settings_button);

/*
	NEVER FORGET THAT THE DEFAULT IS INACTIVE CAPTURE
*/
	iu_wgtrajection(&elem.wd2_decor);
}


void _iu_deinit(void) {
	iu_window_unmap(&elem.window);
	iu_window_destroy(&elem.window);
}

void dwn_draw_model(void);
#include "../maths.h"
#include "../linux/time.h"

void _iu_tick(void) {
	struct yarn_texture window;
	window.priv = elem.window.surf->render;
	dwn_clear(&window,.4,.4,.4,1);
	if(!_dwn_main.node_cnt)return;
	h_ident(_dwn_main.node_trans);

	h_mscale(_dwn_main.node_trans,_dwn_main.node_scale.value);
	_dwn_main.node_trans[3] = _dwn_main.xdis = 204+512*(_dwn_main.x_scale.value-0.5);
	_dwn_main.node_trans[7] = _dwn_main.ydis = 204+512*(_dwn_main.y_scale.value-0.5);
	_flu_float *flt = _dwn_main.perc_const;
	h_matcopY(flt,_dwn_main.node_trans);

	struct yarn_buffer vtbuf, lines;
	if(_dwn_main.node_cnt>1) {
		yarn_flue_buffer_new(&lines);
		yarn_flue_buffer_data(&lines,sizeof(float)*4*4*(_dwn_main.node_cnt-1));
		yarn_flue_buffer_map(&lines);
	}

	yarn_flue_buffer_new(&vtbuf);
	yarn_flue_buffer_data(&vtbuf,sizeof(float)*4*4*_dwn_main.node_cnt*2);
	yarn_flue_buffer_map(&vtbuf);

	float *vtx = vtbuf.cpu_ptr;
	struct dwn_cnode *n = _dwn_main.nodes;
	while(n != NULL) {
		struct dwn_track *trk = n->tracks;
		vtx[0] = trk->shape[0].x;
		vtx[1] = trk->shape[0].y;
		vtx[2] = 0;
		vtx[3] = 1;

		vtx[4] = trk->shape[1].x;
		vtx[5] = trk->shape[1].y;
		vtx[6] = 0;
		vtx[7] = 1;

		vtx[8] = trk->shape[2].x;
		vtx[9] = trk->shape[2].y;
		vtx[10] = 0;
		vtx[11] = 1;

		vtx+=12;
	
		trk = n->tracks+1;
		vtx[0] = trk->shape[0].x;
		vtx[1] = trk->shape[0].y;
		vtx[2] = 0;
		vtx[3] = 1;

		vtx[4] = trk->shape[1].x;
		vtx[5] = trk->shape[1].y;
		vtx[6] = 0;
		vtx[7] = 1;

		vtx[8] = trk->shape[2].x;
		vtx[9] = trk->shape[2].y;
		vtx[10] = 0;
		vtx[11] = 1;
		vtx+=12;

		n = n->next;
	}

	vtx = lines.cpu_ptr;
	n = _dwn_main.nodes;
	while(n->next != NULL) {
		struct dwn_cnode *f = n->next;
		struct dwn_track *trk = n->tracks;
		vtx[0] = trk->shape[0].x+2;
		vtx[1] = trk->shape[0].y;
		vtx[2] = 0;
		vtx[3] = 1;

		vtx[4] = trk->shape[0].x;
		vtx[5] = trk->shape[0].y;
		vtx[6] = 0;
		vtx[7] = 1;

		trk = f->tracks;
		vtx[8] = trk->shape[0].x-2;
		vtx[9] = trk->shape[0].y;
		vtx[10] = 0;
		vtx[11] = 1;

		vtx[12] = trk->shape[0].x;
		vtx[13] = trk->shape[0].y;
		vtx[14] = 0;
		vtx[15] = 1;
		vtx+=16;
		n = n->next;
	}
	struct yarn_viewport vpor;
	vpor.translate[0] = 0;
	vpor.translate[1] = 0;
	vpor.translate[2] = 0;
	vpor.translate[3] = 0;

	vpor.scale[0] = 1.;
	vpor.scale[1] = 1;
	vpor.scale[2] = 1;
	vpor.scale[3] = 0;

	yarn_flue_viewport(&vpor);

	dwn_draw_begin();
	dwn_draw_tame(&vtbuf);
	dwn_draw_array(_dwn_main.node_cnt*2,DWN_TRI);
	if(_dwn_main.node_cnt>1) {
		dwn_draw_begin();
		dwn_draw_tame(&lines);
		dwn_draw_array(_dwn_main.node_cnt-1,DWN_QUAD);
		yarn_flue_buffer_destroy(&lines);
	}
	yarn_flue_buffer_destroy(&vtbuf);

	h_ident(_dwn_main.trans);
	h_ident(_dwn_main.proj);
	h_ident(_dwn_main.model);
	_dwn_main.model[14] = 0;

#define MOVSCALE 0.1
	trans.x+=_xp;
	trans.y+=_yp;
	trans.z+=_zp;

	h_translate(_dwn_main.trans,trans.x*MOVSCALE,trans.y*MOVSCALE,trans.z*MOVSCALE);
	h_perspective(_dwn_main.proj,trad(90.),0);
	h_frustum(_dwn_main.proj,1,100,10,-10,10,-10);
	h_mxrot(_dwn_main.model,trad(rotx));
	h_myrot(_dwn_main.model,trad(roty));
	struct timespec ts_start,ts_end;
	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	
	
	dwn_draw_model();

	clock_gettime(CLOCK_MONOTONIC, &ts_end);
	double time;
	time = (ts_end.tv_sec-ts_start.tv_sec)+((double)(ts_end.tv_nsec-ts_start.tv_nsec)*(1./1000000000.));
	log_printf(&_dwn_main.logfile,"elapse time: %f-seconds.\n",time);
	

	rotx+=rotonx;
	roty+=rotony;
}


_err_t main(int __argc, char const *__argv[]) {
	y_log_file(&_dwn_main.logfile,"dwnlog");
	cm_connect();
	iu_start();
	cm_disconnect();
	y_log_fileend(&_dwn_main.logfile);
}
