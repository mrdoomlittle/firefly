# ifndef __ffly__exec__h
# define __ffly__exec__h
# include "y_int.h"
# include "resin/exec.h"

struct f_exc_ctx {
	int fd;
	_8_u *hdr;

	_8_u *p;
	_64_u end;
};

typedef struct f_exc_segment {
 	_64_u offset;
	_64_u adr;
	_int_u size;
} *f_exc_segmentp;

enum {
	_ffexec_bc,
	_ffexec_script
};
enum {
	_f_exc_rmc,
	_f_exc_fspt
};

void f_remf_exec(struct f_exc_ctx*);
void f_exc_ldsegs(struct f_exc_ctx*, f_exc_segmentp, _int_u);
void f_exc_su(struct f_exc_ctx*, _int_u);
void f_exc_read(struct f_exc_ctx*, void*, _int_u, _64_u);

void ffexecf(char const*);
void f_excf(char const*);

# endif /*__ffly__exec__h*/
