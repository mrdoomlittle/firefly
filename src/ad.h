# ifndef __ffly__ad__h
# define __ffly__ad__h
# include "y_int.h"

/*
	rename 

	what?

	a collective that vec,buff,map, etc parts share


*/
struct ffly_adentry {
	void*(*alloc)(_ulonglong, _int_u);
	_8_s(*free)(_ulonglong, void*);
	void*(*realloc)(_ulonglong, void*, _int_u);
};

/*
	remove functions and use macros????
*/
struct ffly_adentry* ffly_ad_entry(_16_u);
void* ffly_ad_alloc(_16_u, _ulonglong, _int_u);
_8_s ffly_ad_free(_16_u, _ulonglong, void*);
void* ffly_ad_realloc(_16_u, _ulonglong, void*, _int_u);
# endif /*__ffly__ad__h*/
