#include "../io.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../linux/stat.h"
#include "../linux/fcntl.h"
#include "../linux/unistd.h"
#include "../y_int.h"
#include "../tools.h"
#include "../blf.h"
void em_pic18_dis(_8_u *__buf,_int_u __words);
int main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("no file provided.");
		return -1;
	}
	int fd;
	fd = open(__argv[1],O_RDONLY,0);
	
	struct blf_hdr h;
	read(fd,&h,sizeof(struct blf_hdr));

	struct blf_frag *frags = m_alloc(h.ftsz);
	pread(fd,frags,h.ftsz,h.ft);
	_int_u i;
	i = 0;
	for(;i != h.nfr;i++) {
		struct blf_frag *f = frags+i;
		printf("FRAG-%u.\n",i);
		if (f->size != 0) {
			_8_u *data = m_alloc(f->size);
			pread(fd,data,f->size,f->off+h.start);
			em_pic18_dis(data,f->words);
			m_free(data);
		}
	}

	close (fd);
}

