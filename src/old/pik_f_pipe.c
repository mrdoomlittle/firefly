# include "pik.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/pipe.h"
struct context {
	_int_u pipe;
};

static void* _init(struct f_pik_f_pipe *__par) {
	struct context *ctx;
	ctx = (struct context*)__f_mem_alloc(sizeof(struct context));
	ctx->pipe = __par->pipe;
	return ctx;
}

static void _de_init(struct context *__ctx) {
	__f_mem_free(__ctx);
}

_32_u static
_read(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	ffly_pipe_read(__buf, __size, __ctx->pipe);
	return 0;
}

_32_u static
_write(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	ffly_pipe_write(__buf, __size, __ctx->pipe);
	return 0;
}

_8_s static
_listen(struct context *__ctx) {
	return ffly_pipe_listen(__ctx->pipe);
}

void static
_connect(struct context *__ctx) {
	ffly_pipe_connect(__ctx->pipe);
}

#define CAST (void*)
static struct f_pik_func func = {
	CAST _init,
	CAST _de_init,
	.rws = {
		NULL,
		NULL,
		CAST _read,
		CAST _write,
		NULL,
		CAST listen,
		CAST connect
	}
};

void f_pik_f_pipe_load(struct f_pik_func *__func) {
	*__func = func;
}
