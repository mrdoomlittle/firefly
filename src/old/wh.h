# ifndef __wh__h
# define __wh__h
# include "y_int.h"
/*
	if we need storage thats not in memory
	
	stored across many files(or storage containers)
	and non permanet
*/
void wh_init(_int_u);
void wh_de_init(void);

void* wh_alloc(_int_u);
void wh_free(void*);

void wh_read(void*, void*, _int_u, _32_u);
void wh_write(void*, void*, _int_u, _32_u);
# endif /*__wh__h*/
