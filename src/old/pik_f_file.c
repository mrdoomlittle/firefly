# include "pik.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/file.h"
struct context {
	FF_FILE *f;
};

static void* _init(struct f_pik_f_file *__par) {
	struct context *ctx;
	ctx = (struct context*)__f_mem_alloc(sizeof(struct context));
	ctx->f = __par->f;
	return ctx;
}

static void _de_init(struct context *__ctx) {
	__f_mem_free(__ctx);
}

static _32_u
_pwrite(struct context *__ctx, void *__buf, _int_u __size, _64_u __offset, _8_s *__error) {
	ffly_fpwrite(__ctx->f, __buf, __size, __offset);
	return 0;
}

static _32_u
_pread(struct context *__ctx, void *__buf, _int_u __size, _64_u __offset, _8_s *__error) {
	ffly_fpread(__ctx->f, __buf, __size, __offset);
	return 0;
}

static _32_u
_read(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	ffly_fread(__ctx->f, __buf, __size);
	return 0;
}

static _32_u
_write(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	ffly_fwrite(__ctx->f, __buf, __size);
	return 0;
}

#define CAST (void*)
static struct f_pik_func func = {
	CAST _init,
	CAST _de_init,
	.rws = {
		CAST _pwrite,
		CAST _pread,
		CAST _read,
		CAST _write,
		NULL,
		NULL,
		NULL
	}
};

void f_pik_f_file_load(struct f_pik_func *__func) {
	*__func = func;
}
