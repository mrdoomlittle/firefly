#ifndef __y__haze__h
#define __y__haze__h
#include "y_int.h"
#include "cpt_struc.h"
#include "billet.h"
#include "mutex.h"
#define HAZE_BLOCK_MASK 0x0f
#define HAZE_BLOCK_SHIFT 4
#define HAZE_BLOCK_SIZE (1<<HAZE_BLOCK_SHIFT)

#define _HAZE_BLOCK_ALOAD 0x01
#define HZOUT_MASK 0x01
#define HAZE_OBSHFT 8
#define HAZE_OBUFSZ (1<<HAZE_OBSHFT)
#define HAZE_PREFORM(__ctx,__op)\
	(((struct haze_context*)__ctx)->funcs->preform[__op])
struct haze_block {
	void *p;
	_int_u size;
	_32_u offset;
	_8_u flags;
	struct haze_block *next;
};

struct haze_out {
	_8_u buf[HAZE_OBUFSZ];
	_8_s commence;
};

struct haze_data {
	struct haze_block *b;
	
	struct haze_out out[2];
	_8_u outnx;
	struct y_blt *blt;
	// data flow
	void(*loadblock)(struct haze_block*,struct haze_data*);
	void(*load)(void*, void*, _int_u, _32_u);
	void(*store)(void*, void*, _int_u, _32_u);
};

struct haze_priv {
	mlock lock;
	struct haze_data *d;
};

struct haze_context {
	_8_u user[0x100];
	struct haze_block *top;
	struct haze_func_struc *funcs;
	void *ctx;
	_32_u off;

	_8_u buf[HAZE_BLOCK_SIZE];
	_int_u bufoff;
	_8_s hasbuf;
	struct haze_data data;
};

struct haze_func_struc {
	void*(*init)(void);
	void(*preform[8])(void*, struct haze_data*, struct cpt_out*);
	void(*de_init)(void*);
	struct haze_priv *pv;
};
extern struct haze_func_struc haze_funcs[1];

#define _HAZE_HASH	0
#define _HAZE_ENCPT	1
#define _HAZE_DECPT	2
void haze_tick(_ulonglong);
struct haze_context* haze_ctx_new(struct haze_func_struc*);
void haze_ctx_destroy(struct haze_context*);
void haze_more(struct haze_context*, void*, _int_u);
/*
	op is passed as number because we could use diffrent algo's,

*/
void haze_final(struct haze_context*, void*, struct cpt_out*);
#endif /*__y__haze__h*/
