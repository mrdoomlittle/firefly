#include "tug.h"
#include "../io.h"
#include "../linux/stat.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../linux/net.h"
#include "../inet.h"
#include "../linux/errno-base.h"
#include "../linux/un.h"
#include "../linux/unistd.h"
#include "../assert.h"
#include "../string.h"
#include "../linux/ioctls.h"
#include "../linux/fcntl.h"
#include "../linux/poll.h"
#include "../assert.h"
#include "../system/errno.h"
static struct sockaddr_un u_adr;
static int u_skt;
int tug_msgrcv(struct tug_msghdr *__m, int sk, int *__skt) {
    struct msghdr msg;
    struct iovec iov;
    iov.iov_base = __m;
    iov.iovlen = sizeof(struct tug_msghdr);

    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_name = NULL;
    msg.msg_namelen = 0;

    union {
        struct cmsghdr c_msg;
        _8_u junk[CMSG_ALIGN(sizeof(struct cmsghdr)+sizeof(int))];
    }u;
	*(int*)CMSG_DATA(&u.c_msg) = -1;
	if (!__skt) {
	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	}else{
	msg.msg_control = &u.c_msg;
   	msg.msg_controllen = CMSG_LEN(sizeof(int));
   	}
	int r;
    if ((r = recvmsg(sk,&msg,0))<0) {
        printf("failed to recv message.\n");
        return -1;
    }
	int skt;
	
	*__skt = skt = *(int*)CMSG_DATA(&u.c_msg);
	
	printf("message has size of %u-bytes, from %d by %d\n",__m->size,skt,u_skt);
	return 0;
}

int tug_msgsnd(struct tug_msghdr *__m, int __skt, int sk) {
    struct msghdr msg;
    struct iovec iov;
	iov.iov_base = __m;
    iov.iovlen = sizeof(struct tug_msghdr);

    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_name = NULL;
    msg.msg_namelen = 0;

    union {
        struct cmsghdr c_msg;
        /*
            cmsghdr is allready aligned by yea, dont care.
        */
        _8_u junk[CMSG_ALIGN(sizeof(struct cmsghdr)+sizeof(int))];
    }u;

    //socket layer
    u.c_msg.cmsg_level = SOL_SOCKET;

    //we are transferring the rights of the fd0
    u.c_msg.cmsg_type = SCM_RIGHTS;
	if (sk<0) {
		msg.msg_control = NULL;
		msg.msg_controllen = 0;
	}else{
		*(int*)CMSG_DATA(&u.c_msg) = sk;
		msg.msg_control = &u.c_msg;
		msg.msg_controllen = CMSG_LEN(sizeof(int));
    }
	//cmsg len is the same as msg len as of only single hdr
    u.c_msg.cmsg_len = msg.msg_controllen;
	
    if (sendmsg(__skt,&msg,0)<=0) {
        printf("failed to send message.\n");
        return -1;
    }
}

int tug_read(int __fd,void *__buf,_int_u __size) {
	read(__fd,__buf,__size);
}
int tug_write(int __fd,void *__buf,_int_u __size) {
	write(__fd,__buf,__size);
}

static _64_u buf[64];
static int u_fd;
static struct tug_bay *inuse_bays = NULL;
static struct tug_msghdr *m = buf;
static int fds[8];
/*
	the place in which things happen,
	a client can read or write to any of them
*/
static struct tug_bay bays[12];

static struct tug_bay *bay_free[] = {
	bays+4,
	bays+5,
	bays+6,
	bays+7,
	bays+8,
	bays+9,
	bays+10,
	bays+11
};
static _int_u bay_nfree = 8;

void static inlet_msg(void){
	struct tug_bay *b;
	if (m->bn == ~(_64_u)0) {
		b = bay_free[--bay_nfree];
		_64_u bn = b-bays;
		write(u_fd,&bn,8);
	}else {
		b = bays+m->bn;
	}
	b->fd_in = u_fd;
}
void static outlet_msg(void) {
	struct tug_bay *b = bays+m->bn;
	b->fd_out = u_fd;
}

#include "../tools.h"
void static hello_msg(void) {
	printf("HELLO MESSAGE.\n");
	struct tug_bay *bay = bays+m->bn;
	bay->lullabyed = -1;
	bay->tx = 0;

	bay->bk = &inuse_bays;
	bay->next = inuse_bays;
	if(inuse_bays != NULL) {
		inuse_bays->bk = &bay->next;
	}
	inuse_bays = bay;
}

void static linkup(void);
void static lullaby_msg(void) {
	printf("LILLABY MESSAGE, bay: %u\n",m->bn);
	struct tug_bay *bay = bays+m->bn;

	printf("any pending? %u != %u.\n",bay->tx,m->size);
	while(bay->tx != m->size) {
		linkup();
	}

	printf("bay-%u lullabyed.\n",m->bn);
	ffly_fdrain(_ffly_out);
	*bay->bk = bay->next;
	if (bay->next != NULL)
		bay->next->bk = bay->bk;

	_64_u dum;
	tug_write(bay->fd_in,&dum,4);
	bay->lullabyed = 0;
}

void static done_msg(void) {
	struct tug_bay *bay = bays+m->bn;
	/*
		if the bay we are using is not of royalty then put it with the other peasants.
	*/
	if (m->bn>3) {
		bay_free[bay_nfree++] = bay;
	}
	close(bay->fd_in);
	close(bay->fd_out);
}


void static exec_msg(void){


}
static void(*msg[])(void) = {
	inlet_msg,
	outlet_msg,
	hello_msg,
	lullaby_msg,
	done_msg,
	exec_msg
};
/*
	look ive taken a look at ubus as its code is simplistic,
	anyway i dont know how ubus deals with IPC without going thrugh the deamon,
	but i would have thought that it would be like this? where each
	client has a pipe and we manage the data transfure from pipe to pipe?
*/
void linkup(void) {
	struct tug_bay *s = inuse_bays;
	for(;s != NULL;s = s->next) {
	_back:
		printf("working on BAY-%u.\n",s-bays);
		ffly_fdrain(_ffly_out);
		struct pollfd pfd = {
			.fd = s->fd_out,
			.events = POLLIN,
			.revents = 0
		};
		if (poll(&pfd,1,10)<=0) {
			printf("no bay data.\n");
			ffly_fdrain(_ffly_out);
			continue;
		}
		int r;
		struct tug_pkt p;
		r = read(s->fd_out,&p,sizeof(struct tug_pkt));
	printf("pipe message relay, to: %u, from: %u, size: %u.\n",p.to,p.from,p.size);
	ffly_fdrain(_ffly_out);

		if (r != sizeof(struct tug_pkt)){
			printf("################misfortune error- %d.\n",r);
			continue;
		}

		struct tug_bay *out = bays+p.to;
		assert(p.to<13);
		if (!out->lullabyed) {
			printf("################what the fuck, bay has been lullabyed.\n");
		}else{
		/*
			so it seems to my amazment that linux dosent have a peek system call, ie a read call without changing position
		*/
		r = write(out->fd_in,&p,sizeof(struct tug_pkt));
		if (r != sizeof(struct tug_pkt)) {
			printf("################failed to write packet header.\n");
		}
		r = splice(s->fd_out,NULL,out->fd_in,NULL,p.size,0);
		if(r != p.size) {
			printf("##################tee failure.\n");
		}
		}
		s->tx++;
		goto _back;
	}
}

void tug_pktsnd(struct tug_conn *__con,struct tug_pkt *__pkt, void *__buf) {
	int r;
	r = write(__con->TUG_OUT,__pkt,sizeof(struct tug_pkt));
	if (r != sizeof(struct tug_pkt)) {
		printf("failed to write packet header.\n");
	}
	r = write(__con->TUG_OUT,__buf,__pkt->size);
	if(r != __pkt->size) {
		printf("failed to send packet.\n");
	}
	__con->tx++;
}

_8_s tug_poll(struct tug_conn *__con){
	struct pollfd pfd = {
		.fd = __con->TUG_IN,
		.events = POLLIN,
		.revents = 0
	};
	if(poll(&pfd,1,1)<=0) {
		return -1;
	}
	return 0;
}

void tug_pktrcv(struct tug_conn *__con,struct tug_pkt *__pkt, void *__buf) {
	int r;
	r = read(__con->TUG_IN,__pkt,sizeof(struct tug_pkt));
	if (r != sizeof(struct tug_pkt)) {
		printf("failed to read packet header.\n");
	}
	printf("packet data size: %u-bytes.\n",__pkt->size);
	r = read(__con->TUG_IN,__buf,__pkt->size);
	if(r != __pkt->size) {
		printf("failed to read packet data.\n");
	}
}

void static loop(void) {

	while(1) {
	linkup();	
	struct pollfd pfd = {
		.fd = u_skt,
		.events = POLLIN,
		.revents = 0
	};
	if (poll(&pfd,1,10)<=0) {
		printf("nothing.\n");
		ffly_fdrain(_ffly_out);
		continue;
	}

	int r;
	r = tug_msgrcv(m,u_skt,&u_fd);
	if (r<0){
		printf("failed to recv.\n");
		continue;
	}
	printf("got message, type: %u, bay: %u\n",m->type,m->bn);
	ffly_fdrain(_ffly_out);
	if (m->type<7) {
		msg[m->type]();
	}else{
		printf("illegal type.\n");
		break;
	}
	}
}

void tug_pkt_sndto(struct tug_conn *__con,_32_u __to,void *__buf,_32_u __size) {
	struct tug_pkt pkt;
	pkt.from = __con->bay;
	pkt.to = __to;
	pkt.size = __size;
	tug_pktsnd(__con,&pkt,__buf);
}

#define SOCKNAME "/opt/yage/tug.skt"
void tug_start(void) {
	if((u_skt = socket(AF_UNIX, SOCK_DGRAM, 0))<0){
		return;
	}
	bzero(&u_adr,sizeof(struct sockaddr_un));
	u_adr.sun_family = AF_UNIX;
	mem_cpy(u_adr.sun_path,SOCKNAME,sizeof(SOCKNAME));
	unlink(SOCKNAME);
	int r;
	r = bind(u_skt,(struct sockaddr*)&u_adr,sizeof(struct sockaddr_un));
	chmod(SOCKNAME,S_IRWXU|S_IRWXG|S_IRWXO);
/*
	fcntl(u_skt, F_SETFL,
		fcntl(u_skt, F_GETFL,NULL)
	|	O_NONBLOCK
	);
*/
	loop();

	close(u_skt);
}
