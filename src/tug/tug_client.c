#include "tug.h"
#include "../linux/stat.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../linux/net.h"
#include "../inet.h"
#include "../linux/errno-base.h"
#include "../linux/un.h"
#include "../linux/unistd.h"
#include "../assert.h"
#include "../string.h"
#include "../io.h"
#include "../system/errno.h"
#define CLNT_SOCKNAME "/opt/yage/tug_clnt.skt"
#define SOCKNAME "/opt/yage/tug.skt"
int tug_connect(struct tug_conn *__conn) {
	if((__conn->u_fd = socket(AF_UNIX, SOCK_DGRAM, 0))<0) {
		return -1;
	}
	struct sockaddr_un *u = &__conn->u_adr;

	struct sockaddr_un s_adr;
	s_adr.sun_family = AF_UNIX;
	mem_cpy(s_adr.sun_path,SOCKNAME,sizeof(SOCKNAME));

	if (connect(__conn->u_fd,(struct sockaddr*)&s_adr,sizeof(struct sockaddr_un))<0) {
		printf("failed to connect, %s\n",strerror(errno));
		return -1;
	}
	if(pipe(__conn->fd)<0) {
		printf("failed to create pipe.\n");
		return -1;
	}
	if(pipe(__conn->fd0)<0) {
		printf("failed to create pipe.\n");
		return -1;
	}
	__conn->tx = 0;

	return 0;
}

int tug_done(struct tug_conn *__con) {
	struct tug_msghdr m;
	m.type = TUG_DONE;
	m.size = 0;
	m.bn = __con->bay;
	tug_msgsnd(&m,__con->u_fd,-1);
}

int tug_disconnect(struct tug_conn *__con) {
	tug_done(__con);
	close(__con->u_fd);
}

/*
	tell the server we are ready
*/
int tug_hello(struct tug_conn *__con) {
	struct tug_msghdr m;
	m.type = TUG_HELLO;
	m.size = 0;
	m.bn = __con->bay;
	tug_msgsnd(&m,__con->u_fd,-1);
}

int tug_lullaby(struct tug_conn *__con) {
	struct tug_msghdr m;
	m.type = TUG_LULLABY;
	m.size = __con->tx;
	m.bn = __con->bay;
	tug_msgsnd(&m,__con->u_fd,-1);
	_64_u dum;
	tug_read(__con->TUG_IN,&dum,4);
}

/*
	this setup are side of things
*/
void tug_establish(struct tug_conn *__conn, _64_u __bay) {
	struct tug_msghdr m;
	m.type = TUG_INLET;
	m.size = 0;
	m.bn = __bay;
	/*
		fd[0]		= we read
		fd[1]		= they write
		fd0[0]		= they read
		fd0[1]		= we write
	*/
	tug_msgsnd(&m,__conn->u_fd,__conn->fd[1]);
	m.type = TUG_OUTLET;
	if (__bay == ~(_64_u)0) {
		read(__conn->fd[0],&__bay,8);
		printf("################################.\n");
		ffly_fdrain(_ffly_out);
		m.bn = __bay;
	}

	tug_msgsnd(&m,__conn->u_fd,__conn->fd0[0]);
	__conn->bay = __bay;

//	int skt;
//	tug_msgrcv(&m,__conn->u_fd,&skt);
//	_64_u id;

//	read(__conn->u_fd,&id,8);

//	printf("got client id of %u.\n",id);
}

