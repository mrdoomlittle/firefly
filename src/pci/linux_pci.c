# include "../pci.h"
# include "../strf.h"
#define _LARGEFILE64_SOURCE
#define BUS "/sys/bus/pci/devices"
# include <unistd.h>
# include <fcntl.h>
# include <malloc.h>
# include <stdio.h>
#include <dirent.h>
# include <errno.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
struct device {
	char path[256];
};

static struct device devices[DEVICE_MAX];
static _int_u ndev = 0;

#define DEVICE(__name, __n)\
	struct device *__name;\
	__name = getdev(__n);
#define getdev(__n)\
	(devices+(__n))
void static ncp(_8_u __what, _ulonglong __arg) {

}

void static statechange(struct pci_dev *__dev, _8_u __towhat) {
	DEVICE(dev, __dev->handle);
	char name[256];
	ffly_strf(name, 255, "%senable", dev->path);
	int fd;
	fd = open(name, O_WRONLY);

	if (fd == -1) {
		// error
		printf("failed to open file, %s\n", strerror(errno));
		return;
	}

	char state;
	state = '0'+__towhat;

	printf("writing %c to %s\n", state, name);
	if (write(fd, &state, 1) < 0) {
		printf("write failure %s.\n", strerror(errno));
	}
	close(fd);

}

void static
read_rom(struct pci_dev *__dev, void *__buf) {
	DEVICE(dev, __dev->handle);

	char name[256];
	int fd;
	ffly_strf(name, 255, "%srom", dev->path);
	fd = open(name, O_RDWR);
	if (fd == -1) {
		printf("failed to open rom.\n");
		return;
	}

	struct stat st;
	fstat(fd, &st);
	
	write(fd, "1", 1);
	lseek(fd, 0, SEEK_SET);

	read(fd, __buf, st.st_size);

	lseek(fd, 0, SEEK_SET);
	write(fd, "0", 1);
	close(fd);
}

void static
_confread(struct device *__dev, void *__data, _64_u __offset, _64_u __size) {
	char name[256];
	int fd;
	ffly_strf(name, 255, "%sconfig", __dev->path);
//	printf("reading config at: %s.\n", name);

	fd = open(name, O_RDONLY);
	if (fd == -1) {
		printf("failed to open config.\n");
		return;
	}

	pread(fd, __data, __size, __offset);
	close(fd);
}

void static
_read(struct pci_dev *__dev, void *__data, _64_u __offset, _64_u __size) {
	DEVICE(dev, __dev->handle);
	_confread(dev, __data, __offset, __size);
}

void static
_write(struct pci_dev *__dev, void *__data, _64_u __offset, _64_u __size) {
	DEVICE(dev, __dev->handle);
	char name[256];
	int fd;
	ffly_strf(name, 255, "%sconfig", dev->path);
	
	fd = open(name, O_WRONLY);
	if (fd == -1) {
		printf("failed to open config.\n");
		return;
	}

	pwrite(fd, __data, __size, __offset);
	close(fd);
}

void static
io_open(struct pci_dev *__dev, struct pci_io *__io, int __bar, _64_u __base, _64_u __size) {
	DEVICE(dev, __dev->handle);
	__io->base = __base;
	__io->size = __size;
	char name[256];
	ffly_strf(name, 255, "%sresource%u", dev->path, __bar);
	int fd;
	fd = open(name, O_RDWR);
	if (fd == -1) {
		// error
	}
	__io->fd = fd;
}

void static
io_close(struct pci_io *__io) {
	close(__io->fd);
}

void static
device_probe(struct pci_dev *__dev) {
	DEVICE(dev, __dev->handle);
	_8_u config[0x100];
	char name[256];

	_read(__dev, config, 0, 0x100);

	__dev->header_type = config[0x0e];

	ffly_strf(name, 255, "%sresource", dev->path);
	int fd;
	fd = open(name, O_RDONLY);
	if (fd == -1) {
		// error
		return;
	}

	char *buf;
	struct stat st;
	fstat(fd, &st);
	buf = (char*)malloc(st.st_size);
	read(fd, buf, st.st_size);
	char *p;
	p = buf;

	printf("%s\n", buf);

	_int_u i;
	for(i = 0;i<6;i++) {
		_64_u low_addr, high_addr;
		_64_u flags;
		low_addr = _ffly_hxti(p+2, 16);
		high_addr = _ffly_hxti(p+21, 16);
		flags = _ffly_hxti(p+40, 16);

		struct pci_mem_region *reg;
		reg = __dev->regions+i;
		reg->h_addr = high_addr;
		reg->l_addr = low_addr;
		reg->flags = flags;
		printf("low address:\t%lu,\nhigh address:\t%lu,\nflags:\t\t%lu.\n\n", low_addr, high_addr, flags);
		p+=57;
	}

	close(fd);
}

void static
device_add(struct pci_dev *__dev, _32_u __domain, _8_u __bus, _8_u _dev, _8_u __func) {
	_int_u r;
	r = ndev++;
	struct device *dev;
	dev = getdev(r);
	__dev->domain = __domain;
	__dev->bus = __bus;
	__dev->dev = _dev;
	__dev->func = __func;
	__dev->handle = r;
	ffly_strf(dev->path, 255, "%s/%04x:%02x:%02x.%u/", BUS, __domain, __bus, _dev, __func);
}
# include <limits.h>
struct linux_dirent64 {
    _64_u     d_ino;
    _64_s     d_off;
    unsigned short  d_reclen;
    unsigned char   d_type;
    char        d_name[PATH_MAX-1];
};

# include <string.h>
#define _GNU_SOURCE
#include <sys/syscall.h>
void static probe(void) {
	int fd;
	fd = openat(AT_FDCWD, "/sys/bus/pci/devices", O_RDONLY|O_NDELAY|O_DIRECTORY|O_LARGEFILE|O_CLOEXEC, 0);

#define BUFSZ 1024
	char buf[BUFSZ];
	int r;
	int pos;

	struct linux_dirent64 *de;
_again:
	r = syscall(SYS_getdents64, fd, buf, BUFSZ);
	if (r<=0) {
		goto _end;
	}
	pos = 0;

	for(;pos < r;) {
		de = (struct linux_dirent64*)(buf+pos);
		if (de->d_type == DT_LNK) {
			_int_u dom, bus, dev, func;
			struct scn_block blks[] = {
				{_scnb_h4, 0, 4, &dom},
				{_scnb_h2, 5, 2, &bus},
				{_scnb_h2, 8, 2, &dev},
				{_scnb_i, 11, 1, &func}
			};

			ffly_scnf(4, (char*)de->d_name, blks);
			_int_u r, rr;
			r = _pci_sys->ndev++;
			struct pci_dev *d;
			d = _pci_sys->devices+r;
			struct device *h;
			rr = ndev++;
			h = getdev(rr);
			d->handle = rr;
			d->domain = dom;
			d->bus = bus;
			d->dev = dev;
			d->func = func;
			ffly_strf(h->path, 255, "%s/%s/", BUS, de->d_name);
			_8_u config[64];
			bzero(config, 64);
			_confread(h, config, 0, 64);
			d->vendor_id = *(_16_u*)config;
			d->device_id = *(_16_u*)(config+2);
			d->device_class = (*(_32_u*)(config+8))>>8;
			d->revision = (_8_u)config[9];
			d->subvendor_id = *(_32_u*)(config+44);
			d->subdevice_id = *(_32_u*)(config+46);
//			printf("vendor_id: %u, "
//					"device_id: %u, "
//					"device_class: %u, "
//					"revision: %u, "
//					"subvendor_id: %u, "
//					"subdevice_id: %u\n", d->vendor_id, d->device_id, d->device_class, d->revision, d->subvendor_id, d->subdevice_id);
//
//			printf("name: %s, %u, %u, %u, %u\n\n", de->d_name, dom, bus, dev, func);
		}
		pos+=de->d_reclen;
	}
	if (r>0) {
		goto _again;
	}
_end:
	printf("error: %s.\n", strerror(errno));
	close(fd);
}

static struct pci_sys_funcs funcs = {
	.ncp = ncp,
	.statechange = statechange,
	.device_add = device_add,
	.io_open = io_open,
	.io_close = io_close,
	.device_probe = device_probe,
	.probe = probe
};

void pci_system_linux(void) {
	_pci_sys->funcs = &funcs;
}
