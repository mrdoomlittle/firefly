# ifndef __ffly__fs__m__h
# define __ffly__fs__m__h
# include "../../y_int.h"
# include "../../fsus.h"
# include "../../fs_misc.h"
# include "../../fs.h"
# include "../../msg.h"
#define MSG_BITS_MFS FFLY_MSG_DOMAIN(_DOM_MFS)
/*
	NOTE:
		when i mean disk i mean where ever its being stored i.e. hard storage
*/
// mimicry file system

#define cr_write(__buf, __size, __dst)\
	mfs_swrite(mfs_cr.s, __buf, __size, __dst)
#define cr_read(__buf, __size, __src)\
	mfs_sread(mfs_cr.s, __buf, __size, __src)

#define MFS_SLAB_INUSE 0x01
#define MFS_SLAB_SHIFT 4
#define MFS_SLAB_SIZE (1<<MFS_SLAB_SHIFT)

#define MFS_PC_NULL 0xffffffff
/*
	node is regular
*/
#define MFS_NREG 0
/*
	node is directory
*/
#define MFS_NDIR 1

#define MFS_NNA 0x01
#define MFS_NDNA 0x02
#define MFS_PCP 0x04
#define MFS_NNL 0x08
#define MFS_HHT 0x01

// create file
#define MFS_CREAT F_FS_CREAT

struct mfs_slab {
	_32_u in; // slab ident
	_32_u off;
	struct mfs_slab **bk, *fd;
	struct mfs_slab *next, *prev;
	_8_u flags;
};

struct mfs_node;

struct mfs_slab_struc {
	_32_u in;
	_8_u flags;
	_32_u off;
	_32_u pv, nx;
};

struct ffly_mfs {
	f_fsus_mp m;
	_32_u off;
	struct mfs_slab *top, *bin;
	struct mfs_node *root;
};

struct mfs_scope {
	struct mfs_slab **slabs;
	_int_u slab_c;
};

struct mfs_cauldron {
	struct mfs_scope *s;
	_int_u n;
};

struct mfs_dent {
	char const *name;
	_int_u nlen;
	_8_u id;
	struct mfs_node *n;
};

#define MFS_BALE_NULL	0xffffffff
#define MFS_BALE_SHIFT	4
#define MFS_BALE_SIZE	(1<<MFS_BALE_SHIFT)
struct mfs_bale {
	_32_u slabs[MFS_BALE_SIZE];
	_32_u next;
};

struct mfs_tract {
	// cauldron start location for slab ident
	_32_u cr;
	// node count
	_int_u crnc;
	// root node - cauldron address
	_32_u root;
	// slab count
	_int_u crsc;
	_int_u sc;
	_32_u slabs;
	_32_u off;
};

struct mfs_cb_struc {
	_16_u off;
	_16_u size;
	_32_u n;
};

struct mfs_engrave {
	_32_u h;
	_32_u pc;
	_int_u pcs;
	_8_u mode;
	_8_u flags;
	_32_u nn;
	_int_u n;
	_32_u start;
	_int_u bale_c;
	_32_u cbs;
	_int_u cbc;
	char name[86];
};

struct mfs_cb {
	void *p;
	_16_u off;
	_int_u size;
};

struct mfs_node {
	char name[86];
	// prep code
	struct mfs_cb **cbs;
	_32_u _cbs;
	_int_u cbc;
	_int_u pcs;
	_32_u pc;


	struct mfs_scope *s;
	// cauldron address
	_32_u ca;
	// node number
	_32_u nn;
	_8_u flags;

	// non tacked flags(flags that only exist on disk???)
	/*
		tobe murged with flags 
	*/
	_8_u ntf;

	// why the fuck is this here?
	_int_u size;
	_32_u h;
	_8_u mode;

	//size
	_int_u n;
};

struct mfs_fo;
struct mfs_file {
	struct mfs_node *n;
	_32_u i;
	struct mfs_fo *op;
};

struct mfs_fo {
	void(*write)(struct mfs_file*, void*, _int_u, _64_u);
	void(*read)(struct mfs_file*, void*, _int_u, _64_u);
};

void mfs_swrite(struct mfs_scope*, void*, _int_u, _64_u);
void mfs_sread(struct mfs_scope*, void*, _int_u, _64_u);

extern struct mfs_cauldron mfs_cr;
extern struct ffly_mfs *mfs;
struct mfs_scope* mfs_alloc(_int_u);
void mfs_free(struct mfs_scope*);
void ffly_mfs_init(void);
void ffly_mfs_write(_32_u, void*, _int_u, _64_u);
void ffly_mfs_read(_32_u, void*, _int_u, _64_u);
void mfs_resize(struct mfs_scope*, _int_u);

_32_u mfs_hash_new(void);
void mfs_hash_put(_32_u, _8_u const*, _int_u, long long);
long long mfs_hash_get(_32_u, _8_u const*, _int_u, _8_i*);
struct mfs_file* mfs_file_new(void);
struct mfs_file* mfs_file_get(_32_u);
void mfs_file_destroy(struct mfs_file*);
void mfs_tree(struct mfs_node*);
_8_s mfs_open(f_vmfs_nodep, char const*, _32_u, _32_u);
void ffly_mfs(void);
void ffly_mfs_de_init(void);
_32_u mfs_valloc(_int_u);
void mfs_exec(_8_u*, _int_u);
# endif /*__ffly__mfs__h*/
