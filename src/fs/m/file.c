# include "m.h"
# include "../../memory/mem_alloc.h"
# include "../../memory/mem_free.h"
# include "../../memory/mem_realloc.h"
static struct mfs_file **files = NULL;
static _32_u off = 0;

/*
	this will stay like this
	why???


	say we want to access a file without a given pointer to said file and want 
	to access by id???

	we would have two options 

	pointer table
	or real data table (NO POINTERS)(FASTER)
*/
struct mfs_file* mfs_file_new(void) {
	struct mfs_file *f;
	if (!files) {
		files = (struct mfs_file**)__f_mem_alloc(sizeof(struct mfs_file*));
		off++;
	} else {
		files = (struct mfs_file**)__f_mem_realloc(files, (++off)*sizeof(struct mfs_file*));
	}

	f = (*(files+(off-1)) = (struct mfs_file*)__f_mem_alloc(sizeof(struct mfs_file)));
	f->i = off-1;
	return f;
}

struct mfs_file* mfs_file_get(_32_u __i) {
	return *(files+__i);
}

void mfs_file_destroy(struct mfs_file *__f) {
	__f_mem_free(__f);
}
