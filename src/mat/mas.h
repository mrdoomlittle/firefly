#ifndef __mas__h
#define __mas__h
#include "../y_int.h"
#include "../lib/hash.h"
#include "mat.h"
#define F_MAS_NOPS 3

enum {
	_f_mas_sym_ident,
	_f_mas_sym_spc,
	_f_mas_sym_label,
	_f_mas_sym_int,
	_f_mas_sym_label0,
	_f_mas_sym_var,
	_f_mas_sym_sg,
	_f_mas_sym_str
};

typedef struct f_mas_seg {
	_8_u buf[256];
	_32_u off, dst;
	struct f_mas_seg *next;
} *f_mas_segp;

typedef struct f_mas_sym {
	_8_u kind;
	_int_u len;
	_64_u ival;
	void *p;
	struct f_mas_sym *next;
} *f_mas_symp;

struct f_mas_op {
	_8_u opcode[4];
	char const *id;
	_int_u len;
	_8_u noa;
};

struct f_mas_asc {
	_int_u(*in)(void*, _int_u);
	_int_u(*out)(void*, _int_u);
	void(*wto)(void*, _int_u, _32_u);
	_ulonglong(*iaf)(_8_u, _ulonglong);
	_int_u *ptr;
};

struct f_mas_specadit {
	_32_u list[SEGA_EC];
};

#define _f_mas_sa_ival 0
#define _f_mas_sa_str 0
#define _f_mas_sa_str_len 1
enum {
	_f_mas_int,
	_f_mas_uint,
	_f_mas_str
};

#define _f_mas_vial_a 0
enum {
	_f_mas_vial_clutch,
	_f_mas_vial_r,
	_f_mas_vial_g,
	_f_mas_vial_b,
	_f_mas_vial_pad_left,
	_f_mas_vial_pad_top
};


struct f_mas_val {
	_8_u type;
	union {
		_64_u ival;
		struct {
			char const *s;
			_16_u len;
		};
	};
	struct f_mas_specadit *sa;
};

typedef struct f_mas_vial {
	_8_u buf[256];
	_8_u *p;
	_16_u n;
	struct f_mas_vial *next;
	f_mas_symp sy;
	_int_u type;

	struct f_mas_val val[SEGA_EC];
} *f_mas_vialp;


extern struct f_mas_op f_mas_op_tbl[F_MAS_NOPS];

void f_mat_exec(_8_u*, _int_u);
char f_mas_getchr(void);
void f_mas_retract(char);
f_mas_symp f_mas_eval(_8_u*);
void f_mas_init(void);
void f_mas_de_init(void);
void f_mat_as(void);
extern struct f_mas_asc _f_mas_asc;
#endif /*__mas__h*/
