#include "mas.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../io"
#include "../strf.h"
#define isnum(__c) (__c>='0'&&__c<='9') 
#define isident(__c) ((__c>='a'&&__c<='z') || __c == '_')
#define emptyspace(__c) (__c == ' ' || __c == '\t' || __c == '\0')

f_mas_symp f_mas_eval(_8_u *__state) {
	f_mas_symp s, top = NULL, end = NULL;
	char buf[256];
	char c, *p;
_nextsym:
	s = (f_mas_symp)m_alloc(sizeof(struct f_mas_sym));
	s->p = NULL;
	s->ival = 0;
	if (!top)
		top = s;
	if (end != NULL) {
		end->next = s;
	}
	s->next = NULL;
	end = s;
_again:
	if (_f_mas_asc.iaf(0, 0)) {
		goto _eol;
	}
	c = f_mas_getchr();
	if (emptyspace(c))
		goto _again;

	if (c == '.') {
		s->kind = _f_mas_sym_spc;
		p = buf;
		do {
			c = f_mas_getchr();
			*(p++) = c;
		} while((c>='a'&&c<='z') || c == '_');
		f_mas_retract(c);
		*(--p) = '\0';
		mem_dup(&s->p, buf, (s->len = (p-buf))+1);
	} else if (c == '"') {
		s->kind = _f_mas_sym_str;
		p = buf;
		do {
			c = f_mas_getchr();
			if (c == '\\') {
				c = f_mas_getchr();
				switch(c) {
					case 'n':
						c = '\n';
					break;
				}
			}
			*(p++) = c;
		} while(c != '"');
		*(--p) = '\0';
		mem_dup(&s->p, buf, (s->len = (p-buf))+1);
	} else if (isnum(c)) {
		s->kind = _f_mas_sym_int;
		p = buf;
		while(isnum(c)) {
			*(p++) = c;
			c = f_mas_getchr();
		}
		f_mas_retract(c);
		s->ival = _ffly_dsn(buf, p-buf);
	} else if (isident(c)) {
		p = buf;
		while(isident(c)) {
			*(p++) = c;
			c = f_mas_getchr();
		}

		if (c != ':') {
			s->kind = _f_mas_sym_ident;
			f_mas_retract(c);
		} else {
			s->kind = _f_mas_sym_label;
		}
		*p = '\0';
		mem_dup(&s->p, buf, (s->len = (p-buf))+1);
	}

	if (_f_mas_asc.iaf(0, 0)) {
		return top;
	}
	c = f_mas_getchr();
	if (c != '\n') {
		goto _nextsym;
	}

	return top;
_eol:
	*__state = 1;
	return NULL;
}
