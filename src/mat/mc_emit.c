#include "mat.h"
#include "../string.h"
#include "../ffly_def.h"
void static
outstr(char const *__str, struct f_mc_common *__comm) {
	_int_u len;
	len = strlen(__str);
	__comm->funcs->out(__str, len);
}

void static
out(struct f_mc_common *__comm, char *__buf, _int_u __len) {
	__comm->funcs->out(__buf, __len);
}

char const static *ntctab = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
_int_u static
numtostr(_int_u __num, char *__buf) {
	__buf[0] = ntctab[__num&0x1f];
	__buf[1] = ntctab[__num>>5&0x1f];
	__buf[2] = ntctab[__num>>10&0x1f];
	__buf[3] = ntctab[__num>>15&0x1f];
	__buf[4] = ntctab[__num>>20&0x1f];
	__buf[5] = ntctab[__num>>25&0x1f];
	__buf[6] = ntctab[__num>>30&0x1f];
	return 7;
}

static _int_u label_no = 0;
_int_u static
unique_label_gen(char *__buf) {
	return numtostr(label_no++, __buf);
}

void static 
emit_tag_elems(struct f_mc_common *__comm, struct f_mc_tag_elem *__elems, _int_u __n) {
	struct f_mc_tag_elem *e;

	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		e = __elems+i;
		out(__comm, "\t.", 2);
		out(__comm, e->attr_name, e->attr_nlen);
		out(__comm, " ", 1);
		out(__comm, e->attr_value, e->attr_vlen);
		out(__comm, "\n", 1);
	}
}


struct ctc_entry {
	char const *name;
	char const *out;
	_int_u outlen;
};

#define NCTC 2
static struct ctc_entry ctc_tab[] = {
	{"pos", "0", 1},
	{"colour", "1", 1}
};

struct ctc_entry static* ctc_loopup(char const *__name) {
	struct ctc_entry *e;
	_int_u i;
	i = 0;
	for(;i != NCTC;i++) {
		e = ctc_tab+i;
		if (!strcmp(__name, e->name))
			return e;
	}
	return NULL;
}

void static
emit_tag(struct f_mc_common *__comm, f_mc_nodep __n) {
	char buf[256];
	_int_u len;
	len = unique_label_gen(buf);
	buf[len] = ':';
	buf[len+1] = '\n';

	out(__comm, buf, len+2);
	struct ctc_entry *ctc_ent;
	ctc_ent = ctc_loopup(__n->name);
	if (!ctc_ent) {
		return;
	}
	
	outstr(".clutch ", __comm);
	out(__comm, ctc_ent->out, ctc_ent->outlen);
	out(__comm, "\n", 1);

	emit_tag_elems(__comm, __n->tag_elems, __n->tag_ec);
	
	outstr("start ", __comm);
	out(__comm, buf, len+2);
	out(__comm, "\n", 1);

	if (__n->at_len>0) {
		out(__comm, "out \x22", 5);
		out(__comm, __n->at, __n->at_len);
		out(__comm, "\x22\n", 2);
	}
}

void static
emit_tag_end(struct f_mc_common *__comm, f_mc_nodep __n) {
	outstr("end\n", __comm);
}

static void(*_emit[])(struct f_mc_common*, f_mc_nodep) = {
	emit_tag,
	emit_tag_end
};

# include "../ffly_def.h"
void f_mc_emit(struct f_mc_common *__comm, f_mc_nodep __top) {
	f_mc_nodep n;
	n = __top;
	while(n != NULL) {
		_emit[n->id](__comm, n);
		n = n->next;
	}
}
