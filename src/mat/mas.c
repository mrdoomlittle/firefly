#include "mas.h"
#include "../m_alloc.h"
#include "mf.h"
struct f_mas_asc _f_mas_asc;
static struct f_lhash h_tbl;
static struct f_lhash gh_tbl;
static struct f_lhash spec_tbl;


static char cbuf[64];
static char *cbp = cbuf;
char f_mas_getchr(void) {
	char c;
	if (cbp>cbuf) {
		c = *(--cbp);
		return c;
	}
	if (_f_mas_asc.in(&c, 1) != 1) {
		printf("error.\n");
		return 0;
	}
	return c;
}

void f_mas_retract(char __c) {
	*(cbp++) = __c;
}

void static _addops(void) {
	_int_u i;
	i = 0;
	struct f_mas_op *op;
	for(;i != F_MAS_NOPS;i++) {
		op = f_mas_op_tbl+i;
		f_lhash_put(&h_tbl, op->id, op->len, op);
	}
}

struct spec {
	char const *name;
	_int_u len;
	_int_u id;
};


#define NSPEC 6
static struct spec spec_arr[] = {
	{"clutch", 6, _f_mas_vial_clutch},
	{"r", 1, _f_mas_vial_r},
	{"g", 1, _f_mas_vial_g},
	{"b", 1, _f_mas_vial_b},
	{"pad_left", 8, _f_mas_vial_pad_left},
	{"pad_top", 7, _f_mas_vial_pad_top}
};

static struct f_mas_specadit _sa[] = {
	{{_f_mf_as_clutch}},
	{{_f_mf_as_r}},
	{{_f_mf_as_g}},
	{{_f_mf_as_b}},
	{{_f_mf_as_pad_left}},
	{{_f_mf_as_pad_top}}
};

f_mas_vialp static curvial = NULL;
f_mas_segp static curseg = NULL;
static _int_u n_vials = 0;
static _int_u n_segs = 0;

static _32_u addr = 0;
f_mas_segp static _new_seg(void) {
	if (curseg != NULL) {
		curseg->dst = addr;
		addr+=curseg->off;
	}
	f_mas_segp sg;
	sg = (f_mas_segp)m_alloc(sizeof(struct f_mas_seg));

	sg->next = curseg;
	curseg = sg;
	sg->off = 0;
	n_segs++;
}

f_mas_vialp static _new_vial(f_mas_symp __sy) {
	f_mas_vialp v;
	v = (f_mas_vialp)m_alloc(sizeof(struct f_mas_vial));

	v->type = 0;//__sy->next->ival;
	v->sy = __sy;
	v->n = n_vials;
	n_vials++;
	printf("vial type: %u, name: %s\n", v->type, __sy->p);
	if (v->type == _f_mas_vial_a) {
		v->val[0].ival = 0;
		v->val[0].type = _f_mas_uint;
		v->val[0].sa = _sa;

		v->val[1].ival = 0;
		v->val[1].type = _f_mas_uint;
		v->val[1].sa = _sa+1;

		v->val[2].ival = 0;
		v->val[2].type = _f_mas_uint;
		v->val[2].sa = _sa+2;

		v->val[3].ival = 0;
		v->val[3].type = _f_mas_uint;
		v->val[3].sa = _sa+3;

		v->val[4].ival = 0;
		v->val[4].type = _f_mas_uint;
		v->val[4].sa = _sa+4;

		v->val[5].ival = 0;
		v->val[5].type = _f_mas_uint;
		v->val[5].sa = _sa+5;
	}

	v->p = v->buf;
	v->next = curvial;
	curvial = v;
	f_lhash_put(&gh_tbl, __sy->p, __sy->len, v);
	return v;
}

void f_mas_init(void) {
	f_lhash_init(&h_tbl);
	f_lhash_init(&gh_tbl);
	f_lhash_init(&spec_tbl);
	_addops();

	_new_seg();
	_int_u i;
	i = 0;
	for(;i != NSPEC;i++) {
		struct spec *sp;
		sp = spec_arr+i;

		f_lhash_put(&spec_tbl, sp->name, sp->len, sp);
	}
	*_f_mas_asc.ptr = sizeof(struct f_mf_hdr);
}

void f_mas_de_init(void) {
	f_lhash_destroy(&h_tbl);
	f_lhash_destroy(&gh_tbl);
	f_lhash_destroy(&spec_tbl);

	if (curseg != NULL) {
		curseg->dst = addr;
	}

	struct f_mf_hdr h;
	h.src = sizeof(struct f_mf_hdr);
	h.len = (*_f_mas_asc.ptr)-sizeof(struct f_mf_hdr);//for now too lazy
	h.n_vial = n_vials;
	h.n_seg = n_segs;

	if (n_vials>0) {
		printf("N-vials: %u.\n", n_vials);
		struct f_mf_vial *vials;
		vials = (struct f_mf_vial*)m_alloc(n_vials*sizeof(struct f_mf_vial));
		_int_u n, size;

		n = 0;
		struct f_mas_vial *p;
		p = curvial;
		
		while(p != NULL) {
			struct f_mf_vial *s;
			struct f_mf_astrc as;
			s = vials+n;
			size = 0;
			_int_u i;
			i = 0;
			for(;i != SEGA_EC;i++) {
				struct f_mas_val *v;
				v = p->val+i;
				switch(v->type) {
					case _f_mas_uint:
						as.fill[v->sa->list[_f_mas_sa_ival]] = *_f_mas_asc.ptr;
						size+=sizeof(_32_u);
	
						_f_mas_asc.out(&v->ival, sizeof(_32_u));
					break;
					case _f_mas_str:
						as.fill[v->sa->list[_f_mas_sa_str]] = *_f_mas_asc.ptr;
						as.fill[v->sa->list[_f_mas_sa_str_len]] = (*_f_mas_asc.ptr)+v->len;
						size+=v->len+sizeof(_16_u);
						_f_mas_asc.out(v->s, v->len);
						_f_mas_asc.out(&v->len, sizeof(_16_u));
					break;
				}	
			}
			size = 0;	
			i = 0;
			for(;i != SEGA_EC;i++) {
				printf("dest: %u.\n", as.fill[i]);
			}
	
			s->src = *_f_mas_asc.ptr;
			s->size = size;
			s->n = p->n;
			printf("vial dest: %u.\n", s->n);
			printf("vial src: %u.\n", s->src);
			s->as = as;
			n++;
			p = p->next;
		}

		if (n != n_vials) {
			printf("you might of broken somthing.\n");
			return;
		}
		h.vial = *_f_mas_asc.ptr;
		_f_mas_asc.out(vials, n_vials*sizeof(struct f_mf_vial));
		free(vials);
	}

	struct f_mf_seg *segs;
	segs = (struct f_mf_seg*)m_alloc(n_segs*sizeof(struct f_mf_seg));
	struct f_mas_seg *s;
	s = curseg;
	_int_u n;
	n = 0;
	while(s != NULL) {
		struct f_mf_seg *ss;
		ss = segs+n;
		if (!s->off) {
			ss->size = 0;
			goto _sk;
		}
		ss->src = *_f_mas_asc.ptr;
		ss->size = s->off;
		ss->dst = s->dst;
		_f_mas_asc.out(s->buf, s->off);
	_sk:
		n++;
		s = s->next;
	}

	if (n != n_segs) {
		printf("you might of broken somthing.\n");
		return;
	}

	h.seg = *_f_mas_asc.ptr;
	_f_mas_asc.out(segs, n_segs*sizeof(struct f_mf_seg));
	free(segs);
	
	_f_mas_asc.wto(&h, sizeof(struct f_mf_hdr), 0);
}

char const *s_ntab[] = {
	"ident",
	"spec",
	"label",
	"int",
	"str"
};

void f_mat_as(void) {
	_8_u state;
	f_mas_symp s;
	_int_u i;
	struct f_mas_op *op;
_again:
	state = 0;
	s = f_mas_eval(&state);
	if (state || !s) {
		return;
	}

	if (s->kind == _f_mas_sym_label) {
		_new_vial(s);
	} else if (s->kind == _f_mas_sym_spc) {
		_int_u i;

		struct spec *sp;
		sp = f_lhash_get(&spec_tbl, s->p, s->len);
		if (!sp) {
			while(1);
		}
		i = sp->id;
		printf("spec off: %u, set to %u. %s, type: %u\n", i, s->next->ival, !s->next->p?"NOPE":s->next->p, curvial->val[i].type);

		struct f_mas_val *v;
		v = curvial->val+i;
		switch(v->type) {
			case _f_mas_int:
				v->ival = s->next->ival;
			break;
			case _f_mas_uint:
				v->ival = s->next->ival;
			break;
			case _f_mas_str:
				v->s = s->next->p;
				v->len = s->next->len;
			break;
		}
	} else {
		_8_u opbuf[64];
		_8_u *ob_ptr = opbuf;
		op = (struct f_mas_op*)f_lhash_get(&h_tbl, s->p, s->len);
		if (!op) {
			printf("no such operation known as '%s'%u\n", s->p, s->len);
			return;
		}

		printf("operation-%s.\n", op->id);
		*(ob_ptr++) = op->opcode[0];
		char const *def = "NOPE";
		_int_u i;
		i = 0;
		s = s->next;
		while(s != NULL) {
			if (s->kind == _f_mas_sym_sg) {
				struct f_mas_seg *sg;
				sg = (struct f_mas_seg*)f_lhash_get(&gh_tbl, s->p, s->len);
				*(_16_u*)ob_ptr = sg->dst;
				ob_ptr+=2;
			} else
			if (s->kind == _f_mas_sym_ident) {
				struct f_mas_vial *sg;
				sg = (struct f_mas_vial*)f_lhash_get(&gh_tbl, s->p, s->len);
				if (!sg) {
					printf("trying to kill me eh???\n");
				} else {
					*(_16_u*)ob_ptr = sg->n;
					ob_ptr+=2;
				}
			} else if (s->kind == _f_mas_sym_str) {
				*(_16_u*)ob_ptr = curseg->dst+curseg->off;
				ob_ptr+=2;

				memcpy(curseg->buf+curseg->off, s->p, s->len);
				curseg->off+=s->len;
			} else if (s->kind == _f_mas_sym_int) {
				*(_16_u*)ob_ptr = s->ival;
				ob_ptr+=2;
			} else {
				while(1);
			}
			if (!s->p)
				s->p = def;
			printf("%u: %u, %s, %u.\n", i++, s->kind, s->p, s->ival);
			s = s->next;
		}
		_f_mas_asc.out(opbuf, ob_ptr-opbuf);
	}

	if (!_f_mas_asc.iaf(0, 0))
		goto _again;
}
