# ifndef __ffly__shell__memalloc__h
# define __ffly__shell__memalloc__h
# include "../y_int.h"
void* mem_alloc(_int_u);
void mem_free(void*);
void mem_cleanup(void);
# endif /*__ffly__shell__memalloc__h*/
