# ifndef __ffly__shell__exec__h
# define __ffly__shell__exec__h
# include "../y_int.h"
# include "hash.h"

_8_i extern* ffsh_run;
extern void(*ffsh_er)(char const*, _int_u, char const*[]);
struct arg_s {
	char const *p;
	_int_u l;
};

enum {
	_cmd_help,
	_cmd_exit,
	_cmd_f4,
	_cmd_info,
	_cmd_exec,
	_cmd_er
};

void ffsh_cmdput(struct hash*, char const*, _int_u);
void* ffsh_cmdget(struct hash*, char const*);
void ffsh_exec_cmd(void*, _int_u, struct arg_s**);
# endif /*__ffly__shell__exec__h*/
