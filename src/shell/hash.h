# ifndef __ffly__shell__hash__h
# define __ffly__shell__hash__h
# include "../y_int.h"

typedef struct hash_entry {
	struct hash_entry *fd, *next;
	_8_u const *key;
	_int_u len;
	void *p;
} *hash_entryp;

struct hash {
	struct hash_entry **table;
	struct hash_entry *top;
};

void hash_init(struct hash*);
void hash_destroy(struct hash*);
void hash_put(struct hash*, _8_u const*, _int_u, void*);
void* hash_get(struct hash*, _8_u const*, _int_u);
# endif /*__ffly__shell__hash__h*/
