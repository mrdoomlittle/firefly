# ifndef __ffly__shell__parser__h
# define __ffly__shell__parser__h
# include "../y_int.h"
enum {
	_cmd
};

typedef struct node {
	_8_u kind;
	char const *name;
	_int_u nl;
	void *args[20];
	_int_u argc;
} *nodep;

nodep ffsh_parse(void);
# endif /*__ffly__shell__parser__h*/
