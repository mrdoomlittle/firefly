#include "real_tree.h"
#include "m_alloc.h"
#include "string.h"
void static init_node(struct rtnode *__n){
	mem_set(__n->data,0,0x100*sizeof(_64_u));
}
struct rtnode* rtree_new(void){
	struct rtnode*
	n = m_alloc(sizeof(struct rtnode));
	init_node(n);
	return n;
}
void rtree_deinit(struct rtnode *__root){

}
_64_u *rtree_at(struct rtnode *__n,_64_u __id){
	_64_u part;
	_int_u i;
	i = 56;
	for(;i != 0;){
		part = (__id>>i)&0xff;
		struct rtnode **ip;
		ip = __n->next+part;
		if(!*ip){
			*ip = rtree_new();
		}
		__n = *ip;
		i-=8;
	}
	return __n->data+(__id&0xff);
}
