#include "citric.h"
#include "../ihc/fsl/fsl.h"
#include "../flue/common.h"
#include "../io.h"
#include "../string.h"
#include "../flue/shader.h"
void ct_flue_clear(struct ct_surface *__surf) {
	void *tgs[] = {__surf->render};
   	flue_clear(tgs,1,0,0,0,0);
}

struct program {
	struct flue_shader prog; 
};

void ct_flue_tex_map(struct yarn_texture *tx) {
	yarn_flue_tex_map(tx);
}

void ct_flue_tex_unmap(struct yarn_texture *tx) {
	yarn_flue_tex_unmap(tx);
}

void ct_flue_tex_new(struct yarn_texture *tx,_int_u __width, _int_u __height,struct yarn_fmtdesc *__fmt) {
	yarn_flue_tex_new(tx,__width,__height,__fmt);
}

void ct_flue_tex_destroy(struct yarn_texture *tx) {
	yarn_flue_tex_destroy(tx);
}


void static shader_init(struct program *prog) {
 	flue_shader_new(prog);
}


static struct flue_resspec vs_res_withuv;
static struct flue_resspec vs_res;
static struct flue_resspec vs_res0;

enum {
	_SHADER_PRIMBUF,
	_SHADER_VERTEXBUF,
	_SHADER_POOL,
	_SHADER_CONST
};
void static shader_link(struct program *__shader,char const *__ident,_64_u __id, _64_u __offset, _64_u __placement) {
	__placement = FLUE_USERSLOT0+__placement;
	struct flue_resspec *res;
	_64_u type = 0;
	switch(__id) {
		case _SHADER_PRIMBUF:
			res = &vs_res;
			type = 3;
		break;
		case _SHADER_VERTEXBUF:
			res = &vs_res;
			type = 1;
		break;
		case _SHADER_POOL:
			res = &vs_res0;
		break;
		case _SHADER_CONST:
			type = 2;
		break;
		default:
			return;
	}

	flue_shader_allot(__shader,__ident,str_len(__ident),type,__offset,__placement);
}
#include "../havoc/common.h"
#include "../havoc/resource.h"
//placemat
#define PLACEMENT(__x) (FLUE_USERSLOT0+__x)
static struct program ui_rect_ps;
static struct program ui_rect_vs;
static struct program rect_ps;
static struct program rect_vs;

#define UI_RECT_PS PITHDIR "citric/ui_ps.fsl"
#define UI_RECT_VS PITHDIR "citric/ui_vs.fsl"
/*
	TODO:
		remove transform matrix from draw call specific const data heap and to
		a more global one(remove the need to copy transform matrix over)
*/
void static setup_ui_rect_shader(void) {
	shader_link(&ui_rect_ps,"incolour",_SHADER_CONST,0,0);
	shader_link(&ui_rect_ps,"texture",_SHADER_POOL,0,0);
	flue_shader_compile(&ui_rect_ps.prog,UI_RECT_PS,sizeof(UI_RECT_PS));

	shader_link(&ui_rect_vs,"position",_SHADER_VERTEXBUF,0,PLACEMENT(1));
	shader_link(&ui_rect_vs,"texcoords",_SHADER_VERTEXBUF,4*4,PLACEMENT(1));
	shader_link(&ui_rect_vs,"overlay",_SHADER_PRIMBUF,0,PLACEMENT(2));
	shader_link(&ui_rect_vs,"transform",_SHADER_CONST,4*sizeof(float),0);
	flue_shader_compile(&ui_rect_vs.prog,UI_RECT_VS,sizeof(UI_RECT_VS));
}

#define RECT_PS PITHDIR "citric/crect_ps.fsl"
#define RECT_VS PITHDIR "citric/crect_vs.fsl"
void static setup_rect_shader(void) {
	shader_link(&rect_ps,"incolour",_SHADER_CONST,0,0);
	flue_shader_compile(&rect_ps.prog,RECT_PS,sizeof(RECT_PS));

	shader_link(&rect_vs,"transform",_SHADER_CONST,4*sizeof(float),0);
	shader_link(&rect_vs,"position",_SHADER_VERTEXBUF,0,PLACEMENT(0)); 
	flue_shader_compile(&rect_vs.prog,RECT_VS,sizeof(RECT_VS));
}

void ct_flue_init(void) {
	yarn_flue_init();
	
	vs_res_withuv = (struct flue_resspec){
		.stride = 4*2*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {
			.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),
			.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)
			},
		.id = FLUE_RS_VERTEX
	};

	vs_res = (struct flue_resspec){
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {
			.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),
			.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)
			},
		.id = FLUE_RS_VERTEX
	};

	vs_res0 = (struct flue_resspec){
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSPS,
		.fmt = {
			.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),
			.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)
			},
		.id = FLUE_RS_OTHER
	};



	shader_init(&ui_rect_ps);
	shader_init(&ui_rect_vs);

	shader_init(&rect_ps);
	shader_init(&rect_vs);


	setup_ui_rect_shader();
	setup_rect_shader();
}
void ct_flue_crect(float *__vtx, _int_u __cnt,float __r,float __g,float __b,float __a) {
	struct flue_bo *buf;
	buf = flue_bo_data(sizeof(float)*4*16);
	flue_bo_map(buf);
	float *vtx = buf->cpu_map;
	vtx[0] = __vtx[0];
	vtx[1] = __vtx[1];
	vtx[2] = 0;
	vtx[3] = 1;

	vtx[4] = __vtx[2];
	vtx[5] = __vtx[1];
	vtx[6] = 0;
	vtx[7] = 1;

	vtx[8] = __vtx[0];
	vtx[9] = __vtx[3];
	vtx[10] = 0;
	vtx[11] = 1;
	flue_bo_unmap(buf);

	float *colour = FLUE_CTX->priv->const_data;
	colour[0] = __r;
	colour[1] = __g;
	colour[2] = __b;
	colour[3] = __a;
	h_matcopY(colour+4,ct_com.transform);

	flue_clearslots();
	flue_rsdesc(FLUE_USERSLOT0,0,FLUE_RS_BUFFER_VERTEX);

	void *resources[] = {buf};
	vs_res.desc = FLUE_USERSLOT0;
	flue_resources(resources,&vs_res,1,NULL);

	flue_shader(&rect_ps,FSH_PIXEL_I);
	flue_shader(&rect_vs,FSH_VETX_I);

	flue_draw_array(NULL,0,1,FLUE_RECT);
	flue_bo_destroy(buf);
}

void ct_flue_ui_rect(float *__vtx, _int_u __cnt, struct yarn_texture *__tx,float __r,float __g,float __b,float __a, float *__rgba) {
	if(!__cnt){
		ct_printf("attempt made to draw UI_RECT with count of zero.\n");
		return;
	}
	float *colour = FLUE_CTX->priv->const_data;
	colour[0] = __r;
	colour[1] = __g;
	colour[2] = __b;
	colour[3] = __a;
	h_matcopY(colour+4,ct_com.transform);

	struct flue_bo *buf, *overlay;
	buf = flue_bo_data(sizeof(float)*4*8*__cnt);
	overlay = flue_bo_data(sizeof(float)*4*__cnt);
	flue_bo_map(buf);
	flue_bo_map(overlay);
	float *vtx = buf->cpu_map;
	float *rgba = overlay->cpu_map;
	_int_u i = 0;
	float *vt0 = rgba;
	for (;i != __cnt*24;i+=24) {
	float *vt = vtx+i;
	/*
		V0 = {0,1}
		V1 = {2,3}
	*/
	vt[0] = __vtx[0];
	vt[1] = __vtx[1];
	vt[2] = 0;
	vt[3] = 1;


	/*uv*/
	vt[4] = __vtx[4];
	vt[5] = __vtx[5];
	vt[6] = 0;
	vt[7] = 0;
   

	vt[8] = __vtx[2];
	vt[9] = __vtx[1];
	vt[10] = 0;
	vt[11] = 1;

	/*uv*/
	vt[12] = __vtx[6]-__vtx[4];
	vt[13] = 0;//__vtx[7];
	vt[14] = 0;
	vt[15] = 0;
   

	vt[16] = __vtx[0];
	vt[17] = __vtx[3];
	vt[18] = 0;
	vt[19] = 1;

	/*uv*/
	vt[20] = 0;//__vtx[8];
	vt[21] = __vtx[9]-__vtx[5];
	vt[22] = 0;
	vt[23] = 0;
	vt0[0] = __rgba[0];
	vt0[1] = __rgba[1];
	vt0[2] = __rgba[2];
	vt0[3] = __rgba[3];
	__rgba+=4;
	vt0+=4;
	__vtx+=12;
	}
	flue_bo_unmap(buf);
	flue_bo_unmap(overlay);
  flue_clearslots();
  flue_rsdesc(FLUE_USERSLOT1,1,FLUE_RS_BUFFER_VERTEX);
	flue_rsdesc(FLUE_USERSLOT2,2,FLUE_RS_BUFFER_VERTEX);

	flue_txdesc(FLUE_USERSLOT0,0,-1);
	void *resources[] = {buf,overlay};
	void *textures[] = {__tx->priv};

	vs_res_withuv.desc = FLUE_USERSLOT1;
	flue_resources(resources,&vs_res_withuv,1,NULL);
	vs_res.desc = FLUE_USERSLOT2;
	flue_resources(resources+1,&vs_res,1,NULL);

	_64_u linking[] = {FLUE_USERSLOT0};
	flue_textures(textures,FLUE_RSPS,1,linking);
	flue_shader(&ui_rect_ps,FSH_PIXEL_I);
	flue_shader(&ui_rect_vs,FSH_VETX_I);
	
	flue_draw_array(NULL,0,__cnt,FLUE_RECT);
	
	flue_bo_destroy(buf);
	flue_bo_destroy(overlay);
}
void ct_flue_framebufffer(_int_u __width, _int_u __height) {
	flue_framebuffer(__width,__height);
}
void ct_flue_initvp(struct ct_viewport *__vp) {
	struct flue_viewport *vp = __vp;
	vp->scale[0] = __vp->scale[0];
	vp->scale[1] = __vp->scale[1];
	vp->scale[2] = __vp->scale[2];

	vp->translate[0] = __vp->translate[0];
	vp->translate[1] = __vp->translate[1];
	vp->translate[2] = __vp->translate[2];
}

void ct_flue_viewport(struct ct_viewport *__vp) {
	flue_viewport(__vp,0,1);
}
void ct_flue_rtgs(void **__rtgs,_int_u __n) {
	flue_rendertgs(__rtgs,__n);
}
