#include "citric.h"
struct ct_struc ct_com = {
	.transform = {
		1,0,0,0,
		0,1,0,0,
		0,0,0,0,
		0,0,0,1
	},
	.foward = NULL
};
void ct_mjx_connect(void);
void ct_mjx_close(void);
void ct_mjx_surf_creat(struct ct_surface *__w);
void ct_mjx_surf_map(struct ct_surface *__w, _int_u __x, _int_u __u, _int_u __width, _int_u __height);
void ct_mjx_surf_unmap(struct ct_surface *__w) ;
void ct_mjx_swapbufs(struct ct_surface *__w);
_int_u ct_mjx_msg(struct ct_msg_*);
void ct_mjx_surf_move(struct ct_surface *__w, _int_u __x, _int_u __y);
struct ct_driver ct_mjx = {
		.connect				= ct_mjx_connect,
		.close					= ct_mjx_close,
		.surf_creat		 = ct_mjx_surf_creat,
		.surf_map			 = ct_mjx_surf_map,
		.surf_unmap		 = ct_mjx_surf_unmap,
		.swapbufs			 = ct_mjx_swapbufs,
	.message		= ct_mjx_msg,
	.surf_move = ct_mjx_surf_move
};
void ct_flue_rtgs(void **__rtgs,_int_u __n);
void ct_flue_framebufffer(_int_u,_int_u);
void ct_flue_viewport(struct ct_viewport*);
void ct_flue_crect(float*,_int_u,float,float,float,float);
void ct_flue_ui_rect(float*,_int_u,struct yarn_texture*,float,float,float,float,float*);
void ct_flue_init(void);
void ct_flue_initvp(struct ct_viewport*);
void ct_flue_clear(struct ct_surface*);
void ct_flue_tex_map(struct yarn_texture*);
void ct_flue_tex_unmap(struct yarn_texture*);
void ct_flue_tex_new(struct yarn_texture*,_int_u,_int_u,struct yarn_fmtdesc*);
void ct_flue_tex_destroy(struct yarn_texture*);
extern _64_u yarn_flue_nfmt[8];
extern _64_u yarn_flue_dfmt[8];


struct ct_gc ct_flue = {
		.initvp				 = ct_flue_initvp,
		.init					 = ct_flue_init,
		.clear					= ct_flue_clear,
		.crect					= ct_flue_crect,
	.ui_rect		= ct_flue_ui_rect,
		.framebuffer		= ct_flue_framebufffer,
		.viewport			 = ct_flue_viewport,
		.rtgs					 = ct_flue_rtgs,
		.tex_new				= ct_flue_tex_new,
		.tex_destroy		= ct_flue_tex_destroy,
		.tex_map				= ct_flue_tex_map,
	.tex_unmap		= ct_flue_tex_unmap,
		.nfmt					 = yarn_flue_nfmt,
		.dfmt					 = yarn_flue_dfmt
};

#include "../im.h"
void ct_tex_fileload(struct ct_gc *g, struct yarn_texture *__t, char const *__path) {
	ct_tex_map(g,__t);
	struct y_img im;
	im_load(&im,__path);
	im.rgb_rgba_float(&im,__t->ptr);
	ct_tex_unmap(g,__t);
}

void ct_tex_new(struct ct_gc *g, struct yarn_texture *__t,_int_u __width, _int_u __height, struct yarn_fmtdesc *__fmt) {
	g->tex_new(__t,__width,__height,__fmt);
}

void ct_tex_map(struct ct_gc *g, struct yarn_texture *__t) {
	g->tex_map(__t);
}

void ct_tex_unmap(struct ct_gc *g, struct yarn_texture *__t) {
	g->tex_unmap(__t);
}

void ct_tex_destroy(struct ct_gc *g, struct yarn_texture *__t) {
	g->tex_destroy(__t);
}

_64_u CT_8_8_ALLOC(struct ct_alloc_8_8 *__a) {
		_64_u r;
		r = __a->at;
		__a->at+=64;
		return r;
}



void ct_init(struct ct_gc *g) {
	y_log_file(&ct_com.log,"ctlog");
	/*
		TODO:
			iterate all graphics devies and alloc memory for all?
	*/
 	ct_com.maps_alloc.at = 0;
	struct yarn_fmtdesc fmt = {.depth=4*sizeof(float),.nfmt=YAR_NF_FLOAT,.dfmt=YAR_DF_32_32_32_32};
	g->tex_new(&ct_com._8_8,128,128,&fmt);
	g->tex_map(&ct_com._8_8);
	ct_com.fnt = ct_font_load();
	ct_com.g = g;
}
