#include "citric.h"
#include "ct_font.h"
#include "../m_alloc.h"
#include "../bitfont.h"
#include "../io.h"
#include "../assert.h"
#include "../msg.h"
#include "../domain.h"
#define MSG_BITS MSG_DOMAIN(_DOM_NOWHERE)
// fixed char font

/*
	default font face
*/
void static
fcf(ct_fontp __f, _int_u __size) {
	_int_u i;
	_8_u c;
	struct ct_font_char *cs;
	i = 0;
	float wn, hn;
	wn = 1./ct_com._8_8.width;
	hn = 1./ct_com._8_8.height;
	for(;i != __f->nac;i++) {
		cs = __f->cmap+__f->acarr[i];	
		_64_u loc;
		loc = CT_8_8_ALLOC(&ct_com.maps_alloc);

		double xx, yy;
		xx = CTA_X(loc);
		yy = CTA_Y(loc);
		cs->u0 = xx*wn;
		cs->v0 = yy*wn;
		cs->u1 = (xx+8)*wn;
		cs->v1 = (yy+8)*wn;
		MSG(INFO,"LOCATION ALOTMENT, %fx%f.\n",xx,yy)
		_int_u x, y;
		_8_u *s;
		float *d;
		y = 0;
		/*
			create tone map
		*/
		assert(__size>0);
		for(;y != __size;y++) {
			x = 0;
			for(;x != __size;x++) {
				s = cs->bm+x+(y*__size);
				d = ((float*)ct_com._8_8.ptr)+(_64_u)((xx+x)+((yy+y)*ct_com._8_8.width))*4;
				ffly_fprintf(ffly_err, "---- %p.\n",d);
				if (*s>0) {
					d[0] = 1;
					d[1] = 1;
					d[2] = 1;
					d[3] = 1;
				} else {
					d[0] = 0;
					d[1] = 0;
					d[2] = 0;
					d[3] = 0;
				}
			}
		}
	}
	__f->ratio = 1;
}

void static _bfload(fontp __f) {
	MSG(INFO,"LOADING-bitfont.\n")
	MSG(INFO,"LOADMAP: %s{%ux%u}\n", _bf_info.lm,_bf_info.width,_bf_info.height)
	__f->width = _bf_info.width;
	__f->height = _bf_info.height;
	_int_u bms;
	bms = _bf_info.width*_bf_info.height;
	_8_u *md;
	md = (_8_u*)m_alloc(_bf_info.n*bms);
	struct ct_font_char *cs;
	_int_u i;
	_8_u c;
	i = 0;
	for(;i != _bf_info.n;i++) {
		cs = __f->cmap+(c = _bf_info.lm[i]);
		cs->bm = md+(i*bms);
		_bf_info.bm = cs->bm;
		bfchar(c);
		cs->init = 1;
	}
	__f->acarr = _bf_info.lm;
	__f->nac = _bf_info.n;
	fcf(__f, 8);
}
struct ct_font_driver ct_fnt_drv[20] = {
	{_bfload}
};
/*
	there are 2 ways to load textures 
	from raw font data or thru prepreped font maps(bit maps with all fonts used + additional font loading data

*/
ct_fontp ct_font_load(void) {
	ct_fontp f;
	f = (fontp)m_alloc(sizeof(struct ct_font));
	mem_set(f->cmap,0,sizeof(struct ct_font_char)*0x100);
	f->d = ct_fnt_drv;
	f->d->load(f);
	return f;
}
#include "../assert.h"
/*
	NOTE:
		__linelength is the recep

	(__f)				font used
	(lns)				lines
	(__nlns)		number of lines
	(__len)			overall length(all frags)
	(__size)		size of glyph
	(__mw)			max width, a restriction of visablity of glyph
*/
void ct_font_draw(ct_fontp __f, struct ct_text_line *lns, _int_u __nlns, _int_u __len, float __x, float __y, _8_u __bits, float __size, struct ct_colour *__colour, struct ct_colour *__bg, float __linelength, float *__overlay, float __mw){
//	assert(__str[__len-1] != '\0');
	float __w = __size*(float)__len;
	float __h = __f->ratio*__size;
	struct ct_font_char *cs;
	float *buf;
	buf = m_alloc(__len*16*sizeof(float));

	float *p = buf;
	
	struct row {
		float *p;
		_int_u w;
	};

	char const *__str;
	struct row r[24];
	_int_u cr = 0;
	char c;
	_int_u i;
	_int_u xof = 0;
	r[cr].p = p;
	struct ct_text_line *cln;
	struct ct_text_frag *frg;
	_int_u ln = 0;
	for(;ln != __nlns;ln++) {
		cln = lns+ln;
		frg = cln->frags;
		while(frg != NULL){
			__str = frg->s;
			i = 0;
			while(i != frg->len) {
				c = __str[i++];
				cs = __f->cmap+c;
				if(!cs->init) {
					ct_printf("illegal charicter.\n");
					return;
				}
				p[4] = cs->u0*ct_com._8_8.width;
				p[5] = cs->v0*ct_com._8_8.height;
				p[6] = cs->u1*ct_com._8_8.width;
				p[7] = cs->v0*ct_com._8_8.height;
		
				p[8] = cs->u0*ct_com._8_8.width;
				p[9] = cs->v1*ct_com._8_8.height;
				p[10] = cs->u1*ct_com._8_8.width;
				p[11] = cs->v1*ct_com._8_8.height;
				p+=12;
				xof+=8;
			}
			frg = frg->next;
		}
		if (ln+1 != __nlns) {
			r[cr].w = xof;
			cr++;
			r[cr].p = p;
			xof = 0;
		}
	}

	r[cr].w = xof;
	struct row *_r;
	cr++;
	float _xof;
	float gw, gh;
	gh = __h/cr;
	i = 0;

	float adj;
	if (__bits&FNT_HALIGN) {
		adj = __w*.5;
		__x-=adj;
	}
	if (__bits&FNT_VALIGN) {
		adj = __h*.5;
		__y-=adj;
	}

	for(;i != cr;i++) {
		_r = r+i;
		float *p;
		p = _r->p;
		gw = __w*__linelength;
		_int_u w;
		w = 0;
		_xof = 0;
		for(;w != _r->w/8;w++) {
			p[0] = __x+((float)w*gw);
			p[1] = __y+((float)i*gh);
			p[2] = p[0]+gw;
			p[3] = p[1]+gh;
			if(p[2]>=__mw) break;
			p+=12;
		}

		if(w>0){
		ct_com.g->ui_rect(
			_r->p,
			w,
			&ct_com._8_8,__colour->r,__colour->g,__colour->b,__colour->a,__overlay);
		}
	}
	m_free(buf);
}

void ct_textdraw(char const *__str,_int_u __len,float __x,float __y,float __r,float __g,float __b,float __a){
	struct ct_text_line line;
	struct ct_text_frag frag;
	line.frags = &frag;
	frag.len = __len;
	frag.s = __str;
	frag.next = NULL;
	struct ct_colour c = {__r,__g,__b,__a};
	ct_font_draw(
		ct_com.fnt,
		&line,
		1,
		__len,
		__x,
		__y,
		0,
		32,
		&c,
		NULL,
		1./(float)__len,
		NULL,
		1920/*we dont care about this*/
	);
}
