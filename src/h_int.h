# ifndef __f__h__int__h
# define __f__h__int__h
# include "y_int.h"
/*
	for hard storage ints
*/
// header-ints/hard-ints
typedef _8_u HWORD;//half word
typedef _16_u WORD;//word
typedef _32_u DWORD;//dword
typedef _64_u QWORD;//quad word
# endif /*__f__h__int__h*/
