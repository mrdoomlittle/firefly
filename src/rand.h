# ifndef __ffly__rand__h
# define __ffly__rand__h
# include "y_int.h"
_8_u ffgen_rand8l();
_16_u ffgen_rand16l();
_32_u ffgen_rand32l();
_64_u ffgen_rand64l();
_64_u ffly_rand();
# endif /*__ffly__rand__h*/
