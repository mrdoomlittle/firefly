# ifndef __ffly__clay__hash__h
# define __ffly__clay__hash__h
# include "../y_int.h"
struct clay_hash_entry {
	struct clay_hash_entry *fd, *next;
	_8_u const *key;
	_int_u len;
	void *p;
};

struct clay_hash {
	struct clay_hash_entry **table;
	struct clay_hash_entry *top;
};

void clay_hash_init(struct clay_hash*);
void clay_hash_put(struct clay_hash*, _8_u const*, _int_u, void*);
void* clay_hash_get(struct clay_hash*, _8_u const*, _int_u);
# endif /*__ffly__clay__hash__h*/
