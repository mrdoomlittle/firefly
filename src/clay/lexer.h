# ifndef __ffly__clay__lexer__h
# define __ffly__clay__lexer__h
# include "../clay.h"
struct clay_token* clay_nexttok(clayp);
struct clay_token* clay_peektok(clayp);
void clay_rtok(struct clay_token*);
# endif /*__ffly__clay__lexer__h*/
