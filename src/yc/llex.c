#include "yc.h"
#include "../m_alloc.h"
#include "../assert.h"
#include "../io.h"
#include "../string.h"
#include "../tools.h"
/*
the lesser lexer
*/

static _int_u i = 0;
static _int_u lov;
static char *readblock(void) {
	char *r;
	printf("######## %u -- %u.\n",fc_struc.core.llbn,fc_struc.core.ll_bufi);
	ffly_fdrain(_ffly_out);
	struct yc_core *c = &fc_struc.core;
	struct yc_file *file = fc_struc.file;
	if ((c->llbn-c->ll_bufi)>=64 && !(c->llbits&LL_EXISTING)) {
		printf("EXISTING READ.\n");
		r = c->ll_buf+c->ll_bufi;
		c->ll_bufi+=64;
		return r;
	}else{
		printf("EXITING buffer to small.\n");
		/*
			we cant read any more if Y_ENDOF has been set
		*/
		if (!(c->llbits&LL_EXISTING) && !(file->state&Y_ENDOF)) {
			mem_set(c->ll_buf0,'#',0x100);
			
			printf("copying existing buffer to new location.\n");
			/*
				if we have some leftovers from ll_buf then copy it over
			*/
			lov = c->llbn-c->ll_bufi;
			if (lov>0)
				mem_cpy(c->ll_buf0,c->ll_buf+c->ll_bufi,lov);
			/*
				are final read should be

				fc_struc.core.llbn+NUMREAD
			*/

			yc_read(file,c->ll_buf0+lov, 0x100-lov);

			printf("%w -> %u\n", c->ll_buf0,c->remnants>0?c->remnants+lov:0x100,c->remnants+lov);
			c->ll_bufi = 64;
			c->llbits |= LL_EXISTING;
			return c->ll_buf0;
		} 

		/*
			if we have run dry of data then replenish
		*/
		if (c->ll_bufi+64>0x100 && !(file->state&Y_ENDOF) && c->llbits&LL_EXISTING) {
			printf("replenish buffer with new data.\n");
			/*
				if it was not a complete drought recirculate it
			*/
			lov = 0;
			if (c->ll_bufi<0x100) {
				lov = 0x100-c->ll_bufi;
				mem_cpy(c->ll_buf0,c->ll_buf0+c->ll_bufi,lov);
			}
			c->ll_bufi = 64;
			yc_read(file,c->ll_buf0+lov,0x100-lov);
			printf("%w -> %u\n", c->ll_buf0,c->remnants>0?c->remnants+lov:0x100,c->remnants+lov);
			return c->ll_buf0;
		} else {
			if (!(c->llbits&LL_EXISTING)) {
				r = c->ll_buf+c->ll_bufi;
				c->ll_bufi+=64;
				return r;
			} else{
				r = c->ll_buf0+c->ll_bufi;
				c->ll_bufi+=64;
				return r;
			}
		
		}
	}
}



void static unread(_int_u __n) {
	fc_struc.core.ll_bufi-=__n;
}

char static getc(void) {
	char *b;
	b = readblock();
	unread(63);
	return *b;
}

#include "../tools.h"
void ll_lex_end(void) {
	struct yc_core *c = &fc_struc.core;
	if (c->llbits&LL_EXISTING) {
		
		c->ll_buf = c->ll_buf0+c->ll_bufi;
		if (c->remnants>0) {
			c->llbn = (c->remnants+lov)-c->ll_bufi;
			printf("REM: %u.\n",c->remnants);
		}else {
			c->llbn = 0x100-c->ll_bufi;
		}

		printf("BUFi '%c', %u, %u\n",*c->ll_buf,c->ll_bufi,c->llbn);
	//	assert(1 == 0);

	} else {
		c->ll_buf+=c->ll_bufi;
		c->llbn = c->llbn-c->ll_bufi;
	}
	c->ll_bufi = 0;
	printf("%w",c->ll_buf,c->llbn);
}
#include "../lib.h"
struct ll_token* ll_lex(void) {
	struct ll_token *t;

	t = m_alloc(sizeof(struct ll_token));
	t->kind = ~0;
	char c;
_skip:
	c = getc();
	if (c == ' ' || c == '\t' || c == '\n')
		goto _skip;
	printf("LL-FIRSTLEX: '%c'.\n",c);
	if (c == '"') {
		t->kind = _ll_tok_str;
		t->s = m_alloc(64);
		_int_u i;
		char *b = readblock();
		i = extractupto(t->s,b,'"');
		unread(63-i);
		t->s[i] = '\0';
		t->len = i;
	}else if ((c>='a'&&c<='z') || (c>='A'&&c<='Z')) {
		t->kind = _ll_tok_ident;
		t->s = m_alloc(64);
		*t->s = c;
		_int_u i;
		char *b = readblock();
		i = sextract(t->s+1,b);
		unread(64-i);
		i++;
		t->s[i] = '\0';
		printf("EXT: %s : %u.\n",t->s,i);
		t->len = i;
	} else if (c == '#') {
		t->kind = _ll_tok_dir;
	}else
		goto _skip;

	return t;
}
