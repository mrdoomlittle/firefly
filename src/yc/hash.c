# include "hash.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../string.h"
# include "../crypt/rest.h"
# include "../ffly_def.h"
#include "../assert.h"
yc_hashp yc_hash_new(void) {
	yc_hashp hm;
	hm = (yc_hashp)m_alloc(sizeof(struct yc_hash));
	hm->table = (yc_hash_entp*)m_alloc(sizeof(yc_hash_entp)*FC_HT_SIZE);
	mem_set(hm->table, 0, sizeof(yc_hash_entp)*FC_HT_SIZE);
	hm->top = NULL;
	return hm;
}

void yc_hash_destroy(yc_hashp __h) {
	yc_hash_entp cur, bk;
	cur = __h->top;
	while(cur != NULL) {
		bk = cur;
		cur = cur->link;
		m_free(bk->key);
		m_free(bk);
	}
	m_free(__h->table);
	m_free(__h);
}


void yc_hash_put(yc_hashp __h, _8_u const *__key, _int_u __len, void *__ptr) {
	assert(__len<=256 && __key != NULL);
	_64_u sum = y_hash64(__key, __len);
	yc_hash_entp *t;
	t = __h->table+(sum&FC_HT_MASK);

	struct yc_hash_entry *ent;
	ent = (yc_hash_entp)m_alloc(sizeof(struct yc_hash_entry));
	ent->next = *t;
	*t = ent;
	mem_dup((void**)&ent->key, (void*)__key, __len);
	ent->p = __ptr;
	ent->len = __len;
	ent->link = __h->top;
	__h->top = ent;
}

void* yc_hash_get(yc_hashp __h, _8_u const *__key, _int_u __len) {
	assert(__len<=256 && __key != NULL);
	_64_u sum = y_hash64(__key, __len);
	yc_hash_entp *t;
	t = __h->table+(sum&FC_HT_MASK);
	yc_hash_entp cur = *t;
	if (!*t) {
		goto _fail;
	}

	while(cur != NULL) {
		if (cur->len == __len) {
			if (!mem_cmp(cur->key, __key, __len)) {
				return cur->p;
			}
		}
		cur = cur->next;
	}
_fail:
	printf("lookup failure: %x:%x\n",sum,sum>>32);
	return NULL;
}
