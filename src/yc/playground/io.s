.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern delay
.extern tmr0_stop
.extern tmr0_reset
.extern tmr0_strt
.extern uartinit
.extern icinit
.globl send
.globl recv
recv:
;stput
clrw
clrw
movwi j[$-2
;stput
clrw
clrw
movwi j[$-1
;bo
movlb $0
movf $26
iorlw $8
movlb $0
movwf $26
.L0
;eq
;stfetch
moviw j[$-1
sublw $8
btfsic $3,$2
goto .L11
.L1
movlb $0
btfsis $14,$0
goto .L7
goto .L1
.L7
;stput
movlb $0
movf $14
movwi j[$-3
;stput
;bo
;stfetch
moviw j[$-3
movwf $120
lsr $120
movwi j[$-3
;stput
;bo
;stfetch
moviw j[$-3
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
andlw $1
movwi j[$-3
;stput
;bo
;stfetch
moviw j[$-3
movwf $121
;stfetch
moviw j[$-1
movwf $120
incf j[120
.L8
decf j[120
btfsic $3,$2
goto .L9
lsl j[$121
goto .L8
.L9
movf 121
movwi j[$-3
;stput
;bo
;stfetch
moviw j[$-2
;push
;stash save
movwf $119
;stpos
movlw $3
call subfsr1w
;pop
;stash retrieve
movf $119
iorwf $1
movwi j[$1
.L2
movlb $0
btfsic $14,$0
goto .L10
goto .L2
.L10
;stput
;bo
;stpos
addfsr j[$2
movlw $1
addwf $1
movwf $1
;stpos
addfsr j[$1
goto .L0
.L11
;bo
movlb $0
movf $26
andlw $247
movlb $0
movwf $26
;stfetch
moviw j[$-2
;push
;stash save
movwf $119
;add16
clrw
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
clrw
addwf j[$6
clrw
addwfc j[$7
ret
send:
;stput
clrw
clrw
movwi j[$-2
;stput
;stfetch
moviw j[$-1
movwi j[$-3
;stput
clrw
clrw
movwi j[$-4
;funccall
call tmr0_strt
.L3
movlb $0
btfsic $14,$1
goto .L13
movlb $14
btfsis $12,$5
goto .L12
goto .L6
.L12
goto .L3
.L13
.L4
;eq
;stfetch
moviw j[$-2
sublw $8
btfsic $3,$2
goto .L16
;stput
;bo
;stfetch
moviw j[$-3
movwf $121
;stfetch
moviw j[$-2
movwf $120
incf j[120
.L14
decf j[120
btfsic $3,$2
goto .L15
lsr j[$121
goto .L14
.L15
movf 121
movwi j[$-4
;stput
;bo
;stfetch
moviw j[$-4
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
andlw $1
movwi j[$-4
;stput
;bo
;stfetch
moviw j[$-4
movwf $120
lsl j[$120
lsl j[$120
lsl $120
movwi j[$-4
;bo
;stpos
movlw $4
call subfsr1w
movlb $0
movf $26
iorwf $1
movlb $0
movwf $26
;bo
movlb $0
movf $26
iorlw $4
movlb $0
movwf $26
;funccall
;stput
clrw
movlw $1
movwi j[$2
;stpos
addfsr j[$3
call delay
;bo
movlb $0
movf $26
andlw $243
movlb $0
movwf $26
;funccall
;stput
clrw
movlw $1
movwi j[$-1
call delay
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stpos
addfsr j[$2
goto .L4
.L16
;funccall
call tmr0_reset
.L5
movlb $0
btfsis $14,$1
goto .L18
movlb $14
btfsis $12,$5
goto .L17
goto .L6
.L17
goto .L5
.L18
.L6
;funccall
call tmr0_stop
;add16
clrw
addwf j[$6
clrw
addwfc j[$7
ret
