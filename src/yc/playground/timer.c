#"header.h"
void tmr0_strt() {
	TMR0H = 0;
	TMR0L = 0;
	T0CON1 = aso "2<5";
	T0CON0 = aso "1<7'1<4|''0x10|'";
}

void tmr0_reset() {
	PIR0 = 0;
	TMR0H = 0;
	TMR0L = 0;
}

void tmr0_stop() {
	T0CON0 = 0;
	PIR0 = 0;
}
