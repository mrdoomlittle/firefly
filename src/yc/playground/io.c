#include "header.h"
void recv() {
	_8_u i;
	_8_u word;
	_8_u t;
	word = 0;
	i = 0;
	LATC |= 8;
	while(i != 8) {
		while(!PORTC#0);
		t = PORTC;
		t = t>>1;
		t &= 1;
		t = t<<i;
		word |= t;
		while(PORTC#0);
		i+=1;
	}

	LATC &= 0xf7;
	ret word;
}

void send(_8_u __word) {
	_8_u i;
	_8_u word;
	_8_u t;

	i = 0;
	word = __word;
	t = 0;

	tmr0_strt();
	while(PORTC#1) {
		if (!PIR0#5) {	
			goto end;
		}
	}
	
	while(i != 8) {
		t = word>>i;
		t &= 1;
		t = t<<3;
		LATC |= t;
		LATC |= 4;
		delay(1);
		LATC &= 0xf3;
		delay(1);
		i+=1;
	}
	tmr0_reset();
	while(!PORTC#1){
		if (!PIR0#5) {
			goto end;
		}
	}
end:
	tmr0_stop();
}

