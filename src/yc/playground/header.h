TMR0H#  0x59d
TMR0L#  0x59c
T0CON0# 0x59e
T0CON1# 0x59f
PORTC#  0x0e
TRISC#  0x14
ANSELC# 0x1f4e
PIE0#   0x716
PIR0#   0x70c
LATC#   0x1a
INTCON# 0x00b
SSP1BUF#	0x18c
SSP1STAT#   0x18f

TRISA#	  0x12
PORTA#	  0x0c
ANSELA#	 0x1f38
LATA#	   0x18
PIR3#	   0x70f
SSP1CON2#   0x191
SSP1CLKPPS# 0x1ec5
SSP1DATPPS# 0x1ec6
RA0PPS#	 0x1f10
RA1PPS#	 0x1f11
WPUC#	   0x1f4f

RX1PPS#	 0x1ecb
CK1PPS#	 0x1ecc

RA2PPS#	 0x1f12
RA4PPS#	 0x1f14
RA5PPS#	 0x1f15
RC4PPS#	 0x1f24
RC5PPS#	 0x1f25

RC1REG#	 0x0119
TX1REG#	 0x011a

RC1STA#	 0x011d
TX1STA#	 0x011e
void icinit();
void uartinit();
void tmr0_strt();
void tmr0_reset();
void tmr0_stop();
void recv();
void send(_8_u);
void delay(_8_u);
