.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern send
.extern recv
.extern tmr0_stop
.extern tmr0_reset
.extern tmr0_strt
.extern uartinit
.extern icinit
.globl delay
delay:
;eq
;stfetch
moviw j[$-1
sublw $7
btfsic $3,$0
goto .L4
;stput
;bo
;stfetch
moviw j[$-1
movwf $120
lsr j[$120
lsr j[$120
lsr $120
movwi j[$-3
;stput
;bo
;stpos
movlw $3
call subfsr1w
movlw $1
subwf $1
movwf $1
movlw $0
movlb $11
movwf $29
movlw $0
movlb $11
movwf $28
movlw $64
movlb $11
movwf $31
;cassign
movf $1
movlb $11
movwf $30
;bo
movlb $11
movf $30
iorlw $144
movlb $11
movwf $30
.L0
movlb $14
btfsic $12,$5
goto .L3
goto .L0
.L3
movlw $0
movlb $11
movwf $30
movlw $0
movlb $14
movwf $12
;stpos
addfsr j[$3
.L4
;stput
;bo
;stfetch
moviw j[$-1
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
andlw $7
movwi j[$-3
;eq
;stfetch
moviw j[$-3
sublw $0
btfsic $3,$0
goto .L7
;stput
clrw
clrw
movwi j[$-2
.L1
;eq
;stfetch
moviw j[$-2
;push
;stash save
movwf $119
;stpos
movlw $3
call subfsr1w
;pop
;stash retrieve
movf $119
subwf $1
btfsic $3,$2
goto .L7
goto .L6
movlw $234
movlb $11
movwf $29
movlw $121
movlb $11
movwf $28
movlw $64
movlb $11
movwf $31
movlw $144
movlb $11
movwf $30
.L2
movlb $14
btfsic $12,$5
goto .L5
goto .L2
.L5
movlw $0
movlb $11
movwf $30
movlw $0
movlb $14
movwf $12
;stput
;bo
;stpos
addfsr j[$1
movlw $1
addwf $1
movwf $1
;stpos
addfsr j[$2
goto .L1
.L6
;stpos
addfsr j[$3
.L7
;add16
clrw
addwf j[$6
clrw
addwfc j[$7
ret
