.extern mov01w
.extern mov10
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.globl test
test:
;bo
;stpos
;sub16
movlw $3
subwf j[$6
movlw $0
subfwb j[$7
movlb $0
moviw j[$0
movwf $78
moviw j[$1
movwf $79
;stpos
addfsr j[$2
movlb $0
movf $1
addwf j[$78
clrw
addwfc j[$79
movlb $0
movf $78
movwf $4
movf $79
movwf $5
;stpos
;sub16
movlw $0
subwf j[$6
movlw $0
subfwb j[$7
movf $6
movwf $0
addfsr $1
movf $7
movwf $0
;add16
movlw $1
addwf j[$6
movlw $0
addwfc j[$7
ret
