#include "../yc.h"
#include "../../m_alloc.h"
#include "../../io.h"
#include "../../string.h"
#include "../../assert.h"
struct f_cfm_em *f_cfm_regm;
#define trk_cur (cfm_struc.trk_cur)
#define trk_top (cfm_struc.trk_top)
struct f_cfm_em *f_cfm_em_new(void) {
	f_cfm_emp em;
	em = (f_cfm_emp)m_alloc(sizeof(struct f_cfm_em));
	em->bits = 0x00;
	em->saved = -1;
	return em;
}


void cfm_lp(void);
void(*cfm_proc)(f_cfm_nodep, _int_u);
void
cfm_outfck(f_cfm_trackp __track) {
	f_cfm_superflockp sfck;
	f_cfm_flockp fck;
	f_cfm_nodep p;
	_int_u i, j, k;
	i = 0;
	for(;i != __track->n;i++) { 
		sfck = __track->sf+i;
		j = 0;
		f_printf("flockN: %u.\n", sfck->n);
		for(;j != sfck->n;j++) {
			fck = sfck->fck+j;
			cfm_struc.here = &fck->here;
			f_cfm_fpop op;
			op = fck->po;
			printf("proccing cfm flock.\n");
			printf("preops.\n");
			while(op != NULL) {
				printf("###################pop.\n");
				it_ypop po;
				switch(op->op) {
					case _f_cfm_po_var:
						po = cfm_it_syo(trk_cur->sy-1);
						po->op = _it_yk_var;
						po->sy = op->sy->s;
						po->pi = &op->pi;
					break;
					case _f_cfm_po_ll:
						cfm_lp();
						po = cfm_it_ypo(cfm_it_yolk_new());
						po->op = _it_yk_ll;
						po->sy = op->sy->s;

						op->sy->sp = cfm_savepoint();
					break;
					case _f_cfm_po_jmp: {

					}
					break;
					case _f_cfm_po_aso:
						po = cfm_it_ypo(cfm_it_yolk_new());
						po->op = _it_yk_aso;
						po->pi = &op->pi;
					break;
					case _f_cfm_po_jt:
						cfm_struc.jt = op->sy;
					break;
					case _f_cfm_po_def: {
						po = cfm_it_ypo(cfm_it_yolk_new());
						po->op = _it_yk_aso;
						po->pi = &op->pi;
						char *buf;
						buf = m_alloc(64);
						mem_cpy(buf, op->v, op->vlen);
						mem_cpy(buf+op->vlen, "#define ", 8);
						mem_cpy(buf+op->vlen+8, op->n, op->nlen);
						_int_u len;
						len = op->vlen+9+op->nlen;
						po->pi->s = buf;
						po->pi->len = len;
						buf[len-1] = '\n';
					break;
					}
				}
				op = op->link;
			}

			k = 0;
			while(k != fck->nn) {
				_int_u register off = k+fck->pg_off;
				_int_u register pg, pg_off;
				pg = off>>F_CFM_PAGE_SHFT;
				pg_off = off&F_CFM_PAGE_MASK;
				p = (*(fck->n+pg))+pg_off;
				_int_u n;
				n = F_CFM_PAGE_SIZE-pg_off;
				if (k+n>fck->nn)
					n = fck->nn-k;
				_int_u i;
				i = 0;
				
				for(;i != n;i++) {
					(p+i)->f(p+i);
				}
				printf("CFM: PAGE: %u, PG_OFF: %u.\n", pg, pg_off);
				k+=n;
			}
			if (fck->end != NULL) {
				fck->end(fck);
			}
		}
	}
}

void(*cfm_amd64_func[64])(f_cfm_nodep) = {
};
struct f_cfm_em cfm_eminit = {.bits = 0x00,.saved = -1,.wide = 1};
static struct cfm_vsp vs_reg = {.u = 1,._u=1};
void f_cfm_init(struct cfm_stage *__st) {
	cfm_struc.sytop = NULL;
	cfm_struc.jt = NULL;

	f_cfm_regm = f_cfm_em_new();
	__st->prep();
//	f_cfm_regm->vs = &vs_reg;
//	f_cfm_regm->v0 = &vs_reg;
//	f_cfm_regm->vs_ac = _cfm_ac_reg;
//	f_cfm_regm->v0_ac = _cfm_ac_reg;
}

void cfm_amd64(f_cfm_trackp __track, struct it_com *cm) {

}
