# ifndef __f__it__frag__h
# define __f__it__frag__h
# include "../../y_int.h"
# include "mound.h"
typedef struct it_frag {
	it_moundp m;
	_int_u page_c;
	_int_u off;

	char **p;
	struct it_frag *next;
} *it_fragp;

it_fragp it_frag_new(void);
void it_frag_destroy(it_fragp);
void it_frag_induct(char*, _int_u);
// output
void it_frag_lay(it_fragp, void*, _int_u);
void it_frag_write(it_fragp, void*, _int_u, _64_u);
void it_fragout(void);
extern it_fragp it_cur_frag;
extern it_fragp it_fr_head;
# endif /*__f__it__frag__h*/
