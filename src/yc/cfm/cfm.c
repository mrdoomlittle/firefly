#include "../yc.h"
#include "../../string.h"
#include "../../ffly_def.h"
#include "../../m_alloc.h"
#include "frag.h"
#include "../../io.h"
#include "pic.h"
void cfm_pic_prep(void);
extern void(*cfm_pic_func[64])(f_cfm_nodep);
extern void(*cfm_amd64_func[64])(f_cfm_nodep);
extern struct cfm_lds cfm_pic_sp[64];
void cfm_pic_f(f_cfm_nodep __n, f_cfm_emp __lhs, f_cfm_emp __rhs, _64_u __f0, _64_u __f1);
#define _ NULL

struct cfm_nts **cfm_optab[0x1000] = {
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_
};
void cfm_pic_ret(f_cfm_flockp);
struct cfm_stuff cfm_stf[2] = {
	{},
//	{NULL,NULL,cfm_amd64_func,NULL,NULL},
	{{_pic_btfsic,_pic_btfsis},{},cfm_pic_ret,cfm_pic_func,cfm_pic_sp,cfm_pic_f}
};
struct cfm_stage cfm_st[4] = {
	{.func=cfm_amd64},
	{.func=cfm_pic,.prep = cfm_pic_prep},
	{},
	{}
};

void it_amd64_gas(void);
void it_amd64_gas_proc(it_comp);
void it_amd64_gas_out(void);

// firefly asm binary depiction
void it_amd64_bd(void);

void it_pic(void);
void it_pic_out(void);
struct f_it it_struc = {
    {
        {"GAS", "AMD64", it_amd64_gas, it_amd64_gas_proc, it_amd64_gas_out, "for the gas assembler using at&t syntax"},
        {"FBD", "AMD64", NULL, NULL, NULL, "for the firefly assembler passed using its binary depiction non-text format"},
		{"TAS", "PIC", it_pic, it_amd64_gas_proc, it_pic_out, "blank"}
	}
};

void static pt_init(struct cfm_pant *__pt) {
	__pt->pans = 1<<IT_PANPAGE;
	__pt->pgc = 1<<F_CFM_PAGE_SHFT;
	__pt->off = 0;
	__pt->p = (struct it_pan**)m_alloc(sizeof(struct it_pan*));
	*__pt->p = (struct it_pan*)m_alloc(sizeof(struct it_pan));
	__pt->p[0]->ptrs[0] = m_alloc(__pt->pgsize);
}

it_ypop cfm_it_syo(it_superyolkp __y) {
    it_ypop op;
    op = (it_ypop)m_alloc(sizeof(struct it_ypo));
    op->link = __y->po;
    __y->po = op;

    return op;
}

it_ypop cfm_it_ypo(it_yolkp __y) {
    it_ypop op;
    op = (it_ypop)m_alloc(sizeof(struct it_ypo));
    op->link = __y->po;
    __y->po = op;

    return op;
}

cfm_it_trkp
cfm_it_track_new(void) {
    cfm_it_trkp trk;
    trk = (cfm_it_trkp)m_alloc(sizeof(struct cfm_it_track));
	trk->trk.n_yk = 0;
	trk->sy = trk->sbuf;

    trk->yk_top = NULL;
	trk->pt_yolks.pgsize = F_CFM_PAGE_SIZE*sizeof(struct it_yolk);
	pt_init(&trk->pt_yolks);
    trk->next = cfm_struc.trk_top;
    cfm_struc.trk_top = trk;
}

static void *pt_alloc(struct cfm_pant *__pt) {
	void *ptr;
	_int_u pg;
	struct it_pan *p;
	pg = (__pt->off&IT_PANPAGEMK)>>F_CFM_PAGE_SHFT;
	if (__pt->off>=__pt->pgc) {
		if (__pt->off>=__pt->pans) {
			_int_u pn = __pt->off>>IT_PANPAGE;
			__pt->p = (struct it_pan**)m_realloc(__pt->p, pn*sizeof(struct it_pan*));
			__pt->p[pn] = m_alloc(sizeof(struct it_pan));
			__pt->pans+=1<<IT_PANPAGE;
			printf("NEW PAN.\n");
		}
		p = __pt->p[__pt->off>>IT_PANPAGE];
		p->ptrs[pg] = m_alloc(__pt->pgsize);
		__pt->pgc+=1<<F_CFM_PAGE_SHFT;
		printf("NEW PAGE.\n");
	} else
		p = __pt->p[__pt->off>>IT_PANPAGE];
	__pt->off++;	
	return p->ptrs[pg];
}

it_yolkp cfm_it_yolk_new(void) {
	printf("NEW YOLK, %u\n", cfm_struc.trk_cur->pt_yolks.off);
	it_yolkp yk;
	yk = pt_alloc(&cfm_struc.trk_cur->pt_yolks);
	yk+=(cfm_struc.trk_cur->pt_yolks.off-1)&F_IT_PAGE_MASK;
    yk->pop = 0x00;
    yk->po = NULL;
	yk->n_cf = 0;
	yk->cf = 0;
    cfm_it_yolk_hp yh;
    yh = (cfm_it_yolk_hp)m_alloc(sizeof(struct cfm_it_yolk_h));
    yh->yk = yk;
	
	/*
		private data section
	*/
	mem_cpy(yh->userdata, cfm_struc.it_ykh, 64);
    cfm_struc.yk_prep(yh);
	yh->link = cfm_struc.trk_cur->yk_top;
    cfm_struc.trk_cur->yk_top = yh;
    (cfm_struc.trk_cur->sy-1)->n++;
    return yk;
}

it_superyolkp cfm_it_superyolk_new(void) {
	printf("NEW SUPERYOLK, %u\n", cfm_struc.trk_cur->pt_yolks.off);
	it_superyolkp sy;
    sy = cfm_struc.trk_cur->sy++;
    sy->pop = 0;
    sy->n = 0;
    sy->po = NULL;
	sy->yk = cfm_struc.trk_cur->pt_yolks.off;
	return sy;
}

struct it_cf*
cfm_it_cf_new(void) {
    struct it_cf *cf;
    cf = pt_alloc(&cfm_struc.pt_cf);	
	cf+=(cfm_struc.pt_cf.off-1)&F_IT_PAGE_MASK;
	return cf;
}
struct it_com cm;
static struct it_outstruc *out;
void cfm_init(f_cfm_trackp __track, struct cfm_stage *__st) {
	cfm_struc.trk_top = NULL;
	cfm_struc.trk_cur = NULL;	

	cfm_struc.pt_cf.pgsize = F_CFM_PAGE_SIZE*sizeof(struct it_cf);	
	pt_init(&cfm_struc.pt_cf);

	out = IT_TAS_PIC;
	it_frag_new();

    printf("IT_OUT{codename: %s, for: %s, desc: %s}.\n", out->codename, out->_for, out->desc);
	out->prep();	
	__st->func(__track, &cm);
}

void(*cfm_extern)(void**,_int_u);
void(*cfm_global)(void**,_int_u);
void(*cfm_var)(void**,_int_u);
void cfm_process(void) {
	out->process(&cm);
	out->out();

    struct it_mblock *h, *bk;
    h = it_mb;
    while(h != NULL) {
        h = (bk = h)->next;
        m_free(bk);
    }
}
