# ifndef __f__it__mound__h
# define __f__it__mound__h
# include "../../y_int.h"
typedef struct it_mound {
	char **p;
	_int_u page_c;
	_int_u off;
} *it_moundp;


it_moundp it_mound_new(void);
void it_mound_pack(it_moundp, char*, _int_u);
void it_moundout(it_moundp);
# endif /*__f__it__mound__h*/
