# include "../../yc.h"
# include "../../../as/bd.h"
//# include "../../../mort/string.h"
//# include "../../../mort/stdio.h"
//# include "../../../mort/malloc.h"
//# include "../../../strf.h"
# include "../frag.h"
/*
struct bd_opd;
struct bd_blob {
	struct f_bd_blob blob;
	struct bd_opd *opds;
	_int_u n_opds;
};

struct bd_opd {
	struct f_bd_opd opd;
};

struct bd_op_struc {
	struct bd_blob *blob;
	struct bd_op_struc *next;
};

static struct bd_op_struc *bdos_t = NULL, *bdos_las = NULL;
static struct bd_op_struc*
bd_os_new(void) {
	printf("new bdos.\n");
	struct bd_op_struc *s;
	s = (struct bd_op_struc*)malloc(sizeof(struct bd_op_struc));


	if (!bdos_t)
		bdos_t = s;
	
	if (bdos_las != NULL)
		bdos_las->next = s;

	bdos_las = s;
	return s;
}

_8_u static op_ttbl[] = {
	_f_bd_mov,
	_f_bd_jmp,
	_f_bd_add,
	_f_bd_sub,
	_f_bd_call,
	_f_bd_ret,
	_f_bd_and,
	_f_bd_lea
};

void static
_yolk(struct f_it *__it, it_yolkp __yk) {
	struct it_cf *cf;

	struct bd_op_struc *bdos;	
	struct bd_blob *bd_blobs, *blob;
	struct bd_opd *bd_opds;
	bd_blobs = (struct bd_blob*)it_malloc(__yk->n_cf*sizeof(struct bd_blob));
	bd_opds = (struct bd_opd*)it_malloc(__yk->n_opds*sizeof(struct bd_opd));
	_int_u i;
	i = 0;
	while(i != __yk->n_cf) {
		_int_u register pg, pg_off;
		_int_u register off = i+__yk->pg_off;
		pg = off>>F_IT_PAGE_SHFT;
		pg_off = off&F_IT_PAGE_MASK;
		cf = (*(__yk->cf+pg))+pg_off;

		bdos = bd_os_new();
		bdos->blob = blob = bd_blobs++;
		blob->opds = bd_opds;
		bd_opds+=cf->n_opds;
		_8_u op = op_ttbl[cf->op];
		struct f_bd_opd *opd;
		struct f_bd_blob *b = &blob->blob;
		b->op = op;
		blob->n_opds = cf->n_opds;
		_8_u size = (cf->info>>1)&0x0f;

		// yes this is all just for operands
		if (cf->n_opds>0) {
			struct it_oa_em *ot;
			struct it_oa *oa;

			_64_u info;
			_8_u sign;

			printf("CF: %u.\n", cf->n_opds);
			_int_u ii, iii;
			ii = 0;
			for(;ii != cf->n_opds;ii++) {
				opd = &(bd_opds+ii)->opd;

				oa = cf->operands+ii;
				iii = 0;
				for(;iii != oa->ns;iii++) {
					ot = oa->oaem+iii;
					if ((info&(_f_it_int|_f_it_dis))>0) {
						opd->val = ot->oa;
					}
				}
			}
		}
		i++;
	}
}
*/
//void static
//_ins_entry(struct f_it *__it, struct it_wing *__w) {
/*
	it_superyolkp sy;
	it_ypop po;
	_int_u i, ii;
	i = 0;
	while(i != __w->n_yk) {
		sy = __w->sy+i;
		ii = 0;
		while(ii != sy->n) {
			_int_u register off = ii+sy->pg_off;
			_int_u register pg, pg_off;
			pg = off>>F_IT_PAGE_SHFT;
			pg_off = off&F_IT_PAGE_MASK;

			it_yolkp yk;
			yk = sy->yk[pg]+pg_off;

			_yolk(__it, yk);
			ii++;
		}
		i++;
	}*/
//}

//void static proc(struct f_it *__it, it_comp __c) {
//	_ins_entry(__it, __c->w);
//}
/*
void static out(struct f_it *__it) {
	f_shs_stp st = __it->st;
	struct f_bd_hdr h;

	_64_u hdr_adr;
	if ((hdr_adr = st->nes->ptr) != 0) {
	}
	
	st->seek(sizeof(struct f_bd_hdr));
	h.start = st->nes->ptr;
	_int_u i;
	i = 0;
	if (bdos_t != NULL) {
		bdos_las->next = NULL;
		struct bd_op_struc *cur;
		cur = bdos_t;
		while(cur != NULL) {
			st->write(st->arg, &cur->blob->blob, sizeof(struct f_bd_blob));
			struct f_bd_opd buf[24];
			_int_u ii;
			ii = 0;
			for(;ii != cur->blob->n_opds;ii++) {
				buf[ii] = cur->blob->opds[ii].opd;
			}

			st->write(st->arg, buf, ii*sizeof(struct f_bd_opd));
			cur = cur->next;
			i++;
		}
	}
	h.n_blobs = i;
	h.size = st->nes->ptr-h.start;
	
	struct f_bd_trek stt, syt;
	stt.start = st->nes->ptr;
	void *shs_sap;
	i = 0;
	_int_u pgc = shs_saloc.off>>F_SHS_SA_PAGE_SHFT;
	for(;i != pgc;i++) {
		shs_sap = *(shs_saloc.pages+i);
		st->write(st->arg, shs_sap, F_SHS_SA_FPS);
	}
	_int_u pg_left = shs_saloc.off&F_SHS_SA_PAGE_MASK;
	if (pg_left>0) {
		st->write(st->arg, *(shs_saloc.pages+i), pg_left*F_SHS_STR_MAX);
	}

	stt.size = (stt.n = shs_saloc.off)*F_SHS_STR_MAX;

	syt.start = stt.start+stt.size;
	syt.n = it_nsy;
	syt.size = it_nsy*sizeof(struct f_bd_sym);
	_int_u off = 0;
	struct f_bd_sym *sy_tbl;
	sy_tbl = (struct f_bd_sym*)malloc(syt.size);
	struct f_bd_sym *dy;
	f_it_symp sy;
	sy = it_sym;
	i = 0;
	while(sy != NULL) {
		dy = sy_tbl+i;
		dy->name = sy->s->off;
		dy->len = sy->s->len;
		dy->flags = 0x00;
		printf("DY: %u, %u, %u.\n", dy->name, dy->len, dy->len);
		i++;
		sy = sy->next;
	}
	st->write(st->arg, sy_tbl, syt.size);

	printf("syt_trek: start: %u, size: %u, n: %u.\n", syt.start, syt.size, syt.n);
	printf("stt_trek: start: %u, size: %u, n: %u.\n", stt.start, stt.size, stt.n);
	_64_u of = st->nes->ptr;
	h.syt = of;
	h.stt = of+sizeof(struct f_bd_trek);
	st->write(st->arg, &syt, sizeof(struct f_bd_trek));
	st->write(st->arg, &stt, sizeof(struct f_bd_trek));
	st->pwrite(st->arg, &h, sizeof(struct f_bd_hdr), hdr_adr);
}

static struct f_it_func funcs = {
	.process = proc,
	.out = out
};
*/
void f_it_amd64_bd(void) {
//	*__it->gen.funcs = funcs;
}
