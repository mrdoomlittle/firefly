#ifndef __pic__h
#define __pic__h
#define _pic_movlb	0x00
#define _pic_movlp	0x01
#define _pic_movlw	0x02
#define _pic_movwf	0x03
#define _pic_btfsis	0x04
#define _pic_ret 0x05
#define _pic_goto 0x06
#define _pic_call 0x07
#define _pic_btfsic 0x08
#define _pic_movf 0x09
#define _pic_sublw 0x0a
#define _pic_subwf 0x0b
#define _pic_subfwb 0x0c
#define _pic_andlw 0x0d
#define _pic_andwf 0x0e
#define _pic_iorlw 0x0f
#define _pic_iorwf 0x10
#define _pic_addlw 0x11
#define _pic_addwf  0x12
#define _pic_addwfc 0x13
#define _pic_lsl	0x14
#define _pic_lsr	0x15
#define _pic_decf	0x16
#define _pic_incf	0x17
#define _pic_addfsr	0x18
#define _pic_moviw	0x19
#define _pic_movwi	0x1a
#define _pic_clrf	0x1b
#define _pic_clrw	0x1c
#define _pic_rlf	0x1c+1
#define _pic_rrf	0x1c+2
#define _pic_bcf	0x1c+3
#define _pic_bsf	0x1c+4
#define _pic_xorlw	0x1c+5
#define _pic_xorwf	0x1c+6
#endif /*__pic__h*/
