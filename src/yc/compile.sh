rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd yc
ffly_objs="$ffly_objs llex.o clang.o cfm_amd64.o cfm_pic.o clg_lexer.o clg_parser.o yc.o clg_io.o vec.o hash.o cfm.o cfm_sym.o it_amd64_gas.o it_amd64_bd.o it_frag.o it_mem.o it_mound.o"
gcc $cc_flags -c cfm/mound.c -o it_mound.o
gcc $cc_flags -c cfm/clean.c -o it_clean.o
gcc $cc_flags -c cfm/mem.c -o it_mem.o
gcc $cc_flags -c cfm/frag.c -o it_frag.o
gcc $cc_flags -c cfm/output/amd64_gas.c -o it_amd64_gas.o
gcc $cc_flags -c cfm/output/amd64_bd.c -o it_amd64_bd.o
gcc $cc_flags -c cfm/sym.c -o cfm_sym.o
gcc $cc_flags -c cfm/cfm.c -o cfm.o
gcc $cc_flags -c cfm/cfm_amd64.c -o cfm_amd64.o
gcc $cc_flags -c cfm/cfm_pic.c -o cfm_pic.o
gcc $cc_flags -c clang/parser.c -o clg_parser.o
gcc $cc_flags -c clang/lexer.c -o clg_lexer.o
gcc $cc_flags -c clang/clang.c
gcc $cc_flags -c vec.c
gcc $cc_flags -c hash.c
gcc $cc_flags -c clang/io.c -o clg_io.o
gcc $cc_flags -c yc.c
gcc $cc_flags -c llex.c
cd ../as
gcc $cc_flags -c -o opcodes/pic.o opcodes/pic.c
gcc $cc_flags -c -o opcodes/amd64_tbl.o opcodes/amd64_tbl.c
gcc $cc_flags -c -o opcodes/resin_tbl.o opcodes/resin_tbl.c
gcc $cc_flags -c -o output/pic.o output/pic.c
gcc $cc_flags -c -o output/resin.o output/resin.c
gcc $cc_flags -c -o output/amd64.o output/amd64.c
gcc $cc_flags -c stt.c
gcc $cc_flags -c frag.c
gcc $cc_flags -c symbol.c
gcc $cc_flags -c alloca.c
gcc $cc_flags -c proc_text.c
gcc $cc_flags -c parser.c
gcc $cc_flags -c exp.c
gcc $cc_flags -c turn.c
gcc $cc_flags -c local.c
gcc $cc_flags -c input.c
gcc $cc_flags -c m_frag.c
gcc $cc_flags -c mp.c
gcc $cc_flags -c pic.c
gcc $cc_flags -c opts.c
gcc $cc_flags -c tang.c
gcc $cc_flags -c remf.c
gcc $cc_flags -c blf.c
gcc $cc_flags -c urek.c
#cc $cc_flags -c elf.c
gcc $cc_flags -c hash.c
gcc $cc_flags -c -o output.o output.c
gcc $cc_flags -c malk.c
cd ../yc
ffly_objs="$ffly_objs ../as/pic.o ../as/opcodes/amd64_tbl.o ../as/opcodes/resin_tbl.o ../as/output/resin.o ../as/output/amd64.o \
../as/stt.o ../as/frag.o ../as/symbol.o ../as/alloca.o ../as/proc_text.o ../as/parser.o ../as/exp.o ../as/turn.o \
../as/local.o ../as/input.o ../as/mp.o ../as/tang.o ../as/remf.o ../as/hash.o ../as/output.o \
../as/m_frag.o ../as/blf.o ../as/urek.o ../as/opts.o ../as/malk.o ../as/opcodes/pic.o ../as/output/pic.o"
gcc $cc_flags -o yc main.c $ffly_objs -nostdlib
