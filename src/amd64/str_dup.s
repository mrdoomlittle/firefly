	.globl str_dup
	.globl mem_dup
	.globl str_len
	.text
/*
*	rdi - src
*	rax - ret
*/
str_dup:
	push %rbp
	movq %rsp, %rbp
	subq $8, %rsp

	push %rdi
	call str_len
	pop %rdi
	incq %rax

	movq %rdi, %rsi
	leaq -8(%rbp), %rdi
	movq %rax, %rdx
	call mem_dup
	
	movq -8(%rbp), %rax
	movq %rbp, %rsp
	pop %rbp
	ret
