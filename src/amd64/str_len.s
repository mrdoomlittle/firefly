	.globl str_len
	.text
/*
*	rdi - string
*	rax - count
*/
str_len:
	movq %rdi, %rax
.L0:
	incq %rdi
	cmpb $0, -1(%rdi)
	jne .L0
	decq %rdi

	subq %rax, %rdi
	movq %rdi, %rax
	ret
