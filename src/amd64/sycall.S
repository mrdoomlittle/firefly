# 1 "amd64/posix/syscall.s"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "amd64/posix/syscall.s"
 .include "syscall.h"




 .globl open
 .text
open:
 movq $sys_open, %rax
 syscall cmpq -MAX_ERRNO, %rax jmp _fault
 ret
close:
 movq $sys_close, %rax
 syscall cmpq -MAX_ERRNO, %rax jmp _fault
 ret
read:
 movq $sys_read, %rax
 syscall cmpq -MAX_ERRNO, %rax jmp _fault
 ret
write:
 movq $sys_read, %rax
 syscall cmpq -MAX_ERRNO, %rax jmp _fault
 ret
_fault:
 call __set_errno
 movq $-1, %rax
 ret
