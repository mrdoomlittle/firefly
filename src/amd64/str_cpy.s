	.globl str_cpy
	.globl str_len
	.globl mem_cpy
	.globl str_scpy
	.globl str_cpyut
	.text
/*
*	rdi - dst
*	rsi - src
* cheap way to copy string
*/
str_cpy:
	call str_scpy
	movb $0x00,(%rdi,%rdx)
	movq %rdx,%rax
	ret
/*
* copy string and dont include '\0'
*/
str_scpy:
	xorq %rdx,%rdx
0:
	movb (%rsi,%rdx),%al
	cmpb $0x00,%al
	je 0f
	movb %al,(%rdi,%rdx)
	incq %rdx
	jmp 0b
0:
	ret
/*
* short for: copy up to = cpyut
*/
str_cpyut:
	xorq %rcx,%rcx
0:
	movb (%rsi,%rcx),%al
	cmpb %dl,%al
	je 0f
	movb %al,(%rdi,%rcx)
	incq %rcx
	jmp 0b
0:
	movq %rcx,%rax
	ret


