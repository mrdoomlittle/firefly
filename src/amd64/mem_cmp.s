	.globl mem_cmp
	.globl mem_cmpc
	.text
/*
*	rcx - string length
*	mem cmp with extra checking
*/
mem_cmpc:
	cmpq %rdx, %rcx
	jne .L3
	call mem_cmp
.L3:
	ret
/*
*	rdi - p0
*	rsi - p1
*	rdx - no
*/
mem_cmp:
	push %rbx
	movw $0, %bx
.L0:
	movb (%rdi), %al
	cmpb %al, (%rsi)
	jne .L2
	incw %bx
	cmpw %dx, %bx
	je .L1
	incq %rdi
	incq %rsi
	jmp .L0
.L1:
	movb $0x0, %al
	pop %rbx
	ret
.L2:
	movb $-1, %al
	pop %rbx
	ret
