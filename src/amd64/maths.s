	.globl ceil
	.globl cos
	.globl floor
	.globl sin
	.globl round
	.globl sqr
	.globl tan
	.text
sqr:
    pushq %rbp
    movq %rsp, %rbp
    subq $8, %rsp
    movq %xmm0, -8(%rbp)

    fldl -8(%rbp)
    fsqrt
    fstpl -8(%rbp)

    movq -8(%rbp), %xmm0
    movq %rbp, %rsp
    popq %rbp
    ret
round:
    pushq %rbp
    movq %rsp, %rbp
    subq $20, %rsp

    movq %xmm0, -8(%rbp)
    pxor %xmm0, %xmm0

    fldl -8(%rbp)
    fstcw -10(%rbp)
    fclex

    movw $0b0001001000111111, -12(%rbp)
    fldcw -12(%rbp)
    
    frndint
    fclex

    fldcw -10(%rbp)
    fstpl -20(%rbp)
    movq -20(%rbp), %xmm0

    movq %rbp, %rsp
    popq %rbp
    ret
sin:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp

    movq %xmm0, -8(%rbp)
    pxor %xmm0, %xmm0

    fldl -8(%rbp)
    fsin

    fstpl -16(%rbp)
    movq -16(%rbp), %xmm0

    movq %rbp, %rsp
    popq %rbp
    ret
tan:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp

    movq %xmm0, -8(%rbp)
    pxor %xmm0, %xmm0

    fldl -8(%rbp)
    
    fptan
	fstp %ST(0)
	fstpl -16(%rbp)
	movq -16(%rbp), %xmm0
    movq %rbp, %rsp
    popq %rbp
    ret
floor:
	pushq %rbp
	movq %rsp, %rbp
	subq $20, %rsp

	movq %xmm0, -8(%rbp)
	pxor %xmm0, %xmm0

	fldl -8(%rbp)
	fstcw -10(%rbp)
	fclex

	movw $0b0001011000111111, -12(%rbp)
	fldcw -12(%rbp)
	
	frndint
	fclex

	fldcw -10(%rbp)
	fstpl -20(%rbp)
	movq -20(%rbp), %xmm0

	movq %rbp, %rsp
	popq %rbp
	ret
cos:
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp

	movq %xmm0, -8(%rbp)
	pxor %xmm0, %xmm0

	fldl -8(%rbp)
	fcos

	fstpl -16(%rbp)
	movq -16(%rbp), %xmm0

	movq %rbp, %rsp
	popq %rbp
	ret
ceil:
	pushq %rbp
	movq %rsp, %rbp
	subq $20, %rsp

	movq %xmm0, -8(%rbp)
	pxor %xmm0, %xmm0

	fldl -8(%rbp)
	fstcw -10(%rbp)
	fclex

	movw $0b0001101000111111, -12(%rbp)
	fldcw -12(%rbp)
	
	frndint
	fclex

	fldcw -10(%rbp)
	fstpl -20(%rbp)
	movq -20(%rbp), %xmm0

	movq %rbp, %rsp
	popq %rbp
	ret
