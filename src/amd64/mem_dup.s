	.globl mem_dup
	.globl m_alloc
	.globl mem_cpy
	.text
/*
*	rdi - pp
*	rsi - src
*	rdx - no
*/
mem_dup:
	push %rbp
	movq %rsp, %rbp

	push %rdi
	push %rsi
	push %rdx
	movq %rdx, %rdi
	call m_alloc
	pop %rdx
	pop %rsi
	pop %rdi

	movq %rax, (%rdi)
	movq %rax, %rdi
	call mem_cpy
	movq %rbp, %rsp
	pop %rbp
	ret
