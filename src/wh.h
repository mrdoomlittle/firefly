#ifndef __yage__wh__H
#define __yage__wh__H
#include "y_int.h"
#include "types.h"
#define WH_EXISTS 1
#define WH_BACKING 2
#include "slab_dealer.h"
#include "file.h"
#include "real_tree.h"

struct wh_file_fossil{
	/*
		offset to slab table
	*/
	_64_u slabs;
	_32_u nslabs;
	_64_u info;
	_64_u key;
};

struct wh_hdr{
	_64_u bits;
	_64_u files;
	_int_u nfiles;
	_64_u free_slabs;
	_int_u nfree;
	_64_u offset;
};

struct wh_file{
	_64_u info;
	_64_u key;
	struct sd_area *sda;
	struct wh_file *next;
};

void wh_init(void);
void wh_deinit(void);

struct wh_file* wh_open(_ulonglong);
void wh_backing(struct wh_file*,_64_u);
void wh_close(struct wh_file*);
void wh_stat(_ulonglong,_64_u*);
void wh_read(struct wh_file*,void*,_64_u,_64_u);
void wh_write(struct wh_file*,void*,_64_u,_64_u);
#endif/*__yage__wh__H*/
