#include "rws.h"
#include "file.h"
#include "m_alloc.h"
struct context{
	struct file f;
};

void static* _open(struct rws_schematic *par, _64_u __opt,_64_u __mode){
	struct context *c;
	c = m_alloc(sizeof(struct context));
	bopen(&c->f,par->path,__opt,__mode);
	return c;
}

_32_u static _read(struct context *c, void *__buf, _int_u __size, _8_s *__error){
	bread(&c->f,__buf,__size);
}
void static _close(struct context *c) {
	bclose(&c->f);
}

void static _stat(struct context *c, struct rws_stat *__st){
	struct _stat st;
	bstat(&c->f,&st);
	__st->st_size = st.st_size;
}

struct f_rw_funcs rws_file = {
	.read = _read,
	.open = _open,
	.close = _close,
	.stat = _stat,
	.opttab = {
			_O_RDONLY,
			_O_WRONLY,
			_O_RDWR
	},
	.modetab = {
		_S_IRWXU,
		_S_IRUSR,
		_S_IWUSR,
		_S_IXUSR
	}
};
