#include "ar_internal.h"
#include "../linux/mman.h"
#include "../linux/unistd.h"
#include "../linux/signal.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../system/util/checksum.h"
#include "../thread.h"
#include "../mutex.h"
#include "../config.h"
#include "../system/tls.h"
#include "../system/string.h"
#include "../string.h"
#include "../io.h"
#include "../m.h"
#include "../lib.h"
/*
	TODO:
		force alignment of contents of all bale to 8-bytes for size
		this means when it comes to copying the bales contents we dont need to worry about the remainder
	
	NOTE: if we are approching the POTS from lower to higher address
	space then we check 2^20 POT header first if exists.
	as if we check the next POT it might be apart of a larger set(meaning no header is present)
*/
void static lspot_decompose(potp p, _64_u __amo);
void static lspot_prep_nolink(potp);
void static lot_renounce(struct lot*);
void static pot_repossession(potp);
void static _m_free_lesser_p(void*);
void static* _m_alloc_lesser(_int_u);
void static lot_renounce_guarded(struct lot*,struct arena*);
void static lot_renounce_cbforeign(struct lot*);
void static deal_with_subsidiarys(struct arena*);
void static auctionoff(potp __pot, balep blk);
void static lesser_subsidiary_free(struct arena *an,_32_u rl,balep blk);
void static spr_remove(struct arena *anr, struct sprout *spr, _32_u __gr);
void static bookcase_remove_nolock(struct lot*);
void static bookcase_remove(struct lot*);
void static bookcase_add(struct lot*,struct accommodation*);
_8_s jointventure(struct lot *lt,potp up,potp __pot,struct arena *anr);
balep _ar_balloc_guarded(potp __pot,_int_u __bc);

#define be_vigilant
//#define graph_plots

#ifdef graph_plots
#include "graph.c"
#endif

#ifdef _hooks_permitted
void*(*__m_alloc)(_int_u) = _m_alloc;
void(*__m_free)(void*) = _m_free;
void*(*__m_realloc)(void*,_int_u) = _m_realloc;
void*m_alloc(_int_u __size){
	return __m_alloc(__size);
}
void m_free(void * __ptr) {
	__m_free(__ptr);
}
void* m_realloc(void *__ptr, _int_u __size) {
	return __m_realloc(__ptr,__size);
}
#endif
/*
	NOTE:
		this is complete garbage and will stay this way until i have time to work on it,
		full of quick fixes.
*/

/*
	ISSUES:
		reallocate of same size causes failue i think?
*/
//	might want to keep track of mmaps

/*
	rename dispose to dislodge???
*/

/*
	TODO:
		remove mmap and brk and push it to an function that will i will choose
		what to use. why to allow the user to divert it.

	TONOTE:
		needs to be worked on 
*/
/*
	TODO:
		cleanup

		remove mmap and brk and add function pointers to them
		so can be used for diffrent uses
*/


/*
	IGNORE: im just talking to my self here

	so we have two options, the tree method or larger pot method.


	should we sacrifice linearity? i mean if we get an allocation that dosent fit in either 
	the lesser or larger pots then its a system allocation right???? that slow right?

	why not try to extent a map we already have? 

	that where the issue arises.

	say we have a allocation that dosent fit in a greater pot,
	it spills over to the adjcent greater pot.

	ISSUE. the header is in the way of lineary expanding, forcing us to use system allocation approch MMAP
	this is where the tree method helps is in this regard. we dont need a header.?


	X
	X
	X -> greater POT header

	X
	X
	X -> greater POT header

	X
	X
	X ....

	how do we deal with spans/extents that overlap with the greater POT header?
	meaning there aligned to lesser POT sizes but not greater.

	if we align the extent to greater POT is dosent help is either.
	as when we try to check the greater POT header of the extent it will within the extent itself.


	also if we have a POTFIT allocation. then we can pile it on to itself?

	meaning appending a POTFIT allocation at the end of another if the adjcent lesser POT is not inuse,

	
	NOTE:
		greater POT are not for allocation of 2^25 but for allocation much smaller then that.
		there for allocation that dont fit in the lesser POTS.

		meaning if a allocation is greater then 1/512 the size of a lesser POT it certifies
		to be a candidate for the greater POTS.

		2^25/(2^23/512) = 2048 

		NOTE: it might not stay at 2^25
		so 
		2^28/(2^23/512) = 16384
*/
#define be_vigilant
//# define barebone
/*
	erase bale contents when freed
*/

//# define ERASE


// erase bale contents
void(*ar_erase)(void*, _int_u) = NULL;

#include "../system/err.h"
#include <stdarg.h>
//# define DEBUG
/* not the best but will do */
void static
copy(void *__dst, void const *__src, _int_u __bc) {
	_8_u *p = (_8_u*)__dst;
	_8_u *end = p+__bc;
	_int_u left;
	while(p != end) {
		left = __bc-(p-(_8_u*)__dst);
		if (left>>3 > 0) {
			*(_64_u*)p = *(_64_u*)((_8_u*)__src+(p-(_8_u*)__dst));
			p+=sizeof(_64_u);
		} else {
			*p = *((_8_u*)__src+(p-(_8_u*)__dst));
			p++;
		}
	}
}

/*
	using real one will only take more time and cause more obstacles in the future
*/
int static arout;
_int_u static
gen(char *buf, char const *__format, va_list __args) {
	char *bufp = buf;
	char const *p = __format;
	char *end = p+str_len(__format);
	while(p != end) {
		if (*(p++) == '%') {
			if(*p == 'd'){
				_32_s v = va_arg(__args, _32_s);
				if(v<0){
					*(bufp++) = '-';
					v = -v;
				}
				bufp+=ffly_nots(v, bufp);
			}else
			if(*p == 'u') {
				_32_u v = va_arg(__args, _32_u);
				bufp+=ffly_nots(v, bufp);
			} else if (*p == 'x') {
				_32_u v = va_arg(__args, _32_u);
				bufp+=ffly_noths((_64_u)v, bufp);
			} else if (*p == 'p') { 
				void *v = va_arg(__args, void*);
				*(bufp++) = '0';
				*(bufp++) = 'x';
				bufp+=ffly_noths((_64_u)v, bufp);
			} else if (*p == 's') {
				char *s = va_arg(__args, char*);
				bufp+=str_cpy(bufp, s);
			}
			p++;
		} else
			*(bufp++) = *(p-1);
	}
	*bufp = '\0';
	return bufp-buf;
}

/*
only used in the case of errors and output at deinit and init and so.
to bypass _ar_mesg
*/
void
_ar_printf(char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	_int_u len;
	char buf[2048];
	len = gen(buf,__format, args);
	write(arout, buf,len);
	va_end(args);
}

static _64_u msgpass = MESG_FILTER;
void static _ar_mesg(_64_u __type,char const *__format,...){
	if(!((msgpass&0xffff)&__type))return;
	if(!((msgpass&~_64u_(0xffff))&__type))return;
	va_list args;
	va_start(args, __format);
	_int_u len;
	char buf[2048];
	len = gen(buf,__format, args);
	write(arout, buf,len);
	va_end(args);
}


void ar_printfb(char *buf,_int_u len) {
	write(arout, buf,len);
}
_int_u ar_printfit(char *buf,char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	_int_u len;

	len = gen(buf,__format, args);


	va_end(args);
	return len;
}

static struct pot main_pot;
balep static _ar_balloc(potp, _int_u);
void static _ar_bfree(potp,balep);
void static _ar_bfree_guarded(potp,balep);
void dump_stats(void);
/*
	TODO:
		free all memory - dont realy care for now but later this will need to be done.
*/
#include "../linux/signal.h"
void
ar_abort(void) {
	dump_stats();
	ar_printf("ar, abort.\n");
	close(arout);
	while(1){
		kill(getpid(),SIGABRT);
	}
}

#include "../assert.h"
mlock static plock = FFLY_MUTEX_INIT;
void ffly_process_prep(void) {
}

static char const *pot_flags[] = {
	"free",
	"last_in_line",
	"apart",
	"take",
	"sprout",
	"interfere",
	"link",
	"acend"
};
/*
	look this is not amazing, i realy hate it.
	however when debugging we need to know what
	happend to prior.

	i mean in the case we have an issue in the free function,
	then the issue most likly arose in the allocation function.
	because of this we need to know where it came from.

	what where the prosses undertooken to get the allocation.
	was it a system call, was it salvaged from somewhere?
	was taken from a greater pot?


	there are many ways to store this data.

	method one.

	we could store it all as bits,
	one bit for each piece of text.

	option two, is divide a 32-bit int into 4-bit parts.
	and use each 4-bit part to select a piece of text from this list to tell us whats happend.

	the two options above are good, however its not making use of a lot.


	here we are just using

	the first 4-bits tell us where it all started.
	was it a greater pot birth,lesser pot birth?

	then 4-bits over, dependent on the first 4-bits.
	tell us what piece of text to select.

	the other 4-bits are miscellaneous?.

	this method is okay, however its abit tacky/tedious to work with.


	why this way? lets give an example in a print function

	print("birth,pure,successor");	condenced -> 'birth','pure','successor'
	print("fate,resurrected");			condenced -> 'fate','resurrected'

	we have two functions that print infomation,
	for the first one we have 3-tags, lets use an array method here
	so each tags-text is stored in an array like tags_str_list.
	this means each tag would need 5-bits. to in total we need 15-bits
	to store those 3-tags.

	now lets say we wanted the ability to have 4-tags,
	well that cant work can it? 4*5 = 20-bits
	all the tags need to fit within a 16-bit int.

	so we do this

	birth -> {pure,successor,forth}

	so the first 4-bits are {birth,fate}

	now we only need 3-bits to state {pure,successor,forth}
	4+(3*4) = 16

	meaning we can now sit 4-tags within a 16-bit int.
*/
static char const *tags_str_list[] = {
	"no-longer-alive",			//0
	"pure",									//1
	"partly",								//2
	"complete",							//3
	"sprout",								//4
	"resurrected",					//5
	"decomposed",						//6
	"adopted",							//7
	"nothing",							//8
	"acend",								//9
	"successor",						//10
	"salvage",							//11
	"greater_pot_takings",	//12
	"repossessed",					//13
	"rejoin",								//14
	"alone",								//15
	"conjoined",						//16
	"affiliated",						//17
	"recombined",						//18
	"(greater)(birth)",			//19
	"(greater)(fate)",			//20
	"(lesser)(birth)",			//21
	"(lesser)(fate)",				//22
	"(lesser)(spanned)",		//23
	"(uncertain)"						//24
};

struct pt_fac{
	_16_u fac1[16];
	_16_u fac2[16];
	_16_u codename;
};
#define _dupe16(__val)\
	__val,__val,__val,__val,__val,__val,__val,__val,\
	__val,__val,__val,__val,__val,__val,__val,__val
/*
	used to describe a operation where by it has suboperations or that
	lesser birth
		-> pure(from system)
	greater birth
		-> pure(from system)
*/
struct pt_fac static pot_tags[] = {
	{
		.codename = 24
	},
	//birth
	{
		.fac1 = {
			0,	//PT_FAC1_BIRTH_NLA
			1,	//PT_FAC1_BIRTH_PURE
			2,	//PT_FAC1_BIRTH_PARTLY
			3,	//PT_FAC1_BIRTH_COMPLETE
			4		//PT_FAC1_BIRTH_SPROUT
		},
		.fac2 = {
			8,
			9,
			10
		},
		.codename = 19
	},
	//death
	{
		.fac1 = {
			5,	//PT_FAC1_FATE_RESURRECTED
			6,	//PT_FAC1_FATE_DECOMPOSED
			7		//PT_FAC1_FATE_ADOPTED
		},
		.fac2 = {
			8,
			18
		},
		.codename = 20
	},
	//lesser birth
	{
		.fac1 = {
			11,	//PT_FAC1_LESSER_BIRTH_SALVAGE
			4,	//PT_FAC1_LESSER_BIRTH_SPROUT
			12,	//PT_FAC1_LESSER_BIRTH_GPT
			1		//PT_FAC1_LESSER_BIRTH_PURE
		},
		.fac2 = {
			_dupe16(8)
		},
		.codename = 21
	},
	{
		.fac1 = {
			4,	//PT_FAC1_LESSER_DEATH_SPROUT
			13,	//PT_FAC1_LESSER_DEATH_REPOSSESSED
			14	//PT_FAC1_LESSER_DEATH_REJOIN
		},
		.fac2 = {
			_dupe16(8)
		},
		.codename = 22
	},
	{
		.fac1 = {
			15,	//PT_FAC1_LESSER_SPANNED_ALONE
			16,	//PT_FAC1_LESSER_SPANNED_CONJOINED
			17	//PT_FAC1_LESSER_SPANNED_AFFILIATED
		},
		.fac2 = {
			_dupe16(8)
		},
		.codename = 23
	}
};

void static print_pot_info(potp p, char const *__rel){
	_int_u i;
	i = 0;
	_ar_printf("POT-%s:\n-flags: ",__rel);
	for(;i != 8;i++){
		if((p->flags&(1<<i))>0){
			_ar_printf("%s,",pot_flags[i]);
		}
	}
	_ar_printf("\n-tags: \n");
	i = p->tag_ptr;
	_int_u et = (i+1)&0xf;
	_int_u j;
	j = 0;
	/*
		we backtrace through history of this pot.
		helps to determine issues when debugging
	*/
	while(i != et){
		if(!(p->valid_tags&(1<<i))){
			break;
		}
		_ar_printf("# backlog-%u, for: %u.\n",j++,PT_FAC0_EXTR(p->tags[i]));
		_ar_printf(" type: %s\n",!(p->tags[i]&PT_GREATER)?"lesser":"greater");
		struct pt_fac *fac;
		_16_u f;
		f	= PT_FAC0_EXTR(p->tags[i]);
		if(f<6){
			fac = pot_tags+f;
			_16_u f1,f2;
			f1 = fac->fac1[PT_FAC1_EXTR(p->tags[i])];
			f2 = fac->fac2[PT_FAC2_EXTR(p->tags[i])];
				if(f1 <19 && f2<19){
			_ar_printf("%s, (%s), (%s).\n",tags_str_list[fac->codename],tags_str_list[f1],tags_str_list[f2]);
			}else{
			_ar_printf("out of range: %u, %u.\n",f1,f2);
			}
		}else{
			_ar_printf("invalid: %u.\n",f);
		}
		i = (i-1)&0xf;
	}
	_ar_printf("\n-other: \n");
	_ar_printf(" offset: %u.\n",p->off);
	_ar_printf(" limit: %u.\n",p->limit);
	_ar_printf(" underneath: %u.\n",p->underneath);
	_ar_printf(" lot: %x.\n",p->lot);
}

void ff_ar_info(struct ff_ar_info *__p) {
	__p->bale_c = main_pot.blk_c;
	__p->dead = _ar_dead(&main_pot);
	__p->used = _ar_used(&main_pot);
	__p->buried = _ar_buried(&main_pot);
}

void ensure_nongreater(void *__p){
	potp gtp;
	gtp = get_lspot(__p);
	_16_u 
	ilp = 1<<((_64u_(__p)&LGRPOT_ILVBITS)>>POT_SIZE_SHIFT);
	if((gtp->real_flags&ilp)>0){
		ar_assert(1 == 0,"a pot inside a pot?, %x\n", ((_64_u)__p)>>25);
	}
}

void static dish_free(struct arena *anr,struct bin_bay_dish *dish, _32_u __ds){
	mem_set(dish->bins,0xff,sizeof(_32_u)*no_bins);
	anr->free_dishes[anr->next_dish++] = __ds;
}

void static pot_primative_init(potp p){
	p->underneath = 0;
	p->flags = 0;
	p->inuse_lock = MUTEX_INIT;
	p->lot = AR_NULL;
	p->tag_ptr = 0;
	p->valid_tags = 0;
	p->lock = MUTEX_INIT;
	below_flags(p) = 0;
	p->apd.lock = MUTEX_INIT;
}

void static sort_by_size(potp __pot,balep __ft, balep __b) {
	if(__ft->size<=__b->size){
		return;
	}
	balep ft,bk;
	ar_off_t fwd = __ft->fd;
	ar_off_t bck = __b->bk;
	ft = get_bale(__pot,fwd);
	bk = get_bale(__pot,bck);
	if(not_null(fwd) && not_null(bck)){
		_ar_ms.sizesort_hits++;
		bk->fd = bale_offset(__ft);
		__ft->bk = bck;

		__b->fd = __ft->fd;
		__ft->fd = bale_offset(__b);
		__b->bk = bk->fd;
		ft->bk = __ft->fd;
	}
}

void static try_sizesort(potp __pot,balep __b) {
	ar_off_t fwd = __b->fd;
	balep ft,bk;
	ft = get_bale(__pot, fwd);
	if(not_null(fwd)){
		sort_by_size(__pot,ft,__b);
	}

	ar_off_t bck = __b->bk;
	bk = get_bale(__pot, bck);
	if(not_null(bck)){
		sort_by_size(__pot,__b,bk);
	}
}

void static delink(balep __b){
	ar_off_t fwd = __b->fd;
	ar_off_t bck = __b->bk;	
	balep ft,bk;
	ft = get_bale(__b,fwd);
	bk = get_bale(__b,bck);
	if(not_null(fwd)){
	/*
		short by size(every little helps)
	*/
	if(not_null(ft->fd) && not_null(bk->bk)){
		if(ft->size>bk->size){
			get_bale(__b,bk->bk)->fd = fwd;
			get_bale(__b,ft->fd)->bk = bck;
			ft->bk = bk->bk;
			bk->fd = ft->fd;

			ft->fd = bck;
			bk->bk = fwd;
			return;
		}

	}	

		get_bale(__b, fwd)->bk = bck;}
	else{
		bk->fd = AR_NULL;
		return;	
	}
		
	if(not_null(bck)){
		bk->fd = fwd;}
	else{
		ar_printf("back pointer null.\n");
		ar_abort();
	}
}

void static
detatch_nameonly(potp __pot, balep __b,_32_u bn){
	struct arena *anr;
	anr = myarena;
	ar_assert(__pot->bincnt[bn]>0, "somting went wong.\n");

	__pot->bincnt[bn]--;
	anr->bincnt[bn]--;
	if(not_null(__pot->b2) && !__pot->bincnt[bn]){
		struct bin_bay_dish *dish;
		dish = _ar_ms.dishes+__pot->b2;
		_32_u *ds = dish->bins+bn;
		_32_u sd;
		if(not_null(sd = *ds)){
			struct bin_bay *bay;
			bay = anr->bays+bn;
			ar_assert(__pot == bay->pots[sd],"why?.\n");
			_32_u m;
			potp k;
			ar_assert(sd>=bay->n,"why no?.\n");
			k = bay->pots[m = (bay->n++)];
			bay->pots[m] = NULL;//for debug
			bay->pots[sd] = k;
			struct bin_bay_dish *d2;
			d2 = _ar_ms.dishes+k->b2;
			d2->bins[bay->dish[m]] = sd;
			bay->dish[sd] = bay->dish[m];
			dish->cnt--;
			*ds = AR_NULL;
			if(!dish->cnt){
				dish_free(anr,dish,__pot->b2);
				__pot->b2 = AR_NULL;
			}
		}
	}	
}

_8_s static
detatch_inname(potp __pot, balep __b,_32_u bn) {
	_64_u boff;
	boff = bale_offset(__b);	
	ar_assert(__b->size<POT_REAL_SIZE,"a bale with a size larger then the pot size!, how did it get to this? %u\n",__b->size);
	if(__b->flags&BALE_UBIN){
		_int_u bix,bx;
		bix = ubin_indx(__b->size);
		bx = ubin_bindx(__b->size);
		ar_assert((__pot->ubin_bits&(_64u_(1)<<bx))>0,"questionable, %x\n", __pot->ubins[ubin_aindx(bx,__b->size)]);
		__pot->u_binsdwn[bx] &= ~(_64u_(1)<<bix);
		AR_BALE_OFFSET *ub = __pot->ubins+ubin_aindx(bx,__b->size);
		if(*ub == boff){
			*ub = __b->fd;
			if(not_null(__b->fd)){		
				get_bale(__b,__b->fd)->bk = AR_NULL;
			}else{
				__pot->ubin_bits &= ~(_64u_(1)<<bx);
				ar_assert(!__pot->u_binsdwn[bx], "somting wrong.\n");
			}
		}else{
			ar_assert(not_null(__b->bk),"issue.\n");
			get_bale(__b,__b->bk)->fd = __b->fd;
			if(not_null(__b->fd)){
				get_bale(__b,__b->fd)->bk = __b->bk;
			}
		}
		__b->fd = AR_NULL;
		__b->bk = AR_NULL;
		__b->flags &= ~BALE_UBIN;
		return -1;
	}

	ar_off_t *bin = bin_for(__pot,bn);
	ar_off_t fwd = __b->fd;
	ar_off_t bck = __b->bk;
	
	if(is_null(*bin)){
		AR_MESG(ARMSG_LV0|ARMSG_BALE,"bin at this location is empty.\n")
		ar_printf("bin %u, this location is empty, size: %u, %x, inuse: %u\n", bin-__pot->bins, __b->size, boff, is_free(__b));
		ar_abort();
	}

	if(boff == *bin){
		if(not_null(bck)){
			ar_printf("bale seems to have a back link.\n");}
		if(not_null(*bin = fwd)){
			get_bale(__pot, fwd)->bk = AR_NULL;}
		else{
			ar_assert(__pot->bincnt[bn] == 1, "bincount dident reach one as it should have.\n");	
			bin_bit_zero(__pot,bn);	}
	}else{
		delink(__b);
	}
	__b->fd = AR_NULL;
	__b->bk = AR_NULL;
	return 0;
}

void static
detatch(potp __pot, balep __b){
	_int_u bn = bin_no(__b->size);
	if(!detatch_inname(__pot,__b,bn)){
		detatch_nameonly(__pot,__b,bn);
	}
}


/*
	dynamic approch or NO?

	we have an issue, where by bale_offset
	does bit magic on a pointer.

	this magic value is diffrent from the larger set to lesser ones.

	how to deal with this issue??

	i know why not just put the MASK or somthing, and pass it to the function?.

	is it faster to do bit magic with const literals or register or memory?

	X = X&LITERAL
	X = X&REGISTER
	X = X&(MEMORY)

	we know the const literal one is alot faster then any alternative.

	but is this the best option?

	the X = X&(MEMORY) is never going to happen.


	leaves only two

	X = X&LITERAL
	X = X&REGISTER

	we could pass it into a register from function then use it??

	what we realy need is registers that GCC doesent touch, like a const register for constants only or somting :(
*/

/*
	we expect the bale to have been coupled before hand.
	NOTE:
		if the bale was the only bale in existence, then recoupling the bale wont work.
		as the bale will be removed completly.
*/
void static
_recouple(potp __pot, balep __b, _64_u register __mask) {
	ar_off_t *end = &__pot->end_bale;
	ar_off_t off = balem_offset(__b,__mask);

	ar_off_t rr, ft;

	rr = __b->prev;
	ft = __b->next;

	/*
		this should never be the case, if both rr and ft are null then the bale was never coupled to begin with.
	*/		
	ar_assert(!(is_null(ft) && is_null(rr)), "bale should not have both rear and front bales as NULL.\n");
	/*
		look when it comes to this we have a couple choices,
		A) we could pad with bale headers. what i mean is we could remove this buy placing a
		valid bale header at the start of the pot and the end, meaning both ft, and rr will always be valid.

		however im not doing this, mainly as it involves copying a bale header over and over again as the pot offset increases(more new bales are created).
		to summerize its a if statment as such, or copying a bale.

		both ways work, this way is for more non-volatile things. ie no repeated allocations and frees.
		for the other way its more for volatile things as removing this would bring better performance.
	*/
	if(is_null(rr|ft)){
		/*
			if are rear is NULL, then this means that we are the TOP-MOST bale of the POT.
			only the first bale to be created will have a rear of NULL.
		*/
		if(is_null(rr)){	
			ar_assert(off<ft,"tampered bale, %x < %x\n",off,ft);
			ar_assert(get_balem(__pot,ft,__mask)->prev == AR_NULL,"the bales next->prev should be NULL.\n");
			get_balem(__pot,ft,__mask)->prev = off;
			return;
		}
		/*
			if the front bale is NULL, this means that bale=__pot->end_bale.
			it means the most recent bale thats been created.
		*/
		//is_null(ft)
		else{
			ar_assert(off>rr,"tampered bale, %x > %x\n",off,rr);
			ar_assert(*end == rr);
			get_balem(__pot, rr,__mask)->next = off;
			*end = off;
			return;
		}
	}
	if(not_null(ft)){
		balem_next(__b,__mask)->prev = off;
	}else{
		ar_printf("front of bale is null when its not ment to be.\n");
		ar_abort();
	}
	if(not_null(rr)){
		balem_prev(__b,__mask)->next = off; 
	}else{
		ar_printf("rear of bale is null when its not ment to be.\n");
		ar_abort();
	}
}

#define lsrecouple(__pot,__b)\
	_recouple(__pot,__b,MASK_OF(LGRPOT_SIZE_SHIFT));
#define recouple(__pot,__b)\
	_recouple(__pot,__b,MASK_OF(POT_SIZE_SHIFT));
void static _decouple(potp __pot, balep __b, _64_u register __mask){
	ar_off_t *end = &__pot->end_bale;
	ar_off_t off = balem_offset(__b,__mask);

	ar_off_t rr = __b->prev;
	ar_off_t ft = __b->next;

	if(is_null(rr|ft)){
		/*
			if the rear bale is NULL then we know that its reached the end of the list in that direction
		*/
		if(is_null(rr)){
			if(not_null(ft)){
				get_balem(__pot,ft,__mask)->prev = AR_NULL;
			}else{
				*end = AR_NULL;
			}
			return;
		//is_null(ft)
		}else{
			ar_assert(off == *end,"somthing is wrong, very wong. even know the front bale is NULL, %u, dosent == %x\n",*end,off);
			/*
				NOTE:
					the rear will never be NULL because of aboves statment of is_null(rr)
					if it does occur then this means a multithread issue
			*/
			ar_assert(rr != AR_NULL,"someone out of the ordanary has happend.\n");
			*end = rr;
			get_balem(__pot,rr,__mask)->next = AR_NULL;	
			return;
		}
	}
	/*
		if all falls through then its not nether ft or rr should be NULL,
		we check anyways just in case.
	*/

	if(not_null(ft)){
		balem_next(__b,__mask)->prev = rr;
	}
#ifdef be_vigilant
	else{
		ar_printf("front bale is null.\n");
		ar_abort();
	}
#endif
	if (not_null(rr)){
		balem_prev(__b,__mask)->next = ft;
	}
#ifdef be_vigilant
	else{
		ar_printf("rear bale is null.\n");
		ar_abort();
	}
#endif
}

#define lsdecouple(__pot,__b)\
	_decouple(__pot,__b,MASK_OF(LGRPOT_SIZE_SHIFT));

#define decouple(__pot,__b)\
	_decouple(__pot,__b,MASK_OF(POT_SIZE_SHIFT));

/* dead memory */
/*
	memory that hasent been touched yet,
	- the large chunk of memory that has no bales
*/
_64_u
_ar_dead(potp __pot) {
	return POT_SIZE-__pot->off;
}

/* used memory */
_64_u
_ar_used(potp __pot) {
	return __pot->off-((__pot->blk_c*bale_size)+__pot->buried);
}

/* buried memory */
/*
	memory that is occupied but not being used at this moment
*/
_64_u
_ar_buried(potp __pot) {
	return __pot->buried;
}

#include "../system/nanosleep.h"
void ffly_arstat(void) {

}

_y_err init_pot(potp __pot) {
	mem_set(__pot->ubins,0,sizeof(AR_BALE_OFFSET)*64);
	mem_set(__pot->u_binsdwn,0,sizeof(_64_u)*64);
	mem_set(__pot->binmap,0,16);
	mem_set(__pot->bincnt,0,sizeof(_32_u)*no_bins);
	__pot->ubin_bits = 0;
	__pot->end_bale = AR_NULL;
	__pot->off = 0;
	__pot->limit = 0;
	__pot->blk_c = 0;
	__pot->next = NULL;
	__pot->previous = NULL;
	__pot->fd = NULL;
	__pot->bk = NULL;
	__pot->buried = 0;
	ar_off_t *bin = __pot->bins;
	while(bin != __pot->bins+no_bins)
		*(bin++) = AR_NULL;
}

void static tls_init(void) {
	TLS->arena = AR_NULL;
	TLS->tmp = NULL;
	ar_printf("tls init.\n");
}

void static
_erase(void *__p, _int_u __size) {
	mem_set(__p,0xff,__size);
}
void static *top_most_limit;
struct main_struc _ar_ms;
_32_u lot_alloc(void){
	struct arena *anr = myarena;
	_32_u lot;
	if(!anr->next_table){
		mt_lock(&_ar_ms.spill_lock_table);
		ar_assert(_ar_ms.spill_ltn>0,"not enough table slots.\n");
		lot = _ar_ms.spill_lot_table[--_ar_ms.spill_ltn];
		mt_unlock(&_ar_ms.spill_lock_table);
	}else{
		lot = anr->table_take[--anr->next_table];
	}
	ar_assert(lot<512,"this lot is invalid, got: %x, with next: %u\n",lot, anr->next_table);

	spanlgn(lot) = 0;
	return lot;
}

_32_u lot_strc_alloc(void){
	struct arena *anr = myarena;
	_32_u lot;
	if(!anr->next_take){
		mt_lock(&_ar_ms.spill_lock_strc);
		ar_assert(_ar_ms.spill_lsn>0,"not enough lot structures.\n");
		lot = _ar_ms.spill_lot_strc[--_ar_ms.spill_lsn];
		mt_unlock(&_ar_ms.spill_lock_strc);
	}else{
		lot = anr->take[--anr->next_take];
	}
	struct lot *lt;
	lt = _ar_ms.lots+lot;
	ar_assert(lt->issued == -1,"??????.\n");
	lt->issued = 0;
	lt->flags = 0;
	lt->pot = NULL;
	lt->next_free = NULL;
	lt->isucnt++;
	lt->prev_next_free = NULL;
	return lot;
}
//fft = free from table
#define lot_strc_fft(__lot)\
	lot_strc_free(lot_table(__lot))
void lot_strc_free(_32_u __lot){
	struct arena *anr = myarena;
	ar_assert(__lot<512,"this lot is invalid, got: %x.\n",__lot);
	ar_assert(!_ar_ms.lots[__lot].issued,"cant unissued somthing thats already been unissued.\n");
//	ar_assert(!_ar_ms.lots[__lot].size_in_pots,"not going well, got: %u\n",in_lpts(_ar_ms.lots[__lot].size_in_pots));
	_ar_ms.lots[__lot].issued = -1;
	_ar_ms.lots[__lot].flags = 0;
	if(anr->next_take>63){
		mt_lock(&_ar_ms.spill_lock_strc);
		ar_assert(_ar_ms.spill_lsn<64,"error.\n");
		_ar_ms.spill_lot_strc[_ar_ms.spill_lsn++] = __lot;
		mt_unlock(&_ar_ms.spill_lock_strc);
		return;
	}

	anr->take[anr->next_take++] = __lot;
}
void lot_map(_32_u __lot,_32_u __lot0){
	struct arena *anr = myarena;
	_ar_ms.lot_table[__lot] = __lot0;
}

void lot_free(_32_u __lot){
	struct arena *anr = myarena;
	ar_assert(__lot<512,"this lot is invalid, got: %x\n",__lot);
	if(anr->next_table>63){
		mt_lock(&_ar_ms.spill_lock_table);
		ar_assert(_ar_ms.spill_ltn<64,"error.\n");
		_ar_ms.spill_lot_table[_ar_ms.spill_ltn++] = __lot;
		mt_unlock(&_ar_ms.spill_lock_table);
	}else{
		anr->table_take[anr->next_table++] = __lot;
	}
}

void static init_acm(struct accommodation *acm){
	_int_u i;
	i = 0;
	for(;i != 16;i++){
		acm->_free_pots[i] = NULL;
	}
	acm->free_pots_lock = MUTEX_INIT;
	
	i = 0;
	for(;i != BOOKCASE_SIZE;i++){
		acm->bc_free_x[i] = i;
		acm->bc_free[i] = i;
	}

	acm->bcs_n = BOOKCASE_SIZE;
	acm->bookcase_lock = MUTEX_INIT;
	acm->gpac_miss = 0;
	acm->gb_insufficient = 0;
	acm->outlanders = 0;
}

//repuspose
void arena_new(struct arena *__anr,_32_u index){
	__anr->acm = _ar_ms.acm+index;
	__anr->sieve.sieve_reset = 0;
	mem_set(__anr->sieve.sieve_table,0xff,512*sizeof(AR_BALE_OFFSET));
	_int_u i;
	__anr->grsp_cnt = 0;
	i = 0;
	for(;i != 64;i++){
		__anr->grp[i] = 0;
		__anr->grp_free[i] = i;
	}
	__anr->grp_next = 64;
	__anr->spr_next = SPROUT_MAX;
	__anr->spr_cnt = SPROUT_MAX;
	mem_set(__anr->subsidiary_frees,0,8*16*sizeof(void*));
	mem_set(__anr->largest_chasm,0,sizeof(void*)*8);
	mem_set(__anr->bincnt,0,sizeof(_int_u)*no_bins);
	init_acm(__anr->acm);

	__anr->potbin_lock = MUTEX_INIT;
	__anr->potbin[0] = NULL;
	__anr->potbin[1] = NULL;
	__anr->next_table = 64;
	__anr->next_take = 64;
	__anr->flags = 0;
	__anr->access_lock = MUTEX_INIT;
	__anr->subf_n = 0;
	__anr->subf_lock = MUTEX_INIT;
	__anr->next_stock = 14;
	__anr->stock_lock = MUTEX_INIT;
	__anr->debug_lock = MUTEX_INIT;
	__anr->spr_lock = MUTEX_INIT;
	__anr->gpsal_miss = 0;
	__anr->gpbc_miss = 0;
	__anr->lpsal_miss = 0;
	__anr->lots_acum = 0;
/*
	NOTE: this is not sufficient we need a way to siphon off from other arenas to satisfy our needs.
*/
	i = 0;
	for(;i != 14;i++){
		__anr->frontbale[i] = NULL;
		__anr->stock[i] = i;
	}
	i = 0;
	for(;i != 8;i++){
		__anr->subf_cnt[i] = 0;
		__anr->subsidiary_drains_cnt[i] = 0;
		__anr->subsidiary_frees_cnt[i] = 0;
	}

	i = 0;
	for(;i != no_bins;i++){
		struct bin_bay *b;
		b = __anr->bays+i;
		mem_set(b->pots,0,sizeof(potp)*32);
		b->n = 32;
	}
	__anr->stg_n = STAG_SIZE;
}

_32_u static arena_alloc(void){
	_32_u rt;
	mt_lock(&_ar_ms.arena_alloc_lock);
	rt = _ar_ms.free_arenas[--_ar_ms.arena_next];
	mt_unlock(&_ar_ms.arena_alloc_lock);	
	struct arena *anr;
	anr = arena_at(rt);
	anr->flags |= ARENA_ISSUED;
	return rt;
}

void static arena_free(_32_u __anr){
	mt_lock(&_ar_ms.arena_alloc_lock);
	_ar_ms.free_arenas[_ar_ms.arena_next++] = __anr;
	mt_unlock(&_ar_ms.arena_alloc_lock);
	struct arena *anr;
	anr = arena_at(__anr);
	anr->flags &= ~ARENA_ISSUED;
}

void spaninit(potp,_64_u);
void pot_span(potp __pot, _64_u __end);
_y_err
ffly_ar_init(void){
	_int_u i;
	i = 0;
	_int_u g,s,w;
	g = 0;
	s = 0;
	w = 0;
	for(;i != 8;i++){
		struct arena *anr;
		anr = _ar_ms.arenas+i;
		_ar_ms.free_arenas[i] = i;
		anr->stamp = NULL;
		arena_new(anr,i);
		_int_u j;
		j = 0;
		for(;j != 64;j++){
			anr->table_take[j] = g+j;
			anr->take[j] = g+j;
		}
		g+=64;

		j = 0;
		for(;j != 8*no_bins;j++){
			struct bin_bay_dish *dish;
			dish = _ar_ms.dishes+j+s;
			mem_set(dish->bins,0xff,sizeof(_32_u)*no_bins);
			dish->cnt = 0;
			anr->free_dishes[j] = j+s;
		}
		s+=8*no_bins;
		anr->next_dish = 8*no_bins;	
	}
	_ar_ms.arena_next = 7;	
	arena_at(7)->flags |= ARENA_ISSUED;
	i = 0;
	for(;i != 512;i++){
		struct lot *lt;
		lt = _ar_ms.lots+i;
		_ar_ms.lot_table[i] = AR_NULL;
		_ar_ms.span_lgn[i] = AR_NULL;
		lt->issued = -1;
		lt->size_in_pots = 0;
		lt->isucnt = 0;
		lt->lock = MUTEX_INIT;
	}
#ifdef graph_plots
	graphp();
#endif
	_ar_ms.spill_lock_strc = MUTEX_INIT;
	_ar_ms.spill_lock_table = MUTEX_INIT;
	_ar_ms.spill_lsn = 0;
	_ar_ms.spill_ltn = 0;
	_ar_ms.arena_alloc_lock = MUTEX_INIT;
	_ar_ms.subsidiary_drains_by_outlander = 0;
	_ar_ms.subsidiary_frees_by_outlander = 0;
	_ar_ms.subsidiary_flunks = 0;
	_ar_ms.subsidiary_frees = 0;
	_ar_ms.subsidiary_drains = 0;
	_ar_ms.gnarl_formed = 0;
	_ar_ms.gnarl_complete_cycles = 0;
	_ar_ms.rinse_peak = 0;
	_ar_ms.sieve_rinses = 0;
	_ar_ms.gnarl_hits = 0;
	_ar_ms.gnarl_misses = 0;
	_ar_ms.gyre_steps = 0;
	_ar_ms.usage_peak = 0;
	_ar_ms.lpot_alloced = 0;
	_ar_ms.lpot_freed = 0;
	_ar_ms.lpot_peak = 0;
	_ar_ms.chasm_hits = 0;
	_ar_ms.chasm_misses = 0;
	_ar_ms.ubin_scrubed_snaffles = 0;
	_ar_ms.ubin_snaffles = 0;
	_ar_ms.ubin_misses = 0;
	_ar_ms.ubin_hits = 0;
	_ar_ms.sizesort_hits = 0;
	_ar_ms.cspr_harvests = 0;
	mem_set(_ar_ms.bin_hits_by_bin,0,sizeof(_int_u)*no_bins);
	mem_set(_ar_ms.bin_misses_by_bin,0,sizeof(_int_u)*no_bins);
	mem_set(_ar_ms.bin_deadends_by_bin,0,sizeof(_int_u)*no_bins);

	mem_set(_ar_ms.abc_hits_by_bin,0,sizeof(_int_u)*no_bins);
	mem_set(_ar_ms.abc_misses_by_bin,0,sizeof(_int_u)*no_bins);
	mem_set(_ar_ms.abc_deadends_by_bin,0,sizeof(_int_u)*no_bins);

	_ar_ms.poted_bales_alloced = 0;
	_ar_ms.poted_bales_freed = 0;
	_ar_ms.abc_zero_case = 0;
	_ar_ms.pot_peak = 0;
	_ar_ms.abc_hits = 0;
	_ar_ms.abc_misses = 0;
	_ar_ms.pot_alloced = 0;
	_ar_ms.pot_freed = 0;
	_ar_ms.pot_hits = 0;
	_ar_ms.pot_misses = 0;
	_ar_ms.bin_hits = 0;
	_ar_ms.bin_misses = 0;
	mem_set(_ar_ms.sfb_hits,0,sizeof(_int_u)*8);
	mem_set(_ar_ms.sfb_misses,0,sizeof(_int_u)*8);
	mem_set(_ar_ms.sfb_deadends,0,sizeof(_int_u)*8);

	_ar_ms.lock = MUTEX_INIT;
	_ar_ms.more_lock = MUTEX_INIT;
	ar_erase = _erase;
#ifndef __tty
	arout = open("arout", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
#else
	arout = open("/dev/tty", O_WRONLY, 0);
#endif
	if (arout == -1) {
		reterr;
	}

	ffly_tls_toinit(tls_init);
	tls_init();
	TLS->real = TLS->arena = 7;
	ffly_process_prep();

	_64_u brkp = (_64_u)brk((void*)0);
	_64_u fix = MASK_OF(POT_SIZE_SHIFT)^(brkp&MASK_OF(POT_SIZE_SHIFT));

	_64_u top = brkp+fix+1;
	/*adjust*/
	brk(top);
	_ar_ms.off = fix+1;
	_ar_ms.limit = 0;
	_ar_ms.end = brkp; // should check for error
	_ar_ms.top = _ar_ms.end;
	/*
		we ignore this for as of now.
	*/
	_64_u top_most;
	top_most = _ar_sysmore(POT_SIZE);
	top_most_limit = ((_64_u)top_most)|POT_REAL_SIZE;
	potp top_pot;
	top_pot = top_most;
	top_pot->flags = 0;
	top_pot->underneath = 0;

	_64_u addr = ((_64_u)_ar_ms.end)+_ar_ms.off;
	ar_assert(!(addr&MASK_OF(POT_SIZE_SHIFT)),"not correctly POT aligned: %x.\n",addr);

	_64_u m = LGRPOT_SIZE-(addr&MASK_OF(LGRPOT_SIZE_SHIFT));
	_64_u ptr;
	potp p;
	ar_printf("moving ahead by: %x.\n",m);
	/*
		if m is not zero, meaning its not aligned to LGRPOT_SIZE
	*/
	if(m != 0){
		ptr = _ar_sysmore(m);
		ptr+=m;
	}

	_64_u adr;
	adr = ((_64_u)_ar_ms.end)+_ar_ms.off;
	adr -= POT_SIZE;
	adr |= POT_REAL_SIZE;
	p = adr;
	pot_primative_init(p);
	p->flags |= POT_GTRESTRICT;
	below_flags(p) |= POT_LAST_IN_LINE;

	pot_span(p,m);
	spaninit(p,m);
	addr = ((_64_u)_ar_ms.end)+_ar_ms.off;
	ar_assert(!(addr&MASK_OF(LGRPOT_SIZE_SHIFT)),"not POT aligned: %x.\n",addr);
	ar_printf("BRK start %p.\n", brkp);
	retok;
}

void static print_acm(struct accommodation *acm){
	_int_u i;
	i = acm->bcs_n;
	for(;i != BOOKCASE_SIZE;i++){
		struct lot *lot;
		lot = acm->bookcase[acm->bc_free[i]];
		_ar_printf("bookcase-%u: lot-size: %u-lpts.\n",i,in_lpts(lot->size_in_pots));
	}

	_ar_printf("bookcased: %u.\n",acm->bcs_n);
	_ar_printf("gpac_miss: %u.\n",acm->gpac_miss);
	_ar_printf("gb_insufficient: %u.\n",acm->gb_insufficient);
	_ar_printf("outlanders: %u.\n",acm->outlanders);
}

void static print_arena(struct arena *__anr){
	print_acm(__anr->acm);
	_int_u i;
	i = 0;
	for(;i != no_bins;i++){
		_ar_printf("abin-%u: cnt: %u\n",i,__anr->bincnt[i]);
	}

	_ar_printf("potential greater pots: %u.\n",__anr->spr_cnt);
	_ar_printf("next dish: %u.\n",__anr->next_dish);
	i = 0;
	for(;i != no_bins;i++){
		_ar_printf("bay: n{%u}.\n",__anr->bays[i].n);
	}

	_ar_printf("lpsal_miss: %u, gpsal_miss: %u, gpbc_miss: %u, lots_acum: %u.\n",
		__anr->lpsal_miss,__anr->gpsal_miss,__anr->gpbc_miss,__anr->lots_acum
	);
	i = 0;
	for(;i != 8;i++){
		_ar_printf("%u(subsidiary), frees: %u, drains: %u, count: %u\n",i,__anr->subsidiary_frees_cnt[i],__anr->subsidiary_drains_cnt[i],__anr->subf_cnt[i]);
	}
}

char const static *mprefix[] = {
	"bytes",
	"kb",
	"mb",
	"gb"
};
char const*memory_prefix(_64_u *__dst, _64_u *__exp, _64_u __bytes){
	_int_u i = 0;
	/*
		look this is an estimation
		as 1024 != 1000 
		1024 != kilo
	*/
	while((__bytes>>(i*10))>1024 && i<4){i++;}
	*__exp = __bytes&((1<<(i*10))-1);
	*__dst = __bytes>>(i*10);
	return mprefix[i];
}

void dump_stats(void){
	_int_u i;
	i = 0;
	for(;i != 512;i++) {
		struct lot *lot;
		lot = _ar_ms.lots+i;
		mt_lock(&lot->lock);
		_32_u lt;
		lt = _ar_ms.lot_table[i];
		_ar_printf("%u(%s), lot: pots-inlisted: %u, table: (%x)(%s), issued(%s), isucnt: %u, acm: %u, base: %p, %s\n",
			i,(!lot->size_in_pots && !lot->issued)?"ERROR":"ALL GOOD",in_lpts(lot->size_in_pots),lt,is_null(lt)?"NULL":"OKAY",!lot->issued?"yes":"nope",lot->isucnt,lot->acm,lot->pot,(lot->flags&LOT_BOOKED)?"(BOOKED)":"");
		if(lot->pot != NULL && !lot->issued){
			potp pot;
			pot = lot->pot;
			mt_lock(&pot->apd.lock);
			_ar_printf("- lot pot flags: %x, offset: %u, below_flags: %x\n",pot->flags,pot->off,below_flags(pot));
			mt_unlock(&pot->apd.lock);
		}
		mt_unlock(&lot->lock);
		_32_u j;
		_int_u nulls = 0;
	}

	i = _ar_ms.arena_next;
	while(i != 8){
		print_arena(_ar_ms.arenas+i++);
	}
	i = 0;
	for(;i != no_bins;i++){
	_ar_printf("bin-%u: hits: %u, misses: %u, deadends: %u\n",i,_ar_ms.bin_hits_by_bin[i],_ar_ms.bin_misses_by_bin[i],_ar_ms.bin_deadends_by_bin[i]);
	}
	
	i = 0;
	for(;i != no_bins;i++){
	_ar_printf("abc_bin-%u: hits: %u, misses: %u, deadends: %u\n",i,_ar_ms.abc_hits_by_bin[i],_ar_ms.abc_misses_by_bin[i],_ar_ms.abc_deadends_by_bin[i]);
	}

	_ar_printf("potted_bales: alloced: %u, freed: %u.\n",_ar_ms.poted_bales_alloced,_ar_ms.poted_bales_freed);
	_ar_printf("memory: peak: %u-kb.\n",(_ar_ms.pot_peak*POT_SIZE)/1000);
	_ar_printf("arena-bins-count: zero_cases: %u, hits: %u, misses: %u.\n",_ar_ms.abc_zero_case,_ar_ms.abc_hits,_ar_ms.abc_misses);
	_ar_printf("pot: hits: %u, misses: %u, alloced: %u, freed: %u, peak: %u\n",_ar_ms.pot_hits,_ar_ms.pot_misses,_ar_ms.pot_alloced,_ar_ms.pot_freed,_ar_ms.pot_peak);
	_ar_printf("lpot: alloced: %u, freed: %u, peak: %u\n",_ar_ms.lpot_alloced,_ar_ms.lpot_freed,_ar_ms.lpot_peak);
	_ar_printf("bin: hits: %u, misses: %u.\n",_ar_ms.bin_hits,_ar_ms.bin_misses);		
	i = 0;
	for(;i != no_bins;i++){
	_ar_printf("swift-%u: hits: %u, misses: %u, deadends: %u, attempts: %u.\n",i,_ar_ms.sfb_hits[i],_ar_ms.sfb_misses[i],_ar_ms.sfb_deadends[i],_ar_ms.sfb_hits[i]+_ar_ms.sfb_misses[i]);
	}
	_ar_printf("ubin: hits: %u, misses: %u, snaffles: %u, scrubed snaffles: %u\n",_ar_ms.ubin_hits,_ar_ms.ubin_misses,_ar_ms.ubin_snaffles,_ar_ms.ubin_scrubed_snaffles);
	_ar_printf("sizesort: hits: %u.\n",_ar_ms.sizesort_hits);
	_ar_printf("chasm: hits: %u, misses: %u.\n",_ar_ms.chasm_hits,_ar_ms.chasm_misses);
	_ar_printf("complete(sprout) harvests: %u.\n",_ar_ms.cspr_harvests);
	_ar_printf("gyre_steps: %u, gnarl_hits: %u, gnarl_misses: %u, sieve_rinses: %u, rinse_peak: %u, gnarl_complete_cycles: %u, gnarl_formed: %u\n",
		_ar_ms.gyre_steps,_ar_ms.gnarl_hits,_ar_ms.gnarl_misses,_ar_ms.sieve_rinses,_ar_ms.rinse_peak,_ar_ms.gnarl_complete_cycles, _ar_ms.gnarl_formed);
	_ar_printf("subsidiary_frees: %u(by outlander: %u), subsidiary_flunks: %u, subsidiary_drains: %u(by outlander: %u).\n",
		_ar_ms.subsidiary_frees,_ar_ms.subsidiary_frees_by_outlander,_ar_ms.subsidiary_flunks,_ar_ms.subsidiary_drains,_ar_ms.subsidiary_drains_by_outlander);
	_64_u ub;
	_64_u ue;
	char const *prefix;
	prefix = memory_prefix(&ub,&ue,_ar_ms.usage_peak);
	_ar_printf("brk depth: %u, potsize: %u-bytes, mem_usage_peak: %u.%u-%s\n",_ar_ms.off,pot_size,ub,ue,prefix);	
}
void static dump_pot_lot_details(potp p){
	struct lot *lot;
	lot = get_lot(p->lot);
	_ar_printf("(dump_pot_lot_details) pot-%p, size_in_pots: %u-lpts.\n",p,lot->size_in_pots>>POT_SIZE_SHIFT);
}

/*
	TODO:
		mem stat size formatter??
*/
_y_err ffly_ar_cleanup(void) {
	dump_stats();
	potp p;
	p = top_most_limit;
	_int_u i;
	i = 0;
	while(p <_ar_ms.top){
		_ar_printf("pot-%u:%u, flags: %x, lot: %u, acm: %u, offset: %u, below_flags: %x, real_flags: %x, limit: %u\n",
			i++,in_lpts(_64u_(p)&LGRPOT_ILVBITS),p->flags,p->lot,p->acm,p->off,below_flags(p),p->real_flags,p->limit
		);
		p = blpot(p);
	}

	
	close(arout);
	brk(_ar_ms.end);
#ifdef graph_plots
	graph_finish();
#endif
}

void ffly_arbl(void *__p) {
	balep blk = (balep)((_8_u*)__p-bale_size);
	ar_printf("bale: off: %x, size: %u, pad: %u\n", bale_offset(blk), blk->size, blk->pad);
}
void pot_free_guarded(potp);
void static dispose(potp, ar_off_t, _int_u);
/*
	hang the pots, dead parts will drop off
	this is thread dependent
*/
void ffly_arhangi(void){
	ffly_arhang(TLS);
}

void ffly_arhang(void *tls) {
	struct tls_storage *t = tls;
	struct arena *anr;
	anr = arena_at(t->arena);
	ar_assert((anr->flags&ARENA_ISSUED)>0,"arena hasent been issued, %u\n",t->arena);
	arena_free(t->arena);
	return;
	potp top = myarena->top;
	potp p;
	if (!(p = top)) {
#ifdef __ffly_debug
		ar_printf("ar, pot has already been axed, or has been like this from the start.\n");
#endif
		return;
	}

	potp ft, rr;
	potp bk;
	while(p != NULL) {
		bk = p;
		p = p->fd;

		ft = bk->fd;
		rr = bk->bk;

		if (ft != NULL)
			ft->bk = rr;
		if (rr != NULL)
			rr->fd = ft;

		bk->fd = NULL;
		bk->bk = NULL;
		if (!bk->off) {
			/*
				this tells us the POT contains no bales as offset is zero,
				meaning we can get rid of it.
			*/
			pot_free_guarded(bk);
		}else{
			/*
				this POT still contains bales that are inuse,
				so orthan them off to others.
			*/
			mt_lock(&_ar_ms.lock);
			bk->bk = NULL;
			if(_ar_ms.indulge != NULL)
				_ar_ms.indulge->bk = bk;
			bk->fd = _ar_ms.indulge;
			_ar_ms.indulge = bk;
			mt_unlock(&_ar_ms.lock);
		}
	}
	arena_free(TLS->arena);
}

void static largest_chasm_rm(potp __pot){
	struct arena *anr;
	anr = myarena;

	_int_u n;
	n = __pot->off>>(POT_SIZE_SHIFT-3);
	potp chasm = anr->largest_chasm[n];
	if(chasm == __pot)
		anr->largest_chasm[n] = NULL;
}

void static largest_chasm(potp __pot){
	struct arena *anr;
	anr = myarena;
	
	_int_u n = __pot->off>>(POT_SIZE_SHIFT-3);
	potp *chasm = anr->largest_chasm+n;
	if(*chasm != NULL) {
		if(__pot->off<(*chasm)->off)
			*chasm = __pot;
	}else{
		*chasm = __pot;
	}
}

#define meets_ubin_criteria(__pot,bix,bx)\
	(!(__pot->u_binsdwn[bx]&(_64u_(1)<<bix)))

void static place_in_ubin(potp __pot, balep blk, _int_u boff, _int_u bx) {	
	AR_BALE_OFFSET *ub = __pot->ubins+ubin_aindx(bx,blk->size); 
	blk->fd = AR_NULL;
	if((__pot->ubin_bits&(_64u_(1)<<bx))>0){
		ar_assert(not_null(*ub),"issue");
		balep occupant;
		occupant = get_bale(__pot,*ub);
		occupant->bk = boff;
		blk->fd = *ub;
	}
	blk->bk = AR_NULL;
	*ub = boff;
	blk->flags |= BALE_UBIN;			
	__pot->ubin_bits |= _64u_(1)<<bx;
}

/*
	has not been finished
	TODO:
*/

/*
	this means to give or take space from its adjcent bales

	NOTE:
		no new bales are created
*/
void static*
shrink_blk(potp __pot, balep __b, ar_uint_t __size) {
	if(__b->size<=__size || is_free(__b)) {
#ifdef __ffly_debug
		ar_printf("forbidden alteration.\n");
#endif
		return NULL;
	}
	ar_uint_t size;
	size = __b->size-__b->pad;
	void *p;
	balep rr,ft;
	void *ret = NULL;
	// how much to shrink
	ar_uint_t dif = size-__size;
	ar_off_t off = bale_offset(__b);
	_8_u freed, inuse;
	AR_MESG(ARMSG_LV1|ARMSG_BALE,"attempting to shrink bale by %u bytes.\n", dif)
	if(dif>MAX_SH) {
		AR_MESG(ARMSG_LV1|ARMSG_BALE,"can't strink bale, %u bytes is too much to cutoff.\n", dif)
		goto _r;	
	}

	/*
		if an attempt to shrink the bale is less then X then just
		write it down as padding/extra.
	*/
	if (dif<(MAX_SH+1) && __b->pad<(MAX_SH+1)) {
		__b->pad+=dif;
		ret = (void*)((_8_u*)__b+bale_size);
		goto _r;
	}

	AR_MESG(ARMSG_LV1|ARMSG_BALE,"cant add as padding, moving on to diffrent approch...\n")
	
	rr = bale_prev(__b); //rear
	ft = bale_next(__b); //front

	/*
		if the previus/rear bale exists
	*/
	if(not_null(__b->prev)) {
		/*
			if the rear bale is free or is inuse and has free space 
		
			the operation takening place here is.
			in the case the rear bale is free of use 
			then we move every thing in the bale we are working 
			on downwards and write the rear bale as having extra space
		*/
		if((freed = is_free(rr)) || ((inuse = is_used(rr)) && rr->pad<MIN_PAD_TBGM)) {
			if(freed) {
				__pot->buried+=dif;
				if(is_flag(rr->flags, BALE_LINKED)){
					detatch(__pot, rr);
				}
			}
			p = __b;
			off+=dif;
			if (off-dif == __pot->end_bale)
				__pot->end_bale = off;
			rr->next = off;
			if (not_null(__b->next))
				ft->prev = off;
			__b->size-=dif;
			if (inuse)
				rr->pad+=dif;

			rr->size+=dif;
			
			__b = (balep)((_8_u*)__b+dif);//get the new bale pointer/advance by '+dif'
			ret = (void*)((_8_u*)__b+bale_size);
			copy(__b,p,__size+bale_size);
			if(freed) {
				auctionoff(__pot,rr);
			}
			goto _r;
		}
	}
	AR_MESG(ARMSG_LV1|ARMSG_BALE,"can't shrink bale.\n")
_r:
	return ret;
}

void static*
grow_blk(potp __pot, balep __b, ar_uint_t __size) {
	if(__b->size-__b->pad >= __size || is_free(__b)) {
#ifdef __ffly_debug
		ar_printf("forbidden alteration.\n");
#endif
		return NULL;
	}
	
	ar_uint_t size;
	size = (__b->size-__b->pad);
	void *p;
	void *ret = NULL;
	ar_uint_t dif = __size-size;
	ar_off_t off = bale_offset(__b);
	_8_u freed, inuse;
	balep rr,ft;
	AR_MESG(ARMSG_LV1|ARMSG_BALE,"attempting to grow bale by %u bytes.\n", dif)
	if(dif>MAX_GR) {
		AR_MESG(ARMSG_LV1|ARMSG_BALE,"can't grow bale, %u bytes is to much to add on.\n", dif)
		goto _r;
	}

	if(__b->pad>=dif) {
		__b->pad-=dif;
		ret = (void*)((_8_u*)__b+bale_size);
		goto _r;
	}

	rr = bale_prev(__b); //rear
	ft = bale_next(__b); //front
	if(not_null(__b->prev)) {
		if(((freed = is_free(rr)) && rr->size > dif<<1) || ((inuse = is_used(rr)) && rr->pad >= dif)) {
			if (freed) {
				__pot->buried-=dif;
				if(is_flag(rr->flags, BALE_LINKED)){
					detatch(__pot, rr);
				}
			}
		
			p = __b;
				
			off-=dif;
			if (off+dif == __pot->end_bale)
				__pot->end_bale = off;
			rr->next = off;
			if (not_null(__b->next))
				ft->prev = off;
			__b->size+=dif;
			rr->size-=dif;
			if (inuse)
				rr->pad-=dif;
				
			__b = (balep)((_8_u*)__b-dif);
			copy(__b,p,size+bale_size);	
			ret = (void*)((_8_u*)__b+bale_size);
			/*
				TODO: dont rely of it
					dont immediately auction off, this is because these routines are expensive.

				TODO:
					make routine that only places it in the bins a without altering counters.
			*/
			if (freed) {
				auctionoff(__pot,rr);
			}
			goto _r;
		}
	}

	if (not_null(__b->next)) {
		if (is_free(ft)) {
			
		}
	}
	AR_MESG(ARMSG_LV1|ARMSG_BALE,"can't grow bale.\n")
_r:
	return ret;
}

// user level
void*
ffly_arsh(void *__p, _int_u __to) {
	balep b = (balep)((_8_u*)__p-bale_size);
	potp p;

	p = get_pot(b);
	void *ret;
	
	ret = shrink_blk(p, b, __to);
	return ret;
}

void*
ffly_argr(void *__p, _int_u __to) {
	balep b = (balep)((_8_u*)__p-bale_size);
	potp p;

	p = get_pot(b);
	void *ret; 
	
	ret = grow_blk(p, b, __to);
	return ret;
}
void lot_link(struct lot*,_32_u);

void static pluck_off_pot(potp pot,struct lot *lot,struct accommodation *acm){	
	_64_u check;
	check = lot->size_in_pots;
	lot->size_in_pots-=POT_SIZE;
	spanlgn(pot->lot)-=POT_SIZE;

	ar_assert((check-POT_SIZE) == lot->size_in_pots,"somthing is wrong here.\n");
	if(!lot->size_in_pots){
		ar_assert(!spanlgn(pot->lot),"no way real?.\n");
		if(!(lot->flags&LOT_BOOKED)){
			lot_renounce_cbforeign(lot);
		}else{
			bookcase_remove_nolock(lot);
		}
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"nothing left.\n");
		lot_strc_free(lot_table(pot->lot));
		lot_free(pot->lot);
	}else{
		ar_assert(pot != lot->pot,"fatal error.\n");
		/*
			NOTE: lot->pot is not affected by trimming/plucking
			downgrade ranking
		*/
		if(!(lot->flags&LOT_BOOKED)){
			ar_assert(lot->acm == acm_idx(acm),"know why.\n");
			lot_renounce_cbforeign(lot);
			lot_link(lot,linkidx(lot->size_in_pots));
		}
		/*
			here we are trying to take a trim off of the top
		*/
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"we are dealing with more then one pot: %u.\n",in_lpts(lot->size_in_pots));
		potp up;
		/*
			this is used to determan if we are steping over a lot boundry
		*/
		up = _64u_(pot)+POT_SIZE;
		ar_assert((up->flags&POT_FREE) && not_null(up->lot),"seems we have are selfs a NULL lot.\n");
		if(up->lot != pot->lot){
			ar_assert(!spanlgn(pot->lot),"should be zero.\n");
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"non-matching lots, meaning we are traversing the lot boundary, %x != %x.\n",up->lot,pot->lot);			
			ar_assert(lot_table(up->lot) == lot_table(pot->lot),"pots dont have same lot origin.\n");
			/*
				as this means we are setting foot in a new place. we need to free up this resource as its longer used.
				this only takes place when we take a step into another lot range.
			*/
			lot_free(pot->lot);
		}
	}
	/*
		must be done before unlocking pot'u'
	*/
	pot->flags &= ~POT_FREE;
	pot->lot = AR_NULL;
}

potp take_from_bookcase(struct accommodation *acm){
	mt_lock(&acm->bookcase_lock);
	_int_u i;
	i = acm->bcs_n;
	for(;i != BOOKCASE_SIZE;i++){
		struct lot *lot;
		potp pt;
		lot = acm->bookcase[acm->bc_free[i]];
		ar_assert(lot != NULL,"fuck.\n");
		if(mt_trylock(&lot->lock) == -1){
			continue;
		}
		pt = lot->pot;
		if(mt_trylock(&pt->apd.lock) == -1){
			mt_unlock(&lot->lock);
			continue;
		} 
		potp u;
		u = _64u_(pt)-lot->size_in_pots;
		potp pot;
		pot = blpot(u);
		mt_lock(&u->apd.lock);
		pluck_off_pot(pot,lot,acm);
		mt_unlock(&u->apd.lock);
		mt_unlock(&lot->lock);
		mt_unlock(&pt->apd.lock);
		mt_unlock(&acm->bookcase_lock);
		return pot;
	}
	mt_unlock(&acm->bookcase_lock);
	return NULL;
}
/*
	there are two types of lot spans,
	the ones that are allowed to grow and
	the ones we trim down to size all the time.
*/
potp pot_salvage(struct accommodation *acm,_32_u idx){
	potp pt,pot;
	struct lot *lot;
	mt_lock(&acm->free_pots_lock);
	lot = acm->_free_pots[idx];
	/*
		this stops others from interfering with us, what is stops
		is anyone joing themselfs to us who arnt the arena owner.
		NOTE:
			its to stop the pot at the other end of the span from
			tampering with it.
	
		we go another route in locking this by locking the the bottom and tops above pot,
		howver inorder to get the tops above pot we need to know the size of the lot.
		and without locking we could end of getting a pot that may no longer be apart of the lot.
	
		!by locking the lot we stop any retraction of pots.
	
		this wont happen to the bottom pot of the lot because we already have the lock for that,
		however the top pot isnot safe.
	*/
_ndattempt:
	if(!lot){
		mt_unlock(&acm->free_pots_lock);
		return NULL;
	}
	if(mt_trylock(&lot->lock) == -1){
		lot = lot->next_free;
		goto _ndattempt;
	}
	pt = lot->pot;
	ar_assert(lot->size_in_pots>0,"wtf.\n");
	
	/*
		TODO-FIXME:
			trylocks are slow
	*/
	if(mt_trylock(&pt->apd.lock) == -1){
		mt_unlock(&lot->lock);
		lot = lot->next_free;
		goto _ndattempt;
	}
	mt_unlock(&acm->free_pots_lock);
	ar_assert(lot->acm == acm_idx(acm),"pot in free list should be apart of are acm but is not?.\n");
	/*
		if we aquired both locks then its mine and no one can take it away
	*/
	ar_assert(not_null(pt->lot),"a free pot without attached lot?.\n");
	potp u;
	u = _64u_(pt)-lot->size_in_pots;

	
	pot = blpot(u);
	/*
		the point of the locks here is to stop adjcent pots
		peeking into the pots that makeup the span
	
		makes sure that is the adjecent pots peek,
		that they wont end up with an invalid lotID
	
		TODO:	trylock?
	*/
	mt_lock(&u->apd.lock);
	pluck_off_pot(pot,lot,acm);
	mt_unlock(&pt->apd.lock);
	mt_unlock(&u->apd.lock);
	mt_unlock(&lot->lock);
	return pot;
}

void static make_endpot(potp p){
	/*
		NOTE:
			this sould always be a valid operation,
			there should be no issue accessing the pot above us!.
			even in the case of a larger pot or potfit the above header should always exist.
		
			!no locking should be required 
	*/
	potp up;
	up = abpot(p);
	ar_assert((below_flags(up)&POT_LAST_IN_LINE)>0, "this should not be possiple unless somthing has gone terribly wrong.\n");
	below_flags(up) &= ~POT_LAST_IN_LINE;
	below_flags(p) |= POT_LAST_IN_LINE;
}
/*
	things to take NOTE-of is that only the arena that the sprout belongs to can removes pots from it, any arena can add pot however.

	NOTE:
	 sprouts are seen by others to be inuse and dont harber the POT_FREE flag
*/
potp static pta_using_spr(struct arena *anr){
	potp p;
	struct sprout *spr;
	/*
		no locking required as we are the only ones picking from these sprouts(its arena specific)
	*/
	spr = anr->spr_inuse[anr->spr_next];
	mt_lock(&spr->lock);
	/*
		the pot we are locking here is the pot above the most recently added pot
	*/
	potp abt;
	abt = abpot(spr->p);
	mt_lock(&abt->apd.lock);
	mt_unlock(&spr->lock);
	/*
		here we are taking are pickings from a pot thats not greater pot aligned.
	*/
	if(below_flags(spr->p)&POT_SPROUT){
		p	= spr->p;
		/*
			this tells us the sprout has its poster on the wall basiticly for greater pot taking.
		*/
		if(spr->bits&128){
			if(anr->spr_cnt>CSPRTR){
				p = NULL;
				goto _retval;
			}
			mt_lock(&anr->spr_lock);
			ar_assert(spr->finalize<SPROUT_MAX,"invalid.\n");
			ar_assert(anr->spr_cnt<SPROUT_MAX,"this is an issue.\n");
			struct sprout *spr0;
			spr0 = anr->spr_complete[anr->spr_cnt++];
			anr->spr_complete[spr->finalize] = spr0;
			spr0->finalize = spr->finalize;
			mt_unlock(&anr->spr_lock);
		}
		ar_assert((_64u_(spr->p)&LGRPOT_MASK) != LGRPOT_REAL_SIZE,"malfunction.\n");
		spr->p = blpot(spr->p);

		if((_64u_(spr->p)&LGRPOT_MASK) != LGRPOT_REAL_SIZE){
			ar_assert((below_flags(spr->p)&POT_SPROUT)>0,"issue.\n");
		}
		spr->bits>>=1;
		below_flags(abpot(p)) &= ~POT_SPROUT;
		/*
			NOTE: as we are removing this POT from the sprout, otherwise it wont be able to reattach itself if
			this flag is not present(making sprouting usless).
			the only flag that would need to be removed is from the pot above us.
		*/
		ar_assert((below_flags(p)&POT_SPROUT)>0,"why does this not exist?.\n");
	}else{
		ar_assert((_64u_(spr->p)&LGRPOT_MASK) == LGRPOT_REAL_SIZE,"malfunction.\n");
		/*
			this is greater pot aligned and the start but also now the end of this.
		*/
		anr->spr_next++;
		potp up;
		up = abpot(spr->p);
		below_flags(up) &= ~POT_SPROUT;
		p = spr->p;
		ar_assert(spr->bits == 1,"how could this happen?.\n");
		ar_assert((p->flags&POT_INTERFERE)>0,"expected flag that dosent exist from base?.\n");
		/*
			only the pot at the base of the sprout should not have this flag 
		*/
		ar_assert(!(below_flags(p)&POT_SPROUT),"why does this exist here?.\n");
	}
	ar_assert((p->flags&POT_INTERFERE)>0,"expected flag that dosent exist?.\n");
	p->flags &= ~POT_INTERFERE;
	ar_assert(!p->off,"so this pot is being used, question is how did it end up here?, offset: %u.\n",p->off);
_retval:
	mt_unlock(&abt->apd.lock);
	return p;		
}

void static pot_init_check(potp pot){
	ar_assert(!pot->off,"offset of pot is not zero, got(%x)\n",pot->off);
}

potp static acm_salvage(struct accommodation *acm){
	potp p;
	_int_u i;
	i = 0;
	for(;i != 16;){
		i++;
		p = pot_salvage(acm,(i&(16-1)));
		if(p != NULL)break;
	}
	return p;
}

potp static foreign_salvage(struct accommodation *skip){
	potp p;
	p = NULL;
	mt_lock(&_ar_ms.arena_alloc_lock);
	_32_u i;
	i = 8;
	while(i != _ar_ms.arena_next && !p){
		i--;
		struct accommodation *ac;
		ac = _ar_ms.acm+i;
		if(ac != skip){
			p = acm_salvage(ac);
			atmq_inc(&ac->outlanders);
		}
	}
	mt_unlock(&_ar_ms.arena_alloc_lock);
	return p;
}

/*
	greater pot signing ceremony
*/
potp pot_alloc(struct arena *anr) {
	_int_u current = _ar_ms.pot_alloced-_ar_ms.pot_freed;
	_ar_ms.pot_peak = current>_ar_ms.pot_peak?current:_ar_ms.pot_peak;
	/*
		all pots except the 'main_pot' are children of the 'main_pot'
		NO lock required unless interacting with adjcent pots.
	*/
	_8_s should_clear = -1;
	potp p;
	void *ptr;
	p = acm_salvage(anr->acm);
	if(!p){
		p = foreign_salvage(anr->acm);
	}

	if(p != NULL){
		pot_init_check(p);//debug
		AR_MESG(ARMSG_LVK|ARMSG_POT,"salvaged POT.\n");
		pot_newtag(p);
		pot_curtag(p) = PT_FAC0_LESSER_BIRTH|PT_FAC1_LESSER_BIRTH_SALVAGE;
		/*
			this function should not affect anything like adjcent pots.
			if this flag is present it means its not a recirculated pot.
		*/
//		if(pot->flags&POT_INCHOATE){
			/*
				NOTE: this routene is time consuming(!AVOID if can)
			*/
			init_pot(p);
			p->flags &= ~POT_INCHOATE;
//		}
		return p;
	}else{
		anr->lpsal_miss++;
		if(anr->acm->bcs_n<BOOKCASE_SIZE){
			p = take_from_bookcase(anr->acm);
			if(p != NULL){
				init_pot(p);
				return p;
			}
		}
		/*
			this is where we try to make greater pots from lesser pots.
			they are incomplete, however if all fails take from here. 
		*/
		if(anr->spr_next<SPROUT_MAX){	
			p = pta_using_spr(anr);
			if(p != NULL){
				pot_newtag(p);
				pot_curtag(p) = PT_FAC0_LESSER_BIRTH|PT_FAC1_LESSER_BIRTH_SPROUT;
				return p;
			}
		}else
		/*
			take from greater pots
		*/
		if(anr->stg_n>0 &&!1){
			AR_MESG(ARMSG_LVK|ARMSG_POT,"nothing left to salvage, however spare greater pots exist. proceeding to decompose greater pot.\n");
			potp pt;
			/*
				NOTE: in the event that its decomposed it may still find its way back into being a greater pot
			*/
			pt = anr->stagnation[--anr->stg_n];
			p = _64u_(pt)-((LGRPOT_SIZE-POT_SIZE)-pt->limit);
			/*
				this pot is comming from a fresh location so,
				clearing of some things like flags needs to be done.
			*/	
			init_pot(p);
			p->flags = 0;
			p->tag_ptr = 0;
			p->valid_tags = 0;
			if(!pt->off && anr->grsp_cnt<4){
				AR_MESG(ARMSG_LVK|ARMSG_POT,"siphoning off whats required from greater pot.\n");
				anr->grs_partly[anr->grsp_cnt++] = pt;
				pt->real_flags &= ~1;
				AR_MESG(ARMSG_LVK|ARMSG_POT,"shortening greater pot down by a single lesser POT.\n");
				pt->off = POT_SIZE;
				pt->limit = POT_SIZE;
				p->underneath = LGRPOT_SIZE-POT_SIZE;
				abpot(p)->underneath = 0;
				p->flags |= POT_APART;
				pt->gr = anr->grp_free[--anr->grp_next];
				anr->grp[pt->gr] = 1;
				p->gr = pt->gr;
			}else{
				pt->real_flags = 0;
				potp up;
				up = abgpot(pt);
				up->underneath = 0;
				/*
					as we are stripping a lesser pot away from the greater pot,
					the rest of the lesser pots that make up the greater pot will have to be decomposed aswell.
				*/
				lspot_decompose(pt,LGRPOT_SIZE-POT_SIZE);
			}
			pot_newtag(p);
			pot_curtag(p) = PT_FAC0_LESSER_BIRTH|PT_FAC1_LESSER_BIRTH_GPT;
			return p;
		}

		AR_MESG(ARMSG_LVK|ARMSG_POT,"pure pot alloc.\n");
		more_lock();
		_ar_ms.pot_alloced++;
		if(!(ptr = _ar_sysmore(POT_SIZE))) {
			// err
		}

		ar_assert(!(_64u_(ptr)&POT_MASK),"not properly aligned.\n");	
		should_clear = 0;
	}
	_64_u addr = (_64_u)ptr;
	addr |= MASK_OF(POT_SIZE_SHIFT)^sizeof(struct pot);
	p = addr;
	
	/*
		more lock must be locked
	*/
	potp ab;
	ab = abpot(p);
	mt_lock(&ab->apd.lock);

	if(!should_clear){		
		/*
			BIGNOTE: no flags needs to be passed to restrict the pot from sprouting.
			this is because sprouts start from a greater pot aligned pot.
		*/
		p->underneath = 0;
		p->flags = 0;
		p->lot = AR_NULL;
		p->tag_ptr = 0;
		p->valid_tags = 0;
		p->lock = MUTEX_INIT;
		/*
			this is required as adjcent pot may try to take a peek at us
			once the end of line flags is relocated, but also when the more lock 
			is released another pot could come into existance below.
		*/
		p->apd.lock = MUTEX_LOCKED;
		p->inuse_lock = MUTEX_INIT;
		make_endpot(p);
		pot_newtag(p);
		pot_curtag(p) = PT_FAC0_LESSER_BIRTH|PT_FAC1_LESSER_BIRTH_PURE;
		
		_64_u dif;
		addr = ((_64_u)_ar_ms.end)+_ar_ms.off;
		ar_assert(!(addr&MASK_OF(POT_SIZE_SHIFT)),"missaligned.\n");
		dif = LGRPOT_SIZE-(addr&MASK_OF(LGRPOT_SIZE_SHIFT));
		dif &= MASK_OF(LGRPOT_SIZE_SHIFT);
		if(dif != 0){
			_64_u spare;
			spare = _ar_sysmore(dif);
			potp up;
			up = ab0pot(spare);
			ar_assert((below_flags(up)&POT_LAST_IN_LINE)>0,"not helping.\n");
			below_flags(up) &= ~POT_LAST_IN_LINE;
			
			potp p;
			p = spare|LGRPOT_REAL_SIZE;
			ar_assert((_64u_(p)&LGRPOT_MASK) == LGRPOT_REAL_SIZE,"fucked up alignment.\n");
			ar_assert((_64u_(more_uptr)-1) == (spare|LGRPOT_MASK),"this is so fucked.\n");
			pot_primative_init(p);
			below_flags(p) |= POT_LAST_IN_LINE;	
			/*
				NOTE:
					spare must be greater pot aligned(its required)
	
				order matters here, spaninit should be the last thing we do
			*/
			ar_assert(_64u_(p)-dif, == up,"this is no good.\n");
			pot_span(p,dif);	
			spaninit(p,dif);
		}

		ar_assert(!(_64u_(more_uptr)&MASK_OF(LGRPOT_SIZE_SHIFT)),"alignment issue.\n");	
	}
	/*
		init_pot sets flags to zero
	*/
	init_pot(p);
	p->flags &= ~POT_FREE;
	ar_printf("pot_alloc: %p\n",p);

	mt_unlock(&p->apd.lock);
	mt_unlock(&ab->apd.lock);
	if(!should_clear){
		more_unlock();
	}
	return p;
}

potp pot_alloc_guarded(struct arena *anr){
	potp p;
	p = pot_alloc(anr);	
#define POT_ALLOC_NON_ACCEPTABLE_FLAGS POT_FREE
	ar_assert(!(p->flags&POT_ALLOC_NON_ACCEPTABLE_FLAGS),"non-acceptable flags for pot allocation, pot has potentially problematic flags.\n");
	p->acm = arena_distinct;	
	p->b2 = AR_NULL;
	ar_assert(_64u_(p)<(_64u_(_ar_ms.end)+_ar_ms.off),"somthing went wrong.\n");

	ensure_nongreater(p);
	return p;
}
void lot_link(struct lot *__lot,_32_u idx){
	struct arena *anr = myarena;
	__lot->acm = TLS->arena;
	atmq_inc(&anr->lots_acum);
	struct accommodation *acm = anr->acm;
	mt_lock(&acm->free_pots_lock);
	__lot->prev_next_free = &acm->_free_pots[idx];
	__lot->next_free = acm->_free_pots[idx];
	if(acm->_free_pots[idx] != NULL)
		acm->_free_pots[idx]->prev_next_free = &__lot->next_free;
	acm->_free_pots[idx] = __lot;
	mt_unlock(&acm->free_pots_lock);
}

void lot_renounce(struct lot *__lot) {
//	ar_assert((__pot->flags&POT_FREE)>0,"for a pot to be renounced is must be free know?.\n");
	ar_assert(__lot->prev_next_free != NULL,"pot must have been renounced twice?.\n");
	struct arena *anr;
	anr = arena_at(__lot->acm);
	atmq_dec(&anr->lots_acum);
	*__lot->prev_next_free = __lot->next_free;
	if(__lot->next_free != NULL) {
		ar_assert(__lot->acm == __lot->next_free->acm,"lot chained with lot from another arena?, arena(%u), arena(%u)\n",__lot->acm,__lot->next_free->acm);
		__lot->next_free->prev_next_free = __lot->prev_next_free;
	}
	__lot->prev_next_free = NULL;
	__lot->acm = AR_NULL;
}

/* pot renounce could be foreign

	in the case we might be unlinking adjcent pots,
	not the pot we are working with.
*/

void lot_renounce_cbforeign(struct lot *__lot){
	struct accommodation *acm;
	acm = acm_at(__lot->acm);
	ar_assert(__lot->acm<8,"outside valid arena index.\n");
	mt_lock(&acm->free_pots_lock);
	lot_renounce(__lot);
	mt_unlock(&acm->free_pots_lock);
}

void lot_renounce_guarded(struct lot *__lot,struct arena *anr) {
	lot_renounce(__lot);
}


//TODO: rename

struct lot* pot_lotted(potp __pot){
	//alloc mapping
	__pot->lot = lot_alloc();
	//if we dont map then get_lot wont work
	/*
		NOTE: lot_map allocates the lot structure by but also maps it 
	*/
	lot_map(__pot->lot,lot_strc_alloc());
	struct lot *lot;
	lot = get_lot(__pot->lot);
	return lot;
}
struct lot static* sidespan(potp __pot, potp bw, potp ab);
/*
	NOTE: an important thing to notice is that mergers of lots can only take place when
	the base pot has the POT_FREE flag. this POT_FREE flag is set in the spaninit function
*/

void spaninit(potp __pot, _64_u __end){
	_32_u __lot = __pot->lot;
	potp pot;
	pot = __pot;
	_64_u i;
	i = POT_SIZE;
	for(;i != __end;i+=POT_SIZE){				
		pot = _64u_(pot)-POT_SIZE;
		pot_primative_init(pot);
		pot->flags = POT_FREE|POT_INCHOATE;
		pot->lot = __lot;
		pot->tag_ptr = 0;
		pot->valid_tags = 0;
		pot_newtag(pot);
		pot_curtag(pot) = PT_FAC0_LESSER_SPANNED|PT_FAC1_LESSER_SPANNED_AFFILIATED;
	}
	//should be last thing we do else would cause issues regarding a pot that wasent been inited as of yet.
	__pot->flags |= POT_FREE|POT_INCHOATE;
}
/*
	for now, if a lot happens to incounter a greater pot then add the lot to a greater pot bookcase,
	this allows us to grow exiting mappings of greater pots, preventing greater pots being spread out.
	what we want is greater pots to be concentrated in one place.
*/
_8_s jointventure(struct lot *lt,potp up,potp __pot,struct arena *anr){
	return -1;
	/*
	if(!(up->flags&POT_GREATER)) return -1;
	if(anr->bcs_n>0 && anr == arena_at(lt->acm)){
		lt->flags |= LOT_BOOKED;
		bookcase_add(lt,anr);
		return 0;
	}
	*/
	return -1;
}


void pot_span(potp __pot, _64_u __end){
	pot_newtag(__pot);
	potp up;
	up = _64u_(__pot)-__end;
	_32_u lot;
	struct arena *anr;
	anr = myarena;
	/*
		in either case __pot is linked to the free chain
	*/
	if(!(up->flags&POT_FREE)){
		struct lot *lt;
		lt = pot_lotted(__pot);
		lt->pot = __pot;
		if(jointventure(lt,up,__pot,anr) == -1){
			lot_link(lt,in_lpts(__end));
		}
		lt->size_in_pots = __end;
		spanlgn(__pot->lot) = __end;
		pot_curtag(__pot) = PT_FAC0_LESSER_SPANNED|PT_FAC1_LESSER_SPANNED_ALONE;
	}else{
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"upper pot appers to be free, going to conjoin areselfs with it.\n");
		/*
			we extent upwards, this means as we are below the pot we are looking at 
			its top is will no longer be valid and thus __pot becomes the new top.
		*/
		lot = up->lot;
		struct lot *lt;
		lt = get_lot(lot);
		mt_lock(&lt->lock);
		lt->size_in_pots+=__end;
		lt->pot = __pot;
		if(!(lt->flags&LOT_BOOKED)){
			lot_renounce_cbforeign(lt);
			lot_link(lt,linkidx(lt->size_in_pots));
		}
		spanlgn(lot)+= __end;
		__pot->lot = lot;
		mt_unlock(&lt->lock);
		pot_curtag(__pot) = PT_FAC0_LESSER_SPANNED|PT_FAC1_LESSER_SPANNED_CONJOINED;
	}
}
/*
	we assume going into this routuine that bw is valid
	we also assume that all the pots are locked.

	NOTE: the below lot should be valid aslong as '__pot'
	has been locked.

	NOTE: below dosent get locked, however anyone using pot'bw' lockes the pot above the lot "__pot"
*/
struct lot*
sidespan(potp __pot, potp bw, potp ab){
	struct arena *anr = myarena;
	ar_assert(not_null(bw->lot),"null lot?.\n");
	struct lot *bwl;
	bwl = get_lot(bw->lot);
	/*
	 first we attempt to obtain the lock for the lot.
	*/
	if(mt_trylock(&bwl->lock) == -1){
		/*
		 looks like we failed to obtain it, this means that someone in
		 'trim_lotted_pot' has it. by that they are waiting for us to 
		 release the lock on the '__pot'pot.

		 this means that bw is now invalid, and that we can no longer be working on it.
		 as this function requires that bw is valid.
		
		 by retuning NULL we expect the __pot lock to be released at some stage
		*/
		return NULL;
	}

	ar_assert(!bwl->issued,"lot hasent been issued(%x), pot-flags: %x.\n",bw->lot,bw->flags);
	if(!(bwl->flags&LOT_BOOKED)){	
		lot_renounce_cbforeign(bwl);
	}
	
	/*
		this means we have it another linear span of free pots
	*/
	if(ab->flags&POT_FREE && not_null(ab->lot)){
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"collided with another linear span.\n");
		potp apex;
		struct lot *abl;
		abl = get_lot(ab->lot);
		mt_lock(&abl->lock);
		ar_assert(abl->size_in_pots>0,"insufficent.\n");
		
		/*
			we do this because this pot may experance a free,
			and would look into its adjcent pots. the issue
			here is while we are updateing the tables its 
			pot->lot may resolve to the old lot structure.
			so lock it untill everthing is in place.
		*/
		apex = _64u_(ab)-abl->size_in_pots;
		mt_lock(&apex->apd.lock);
		/*
			conjoing with areselfs???
		*/
		ar_assert(ab->lot != bw->lot,"good god.\n");
		ar_assert(bwl != abl,"good god.\n");

		ar_assert(!abl->issued,"lot hasent been issued(%x).\n",ab->lot);
		_32_u ab_lot;
		ab_lot = lot_table(ab->lot);
		/*
			NOTE: down here its a complete/full merger
		*/

		/*
			this means that the above linear span is made of more then a single pot.
		
			also NOTE:
				spanlgn is not effected
		*/
		if(abl->size_in_pots>POT_SIZE){		
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"span has been found to have more then one pot, taking another approch %u-lpts.\n",in_lpts(abl->size_in_pots));
			/*
				this remaps the lookup table to another lot structure,
				this means we dont have to update the lot var in all the pots.
			*/
			_64_u
			acum = 0;
			_32_u 
			sve = lot_table(ab->lot);
			_32_u i;
			potp c;
			c = ab;
			while(acum != abl->size_in_pots){
				i = c->lot;
				if(lot_table(i) != sve){
					struct lot *u;
					u = get_lot(i);
					_ar_printf("tables dont resolve to same lot(%x, size: %u-lpts)(%x, size: %u-lpts)\n",lot_table(i),in_lpts(u->size_in_pots),lot_table(ab->lot),in_lpts(abl->size_in_pots));
					ar_abort();
				}
				lot_table(i) = lot_table(bw->lot);
				acum+=spanlgn(i);
				c = _64u_(c)-spanlgn(i);
			}
			ar_assert(acum == abl->size_in_pots,"somthing dosent addup.\n");
		}else{
			ar_assert(abl->size_in_pots == POT_SIZE,"not of size.\n");
			ar_assert(spanlgn(ab->lot) == abl->size_in_pots,"should be equal, got: %u.\n",in_lpts(spanlgn(ab->lot)));
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"span only includes a single pot.\n");	
			/*
				we dont need this.
			*/
			lot_free(ab->lot);
			/*
				if its just a single pot then its faster to do this.
			*/
			ab->lot = bw->lot;
			spanlgn(bw->lot)+=abl->size_in_pots;
		}
		bwl->size_in_pots+=abl->size_in_pots;
		/*
			if we hit somthing while going this way, then its 
			going to have a free chan linkage.
	
			so remove it.
		*/
		if((abl->flags&LOT_BOOKED)>0){
			if((bwl->flags&LOT_BOOKED)>0){
				bookcase_remove(abl);
			}else{
				bwl->flags |= LOT_BOOKED;
				bwl->bcs_idx = abl->bcs_idx;
				bwl->acm = abl->acm;
				struct accommodation *acm;
				acm = acm_at(abl->acm);
				mt_lock(&acm->bookcase_lock);
				acm->bookcase[bwl->bcs_idx] = bwl;
				mt_unlock(&acm->bookcase_lock);
			}
		}else{
			lot_renounce_cbforeign(abl);
		}
		mt_unlock(&abl->lock);
		mt_unlock(&apex->apd.lock);	
		/*
			we no longer need the lot structure for this
			as a merger is about to take place. meaning 
			they both will share the same lot structure.
		*/
		abl->size_in_pots = 0;//for-debug
		lot_strc_free(ab_lot);
	}else{
//		if((ab->flags&POT_GREATER)>0 && anr->bcs_n>0){
//			bwl->flags |= LOT_BOOKED;
//			bookcase_add(bwl,anr);
//		}
	}
_overto:
	AR_MESG(ARMSG_LVK|ARMSG_POT,"merger with pot from behind.\n");
	/*
		NOTE: pot must be assigned belows lot nothing else
	*/
	__pot->lot = bw->lot;
	bwl->size_in_pots+=POT_SIZE;
	spanlgn(__pot->lot)+=POT_SIZE;
	if(!(bwl->flags&LOT_BOOKED)){
		lot_link(bwl,linkidx(bwl->size_in_pots));
	}
	return bwl;
}

/*
	put the pot up for repossession
	we assume __pot is locked before entering this function
*/
void pot_repossession(potp __pot) {
	AR_MESG(ARMSG_LVK|ARMSG_POT,"pot repossession.\n");
	potp bw,ab;
	/*
	 this tells us that we are the end pot,
	 meaning the linear extent we have from the system
	 we are at the end of that span.
	
	 only thing we can do here if put the pot in the free chan or
	 check the above pot if its free, this would also mean 
	 that are above pot will need to be unlinked from the free chain aswell.
	*/

	/*
	 by locking here the statment of ab->apd.lock wont hold true for the pot below
	*/
	mt_lock(&__pot->apd.lock);
	/*
	 we extent upwards ^
	*/
	ab = abpot(__pot);
	mt_lock(&ab->apd.lock);		
	__pot->flags |= POT_FREE;
	//REMOVE:
	if(!(below_flags(__pot)&POT_LAST_IN_LINE)){
		/*
		 we we happen to encounter a non-standard POT,
		 in any case when a non-standard gets freed its turned back into pots.
		 so they only exist when used, meaning this is shortcut to say that 
		 POT_FREE flag dosent exist.
		*/

		//REMOVE:
		if(__pot->underneath>0){
			goto _sk2else;
		}
		/*
			NOTE: this should always be valid, if __pot has been locked then if there was an attempt to trim bw from existance it 
			will try to lock __pot, so as we have the lock this means they failed to lock(meaning no trimming until we release are lock)

			I HOPE!
		*/
		bw = blpot(__pot);
		if(bw->flags&POT_FREE && not_null(bw->lot)){
			AR_MESG(ARMSG_LVK|ARMSG_POT,"sidespanned.\n");
			struct lot *lot;
			lot = sidespan(__pot,bw,ab);
			/*
			 NOTE: in the circumstance that we get given a NULL in return, bw will be valid up till
			 the lock on '__pot' is released.
			*/
			if(!lot){
				/*
					as we returned null, this means that this wont work,
					so fallback to trying to conjoin with upper pot or create new lot.
				*/
				goto _sk2else0;
			}
			mt_unlock(&ab->apd.lock);
			mt_unlock(&__pot->apd.lock);
			mt_unlock(&lot->lock);
			return;
		}else{
		_sk2else0:
			/*
			 here we are dealing with only the above pot, so theres no need to keep this locked.
			*/
//			mt_unlock(&bw->lock);

		_sk2else:
			/*
			 if the above pot is free then conjoin are selfs to them
			*/
			if((ab->flags&POT_FREE)>0 && not_null(ab->lot)) {
				struct lot *abl;
				abl = get_lot(ab->lot);
				mt_lock(&abl->lock);
				ar_assert(abl->size_in_pots>0,"fuck me.\n");
				__pot->lot = ab->lot;
				_ar_printf("newnew: %u.\n",in_lpts(abl->size_in_pots));
				abl->size_in_pots+=POT_SIZE;
				spanlgn(ab->lot)+=POT_SIZE;
				/*
					either way this is the new base
				*/
				abl->pot = __pot;
				if(!(abl->flags&LOT_BOOKED)){
					/*
						TODO:
							add some sort of transission space.
							some threshhold?
					*/
					lot_renounce_cbforeign(abl);
					lot_link(abl,linkidx(abl->size_in_pots));
				}
				AR_MESG(ARMSG_LVK|ARMSG_POT,"merger with pot at the front, size_in_pots: %u-lpts.\n",abl->size_in_pots>>POT_SIZE_SHIFT);
				mt_unlock(&abl->lock);
				mt_unlock(&ab->apd.lock);
				mt_unlock(&__pot->apd.lock);
				return;
			}else{
				AR_MESG(ARMSG_LVK|ARMSG_POT,"below or above pot isent free, this makes conjoining not possible.\n");
			}
		}
	}else{
		/*
		 not this will never come to be
		*/
		AR_MESG(ARMSG_LVK|ARMSG_POT,"pot is last in line, %p.\n",__pot);
	}

	struct lot *lt;
	lt = pot_lotted(__pot);
	lt->pot = __pot;
	if(jointventure(lt,ab,__pot,myarena) == -1){
		lot_link(lt,1);
	}
	lt->size_in_pots = POT_SIZE;
	spanlgn(__pot->lot) = POT_SIZE;
	mt_unlock(&ab->apd.lock);
	mt_unlock(&__pot->apd.lock);
}

void static
free_lot_resources(potp c,_64_u __size){
	_64_u acum;
	acum = 0;
	_32_u lot;
	_64_u lgn;
_again:
	lot = c->lot;
	lgn = spanlgn(lot);
	acum+=lgn;

	if(acum<=__size){
		lot_free(lot);
		if(acum != __size){
			c = _64u_(c)-lgn;
			goto _again;
		}
	}

	_64_u left;
	left = acum-__size;
	if(left>0){
		spanlgn(lot) = left;
	}
}

void static sanity_check_lotted(potp __pot, struct lot *lot){
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"doing sanity check.\n");
	_64_u i;
	i = POT_SIZE;
	_64_u acum;
	acum = spanlgn(__pot->lot);
	_32_u lt = __pot->lot;
	for(;i != lot->size_in_pots;i+=POT_SIZE){
		potp p;
		p = _64u_(__pot)-i;
		ar_assert(p->flags&POT_FREE,"how has a used pot ended up in a free linear span?, %u(%u)\n",in_lpts(i),in_lpts(lot->size_in_pots));
		if(lt != p->lot){
			acum+=spanlgn(p->lot);	
			lt = p->lot;
		}
	}
	ar_assert(acum == lot->size_in_pots,"not identical.\n");
}

/*
 we assume that __pot is the lowest pot

and that:
 __pot has been locked in advanced

TODO:
	we need to tell the pot above the pot_lotted span that we got rid of it


	isolate the lot from its surroundings,
	make sure that its surroundings cant interact with it.
	in this case the pot above this lot that not aprt of this lot.
*/
struct lot static* isolate_lot(potp __pot, potp *_up){
	struct lot *lot;
	lot = get_lot(__pot->lot);
	ar_assert(lot->pot == __pot,"issue.\n");
	/*
	 we lock the span, so that nothing is added or removed from it while we are working on it.
	*/
	mt_lock(&lot->lock);
	/*
	 first we lock the lot, next once we have the lock we lock the
	 only place where the locking of are lock would occur.

	 by locking the lot we make sure that size_in_pots wont change,
	 else we would end up in a cat and mouse chase where by the pot
	 above the span is moving about and changing its location to the above of above.
	*/
	potp up;
	up = _64u_(__pot)-lot->size_in_pots;
	*_up = up;
	/*
	 once we have this lock this means that any action taken on this pot that involves
	 checking the lot of its below pot wont occur. and then once the lot span and its pots 
	 are removed it will have the last_in_line flags detering it from checking its below.
	*/
	mt_lock(&up->apd.lock);
	return lot;
}
/*
	__pot should be locked in advance, this is to stop
	further and complete removal of span lot

	we expect morelock to be locked
*/
void trim_lotted_pot(potp __pot){
	struct arena *anr;
	anr = myarena;
	ar_assert(not_null(__pot->lot),"free pot with no lot?\n");
	_64_u trim;
	potp up;
	potp top;
	struct lot *lot;
	lot = isolate_lot(__pot,&up);
	ar_assert(lot->size_in_pots>0,"lot size with zero?.\n");
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"lot size: %u-lpts.\n",in_lpts(lot->size_in_pots));	
	/*
		we align to greater pot size
	*/
	trim = lot->size_in_pots&~LGRPOT_MASK;
	_ar_printf("trimming: %u, ~%u.\n",in_lpts(trim),in_lpts(lot->size_in_pots));
	if(!trim){
		mt_unlock(&__pot->apd.lock);
		mt_unlock(&up->apd.lock);
		mt_unlock(&lot->lock);
		return;
	}

	/*
	 we know this pot is going to be freed but how about the pot over there?
	*/
	sanity_check_lotted(__pot,lot);

	/*
		in the case the lot size is missaligned,
		top will be the new base of this lot
	*/
	top = _64u_(__pot)-trim;
	if(!(lot->flags&LOT_BOOKED)){
		lot_renounce_cbforeign(lot);
	}
	
	below_flags(top) |= POT_LAST_IN_LINE;
	_32_u ls;
	ls = lot_table(__pot->lot);
	/*
		any resources for the lot
	*/
	free_lot_resources(__pot,trim);	
	
	if(trim != lot->size_in_pots){
		ar_assert(lot->size_in_pots>trim,"fuck.\n");
		lot->size_in_pots-=trim;
		lot->pot = top;
		sanity_check_lotted(top,lot);
		if(!(lot->flags&LOT_BOOKED)){
			lot_link(lot,linkidx(lot->size_in_pots));
		}
		mt_unlock(&lot->lock);
	}else{/*this means the lot was depleted*/
		if((lot->flags&LOT_BOOKED)>0){
			bookcase_remove(lot);
		}
		mt_unlock(&lot->lock);
		/*
			free up lot structure involved
		*/
		lot_strc_free(ls);
	}
	/*
		NOTE: we dont need to unlock __pot
	*/
	mt_unlock(&up->apd.lock);
	
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"trimming %u-lpts\n",in_lpts(trim));
	_ar_systrim(trim);
	ar_assert(top == ab0pot(more_uptr),"not the same pot?. overshoot by: %u, %u\n",in_lpts(_ar_ms.off-1),in_lpts(_64u_(__pot)-_64u_(_ar_ms.end)));
	ar_assert(_64u_(__pot)>_64u_(more_uptr),"somthings gone wrong, %u, %u\n",in_lpts(_64u_(__pot)-_64u_(_ar_ms.end)),in_lpts(_ar_ms.off));
}
/*
	NOTE:
		__pot has no lot linkage here.
	look im thinking long goals here,
	spliting functions like this can have side affects on performance,
	however it wouldent be much of an issue if we could tweek the return address to skip
	some instruction.

	routineB(){
		if(condition){
			return_address+=1;
			return;
		}
		return;
	}

	routineA{

		routineB()
		return;
	}

	it also makes debug easier having things in discrete functions.
*/

_8_s static pot_sprout(potp __pot,struct arena *anr){
	ar_assert((_64u_(__pot)&LGRPOT_MASK) != LGRPOT_REAL_SIZE,"this cant be done.\n");
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"sprout.\n");
	struct sprout *spr;
	struct arena *owner;
	mt_lock(&__pot->apd.lock);
	/*
		in this case the pot was retracted
	*/
	if(!(below_flags(__pot)&POT_SPROUT)){
		mt_unlock(&__pot->apd.lock);
		return -1;
	}
	spr = get_sprout(__pot);
	/*
		once we have this lock, pot removal from sprouts wont go ahead
	*/
	if(mt_trylock(&spr->lock) == -1){
		mt_unlock(&__pot->apd.lock);
		return -1;
	}
	owner = arena_at(spr->acm);
	mt_unlock(&__pot->apd.lock);

	ar_assert(spr->bits>0,"no start point.\n");
	ar_assert(spr->bits<=64,"what is happening here?, %u\n",spr->bits);
	ar_assert(spr->bits != 0,"why?.\n");
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"below gr: %u\n",spr->bits);
	if(spr->bits&64){
		mt_lock(&owner->spr_lock);
		/*
			in this case we have enough greater pots, so proceed to step over this all and just treat as ordinary pot_free.
		*/
		if(!owner->spr_cnt){
			mt_unlock(&spr->lock);
			mt_unlock(&owner->spr_lock);
			return -1;
		}
	
		owner->spr_complete[--owner->spr_cnt] = spr;
		spr->finalize = owner->spr_cnt;
		spr->p = __pot;
		spr->bits<<=1;

		mt_unlock(&owner->spr_lock);
		AR_MESG(ARMSG_LVK|ARMSG_POT,"completed new greater pot-%u spr-%u.\n",owner->spr_cnt,spr->finalize);
		//	NOTE: no removal of POT_SPROUT flags required as
		//	the greater aligned pot dosent have it.		
		goto _r0;
	}
	spr->bits<<=1;
	spr->p = __pot;
	potp ab;
	ab = abpot(__pot);
	below_flags(ab) |= POT_SPROUT;
_r0:
	__pot->flags |= POT_INTERFERE;
	/*
		the meaning by why the lock isent set in pot_free is because of this here,
	*/
	mt_unlock(&spr->lock);
	return 0;
}

void pot_free(potp __pot) {
	ar_assert(__pot->acm == TLS->arena,"what?.\n");
	pot_newtag(__pot);
	ar_assert(!(__pot->flags&POT_FREE),"to free a pot with a free flag?.\n");
	ar_assert(!__pot->off,"the pot seems to be in use.\n");
	
	ensure_nongreater(__pot);

	struct arena *anr;
	anr = myarena;
	potp *chasm = anr->largest_chasm+(__pot->off>>(POT_SIZE_SHIFT-3));
	if(*chasm == __pot){
		*chasm = NULL;
	}

	_ar_ms.pot_freed++;
	AR_MESG(ARMSG_LVK|ARMSG_POT,"freeing POT.\n");
	/*
		this means that the pot we are working on is greater pot aligned
	*/
	if(below_flags(__pot)&POT_SPROUT){
		if(!pot_sprout(__pot,anr)){
			pot_curtag(__pot) = PT_FAC0_LESSER_FATE|PT_FAC1_LESSER_DEATH_SPROUT;
			return;
		}
	}else
	//REMOVE:
	if((_64u_(__pot)&LGRPOT_MASK) == LGRPOT_REAL_SIZE && anr->spr_next>0 && !(__pot->flags&POT_GTRESTRICT)){
		AR_MESG(ARMSG_LVK|ARMSG_POT,"pot was found to be lying on a greater pot boundary.\n");	
		ar_assert(anr->spr_next<=SPROUT_MAX,"what?.\n");	
		struct sprout *spr;
		spr = get_sprout(__pot);
		anr->spr_inuse[spr->spr_idx = --anr->spr_next] = spr;
		spr->lock = MUTEX_INIT;
		spr->bits = 1;
		spr->p = __pot;
		spr->acm = arena_distinct;
		__pot->flags |= POT_INTERFERE;
		pot_curtag(__pot) = PT_FAC0_LESSER_FATE|PT_FAC1_LESSER_DEATH_SPROUT;
		/*
			NOTE:
				this should be keept as the last thing we do.
		*/
		potp ab;
		ab = abpot(__pot);
		below_flags(ab) |= POT_SPROUT;
		return;
	}
_stepover:
	/*
		the pot has been marked, this means its fragment of another.
	*/
	if(__pot->flags&POT_APART){
		/*
			if zero then this tells us that the greater pot was decomposed
		*/
		if(anr->grp[__pot->gr]){
			__pot->flags |= POT_TAKE;
			pot_curtag(__pot) = PT_FAC0_LESSER_FATE|PT_FAC1_LESSER_DEATH_REJOIN;
			return;
		}
		__pot->flags &= ~POT_APART;
		ar_assert(anr->grp_next<64,"issue.\n");
		anr->grp_free[anr->grp_next++] = __pot->gr;
	}
	more_lock();
	potp top;

	top = (_64u_(more_uptr)-1)^sizeof(struct pot);
	ar_assert(!(_64u_(more_uptr)&LGRPOT_MASK),"misalignment.\n");
	ar_assert((below_flags(top)&POT_LAST_IN_LINE)>0,"fuck no.\n");

	pot_curtag(__pot) = PT_FAC0_LESSER_FATE|PT_FAC1_LESSER_DEATH_REPOSSESSED;	
	pot_repossession(__pot);
	mt_lock(&top->apd.lock);
	if(top->flags&POT_FREE){
		trim_lotted_pot(top);
	}else{
		mt_unlock(&top->apd.lock);
	}
	more_unlock();
}

void pot_free_guarded(potp p){
	pot_free(p);	
}

balep newbale(potp __pot,_64_u __bc) {
	largest_chasm_rm(__pot);
	ar_uint_t size = align_to(bale_size+__bc, ALIGN);
	ar_off_t top = __pot->off+size;
	/*
		we cant go over this
	*/
	if (top>POT_REAL_SIZE) {
		return NULL;
	}
	
	balep blk;
	*(blk = (balep)off2ptr(__pot,__pot->off)) = (struct bale) {
		.prev = __pot->end_bale, .next = AR_NULL,
		.size = size-bale_size,
		.fd = AR_NULL, .bk = AR_NULL,
		.flags = BALE_USED
	};
	
	/*
		FIXME:
		if the pot is depleted completly this means end_bale would by the decouple routine set to ZERO
		a better way of doing this needs to be found.
	*/
	if(!__pot->off){
		blk->prev = AR_NULL;
	}

	if(__pot->off != bale_offset(blk)) {
		ar_printf("missalignment.\n");
		ar_abort();
	}
	blk->pad = blk->size-__bc;

	if (not_null(__pot->end_bale))
		get_bale(__pot, __pot->end_bale)->next = __pot->off;

	__pot->end_bale = __pot->off;
	__pot->off = top;

	__pot->blk_c++;
	largest_chasm(__pot);
	return blk;
}

balep static splice_bale(potp __pot,balep blk, _64_u __bc, _64_u junk, _64_u register __mask) {	
	__pot->blk_c++;
	balep p;
	p = ((_8_u*)blk)+bale_size+__bc;
	_64_u boff = balem_offset(blk,__mask);
	ar_off_t off = boff+bale_size+__bc;
	*p = (struct bale){
		.prev = boff, .next = blk->next,
		.size = junk-bale_size,
		.fd = AR_NULL, .bk = AR_NULL,
		.flags = BALE_USED|(blk->flags&BALE_SHAM),
		.pad = 0
	};

	if(off != balem_offset(p,__mask)) {
		ar_printf("misalignment.\n");
		ar_abort();
	}

	blk->pad = 0;
	blk->size = __bc;
	blk->next = balem_offset(p,__mask);
	if (balem_end(p,__mask) == __pot->off){
		ar_assert(is_null(p->next),"for a bale to be the end_bale its next should be NULL(%x).\n",p->next);
		__pot->end_bale = balem_offset(p,__mask);
	}
	if (not_null(p->next))
		get_balem(__pot, p->next,__mask)->prev = balem_offset(p,__mask);
	return p;
}

balep refurbish_bale(potp __pot,balep blk,_64_u __bc){
	detatch(__pot, blk);
	_64_u boff = bale_offset(blk);
	blk->pad = blk->size-__bc;
	blk->flags = (blk->flags&~(BALE_LINKED))|BALE_USED;
	__pot->buried-=blk->size;
	/*
	* if bale exceeds size then trim it down and split the bale into two parts.
	*/
	ar_uint_t junk;
	if((junk = (blk->size-__bc)) > bale_size+TRIM_MIN){
		ar_printf("bale has been considered overly large, so it will be spliced into two parts{%u-bytes, and the other of %u-bytes}.\n",__bc,junk);
		balep p;
		p = ((_8_u*)blk)+bale_size+__bc;
		if(blk->flags&BALE_SHAM){
			while(1);
			/*
			*p = (struct bale){
				.prev = AR_NULL,.next = AR_NULL,
				.size = junk-bale_size,
				.fd = AR_NULL,.bk = AR_NULL,
				.flags = BALE_USED|(blk->flags&BALE_SHAM),
				.pad = 0
			};
	
			auctionoff(__pot,p);
		*/
		}else{
			balep p;
			p = splice_bale(__pot,blk,__bc,junk,MASK_OF(POT_SIZE_SHIFT));
			_ar_bfree(__pot,p);
		}
	}
	return blk;
}

balep static attempt_to_snare(potp __pot,balep blk,_64_u __bc){
	balep rr, ft;

	rr = bale_prev(blk);
	ft = bale_next(blk);

	_int_u h;
	_8_u flags;
			
	flags = 0x0;
	h = 0;
	if (not_null(blk->prev)) {
		if (is_free(rr)) {
			h+=rr->size+bale_size;
			flags |= 0x1;
		}
	}
	if (not_null(blk->next)) {
		if (is_free(ft)) {
			h+=ft->size+bale_size;
			flags |= 0x2;
		}
	}

	/*
		TODO:
			if oversized shrink but only if large enough
	*/
	if (blk->size+h>=__bc) {
		detatch(__pot, blk);
		__pot->buried-=blk->size;

		if (flags&0x1) {
			__pot->blk_c--;
			__pot->buried-=rr->size;
			if (is_flag(rr->flags, BALE_LINKED))
				detatch(__pot, rr);
			decouple(__pot, blk);
			rr->size+=blk->size+bale_size;
			rr->next = blk->next;
			blk = rr;
		}

		if (flags&0x2) {
			if (is_flag(ft->flags, BALE_LINKED))
				detatch(__pot, ft);
			__pot->blk_c--;
			__pot->buried-=ft->size;
			decouple(__pot, ft);
			blk->size+=ft->size+bale_size;
			blk->next = ft->next;
		}

		blk->pad = blk->size-__bc;
		blk->flags = (blk->flags&~(BALE_LINKED))|BALE_USED; 
		return blk;
	}
	return NULL;
}

/*
	this function as well as others like it expect that POT is locked before hand,
	this is to avoid, locking issue regarding a _ar_bfree within a _ar_balloc.

	example

	#bfree
	lock-mutex

	unlock-mutex


	#balloc
	lock-mutex

	call bfree

	unlock-mutex

	the above setup would fail, and get stuck at bfree(lock-mutex)

	so we use


	#bfree
	.
	.

	#balloc
	call bfree

	#balloc_guarded
	lock-mutex
	call balloc
	unlock-mutex
*/

balep static try_ubins(potp __pot, _int_u __bc){
	/*
		this makes sure the loop below stays true
	*/
	if((__bc&0xf)>0)return NULL;
	_int_u bix,bx,k;
	bix = ubin_indx(__bc);
	bx = k = ubin_bindx(__bc);

	if((__pot->ubin_bits&((~_64u_(0))<<bx))>0){
		bx = 63;
	_next:
		if((__pot->u_binsdwn[bx]&((~_64u_(0))<<bix))>0){
			balep p;
			AR_BALE_OFFSET vb;
			vb = __pot->ubins[ubin_aindx(bx,__bc)];
			p = get_bale(__pot,vb);
		_again:
			ar_printf("ubin-check: (%u) >= (%u).\n",p->size,__bc);
			if(p->size>=__bc){
				/*
					NOTE: if bale happens to have a ubin linkage then 
					refurbish_bale will remove that.
				*/
				return refurbish_bale(__pot,p,__bc);
			}	
			ar_assert(not_null(p->fd), "shouldent be null.\n");
			p = get_bale(p,p->fd);
			goto _again;
		}else{
			if(bx != k){
				bx--;
				goto _next;
			}
		}
	}
	return NULL;
}

void static try_place_in_ubin(potp __pot, balep __b){
	_int_u bix,bx;
	bix = ubin_indx(__b->size);
	bx = ubin_bindx(__b->size);
	if(meets_ubin_criteria(__pot,bix,bx)){
		_ar_ms.ubin_snaffles++;
		detatch(__pot,__b);
		__pot->u_binsdwn[bx] |= _64u_(1)<<bix;
		place_in_ubin(__pot,__b,bale_offset(__b),bx);
		return;
	}
	_ar_ms.ubin_scrubed_snaffles++;
}

balep _ar_balloc_guarded(potp __pot,_int_u __bc){
	if(__pot->acm != TLS->arena){
		print_pot_info(__pot,"#######");
	}

	ar_assert(__pot->acm == TLS->arena,"wrong arena, we are arena(%u), however the pot is owned by arena(%u), in %s\n",TLS->arena,__pot->acm, !myarena->stamp?"unknown":myarena->stamp->str);
	balep rt;
	lkpot(__pot);
	rt = _ar_balloc(__pot,__bc);
	ulpot(__pot);
	return rt;
}

balep
_ar_balloc(potp __pot, _int_u __bc) {
	ar_assert(!(__pot->flags&POT_FREE),"how did we end of here?.\n");
	ar_assert(!(__pot->flags&POT_INTERFERE), "why is this flag here?.\n");
	void *r = NULL;

	r = try_ubins(__pot,__bc);
	if(r != NULL){
		atmq_inc(&_ar_ms.ubin_hits);
		return r;
	}else{
		atmq_inc(&_ar_ms.ubin_misses);
	}
	_int_u bn;
	bn = bin_no(__bc);
	ar_off_t *bin;
	bin = bin_for(__pot,bn);
	if(__pot->bins[bn] == AR_NULL){
		_ar_ms.bin_deadends_by_bin[bn]++;
	}
	ar_off_t **endbin = __pot->bins+(no_bins-1);
	_int_u steps;
	_64_u x;
	_int_u nbsk = 0;
	ar_printf("BINMAP: %x, %x, %x, %x\n",((_32_u*)__pot->binmap)[0],((_32_u*)__pot->binmap)[1],((_32_u*)__pot->binmap)[2],((_32_u*)__pot->binmap)[3]);
_bk:
	x = __pot->binmap[bn>>3];
	if(!(x>>(bn&0x7))){
		_int_u k;
		k = 8-(bn&0x7);
		bin+=k;
		bn+=k;
		if(bin >= endbin) {
			goto _sk;
		}
		nbsk++;
		goto _bk;
	}
	
	while(!(x&(((_64_u)1)<<(bn&0x7)))){
		if(bin == endbin) {
			goto _sk;
		}
		bin++;
		bn++;
	}

	ar_assert(*bin != AR_NULL,"seems to be an issue.\n");
	
	steps = 0;
	ar_off_t cur = *bin;
	while(not_null(cur)) {
		/*
			TODO:
				for both refurbish and snare try and shave some off if its too large.
		*/
		balep blk;
		blk = get_bale(__pot, cur);
		try_sizesort(__pot,blk);
		if(blk->size>=__bc){
			/*
				this means a bale of size equal or greater then the needed size has been met,
				to take the bale and turn it into a new bale we can use.
			*/
			ar_printf("took %u-steps to find suitable host-bale, and nskiped: %u.\n",steps,nbsk);
			_ar_ms.bin_hits++;
			_ar_ms.bin_hits_by_bin[bn]++;
			return refurbish_bale(__pot,blk,__bc);			
		}
#ifndef barebone
		/*
			here we are going to attmpt to snare the adjacent,
			try to siphon off space from them or completely take possession of.
		*/
		balep t;
		t = attempt_to_snare(__pot,blk,__bc);
		if(t != NULL)
			return t;
#endif
		steps++;
		balep sve;
		cur = (sve = blk)->fd;
		/*
			while we are wasting are time looking for the thing we want,
			try to refill the ubins if bale meets ubin criteria
		*/

		try_place_in_ubin(__pot,sve);
	}

	_ar_ms.bin_misses_by_bin[bn]++;
	_ar_ms.bin_misses++;
	// for now
	if(bin != endbin){
		bin++;
		bn++;
		goto _bk;
	}
_sk:
	r = newbale(__pot,__bc);
	if(!r){
		_ar_ms.pot_misses++;
	}else{
		_ar_ms.pot_hits++;
	}
	return r;
}

/*
	this is very crude
	REMOVE:
*/
balep static locate_bale_crude(potp __pot,_64_u __bc){
	_int_u bn;
	bn = bin_no(__bc);
	ar_off_t *bin;
	bin = bin_for(__pot,bn);

	ar_off_t cur = *bin;
	while(not_null(cur)) {
		balep blk;
		blk = get_bale(__pot, cur);
		if(blk->size>=__bc){
			return blk;
		}
		cur = blk->fd;
	}
}
void static lsplace_in_bin(potp __pot, balep blk){
	blk->flags |= BALE_BINNED;
	_int_u bn;
	_int_u boff = lsbale_offset(blk);
	bn = lsbin_no(blk->size);
	blk->fd = __pot->bins[bn];
	blk->bk = AR_NULL;
	if(__pot->bins[bn] != AR_NULL){
		balep b;
		b = get_lsbale(__pot,__pot->bins[bn]);
		b->bk = boff;
	}
	__pot->bins[bn] = boff;
}
#define UN(__size) (1<<((__size>>6)&0x3f))

void static lsdetatch(potp __pot, balep blk) {
	_int_u bn;
	bn = lsbin_no(blk->size);
	
	ar_assert(lsconsistency(__pot,blk), "consistency issue: %x -- %x\n",__pot,blk);
	if(not_null(blk->front)){
		balep ft;
		ft = get_lsbale(__pot,blk->front);
		ar_assert(lsconsistency(__pot,ft), "consistency issue: %x -- %x\n",__pot,ft);
		ar_assert(blk->front != lsbale_offset(blk));
		if(blk->flags&BALE_BINNED){
			blk->other ^= UN(blk->size);
			/*
				this tells us there is some sort of linkage
			*/
			lsplace_in_bin(__pot,ft);
			if(blk->front != blk->rear){
				balep rr;
				rr = get_lsbale(__pot,blk->rear);
				rr->front = blk->front;
				ft->rear = blk->rear;
				ft->other = blk->other^ft->other;
			}else{
				/*
					this says to us that there are only two bales in this link
					NOTE: by removing its buddy re remove the gnarl from it
				*/
				ft->front = AR_NULL;
				ft->rear = AR_NULL;
				ft->other = 0;
			}
			blk->rear = AR_NULL;
			blk->front = AR_NULL;
			goto _sk;
		}

		/*
			the approch here is to propagate, but also
			to tell are neighbors of are exit.
		
			we collect this when we have the time to do so.
		*/
		if(ft->flags&BALE_BINNED) {
			/*
				this bale is going to be removed, so. 
			*/
			ft->other ^= UN(blk->size)|blk->other;
		}else{
			/*
				pass to neighbors its removal
			*/
			ft->other |= UN(blk->size)|blk->other;
		}

		if(blk->front == blk->rear){
			ar_assert(lsconsistency(__pot,ft), "consistency issue: %x -- %x\n",__pot,ft);
			ft->front = AR_NULL;
			ft->rear = AR_NULL;
		}else{
			balep rr;
			rr = get_lsbale(__pot,blk->rear);
			rr->front = blk->front;
			ft->rear = blk->rear;
		}
		return;
	}
_sk:
	blk->flags &= ~BALE_BINNED;
	if(is_null(blk->bk)){
		ar_assert(lsbale_offset(blk) == __pot->bins[bn],"bale free bin not in check, front: %x, rear: %x, fd: %x, bk: %x\n",blk->front,blk->rear,blk->fd,blk->bk);
		if(blk->fd == AR_NULL){
			__pot->bins[bn] = AR_NULL;
		}else{
			balep b;
			b = get_lsbale(__pot,blk->fd);
			b->bk = AR_NULL;
			__pot->bins[bn] = blk->fd;
		}
		return;
	}

	if(is_null(blk->fd)) {
		balep b;
		b = get_lsbale(__pot,blk->bk);
		b->fd = AR_NULL;
		return;
	}

	balep ft,rr;
	ft = get_lsbale(__pot,blk->fd);
	rr = get_lsbale(__pot,blk->bk);

	rr->fd = blk->fd;
	ft->bk = blk->bk;
}
/*
	im as an impass with this, i need two allocators.
	but i dont like copy-past too much but there no way around it
	realy!
*/
void _ar_sbfree(potp __pot, balep blk){
	if(is_null(blk->next)){
		ar_assert(lsbale_offset(blk) == __pot->end_bale,"well is that so?.\n");
	}

	lsdecouple(__pot,blk);

	balep prev, next, top = NULL, end = NULL;
	/*
		attempt to join the prevous bale and this one together.
	*/
	if (not_null(blk->prev)) {
		prev = lsbale_prev(blk);
		/*
			walk this path until no free bale is found.
			keeping joing.
		*/
		while(is_free(prev)) {
			__pot->blk_c--;
			__pot->buried-=prev->size;
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"found free space above, %u\n", prev->size)
			if (is_flag(prev->flags, BALE_LINKED))
				lsdetatch(__pot,prev);
			lsdecouple(__pot, prev);
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"total freed: %u\n", lsbale_end(prev)-lsbale_offset(prev))
			top = prev;
			if (is_null(prev->prev)) break;
			prev = lsbale_prev(prev);
		}
	}

	/*
		attempt to join the next bale and this one together.
	*/
	if (not_null(blk->next)) {
		next = lsbale_next(blk);
		while(is_free(next)) {
			__pot->blk_c--;
			__pot->buried-=next->size;
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"found free space below, %u\n", next->size)
			if (is_flag(next->flags, BALE_LINKED))
				lsdetatch(__pot,next);
			lsdecouple(__pot, next);
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"total freed: %u\n", lsbale_end(next)-lsbale_offset(next))
			end = next;
			if (is_null(next->next)) break;
			next = lsbale_next(next);
		}
	}

	if (top != NULL) {
		top->size = (lsbale_end(blk)-lsbale_offset(top))-bale_size;
		top->next = blk->next;
		blk = top;
	}

	if (end != NULL) {
		blk->size = (lsbale_end(end)-lsbale_offset(blk))-bale_size;
		blk->next = end->next;
	}

	blk->front = AR_NULL;
	blk->rear = AR_NULL;
	/*
		NOTE: we dont have to touch 'pad' as its only ever used when we have allocated memory
	*/ 
	if(lsbale_end(blk) == __pot->off) {
		__pot->blk_c--;
		ar_printf("bale is at the far side of the POT, meaning we can retract the bale, setting POT-offset to %u from %u (-%u)-byte reduction.\n",lsbale_offset(blk),__pot->off,(__pot->off-lsbale_offset(blk)));
		__pot->off = lsbale_offset(blk);
		return;
	}else{
		ar_assert(not_null(blk->next),"thats weird? the bale is the end bale but the pot offset isent the end of the bale?.\n");
	}

	__pot->buried+=blk->size;

	lsrecouple(__pot,blk);
	blk->flags = (blk->flags&~BALE_USED)|BALE_LINKED;

	lsplace_in_bin(__pot,blk);
}

void static snatch_for_sb(potp __pot, balep blk, struct sieve_struc *sie){
	/*
		this tells us that it isent apart of a gnarl already
	*/
	if(not_null(blk->front)){
		return;
	}
	if(((blk->size>>12)&~511)>0)
		return;
	_int_u boff = lsbale_offset(blk);
	_int_u tn;
	_64_u un;
	un = UN(blk->size);
	tn = (blk->size>>12)&511;
	if(sie->sieve_reset>63){
		return;
	}
	sie->sieve_replace[sie->sieve_reset++] = tn;

	ar_assert(tn<512,"issue.\n");
	/*
		in no case should this be.
	*/
	ar_assert(sie->sieve_table[tn] != boff,"this should never happen.\n");
	blk->other = 0;
	if(sie->sieve_table[tn] != AR_NULL){
		_ar_ms.gnarl_formed++;
		_int_u bf = sie->sieve_table[tn];
		balep bale;
		bale = get_lsbale(__pot,bf);
		if(is_null(bale->fd) && is_null(bale->bk)){
			ar_assert(1 == 0, "your kidding right?, %x\n",bf);
		}

		/*
			avoid re-entrys
		*/
		if(bale->other&un){
			return;
		}

		lsdetatch(__pot,blk);
		blk->fd = AR_NULL;
		blk->bk = AR_NULL;
			
		if(is_null(bale->front)){
			bale->rear = boff;
			blk->front = bf;
		}else{
			balep p;
			p = get_lsbale(blk,bale->front);
			blk->front = bale->front;
			p->rear = boff;
		}

		blk->rear = bf;
		bale->front = boff;
		bale->other |= un;
		return;
	}

	blk->other = un;
	sie->sieve_table[tn] = boff;
}

void static* try_use_lsbale(potp __pot, balep blk, _64_u __bc){
	ar_assert(is_free(blk), "attempting to use bale thats not free??.\n");
	if(blk->size<__bc){
		return NULL;
	}
	ar_printf("found unused bale for larger set\n");
	lsdetatch(__pot,blk);
	blk->pad = blk->size-__bc;
	blk->flags = (blk->flags&~(BALE_LINKED))|BALE_USED;

	_64_u junk = blk->size-__bc;
	//FIXME:
	if(junk>(bale_size+POT_SIZE)) {
		balep p;
		p = splice_bale(__pot,blk,__bc,junk,MASK_OF(LGRPOT_SIZE_SHIFT));
		_ar_sbfree(__pot,p);
	}

	return bale2up(blk);		
}

/*
	im using this sieve approch to only sballoc and not lesser pots,
	this is because the larger pots have a greater variance for the lowest order bits

	what i mean by this is that.

	LESSER_POTS = allocations from 0 to 2^15

	LARGER_POTS = allocations FROM 2^15 TO 2^23 because the allocations are larger the deviation from the power.

	meaning lets say 1057 its power of 1024 and has a variance of 33 where the max variance is 1023 till the next power.
*/
void*
_ar_sballoc(potp __pot, _int_u __bc) {
	struct sieve_struc *sie;
	struct arena *anr;
	anr = myarena;
	//procure sieve;
	sie = &anr->sieve;

	/*
		we have to re NULL the non-nulls
	*/
	if(sie->sieve_reset>0){
		if(sie->sieve_reset>_ar_ms.rinse_peak){
			_ar_ms.rinse_peak = sie->sieve_reset;
		}
		atmq_inc(&_ar_ms.sieve_rinses);
		_int_u i;
		i = 0;
		for(;i != sie->sieve_reset;i++){
			sie->sieve_table[sie->sieve_replace[i]] = AR_NULL;
		}
	}
	sie->sieve_reset = 0;
	ar_assert(sie->sieve_table[0] == AR_NULL,"wtf.\n");
	_int_u bn;
	void *ret;
	bn = lsbin_no(__bc);
_anotherround:
	if(__pot->bins[bn] != AR_NULL) {
		AR_MESG(ARMSG_LVK|ARMSG_BALE,"it appears that there are free bales on the market.\n");
		balep blk;
		_int_u i = 0;
		blk = get_lsbale(__pot,__pot->bins[bn]);
		while(1){
			if((ret = try_use_lsbale(__pot,blk,__bc)) != NULL){
				return ret;
			}else{
				i++;
				if((blk->size>>12) == (__bc>>12)){
					if(blk->front != AR_NULL){
						if(blk->other&UN(__bc)) {
							balep helm = blk;
							_ar_ms.gnarl_hits++;
							_64_u rm = 0;
							_64_u un = UN(blk->size);
						_again:
								blk = get_lsbale(__pot,blk->front);
								if(blk->flags&BALE_BINNED){
									_ar_ms.gnarl_complete_cycles++;
									if((blk->other^rm) != un) {
										/*
											this should never be, i hope so anyways.
										*/
										_ar_printf("inconsistency.\n");
									}
									blk->other = un;
									goto _breakout;
								}
								rm |= blk->other;
								un |= UN(blk->size);
								blk->other = 0;
								if((ret = try_use_lsbale(__pot,blk,__bc)) != NULL){
									helm->other ^= rm;
									return ret;
								}

								_ar_ms.gyre_steps++;
								goto _again;
						}else{
							_ar_ms.gnarl_misses++;
						}
					}
				}
			}
			AR_BALE_OFFSET fd;
_breakout:
			fd = blk->fd;

			snatch_for_sb(__pot,blk,sie);
			if(is_null(fd) || i >40)
				break;
			blk = get_lsbale(__pot,fd);
		}
	}else{
		ar_printf("no free bales, POT-offset: %u.\n",__pot->off);
	}

	if(bn != 63){
		bn++;
		goto _anotherround;
	}

	ar_uint_t size = align_to(bale_size+__bc, ALIGN);
	ar_assert(size>=(bale_size+__bc),"realy?.\n");
	ar_off_t top = __pot->off+size;
	if(top>LGRPOT_REAL_SIZE){
		return NULL;
	}

	balep blk;
	*(blk = (balep)gtoff2ptr(__pot,__pot->off)) = (struct bale) {
		.prev = __pot->end_bale, .next = AR_NULL,
		.size = size-bale_size,
		.fd = AR_NULL, .bk = AR_NULL,
		.flags = BALE_USED
	};

	//PATCHUP:
	if(!__pot->off){
		blk->prev = AR_NULL;
	}

	if(__pot->off != lsbale_offset(blk)) {
		ar_printf("misaligned-bale with {%u != %u}.\n",__pot->off,lsbale_offset(blk));
		ar_abort();
	}
	blk->pad = blk->size-__bc;

	if (not_null(__pot->end_bale))
		get_lsbale(__pot, __pot->end_bale)->next = __pot->off;

	__pot->end_bale = __pot->off;
	__pot->off = top;

	__pot->blk_c++;
	return bale2up(blk);
}

/*
	shear the LSB of the address returned;
*/
void*
s_alloc(_int_u __bc, _64_u __mask) {
	_64_u ptr;
	ptr = (_64_u)m_alloc(__bc+__mask);

	/*
		TODO:
			reinduct the memory we dont use after shearing of those bits
	*/
	ptr = (ptr+(__mask-1))&~(__mask-1);
	return ptr;
}
/*
	each larger set is 33MB
*/
void static lspot_link(struct chain_struc *p,struct chain_struc **tops, _int_u __x){
	p->fd = tops[__x];
	if(tops[__x] != NULL)
		tops[__x]->bk = p;
	p->bk = NULL;
	tops[__x] = p;
}

_8_s attempt_recombine(struct arena *anr, potp p){
	potp up;
	AR_MESG(ARMSG_LVK|ARMSG_POT,"checking above pot(%u).\n",in_lpts(p->limit));
	ar_assert(p->limit>0,"what?.\n");
	up = _64u_(p)-(LGRPOT_SIZE-p->limit);
	if(up->flags&POT_TAKE){
		AR_MESG(ARMSG_LVK|ARMSG_POT,"succeeded in recombining greater POT.\n");
		p->limit = 0;	
		p->off = 0;
		up = _64u_(up)-POT_SIZE;
		up->underneath = LGRPOT_SIZE;
		anr->grp_free[anr->grp_next++] = p->gr;
		//add this back
		p->real_flags |= 1;
		return 0;
	}
	return -1;
}

void lspot_prep_nolink(potp p){
	p->real_flags = 0xff;
	init_pot(p);
	p->lot = AR_NULL;
	p->limit = 0;
}

void static lspot_link_after_alloc(potp __pot){
	lspot_link(__pot,myarena->potbin,0);
}

potp static lspot_scalvage(struct arena *anr){
	if(anr->stg_n<STAG_SIZE){
		potp p;
		p = anr->stagnation[anr->stg_n++];
		p->flags &= ~POT_STAG;
		return p;
	}
	potp up,p;
	//REMOVE:
	if(anr->grsp_cnt>0 && !1){
		potp p;
		p = anr->grs_partly[--anr->grsp_cnt];
		attempt_recombine(anr,p);
		pot_newtag(p);
		pot_curtag(p) = PT_FAC0_BIRTH|PT_FAC1_BIRTH_PARTLY|PT_GREATER;
		return p;
	}
	/*
		this leave at most one greater pot for pot_alloc to dissect
	*/

	if(anr->spr_cnt<SPROUT_MAX){
		atmq_inc(&_ar_ms.cspr_harvests);
		AR_MESG(ARMSG_LVK|ARMSG_POT,"using greater pot that had be constructed using (olds/unused) lesser pots.\n");
		struct sprout *spr;
		/*
			NOTE:
				no sprout locking required only spr_lock
				also spr_remove is okay as well
		
			NOTE:
				spr_complete can only be uncompleted by the owner of the arena(which is us).
		*/
		mt_lock(&anr->spr_lock);
		spr = anr->spr_complete[anr->spr_cnt++];
		mt_unlock(&anr->spr_lock);
		ar_assert(spr->acm == arena_distinct,"not the owner.\n");
		ar_assert(spr->bits == 128,"what?, got: %x\n",spr->bits);

		struct sprout *spr0;
		spr0 = anr->spr_inuse[anr->spr_next++];
		anr->spr_inuse[spr->spr_idx] = spr0;
		spr0->spr_idx = spr->spr_idx;
		/*
		FIXED:
		*/
		potp up;
		up = _64u_(spr->p)-POT_SIZE;
		//no lock required as mov operations are atomic
		up->underneath = LGRPOT_SIZE;
		p = _64u_(spr->p)+(LGRPOT_SIZE-POT_SIZE);
		/*
			remove this flag
		*/
		p->flags &= ~POT_INTERFERE;
		ar_assert((_64u_(p)&LGRPOT_MASK) == LGRPOT_REAL_SIZE,"not aligned.\n");
		pot_newtag(p);
		pot_curtag(p) = PT_FAC0_BIRTH|PT_FAC1_BIRTH_SPROUT|PT_GREATER;
		lspot_prep_nolink(p);//more_lock is used in this(50:50) beware!
		return p;
	}

	return NULL;
}

potp static _lspot_alloc(struct arena *);

void bookcase_add(struct lot *__lot,struct accommodation *acm){
	mt_lock(&acm->bookcase_lock);
	ar_assert(acm->bcs_n>0,"hey.\n");
	__lot->acm = acm_idx(acm);
	acm->bcs_n--;
	__lot->bcs_idx = acm->bc_free[acm->bcs_n];
	acm->bookcase[__lot->bcs_idx] = __lot;
	mt_unlock(&acm->bookcase_lock);
}

void bookcase_remove_nolock(struct lot *__lot){
  struct accommodation *acm;
  acm = acm_at(__lot->acm);
	ar_assert(__lot->bcs_idx<BOOKCASE_SIZE,"fuck.\n");
	ar_assert(acm->bcs_n<BOOKCASE_SIZE,"wrong");	
	_32_u idx;
	idx = acm->bc_free_x[__lot->bcs_idx];
	if(idx == acm->bcs_n){
		acm->bcs_n++;
		return;
	}

	_32_u k;
	k = acm->bcs_n;
	_32_u rpl;
	rpl = acm->bc_free[k];
	_32_u u;
	u = acm->bc_free_x[__lot->bcs_idx];
	ar_assert(acm->bc_free_x[rpl] == k,"wrong.\n");
	acm->bc_free[u] = rpl;
	acm->bc_free[k] = __lot->bcs_idx;
	
	acm->bc_free_x[__lot->bcs_idx] = k;
	acm->bc_free_x[rpl] = u;
	acm->bookcase[__lot->bcs_idx] = NULL;
	acm->bcs_n++;
}

void bookcase_remove(struct lot *__lot){
	struct accommodation *acm;
	acm = acm_at(__lot->acm);
	mt_lock(&acm->bookcase_lock);
	bookcase_remove_nolock(__lot);
	mt_unlock(&acm->bookcase_lock);
}

void static tryto_bookcase(struct lot *lt,struct accommodation *acm){
	potp pt;
	struct lot *next;
_again:
	/*
		as long as we have the free_pots_lock things should be fine
	*/
	next = lt->next_free;
	if(mt_trylock(&lt->lock) == -1){
		goto _goahead;
	}
	pt = lt->pot;
	if(mt_trylock(&pt->apd.lock) == -1){
		mt_unlock(&lt->lock);
		goto _goahead;
	}

	lt->flags |= LOT_BOOKED;
	ar_assert(lt->acm == acm_idx(acm),"why.\n");
	lot_renounce(lt);
	_ar_printf("new book added.\n");
	bookcase_add(lt,acm);

	mt_unlock(&lt->lock);
	mt_unlock(&pt->apd.lock);
_goahead:
	if(next != NULL && acm->bcs_n>0){
		lt = next;
		goto _again;
	}
}

void static stock_bookcase(struct accommodation *acm){
	mt_lock(&acm->free_pots_lock);
	struct lot *lt;
	_32_u i;
	i = 7;
	while(i != 16){
		i++;
		lt = acm->_free_pots[i&(16-1)];
		if(lt != NULL){
			tryto_bookcase(lt,acm);
			if(!acm->bcs_n)
				break;
		}
	}
	mt_unlock(&acm->free_pots_lock);
}

potp pluck_from_lot(struct lot *lt, potp p, _64_u __size){
	_64_u acum;
	acum = 0;
	_64_u lot;
_again:
	lot = blpot(p)->lot;
	_ar_printf("spanlgn: %u.\n",in_lpts(spanlgn(lot)));
	acum+=spanlgn(lot);
	p = _64u_(p)+spanlgn(lot);
	if(acum<=__size){
		lot_free(lot);
		if(acum != __size){
			goto _again;
		}
	}

	_64_u left;
	left = acum-__size;
	if(left>0){
		spanlgn(lot) = left;
	}

	ar_assert(lt->size_in_pots>=__size,"wrong.\n");
	lt->size_in_pots-=__size;
	return _64u_(p)-left;
}
/*
	looking though all the lots in existance is slow,
	so we search all exiting lots, and bookmark promising lots.
	
	the issue is when trying to find suitable lots for greater pots,
	a lot could be under greater pot size. but also we dont want to be randomly selecting
	lots.

	we would just keep a list of all lots that fit the criteria, however it would be a wast
	if we only had lesser pots.
*/
potp static try_bookcase(struct accommodation *acm){
	mt_lock(&acm->bookcase_lock);
	_ar_printf("bookcase size: %u.\n",acm->bcs_n);
	if(acm->bcs_n == BOOKCASE_SIZE){
		_ar_printf("bookcase empty.\n");
		mt_unlock(&acm->bookcase_lock);
		return NULL;
	}
	potp rtn;
	rtn = NULL;
	_int_u i;
	i = acm->bcs_n;
	for(;i != BOOKCASE_SIZE;i++){
		struct lot *lt;	
		potp pt;
		lt = acm->bookcase[acm->bc_free[i]];
		ar_assert(lt != NULL,"fuck.\n");
		if(mt_trylock(&lt->lock) == -1){
			continue;
		}
		pt = lt->pot;
		if(mt_trylock(&pt->apd.lock) == -1){
			mt_unlock(&lt->lock);
			continue;
		}
		potp ab;
		ab = _64u_(pt)-lt->size_in_pots;
		//we cant have the above pot butting in
		mt_lock(&ab->apd.lock);

		ar_assert(pt->flags&POT_FREE,"wrong.\n");
		ar_assert(lt->bcs_idx == acm->bc_free[i],"this cant be.\n");
		if(lt->size_in_pots>=LGRPOT_SIZE){
			/*
				we are ignoreing this for now as later we are to allow for missaligned(in some cases).
			*/
			if((_64u_(ab)&LGRPOT_MASK) == LGRPOT_REAL_SIZE){
				rtn = pluck_from_lot(lt,ab,LGRPOT_SIZE);
				ab->underneath = LGRPOT_SIZE;
				ar_assert((_64u_(rtn)-LGRPOT_SIZE) == _64u_(ab),"wrong.\n");
			}else{
				acm->gpac_miss++;
				_64_u to;
				to = LGRPOT_ILVBITS-(_64u_(ab)&LGRPOT_ILVBITS); 
				_64_s left;
				left = lt->size_in_pots-to;
				if(left>=LGRPOT_SIZE){
					rtn = pluck_from_lot(lt,ab,to+LGRPOT_SIZE);
					ar_assert((_64u_(rtn)-(to+LGRPOT_SIZE)) == _64u_(ab),"fuck.\n");
					ar_assert(lt->size_in_pots == (left-LGRPOT_SIZE),"no.\n");
					ar_assert(_64u_(rtn)<=_64u_(lt->pot),"fuuuuck.\n");
					ar_assert((_64u_(rtn)&LGRPOT_MASK) == LGRPOT_REAL_SIZE,"dear god.\n");
					
					potp rn;
					rn = _64u_(rtn)-LGRPOT_SIZE;
					rn->underneath = LGRPOT_SIZE;
					mt_lock(&rn->apd.lock);
					pot_span(rn,to);
					spaninit(rn,to);
					mt_unlock(&rn->apd.lock);
				}
				//ar_assert(lt->size_in_pots<to_lpts(16),"oversized.\n");
				_ar_printf("missaligned\n");
			}
			if(rtn != NULL){
				ar_assert((rtn->flags&POT_FREE)>0,"wrong, have: %x\n",rtn->flags);
				//needs removing asap other wise other may think the lot is valid
				rtn->flags &=~POT_FREE;
				lspot_prep_nolink(rtn);
			}
		}else{
			acm->gb_insufficient++;
		}
		_ar_printf("size: %u\n",in_lpts(lt->size_in_pots));
		mt_unlock(&pt->apd.lock);
		mt_unlock(&ab->apd.lock);	
		mt_unlock(&lt->lock);
		if(!lt->size_in_pots){
			ar_assert(rtn != NULL,"no good.\n");
			bookcase_remove_nolock(lt);
			lot_strc_free(lot_index(lt));
		}
		if(rtn != NULL){
			goto _rt;
		}
	}
	_ar_printf("total failure.\n");
_rt:
	mt_unlock(&acm->bookcase_lock);
	return rtn;
}

potp foreign_bookcase(struct accommodation *skip){
	potp p;
	p = NULL;
	_32_u i;
	i = 8;
	while(i != _ar_ms.arena_next && !p){
		i--;
		struct accommodation *ac;
		ac = _ar_ms.acm+i;
		if(ac != skip){
			p = try_bookcase(ac);
			atmq_inc(&ac->outlanders);
		}
	}
	return p;
}

potp static lspot_alloc(void){
	struct arena *anr;
	anr = myarena;
	/*
		if this reaches zero then we go hunting for more
	*/
	if(anr->acm->bcs_n>0){
		stock_bookcase(anr->acm);
		if(anr->acm->bcs_n == BOOKCASE_SIZE){
			_ar_printf("failed to stockup.\n");
		}
	}
	potp rtn;
	rtn = lspot_scalvage(anr);
	if(!rtn){
		anr->gpsal_miss++;
		rtn = try_bookcase(anr->acm);
		if(!rtn){
			rtn = foreign_bookcase(anr->acm);
		}
		if(!rtn){
			anr->gpbc_miss++;
			rtn = _lspot_alloc(anr);
		}
	}
	return rtn;
}

potp _lspot_alloc(struct arena *anr){
	_64_u addr,dif;
	void *large;
	potp up,p;
	more_lock();
	AR_MESG(ARMSG_LVK|ARMSG_POT,"resorting to a pure allocation greater pot.\n");
	_ar_ms.lpot_alloced++;
	ar_assert(!(_64u_(more_uptr)&MASK_OF(LGRPOT_SIZE_SHIFT)),"alignment issue.\n");
	
	AR_MESG(ARMSG_LVK|ARMSG_POT,"larger set aloc.\n");
	
	/*
		this is are 33MB larger set
	*/			
	large = _ar_sysmore(LGRPOT_SIZE);
	up = ab0pot(large);
	mt_lock(&up->apd.lock);
	
	below_flags(up) &= ~POT_LAST_IN_LINE;
	up->underneath = LGRPOT_SIZE;
	AR_MESG(ARMSG_LVK|ARMSG_POT,"LSPOT: %x.\n",_64u_(large)>>25);
	ar_assert(!(_64u_(large)&MASK_OF(LGRPOT_SIZE_SHIFT)),"larger set isent aligned properly.\n");
	addr = _64u_(large)|(MASK_OF(LGRPOT_SIZE_SHIFT)^sizeof(struct pot));

	p = addr;
	pot_primative_init(p);
	below_flags(p) = POT_LAST_IN_LINE;
	p->tag_ptr = 0;
	p->valid_tags = 0;
	pot_newtag(p);
	pot_curtag(p) = PT_FAC0_BIRTH|PT_FAC1_BIRTH_PURE|PT_GREATER;
	mt_unlock(&up->apd.lock);
	more_unlock();	
	/*
		caution should be taken when using this,
		somewhere along the line we do a lesser allocation call and 
		this might end up trying to do a more_lock() as the pots in a state of limbo
		we do this after relieving the lock.
	*/
	lspot_prep_nolink(p);
	p->flags |= POT_GREATER;	
	return p;
}
/*
	issue with this routine:
	- allocating in large quantities caused segfault

	TODO:
		add some limit on this just in case.
		meaning some ranking limits.

		EXAMPLE:
		only 100x for allocations below 1kb

	NOTE:
		mmap is aligned by pagesize meaning the some lower bits are zero
*/
#include "../linux/page_types.h"
void static* _sysalloc(_int_u __bc){	
	void *p;
	_int_u size;
	size = bale_size+__bc;
	size = ALIGN_TO(size,KPAGE_SIZE);
	if ((p = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0)) == (void*)-1) {
		// failure
		ar_abort();
	}

	ar_assert(p != NULL,"not ment to be NULL.\n");
	ar_assert(!(_64u_(p)&KPAGE_MASK),"not page aligned, %u.\n",__bc);
	balep blk = (balep)p;
	blk->size = __bc;
	blk->flags = BALE_MMAPED;
	ar_printf("allocated system memory.\n");
	return bale2up(blk);
}

void static* _sysresize(balep blk,_int_u __to){
	_64_u ptr;
	ptr = _64u_(blk)&~KPAGE_MASK;

	_64_u rtn;
	_int_u new_sz,old_sz;
	new_sz = __to+bale_size;
	old_sz = blk->size+bale_size;
	new_sz = ALIGN_TO(new_sz,KPAGE_SIZE);
	old_sz = ALIGN_TO(old_sz,KPAGE_SIZE);

	if(new_sz != old_sz) {
		ar_assert(!(ptr&KPAGE_MASK),"not page aligned.\n");
		rtn = mremap(ptr,old_sz,new_sz,MREMAP_MAYMOVE,NULL);
		ar_assert(!(_64u_(rtn)&KPAGE_MASK));
		if(!~(rtn&~0xff)){//will deal with this later
			return _64u_(-1);
		}
		blk = rtn;
	}
	blk->size = __to;
	return bale2up(blk);
}

void _sysfree(balep blk){
	_64_u ptr;
	ptr = _64u_(blk)&~KPAGE_MASK;
	_32_s r;
	r = munmap((void*)ptr, bale_size+blk->size);
}

void static* last_resort(struct arena *anr,_int_u __bc){	
	void *ret = NULL;
	potp chasm;
	_int_u i;
	i = 7;
	/*
		NOTE: we only iterate if chasm = NULL
		and also we go for the least of the largest
		so to make it fit as much as possable.
	*/
	for(;i != 0;){
		chasm = anr->largest_chasm[--i];
		//this statment is higly unlikly
		if(chasm != NULL){
			ret = _ar_balloc_guarded(chasm,__bc);
			break;
		}
	}

	if(!ret){
		atmq_inc(&_ar_ms.chasm_misses);
	}else{
		atmq_inc(&_ar_ms.chasm_hits);
	}
	return ret;
}
static struct func_stamp ma_lesser_bins = {"lesser bins"};
balep static* alloc_lesser(struct arena *anr,_int_u __bc){
	balep ret;
	potp p,t;
	/*
		arena may not exist, to create/alloc a new one.
	*/
	if (!anr->top) {
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"created initial POT.\n");
		anr->top = pot_alloc_guarded(anr);
		ar_assert(anr->top != NULL,"failed to alloc POT.\n");
	}

	p = anr->top;
	/*
		try and attempt to allocate memory in a pot,
		if return null(meaning no space) then move on the the next one
		in the list.

		TODO:
			rank POTS on space left????
			like bale bins but on space left not space taken
	*/

	struct bin_bay *bay;
	potp pp;
	_int_u n_lookins = 0;
	_int_u bn = bin_no(__bc);
	_int_u cn;
	_int_u dx;
	/*
		if the number of bales of this bin is zero then, we can then assume that all
		pots in the arena bins[bn] = AR_NULL;
	*/
	if(!anr->bincnt[bn]){
		atmq_inc(&_ar_ms.abc_deadends_by_bin[bn]);
		atmq_inc(&_ar_ms.abc_zero_case);
		/*
			check other bins after this bin to make sure there nothing we could do
		*/
		while(bn != no_bins){
			/*
				what we are looking for here is how many of these free bales fit are size,
				bins are not specific sizes so its 50/50 shot we find what we are looking for
				to increase are chances we are only going to be looking in bin if it contains 
				are large amount of these 50/50 bales as we dont want to wast time on looking for
				somthing that wont exist.

				why? because we would just quickly allocate it in a pot with the largest non-used space
			*/
			if(anr->bincnt[++bn]>4){
				atmq_inc(&_ar_ms.abc_hits);
				atmq_inc(&_ar_ms.abc_hits_by_bin[bn]);
				goto _intx0;
			}else{
				atmq_inc(&_ar_ms.abc_misses_by_bin[bn]);
				atmq_inc(&_ar_ms.abc_misses);
			}
		}
		/*
			fall though here, meaning theres nothing visibly free.
			meaning attempt to take from the POT with largest chasm.
		*/
		ret = last_resort(anr,__bc);	
		goto _stepover;
	}	
_intx0:
	bay = anr->bays+bn;
	cn = bay->n;
_intx:
	if(cn != 32){	
		pp = bay->pots[cn];
		ar_assert(pp != NULL,"kill me, please, %u.\n",cn);
		/*
			for this bins are ranked on there abundance of bales in X size
		*/

		anr->stamp = &ma_lesser_bins;
		ret = _ar_balloc_guarded(pp, __bc);
		anr->stamp = NULL;
		if(ret != NULL) {
			atmq_inc(&_ar_ms.sfb_hits[bn]);
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"successfuly swifted bale.\n");
			goto _stepover;
		}
		/*
			this tells us a steal attempt was taken
		*/
		if(pp->bins[bn] != AR_NULL){
			balep bale = get_bale(pp,pp->bins[bn]);
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"failed to swiftly acuqire, bin: %u, size: %u, with-nbalecnt-of: %u, frontsize: %u\n",bn,__bc,pp->bincnt[bn],bale->size);
		}
		atmq_inc(&_ar_ms.sfb_misses[bn]);
		cn++;
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"no shortcut found attempting another.\n");
		goto _intx;
	}else{
		atmq_inc(&_ar_ms.sfb_deadends[bn]);		
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"swift bin empty.\n");
	}		
_again:
	/*
		here we iterate over all pots, however anr->bincnt[bn] must be larger then X(the larger the number the more likly we are going to find what we want)
	
		worst case is the swift bins get clogged with bale sizes below what we want.
		for example all the pots in the swift bin for sizes from 0-64
		where all the pots only contain bales below 40-bytes this means 
		we might end up here.

		its just a worst case might be changed later for somthing better. 
	
		the bigger the bales the more likly this will become as they
		can have greater varence
	*/
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"trying brute force approch.\n");
	if(!(ret = _ar_balloc_guarded(p, __bc))) {
		n_lookins++;
		if(p->fd != NULL){
			t = p->fd;
			p = t;	
			if(p->bincnt[bn]<100 && n_lookins>40)
				goto _sk;
			goto _again;
		}
	_sk:
		ulpot(p);
	}
_stepover:
	/*
		here we have two options try the largest POTS we have,
		or allocate more POTS
	*/
	if(!ret){
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"last resort.\n");
		ret = last_resort(anr,__bc);
	}


	/*
		this means that no space was available for are allocation,
		so create a new POT.

		this allocation differs from the initial alloc(the main POT for the thread).
	
		this means all above has failed, and we lack space to continue.
	*/
	/*
		to avoid at all costs(last of all resorts)
	*/
	if(!ret) {
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"allocation of %u-bytes was unsuccessful due to insufficient space.\n", __bc);

		p = anr->top;		
		t = pot_alloc_guarded(anr);
		ar_assert(t != NULL,"failed to alloc POT.\n");
		t->bk = NULL;
		t->fd = p;
		p->bk = t;
		anr->top = t;
		ret = _ar_balloc_guarded(t,__bc);
		ar_assert(ret != NULL,"major fucking issue.\n");
	}else{
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"allocation of %u-bytes was successful.\n", __bc);
	}
	AR_MESG(ARMSG_LVK|ARMSG_TBD,"got alloc where bale = %u(size being: %u-bytes), with max of %u, took: %u-trys\n",bale_offset(ret),ret->size,POT_REAL_SIZE,n_lookins);
	return ret;
}

	void* _m_alloc_lesser(_int_u __bc){
		balep ret;
		_ar_ms.indulge = NULL;
		if(_ar_ms.indulge != NULL) {
			mt_lock(&_ar_ms.lock);
			
			ret = NULL;
			/*
				should or shouldent we orthan the pot?
			*/
			if(_ar_ms.indulge != NULL){
				//ret = _ar_balloc(pt, __bc);
			}
			mt_unlock(&_ar_ms.lock);
			if(ret != NULL){
				return ret;
			}
		}
		struct arena *anr;
		anr = myarena;
		if(mt_trylock(&anr->access_lock) == -1){
			_32_u rank;
			rank = pw2_ranking(__bc);
			balep b;
			if((b = anr->frontbale[rank]) != NULL){
				if(b->size>=__bc){
					potp p;
					p = get_pot(b);
					ar_assert(!(p->flags&POT_FREE),"not ment to be here.\n");
					_64_u junk = b->size-__bc;
					anr->frontbale[rank] = NULL;
					mt_lock(&anr->stock_lock);
					anr->stock[anr->next_stock++] = rank;
					mt_unlock(&anr->stock_lock);

					if(junk>(bale_size+63)){
						_int_u size;
						size = junk-bale_size;
						rank = pw2_ranking(size);
						balep ble;
						ble = _64u_(b)+bale_size+__bc;
						if(!anr->frontbale[rank]){
							anr->frontbale[rank] = ble;
						}else{
							/*
								TODO: change
							*/
							_32_u rl = TLS->real;
							ar_assert(anr->subf_cnt[rl]<=16,"how did this happen?.\n");						
							if(anr->subf_cnt[rl]<16){
								lesser_subsidiary_free(anr,rl,ble);	
							}else{
								atmq_inc(&_ar_ms.subsidiary_flunks);
								goto _skover;
							}
						}
						balep bl;
						lkpot(p);
						bl = splice_bale(p,b,__bc,junk,MASK_OF(POT_SIZE_SHIFT));
						ulpot(p);
						ar_assert(bl == ble,"where did we go wrong :(.\n");
					}
				_skover:
					ar_assert(b != NULL,"fuck.\n");
					return bale2up(b);
				}
			}
			mt_lock(&anr->access_lock);
		}
		
		if(!mt_trylock(&anr->stock_lock)){
			if(anr->next_stock>0){
				_int_u i;
				i = anr->next_stock;
				while(i-- != 0){
					_int_u n;
					n = anr->stock[i];
					ar_assert(!anr->frontbale[n],"not good.\n");
					anr->frontbale[n] = alloc_lesser(anr,1<<n);
				}
			}
			anr->next_stock = 0;
			mt_unlock(&anr->stock_lock);
		}
		ret = alloc_lesser(anr,__bc);
		ar_assert(ret != NULL,"fuck.\n");
		mt_unlock(&anr->access_lock);
		return bale2up(ret);
	}
	#ifdef IGNORE
	void static* checkandfit(potp p, _int_u nlesser){
		_64_u ret;
		potp upper;
		struct lot *lp;
		lp = get_lot(p->lot);
		//most likly someone else was operating on this
		if(mt_trylock(&lp->lock) == -1){
			return NULL;
		}
		/*
			the lot needs to be locked so no alteration takes place while we are working on it.
		*/
		upper = ((_64_u)p)-POT_SIZE;
		_int_u i;
		i = POT_SIZE;
		if(nlesser>POT_SIZE){
		AR_MESG(ARMSG_LVK|ARMSG_POT,"proceeding to check adjecent pots for free, starting at upper(%s)\n",(upper->flags&POT_FREE)?"yes-free":"not-free");
		}
		/*
			this is not necessary, but for debug to make sure every things going well(sanity wise).
			
			always should be ture; the upper block if free should always be apart of the same lot,
			if not then tells us there was a failure.
		*/
		while(upper->flags&POT_FREE && i != nlesser){
			upper = ((_64_u)upper)-POT_SIZE;
			i+=POT_SIZE;
		}
		if(i == nlesser){
			potp up;
		up = upper;
		/*
			how much wiggle room we have.
		*/
		_int_s leverage;
		/*
			we cant be larger then the lot size, this is because it might result in spaning accross 
			another lot.
		*/
		if((leverage = (get_lot(p->lot)->size_in_pots-nlesser))>0){
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"leverage: %u.\n",leverage>>POT_SIZE_SHIFT);
			struct lot *lot;
			lot = get_lot(up->lot);
			ar_assert(lp == lot,"the lots of both pots should be the same.\n");
			_int_u i;
			i = lp->next;
			_32_u mv;
			/*
				NOTE:
					here we free up all resources, however if we land on the up->lot
					then we dont touch it.
			*/
			_int_u left;
			left = nlesser;
			/*
				NOTE:
					here we are standing  bottom of the lot span(lp->next),
					by the end of this operation we will have extra(more then needed)
			*/
			while((mv = lp->defer[i]) != up->lot){
				ar_assert(!(lp->mask&(1<<i)),"defer is NULL, this should not be, checking if %p != %p, round-%u.\n",lp->defer[i],up->lot,i);
				/*
					if defer is not AR_NULL this means we have reached the limit for how many we can have in this circular array
				*/
				lp->mask |= 1<<i;
				lot_free(mv);
				left-=spanlgn(mv);
				i = (i+1)&DEFER_MASK;
			}
			/*
				 __
				|  | <- defer[0]
				|__|
				|__| <- defer[1]

				in the case we have a fit, meaning what we have snuggly fits in defer[0]+defer[1]
				this means that the size of defer[0 and 1] equal what we are dishing out.
				meaning left will equal zero.

				left = size_of_dish

				left-=defer 0 size
				left-=defer 1 size
					
				by the time we reach here 
				left will be zero.
				there is nothing left.
				left = 0
			*/
			spanlgn(up->lot)-=left;
			lot->next = i;
			lot->size_in_pots-=nlesser;
			lot->pot = up;
			lot_link(lot,linkidx(lot->size_in_pots));
		}else{
			/*
				this is a full fit case.
			*/
			if(!leverage){			
				AR_MESG(ARMSG_LVK|ARMSG_TBD,"leverage was found to be zero, this means it was a full fit, %u-lpts == %u-lpts\n",in_lpts(nlesser),in_lpts(lp->size_in_pots));
				/*
					completely remove the lot
				*/
				_32_u mv;
				_int_u i;
				i = lp->next;
				while(!(lp->mask&(1<<i))){
					mv = lp->defer[i];
					lot_free(mv);
					i = (i+1)&DEFER_MASK;
				}
				lot_strc_fft(p->lot);
			}else{
				/*
					TODO:
						as the upper pot is free this tells us we might be able to conjoin are selfs with it.
				*/
				AR_MESG(ARMSG_LVK|ARMSG_TBD,"leverage is below or equal to zero(-%u)\n",(-leverage)>>POT_SIZE_SHIFT);
				ret = _64u_(NULL);
				goto _retval;
			}
		}
		/*
			this does not require a lock as 
			by entering this function the list is already locked.
			but also we know its apart of are free pots chain list
		*/
		pot_renounce(p);
		up->underneath = nlesser;
		p->flags &= ~POT_FREE;
		p->lot = AR_NULL;
		p->limit = nlesser;

		p = _64u_(up)+POT_SIZE;
		ret = p;
		ret &= ~POT_MASK;
		ar_assert(!(ret&MASK_OF(POT_SIZE_SHIFT)),"not aligned to pot size.\n");
		atmq_inc(&_ar_ms.poted_bales_alloced);
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"found sufficient space, managed to find %u-free-pots\n",nlesser>>POT_SIZE_SHIFT);
	}else{
		ret = _64u_(NULL);
		AR_MESG(ARMSG_LVK|ARMSG_TBD,"number of pots not meet, have: %u, need: %u.\n",i>>POT_SIZE_SHIFT,nlesser>>POT_SIZE_SHIFT);
	}
_retval:
	mt_unlock(&lp->lock);
	return ret;
}
#endif
void static addto_memcntrs(_64_u __bc){
#ifdef __f_debug
	__asm__("lock addq %1, (%0)\n\t"
			"lock incq (%2)": : "r"(&_f_m.used), "r"((_64_u)__bc), "r"(&_f_m.aa));
#endif	
}

void*
_m_alloc(_int_u __bc) {
	if(is_null(TLS->arena)){
		TLS->real = TLS->arena = arena_alloc();
	}
	struct arena *anr = myarena;
	ar_assert((anr->flags&ARENA_ISSUED)>0,"arena hasent been issued.\n");
	if (!__bc){
		ar_printf("got allocation of ZERO.\n");
		return NULL;
	}
	/*
		if are bale doesent fit within a pot then just use mmap for now
		

		best option is to grow the mmap if we get another?
	*/
	_int_u totsize = __bc+bale_size;
	
	addto_memcntrs(__bc);
	if (totsize>=(1<<15)){
		if(totsize<=POT_SIZE){
			potp b;
			mt_lock(&anr->potbin_lock);
			b = anr->potbin[potbinno_for(totsize)];
			_int_u lookat = 0;
			while(b != NULL) {
				potp fd;
				void *r;
				AR_MESG(ARMSG_LVK|ARMSG_TBD,"attempting to use non-lesser pots for allocation.\n");
				/*
					sanity check agenst corroption of the link list
				*/
				ar_assert((_64u_(b)&LGRPOT_MASK) == LGRPOT_REAL_SIZE,"issue.\n");
				fd = b->fd;
				if(!mt_trylock(&b->inuse_lock)){
					r = _ar_sballoc(b,__bc);
					ulpot(b);
					if(r != NULL) {
						mt_unlock(&anr->potbin_lock);
						AR_MESG(ARMSG_LVK|ARMSG_TBD,"using non-lesser pots for allocation.\n");
						return r;
					}
				}
				lookat++;
				b = fd;
			}
//		_ar_printf("looked at : %u-pots.\n",lookat);
			mt_unlock(&anr->potbin_lock);
			AR_MESG(ARMSG_LVK|ARMSG_TBD,"couldent find sufficient space.\n");	
			potp pot;
			pot = lspot_alloc();
			if(pot != NULL){
				pot->acm = TLS->arena;
				void *p;
				p = _ar_sballoc(pot,__bc);
				/*
					no locking is required for _ar_sballoc, because the pot isent in the potbin chain yet
					so no one exept us knows of its existance.
				*/
				mt_lock(&anr->potbin_lock);
				lspot_link_after_alloc(pot);
				mt_unlock(&anr->potbin_lock);
				return p;
			}else{
				AR_MESG(ARMSG_LVK|ARMSG_TBD,"couldent allocate larger-set, taking detour.\n");
			}
		}

		AR_MESG(ARMSG_LVK|ARMSG_TBD,"using system allocation approch. %u-bytes\n",__bc);
		/*
			here we handle any allocations that are over 1/4 of 1MB
			this is because, if we handled such things here then we would have an issue of
			too much unused memory. we could free pages by SYSCALL, meaning we could allocate large swarfs of memory
			and do a SYSCALL to free the pages with the unused parts of it.

			i dont know. we for now we just use this.
			
			!no locking required
		
			default to system allocation
		*/
		return _sysalloc(__bc);
	}

	AR_MESG(ARMSG_LVK|ARMSG_TBD,"using lesser pots for allocation approch.\n");
	return _m_alloc_lesser(__bc);
}

balep static cascade_congregate(potp __pot,balep blk){
	AR_MESG(ARMSG_LV0|ARMSG_BALE,"to free: %u\n", blk->size)

	balep prev, next, top = NULL, end = NULL;
	_int_u n_found = 0;
	/*
		attempt to join the prevous bale and this one together.
		*amalgamate with the prevous free
	*/
	if (not_null(blk->prev)) {
		prev = bale_prev(blk);
		/*
			walk this path until no free bale is found.
			keeping joing.
		*/
		while(is_free(prev)) {
			__pot->blk_c--;
			__pot->buried-=prev->size;
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"found free space above, %u\n", prev->size)
			/*
				this bale might be contained within a bin,
				so we need to remove it.
			*/
			if (bale_flag(prev, BALE_LINKED))
				detatch(__pot, prev);
			
			decouple(__pot, prev);
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"total freed: %u\n", bale_end(prev)-bale_offset(prev))
			top = prev;
			n_found++;
			if (is_null(prev->prev)) break;
			prev = bale_prev(prev);
		}
	}

	/*
		attempt to join the next bale and this one together.
		*amalgamate with the next free
	*/
	if (not_null(blk->next)) {
		next = bale_next(blk);
		while(is_free(next)) {
			__pot->blk_c--;
			__pot->buried-=next->size;
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"found free space below, %u\n", next->size)
			if (bale_flag(next, BALE_LINKED))
				detatch(__pot, next);
			decouple(__pot, next);
			AR_MESG(ARMSG_LV0|ARMSG_BALE,"total freed: %u\n", bale_end(next)-bale_offset(next))
			end = next;
			n_found++;
			if (is_null(next->next)) break;
			next = bale_next(next);
		}
	}

	if (top != NULL) {
		top->size = (bale_end(blk)-bale_offset(top))-bale_size;
		top->next = blk->next;
		blk = top;
	}

	if (end != NULL) {
		blk->size = (bale_end(end)-bale_offset(blk))-bale_size;
		blk->next = end->next;
	}

	// only used when used
	blk->pad = 0;
	AR_MESG(ARMSG_LV0|ARMSG_BALE,"freed: %u, %u, found: %u-bales that where unused\n", blk->size, bale_offset(blk),n_found);
	if (bale_offset(blk)>=__pot->off) {
#ifdef __ffly_debug
		ar_printf("somthing is wrong, the bale offset is not within range. %u >= %u\n",bale_offset(blk),__pot->off);
#endif
		ar_abort();
	}
	return blk;	
}

void auctionoff(potp __pot, balep blk){
	_int_u boff = bale_offset(blk);
	{
		_int_u bix,bx;
		bix = ubin_indx(blk->size);
		bx = ubin_bindx(blk->size);
		if(meets_ubin_criteria(__pot,bix,bx)) {
			__pot->u_binsdwn[bx] |= _64u_(1)<<bix;
			place_in_ubin(__pot,blk,boff,bx);
			return;
		}
	}

	_int_u bn;
	bn = bin_no(blk->size);
	ar_off_t bin;
	
	if (not_null(bin = get_bin_for(__pot,bn))) {
		balep b;
		b = get_bale(__pot, bin);
		if(blk->size>b->size){
			*bin_for(__pot,bn) = boff;
			blk->bk = AR_NULL;
			blk->fd = bin;
			b->bk = *bin_for(__pot,bn);
		}else{
			b->bk = boff;
			blk->fd = bin;
		}
	}else{
		bin_bit_one(__pot,bn);
		blk->fd = AR_NULL;
	}
	blk->bk = AR_NULL;
	*(__pot->bins+bn) = boff;
	struct arena *anr;
	anr = myarena;
	__pot->bincnt[bn]++;
	anr->bincnt[bn]++;

	struct bin_bay *bay;
	bay = anr->bays+bn;
	if(bay->n>0){
		/*
			if we dont have this need get it
		*/
		if(is_null(__pot->b2)){
			_32_u b2;
			ar_assert(anr->next_dish>0,"why is this not so?.\n");
			b2 = anr->free_dishes[--anr->next_dish];
			__pot->b2 = b2;
		}
	
		struct bin_bay_dish *dish;
		dish = _ar_ms.dishes+__pot->b2;

		_32_u *ds;
		ds = dish->bins+bn;
		if(is_null(*ds)){
			dish->cnt++;
			_32_u bay_slot;
			ar_assert(bay->n>0,"how can this be?.\n");
			bay_slot = --bay->n;
			*ds = bay_slot;
			bay->pots[bay_slot] = __pot;
			bay->dish[bay_slot] = bn;
		}
	}
}

void
_ar_bfree(potp __pot, balep blk) {
#ifndef barebone
	/*
		if size if within this range then we do a brisk free
		if are bale_end == __pot->off then dont do.
	*/
	if(blk->size<16 && bale_end(blk) != __pot->off) {
		/*
			if one of the adjacent bales are free(NOT! both) then its okay to quick free,
		*/
		if (not_null(blk->prev) && not_null(blk->next)) {
			balep ft,rr;
			ft = bale_next(blk);
			rr = bale_prev(blk);
			/*
				can be 'used-free' or 'free-used'
			*/
			if((ft->flags&BALE_USED) != (rr->flags&BALE_USED)){
				blk->flags = (blk->flags&~(BALE_USED|BALE_LINKED));
				AR_MESG(ARMSG_LV0|ARMSG_BALE,"speed free.\n");
				__pot->buried+=blk->size;
				return;
			}
		}
	}
#endif
	/*
		decouple the bale from the pot bale chain,
		as we need to work on this bale without interference.
	
		NOTE: decouping a bale, means that the bale is nolonger a bale and just memory that exists in a place.
		this also means that no attemt will be made to free or interact with this memory.
	*/
	decouple(__pot,blk);

	/*
		from this bale onwards we collect and congregate as many bales that are free or have free space they are not using
	*/
	blk = cascade_congregate(__pot,blk);
	if (bale_end(blk) == __pot->off) {
		__pot->blk_c--;
		largest_chasm_rm(__pot);
		__pot->off = bale_offset(blk);
		largest_chasm(__pot);
		return;
	}else{
		/*
			if we are not __pot->off then we expect that the front and back bales are never both NULL
		*/
		ar_assert(
			!(is_null(blk->next) && is_null(blk->prev)),
			"major incident, even know both front and back bales are NULL this bale isent positioned where its expected to be(%u, %u).\n",
			bale_end(blk),__pot->off
		);
	}

	__pot->buried+=blk->size;

	recouple(__pot,blk);
	blk->flags = (blk->flags&~BALE_USED)|BALE_LINKED;

	auctionoff(__pot,blk);
}

void _ar_bfree_guarded(potp __pot, balep blk){
	lkpot(__pot);
	_ar_bfree(__pot,blk);
	ulpot(__pot);
}

void static perf_free_stats(_64_u __size){
#ifdef __f_debug
	__asm__("lock subq %1, (%0)\n\t"
		"lock decq (%2)": : "r"(&_f_m.used), "r"(__size), "r"(&_f_m.aa));
#endif
}

void static _free_lesser(struct arena *anr, balep blk){
	potp p, bk;
	/*
		get the pot associated with this bale
	*/
	p = get_pot(blk);
	ar_assert(anr == arena_at(p->acm),"wrong arena.\n");
	/*
		make sure pointer resides within pot
		should pass with no error, else indicating anomaly
	
		we only do this because it helps with debug.
	
		TODO: start at begining
	*/
	_8_u *__p = blk;
	while(hob(p) != hob(__p)){
		ar_printf("checking range for %p at pot %p.\n",__p,p);
		bk = p;
		//check other pots for the pointer, attempt to find it(for debug)
		if (!(p = p->fd)) {
			ar_printf("bale not located.\n");
			return;
		}
	}
#ifdef ERASE
	/*
		erase what ever might be residing here, infomation/data wise(NOT! header)
		that would be catastrophic :0
	
		NOTE:
			blk->size-blk->pad we assume that blk->pad contains nothing(no data)
	*/
	ar_erase(((_8_u*)blk)+bale_size, blk->size-blk->pad);
#endif	
	/*
		finaly free the bale in its entirety
	*/
	_ar_bfree_guarded(p,blk);
	lkpot(p);
	if(!p->off && p->bk != NULL && has_jurisdiction(p)) {
		potp ft = p->fd;
		potp rr = p->bk;

		rr->fd = ft;
		if(ft != NULL){
			ft->bk = rr;
		}

		AR_MESG(ARMSG_LVK|ARMSG_TBD,"getting rid of unneeded POT.\n");
		ulpot(p);
		pot_free_guarded(p);
	}else{
		ulpot(p);
	}
}

/*
	'anr' = TLS->real
*/
void static drain_subsidiary(struct arena *anr,struct arena *an,_32_u j){
	ar_assert(an == arena_at(j),"so fucking wrong.\n");
	if(TLS->arena != TLS->real){
		atmq_inc(&_ar_ms.subsidiary_drains_by_outlander);
	}
	atmq_inc(&_ar_ms.subsidiary_drains);
	atmq_inc(&anr->subsidiary_drains_cnt[j]);

	_32_u cnt;
	cnt = anr->subf_cnt[j];
	ar_assert(cnt>0,"something is wrong.\n");
	balep b;
	void **dp;
_again:
	cnt--;
	dp = &subsid(anr,j,cnt);
	b = *dp;
	*dp = NULL;
	ar_assert(j == get_pot(b)->acm,"why?.\n");
	_free_lesser(an,b);
	if(cnt != 0){
		goto _again;
	}

	anr->subf_cnt[j] = 0;
	mt_lock(&anr->subf_lock);
	_32_u k,m;
	k = anr->subfs[--anr->subf_n];
	anr->subfs[m = anr->subfsc[j]] = k;
	if(m != anr->subf_n){
		ar_assert(m<anr->subf_n,"wrong.\n");
	}
	anr->subfsc[k] = m;
	mt_unlock(&anr->subf_lock);
}

void deal_with_subsidiarys(struct arena *anr){
	return;
	/*
		best options here is to have dual subf and quickly swap for empty one
		as it removes the need for a lock that spans across this whole thing
	*/
	mt_lock(&anr->subf_lock);
	_int_u i,j;
	i = 0;
	for(;i != anr->subf_n;){
		j = anr->subfs[i];
		struct arena *an;
		an = arena_at(j);
		/*
			here we attempt to free what we have prevously bufferd/accumulated/admassed up
		*/
		if(mt_trylock(&an->access_lock) == -1){
			i++;
			continue;
		}

		_32_u tmp;
		tmp = TLS->arena;
		TLS->arena = j;
		drain_subsidiary(anr,an,j);
		mt_unlock(&an->access_lock);
		TLS->arena = tmp;
	}
	mt_unlock(&anr->subf_lock);
}

/*
	NOTE: 'an' and 'rl' are the same arena its just better then getting it from TLS
*/
void lesser_subsidiary_free(struct arena *an,_32_u rl,balep blk){
	if(TLS->arena != TLS->real){
		atmq_inc(&_ar_ms.subsidiary_frees_by_outlander);
	}
	atmq_inc(&_ar_ms.subsidiary_frees);
	atmq_inc(&an->subsidiary_frees_cnt[rl]);
	/*
		if we have a zero this means this arena has no previus
		frees with this arena.
	*/
	ar_assert(an->subf_cnt[rl]<16,"what the fuck.\n");
	if(!an->subf_cnt[rl]){
		/*
			here we have a list of lists, this is a list that contains 
			directions to another arena free_list for this arena;
		*/
		mt_lock(&an->subf_lock);
		an->subfsc[rl] = an->subf_n;
		an->subfs[an->subf_n++] = rl;
		mt_unlock(&an->subf_lock);
	}
	void **dp;
	dp = &subsid(an,rl,an->subf_cnt[rl]);
	ar_assert(!*dp,"this might means we have a memory leak on are hands, have: %p, %u, %u\n",*dp,rl,an->subf_cnt[rl]);
	*dp = blk;
	an->subf_cnt[rl]++;
}

void _m_free_lesser(balep blk){
	potp p;
	p = get_pot(blk);
	ar_assert(!(p->flags&POT_FREE),"why is this here?.\n");
	struct arena *anr,*an;
	an = arena_at(TLS->real);
	_32_u tmp;
	tmp = TLS->arena;
	TLS->arena = p->acm;
	anr = arena_at(p->acm);
	/*
		if we failed to get the access lock then
	*/
	_32_u rl;
	rl = p->acm;
	/*
		if we cant get the lock for this then save it for later date
		no matter how large as it would only cause another pot to be created
	*/
	if(mt_trylock(&anr->access_lock) == -1){
		lesser_subsidiary_free(an,rl,blk);
		/*
			TODO: remove or remove >0 to worst case
			keep track of overall size?????
			assign range based on overall size and deside if its in are best intrests to force the free to take place?
		*/
		if(an->subf_cnt[rl]>15){
			mt_lock(&anr->access_lock);
			drain_subsidiary(an,anr,rl);			
			mt_unlock(&anr->access_lock);
			ar_assert(!an->subf_cnt[rl],"why is this not?.\n");
		}
		goto _lastthing;
	}
	/*
		only one can operate an arena at one time, this is due to 
		is causing issues with the allocation guidance.
	*/
	_free_lesser(anr,blk);
	mt_unlock(&anr->access_lock);
_lastthing:
	TLS->arena = tmp;
}

void 
_m_free_lesser_p(void *__p){
	_m_free_lesser(up2bale(__p));
}

void static
free_pot_span(potp p, potp upper){
	mt_lock(&p->apd.lock);
	mt_lock(&_ar_ms.more_lock);
	_64_u __span = upper->underneath;
	mt_lock(&upper->apd.lock);
	ar_assert(!(__span&POT_MASK),"issue");
	upper->underneath = 0;
	_64_u end;
	end = (_64u_(more_uptr)-1)^sizeof(struct pot);
	AR_MESG(ARMSG_LVK|ARMSG_POT,"free: %u.\n",(end-((_64_u)p))>>POT_SIZE_SHIFT);
	if(p == (potp)end){
		below_flags(upper) |= POT_LAST_IN_LINE;
		ar_assert(below_flags(p)&POT_LAST_IN_LINE,"pot not end.\n");
		AR_MESG(ARMSG_LVK|ARMSG_POT,"trimming of exess-of: %u-lpts\n",__span>>POT_SIZE_SHIFT);
		_ar_systrim(__span);
		if(upper->flags&POT_FREE && has_jurisdiction(upper)){
			trim_lotted_pot(upper);
		}else{
			mt_unlock(&upper->apd.lock);
		}
		mt_unlock(&_ar_ms.more_lock);
	}else{
		/*
			after the unlock of more_lock this might change if other threads are present,
			it dosent realy matter that much.
		*/
		ar_assert(!(below_flags(p)&POT_LAST_IN_LINE),"should not be.\n");
		mt_unlock(&_ar_ms.more_lock);
		potp bw,ab;
		if(1 == 1){//just lazy
			if(p->underneath>0){
				goto _sktoelse;
			}
			/*
				this pot as its below us could be freed(and become illegal to access)
				this should not be possible here due to the the upper pot being locked

				say free_pot_span(last_in_line) the upper pot of this will be locked,
				so if it happens we have free_pot_span(upper_pot) then it will be locked
			
				NOTE: no locking needed for 'bw';
			*/
			bw = blpot(p);
			/*
				bw must be free for this to work
			*/
			if(!(bw->flags&POT_FREE)){
				goto _sktoelse;
			}
			struct lot *lot;
			lot = sidespan(p,bw,upper);
			/*
				if we returned NULL then this means that 'bw' along the way became invalid
				and no longer exists
			*/
			if(!lot){
				/*
					if this is the case then a check such as 'below_flags(p)&POT_LAST_IN_LINE' should come back true
				
					we fallback to this
				*/
				goto _sktoelse;
			}
			lot->size_in_pots+=__span;
			spanlgn(p->lot)+=__span;
			mt_unlock(&lot->lock);
		}else{
		_sktoelse:
			AR_MESG(ARMSG_LVK|ARMSG_POT,"(pot_span)(%u)(last_in_line).\n",in_lpts(__span));
			pot_span(p,__span);
		}
		spaninit(p,__span);
	}
	mt_unlock(&upper->apd.lock);
	mt_unlock(&p->apd.lock);
}
/*
this is unsightly
*/
void lspot_unlink(struct chain_struc *p, struct chain_struc **tops,_int_u __x){
	if(p == tops[__x]){
		tops[__x] = p->fd;
		potp b;
		if((b = tops[__x]) != NULL){
			b->bk = NULL;
		}
		return;
	}

	if(!p->fd){
		((struct chain_struc*)p->bk)->fd = NULL;
		return;
	}

	((struct chain_struc*)p->bk)->fd = p->fd;
	((struct chain_struc*)p->fd)->bk = p->bk;
}
void lspot_decompose(potp p, _64_u __amo){
	AR_MESG(ARMSG_LVK|ARMSG_POT,"decomprising greater pot back into its building block form of lesser pots\n");
	/*
		reinduct the pot as its decomposed form in lesser pots.
	*/
	pot_span(p,__amo);
	spaninit(p,__amo);
}
/*
	decomposing greater pots is time consuming as it means initing its sub pots and such.
	so we are better of a strategy of the person who want to use it need to init it else its left in its original state. 


	NOTE:
	 unlike lots, this is used for greater and POTFITS in conjunction with lesser lots.
	. all pots stay uninited.
*/
#ifdef IGNORE
void gtdrt(potp __p){
	get_potrel(p,1)->flags = POT_DIRTY;
	get_potrel(p,2)->flags = POT_DIRTY;
	get_potrel(p,3)->flags = POT_DIRTY;
}
#endif

void static gpot_decommission(potp p, struct arena *anr){
	pot_newtag(p);
	struct arena *ran;
	ran = myarena;
	if(ran->stg_n>0){
		p->flags |= POT_STAG;
		ran->stagnation[--ran->stg_n] = p;
		return;
	}
	potp up;
	up = abgpot(p);
	mt_lock(&p->apd.lock);
	mt_lock(&up->apd.lock);
	
	_16_u tags = 0;
_entice:
	if(!p->off){
		potp up;
		up = abgpot(p);
		/*
			TODO:
				add some DONT_TOUCH flag or somthing to POT_FREE is valid later on
		
			if this is ture then it might be good to decompose it. this makes sure that 
			we take a lot from another pot as we are not to create are own(only if theres nothing else we can do).
		*/

		/*
			this is diffrent to complete dissolveing,
			if its not inited then do the bare minimum
			and leave it up to some one else.


			look im unsure about this, think about it this way
			lets say we init it and it makes its way back into the
			hands of greater pots. that would mean initing it was pointless.

			we have a couple situations.
			
downsides:

			- we are using only lesser pots.
			this means we for each pot have to check the flags before hand 
			to see if its a pot that needs to be inited. this would be a complete wast of time!.
			
			- we are using only greater pots.
			this means writing to flags that have no point or use.

			
upsides:
			- if we happen to extend a POTFIT region then we are overwriting the bare minimum.

			- if we recoup a greater pot then not alot of effort has been not in to initing the pots.


		*/
		/*
		if(!(up->flags&POT_FREE)){
			gtdrt(p);
			pot_span(p,LGRPOT_SIZE);
		}
*/
	}
	//HERE	
	AR_MESG(ARMSG_LVK|ARMSG_POT,"greater pot removal.\n");
	if(p->limit>0){
		AR_MESG(ARMSG_LVK|ARMSG_POT,"greater pot found to be incomplete.\n");
		if(attempt_recombine(anr,p) == -1){
			/*
				if recombine was a failure then mark here,
				to tell others that we got decomposed
			*/
			ar_assert(anr->grp[p->gr],"this cant be.\n");
			anr->grp[p->gr] = 0;
		}else{
			tags = PT_FAC2_FATE_RECOMBINED;
			goto _entice;
		}
	}
_stepover:
	p->flags &= ~POT_GREATER;
	p->real_flags = 0;
// FIXME:
//	up = _64u_(p)-(LGRPOT_SIZE-p->limit);
	pot_curtag(p) = PT_FAC0_FATE|PT_FAC1_FATE_DECOMPOSED|PT_GREATER|tags;
	lspot_decompose(p,LGRPOT_SIZE-p->limit);			
	ar_assert(up->underneath == LGRPOT_SIZE,"no.\n");
	//must be left for last
	up->underneath = 0;
	mt_unlock(&p->apd.lock);
	mt_unlock(&up->apd.lock);
}

void
_m_free(void *__p) {
	/*
		make sure no NULL pointer is passesed by accident
	*/
	if (!__p) {
#ifdef __ffly_debug
		ar_printf("error: got null ptr.\n");
#endif
		return;
	}

	balep blk;	
	blk = up2bale(__p);

	/*
		if the bale is mmaped then... yea
	*/
	if (is_flag(blk->flags, BALE_MMAPED)) {
		AR_MESG(ARMSG_LVK|ARMSG_BALE,"bale is found to have been allocated by the system.\n");
		_sysfree(blk);
		return;
	}

	_16_u flags;
	_16_u
	ilp = 1<<((_64u_(__p)&LGRPOT_ILVBITS)>>POT_SIZE_SHIFT);
	potp gtp;
	gtp = get_lspot(__p);
	flags = gtp->real_flags;
	/*
		its illegal to free somthing thats already been freed
	*/
	if (is_free(blk)) {
#ifdef __ffly_debug
		ar_printf("error: bale has already been freed, at: %p\n",blk);
#endif
		return;
	}

	perf_free_stats(blk->size-blk->pad);
	struct arena *anr;
	if((flags&ilp)>0) {
		AR_MESG(ARMSG_LVK|ARMSG_BALE,"bale is apart of larger set.\n");
		_64_u pot;
		pot = (_64u_(__p)|LGRPOT_MASK)^sizeof(struct pot);
		potp p = pot;
		anr = arena_at(p->acm);
		ar_assert(p->off>p->limit,"what the fuck.\n");
		lkpot(p);
		_ar_sbfree(p,blk);
		ar_assert(p->limit == 0,"this should not be.\n");//REMOVE:
		if(p->off == p->limit){
			mt_lock(&anr->potbin_lock);
			lspot_unlink(p,anr->potbin,0);
			mt_unlock(&anr->potbin_lock);
			ulpot(p);
			gpot_decommission(p,anr);
		}else{
			ulpot(p);
		}
		return;
	}else{
		ar_printf("bale is not apart of any larger-set.\n");
	}

	if(is_flag(blk->flags,BALE_SHAM)){
		return;
	}
//	deal_with_subsidiarys(myarena);

	_m_free_lesser(blk);
}

void*
_m_realloc(void *__p, _int_u __bc) {
	if (!__p) {
#ifdef __ffly_debug
	ar_printf("error: got null ptr.\n");
#endif
		return NULL;
	}

	if(!__bc){
#ifdef __ffly_debug
	ar_printf("no such thing as a reloc with zero size.\n");
#endif
		return NULL;
	}
	struct arena *anr;
	anr = myarena;

	ar_uint_t size;
	ar_int_t dif;
	balep blk;
	void *p;
	potp pt;
	blk = up2bale(__p);
	size = blk->size-blk->pad;
	dif = (ar_int_t)size-(ar_int_t)__bc;
#ifndef barebone
	if(!is_flag(blk->flags, BALE_MMAPED)) {
		potp gtp;
		gtp = get_lspot(blk);
		_16_u flags;
		flags = gtp->real_flags;
		_16_u ilp;
		ilp = 1<<((_64u_(__p)&LGRPOT_ILVBITS)>>POT_SIZE_SHIFT);
		if(!(((_64_u)__p)&MASK_OF(POT_SIZE_SHIFT)) && !(flags&ilp)){
			goto _stepover;
		}
		if((flags&ilp)>0) {
			goto _stepover;
		}
		
		pt = get_pot(blk);

		_8_s has_lock = -1;
		if(pt->acm != acm_idx(myarena->acm)){
			/*
				if the bale is small then i might be a better idea to just change ownership,
				for larger ones not realy.
			*/
			if(__bc<64){
				if(mt_trylock(&pt->inuse_lock) == -1){
					goto _stepover;
				}
			}else{
				lkpot(pt);
			}
			has_lock = 0;
		}

		if(dif>0) {
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"shrink.\n")
			if ((p = ffly_arsh(__p, __bc)) != NULL) {
				AR_MESG(ARMSG_LV1|ARMSG_TBD,"shrunk %u bytes.\n", dif)
				return p;
			}
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"can't shrink bale.\n")
		}else if(dif<0) {
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"grow.\n")
			if ((p = ffly_argr(__p, __bc)) != NULL) {
				AR_MESG(ARMSG_LV1|ARMSG_TBD,"shrunk %u bytes.\n", -dif)
				return p;
			}
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"can't grow bale.\n")
		}
		if(!has_lock){
			ulpot(pt);
		}
	}else{
		void *r;
		if((r = _sysresize(blk,__bc)) != (void*)-1){
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"success in resizing mapping, %p\n",r)
			return r;
		}else{
			AR_MESG(ARMSG_LV1|ARMSG_TBD,"couldent resize mapping.\n")
		}
//do remap
	}
#endif
_stepover:
	if (!(p = ffly_balloc(__bc))) {
		// failure
	}

	AR_MESG(ARMSG_LV1|ARMSG_TBD,"prev size: %u, new size: %u, copysize: %u\n", size, __bc, size<__bc?size:__bc)
	copy(p, __p, size<__bc?size:__bc);
	ffly_bfree(__p);
	return p;
}
// migrate to diffrent pot
void static
migrate(potp __spt, potp __dpt, balep __src) {
	while(1);
	void *src = (void*)((_8_u*)__src+bale_size);
	void *dst = _ar_balloc_guarded(__dpt, __src->size);
	copy(dst, src, __src->size);
	_ar_bfree_guarded(__spt,__src);
}

void static
reloc(potp __pot, balep __p, ar_off_t __off) {
	
}

void static
ffly_reloc(void *__p, ar_off_t __off) {
	
}

void static
atb(potp __pot, balep __b) {
	ar_off_t *bin;
	if (not_null(__b->fd = *(bin = bin_at(__pot, __b->size)))) {
		get_bale(__pot, *bin)->bk = bale_offset(__b);
	}

	*bin = bale_offset(__b);
	__b->bk = AR_NULL;
}

/*
	a bale inside a bale?
*/
void _ar_dislodge(balep __bale, _int_u __offset, _int_u __span) {
	balep ft,rr;
	ft = bale_next(__bale);
	rr = bale_prev(__bale);

	if(__span<bale_size+64) {
		return;
	}

	balep bale;
	bale = ((_8_u*)__bale)+__offset;

	*bale = (struct bale){
		.prev = AR_NULL,.next = AR_NULL,
		.size = __span,.pad = 0,
		.fd = AR_NULL,.bk = AR_NULL,
		.flags = BALE_SHAM
	};
	auctionoff(get_pot(__bale),bale);
}

void ar_dislodge(void *__p, _int_u __offset, _int_u __span) {
	_ar_dislodge(((_8_u*)__p)-bale_size,__offset,__span);
}

void ffly_dispose(void *__p, _int_u __span) {
//	void *p0 = _ar_balloc(&main_pot, 20);
//	void *p1 = _ar_balloc(&main_pot, 20);
//	_ar_bfree(&main_pot, p0);
//	potp p = lookup(__p);
//	if(!p) {
//		ar_printf("pot is dead.\n");
//		return;
//	}

//	dispose(p, (_8_u*)__p-(_8_u*)p->end, __span);
}
// realloc without copy
void* f_douse(void *__p, _int_u __nsize) {
	ffly_bfree(__p);
	return ffly_balloc(__nsize);
}
