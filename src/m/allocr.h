# ifndef __ffly__allocr__h
# define __ffly__allocr__h
#define _hooks_permitted
#include "../y_int.h"
#include "../types.h"
#include "../ffly_def.h"
#include "../proc.h"
/*
	for system/thread.c
	this might be removed later but for now in thread.c
	the thread data is stored in the main process memory 
	until we are finished here.
*/
typedef void const* ffly_potp;

struct ff_ar_info {
	_int_u bale_c;
	_64_u dead, used, buried;
};

void ff_ar_info(struct ff_ar_info*);
extern void(*ar_erase)(void*, _int_u);
// rename
/*
	tls, etc
*/
void ffly_process_prep(void);
_f_err_t ffly_ar_init(void);
_f_err_t ffly_ar_cleanup(void);
/*
	hang pot/subpots | kill them off
*/
void ffly_arhangi(void);
void ffly_arhang(void*);
void ffly_arstat(void);

# define ffly_alloc(__size) \
	ffly_balloc(__size)
# define ffly_free(__p) \
	ffly_bfree(__p)
# define ffly_realloc(__oldp, __nsz) \
	ffly_brealloc(__oldp, __nsz)

#define ffly_balloc		m_alloc
#define ffly_bfree		m_free
#define ffly_brealloc	m_realloc
#ifdef _hooks_permitted
extern void*(*__m_alloc)(_int_u);
extern void(*__m_free)(void*);
extern void*(*__m_realloc)(void*,_int_u);
void*m_alloc(_int_u);
void m_free(void*);
void* m_realloc(void*, _int_u);
#else
#define m_alloc			_m_alloc
#define m_free			_m_free
#define m_realloc		_m_realloc
#endif
// allocate block
void* _m_alloc(_int_u);
// free block
void _m_free(void*);

// realloc block
void* _m_realloc(void*, _int_u);
void* f_douse(void*, _int_u);
/*
	TODO:
		rename arsh argr - why because there for getting rid of small amount
		of space no larger then 16 i think?

		what it does?
			puts the spare space up for sale to the upper or lower block, or
			if the u or l block is freed?? or alloced it will become apart of that
			block and not this

		TODO:
			allow for space to float up or down when alloc or free



			-----------------------------
			|							|	block 0
			-----------------------------
			-----------------------------
			|							|	block 1
			-----------------------------

			XXXXXXXXXXXXXXXXXXXXXXXXXXXXX	<- free space after alloc or free
			-----------------------------
			|							|	block 2
			-----------------------------

			if upper block get freed or alloced push free space up to where X <- if needed
			this should depend on other things

			*****************************	extra space thats left over after resize
			-----------------------------
			|							|	block 3
			-----------------------------
*/
//trim
/*should only be used for trimming small amount off*/
// non user functions or to be used before using
void* ffly_arsh(void*, _int_u);
void* ffly_argr(void*, _int_u);
void* s_alloc(_int_u,_64_u);
/*
	rename, used it for debugging
*/
void ffly_arbl(void*);
void ar_dislodge(void *__p, _int_u __offset, _int_u __span);
# endif /*__ffly__allocr__h*/
