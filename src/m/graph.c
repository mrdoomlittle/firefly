#include "allocr.h"
#include "../string.h"
#include "../rand.h"
#include "../ffly_def.h"
#include "../y_int.h"
#include "../time.h"
#include "../linux/time.h"
#include "../graph.h"
#ifdef _hooks_permitted

struct graph graph;

#define apply_errorcorrection(__delta)\
	__delta -= __delta<150?__delta:150;
static _int_u cnt = 0;
void static* do_malloc(_int_u __size){
  struct timespec ts,te;
  void *r;
  clock_gettime(CLOCK_MONOTONIC,&ts);
  r = _m_alloc(__size);
  clock_gettime(CLOCK_MONOTONIC,&te);
	_64_u delta;
  delta = (te.tv_sec-ts.tv_sec)*1000000000+(te.tv_nsec-ts.tv_nsec);
	apply_errorcorrection(delta);
	graph_plot(&graph,cnt++,delta);
  return r;
}
void static do_free(void *__ptr){
  struct timespec ts,te;
	clock_gettime(CLOCK_MONOTONIC,&ts);
 	_m_free(__ptr);
  clock_gettime(CLOCK_MONOTONIC,&te);
 	_64_u delta;
  delta = (te.tv_sec-ts.tv_sec)*1000000000+(te.tv_nsec-ts.tv_nsec);
  /*
		150ns of error
	*/
	apply_errorcorrection(delta);
	graph_plot(&graph,cnt++,delta);
}

void graphp(void){
	graph_open(&graph,"graph.plt");
//	__m_free = do_free;


	__m_alloc = do_malloc;
}

void graph_finish(void){
 graph_close(&graph);
}

#endif
