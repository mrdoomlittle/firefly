#!/bin/sh
flags="-O3 -g -D__ffly_crucial -mincoming-stack-boundary=4 -D__f_debug -D__ffly_mscarcity -D__ffly_debug -fno-builtin -D__ffly_no_task_pool -D__ffly_use_allocr -D__fflib -D__ylib -D__ffly_source -D__slurry_client"
$ffly_cc $flags -c -o $dst_dir/allocr.o $root_dir/allocr.c
$ffly_cc $flags -c -o $dst_dir/ar_sys.o $root_dir/ar_sys.c
$ffly_cc $flags -c -o $dst_dir/ar_info.o $root_dir/ar_info.c
$ffly_cc $cc_flags -c -o $dst_dir/alloca.o $root_dir/alloca.c
export ffly_objs="$dst_dir/allocr.o $dst_dir/ar_info.o $dst_dir/ar_sys.o $dst_dir/alloca.o"
