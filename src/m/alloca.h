# ifndef __ffly__alloca__h
# define __ffly__alloca__h
/*
    small allocations only.
    frame method.
*/
# include "../y_int.h"
# include "../types.h"
# ifdef __cplusplus
extern "C" {
# endif
// the real one
void *_alloca(_32_u);
void *ffly_frame();
void* ffly_alloca(_int_u, _f_err_t*);
void ffly_trim(_int_u);
void ffly_collapse(void*);
void ffly_alloca_cleanup();
void ffly_alss(void*, _int_u);
void ffly_alsst(void*);
void ffly_alsld(void*, _int_u);
_int_u ffly_alssz();
void ffly_alrig();
void ffly_alrr();
void ffly_alad(void**);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__alloca__h*/
