# include "file.h"
# include "../m_alloc.h"
# include "../io.h"
# include "errno.h"
# include "../string.h"
# include "error.h"
# include "fs.h"
# define is_flag(__flags, __flag) \
	(((__flags)&(__flag))==(__flag))
# ifndef __fflib
# include <string.h>
/*
	TODO:
		i dont think we need to keep checking the file desc if valid
		remove valid_fd from r/w routines
	allow for flags to be passed to write to bypass the buffer for errors
*/
/// might be right or wong dont know i think its corrent???
char const static* mode_str(_32_u __flags) {
	char static buf[24];
	char *p = buf; 
	if (is_flag(__flags, FF_O_RDWR)) {
		*(p++) = 'r';
		*(p++) = '+';
	} else if (is_flag(__flags, FF_O_WRONLY) && is_flag(__flags, FF_O_CREAT) && is_flag(__flags, FF_O_TRUNC))
		*(p++) = 'w';
	else if (is_flag(__flags, FF_O_RDWR) && is_flag(__flags, FF_O_CREAT) && is_flag(__flags, FF_O_TRUNC)) {
		*(p++) = 'w';
		*(p++) = '+';
	} else if (is_flag(__flags, FF_O_WRONLY)) {
		*(p++) = 'w';
	} else {
		*(p++) = 'r';
	}
	*p = '\0';
	return (char const*)buf;
}
# endif

void ffly_fopt(struct ffly_file *__f, _8_u __flag) {
	__f->flags |= __flag;
}

_f_err_t drain(struct ffly_file *__f) {	
	_8_s err;
	_int_u size;
	if (!(size = __f->ob_p-__f->obuf))
		return FFLY_SUCCESS;
	if (is_flag(__f->flags, FF_STREAM)) {
		if (fswrite(__f->fn, __f->obuf, size, &err) == -1) {

		}
	} else {
		// not tested
		if (fspwrite(__f->fn, __f->obuf, size, __f->bufdst, &err) == -1) {
			ffly_fprintfs(ffly_err, "failed to write to file, %s\n", __f->path);
			return FFLY_FAILURE;
		}
		__f->bufdst+=size;
	}
	__f->ob_p = __f->obuf;
	return FFLY_SUCCESS;
}

_f_err_t ffly_fdrain(struct ffly_file *__f) {
	_f_err_t err;
	mt_lock(&__f->lock);
	err = drain(__f);
	mt_unlock(&__f->lock);
	return err;
}

struct ffly_file*
ffly_fopen(char const *__path, int __flags, _32_u __mode, _f_err_t *__err) {
	struct ffly_file *file = (struct ffly_file*)m_alloc(sizeof(struct ffly_file));
	if ((file->fn = fsopen(__path, __flags, __mode)) == -1) {
		ffly_fprintfs(ffly_err, "file, failed to open file, error: %d, %s\n", errno, strerror(errno));
		return NULL;
	}

	if (_err(mem_dup((void**)&file->path, __path, str_len(__path)+1))) {
		ffly_fprintfs(ffly_err, "failed to dupe file path.\n");
		*__err = FFLY_FAILURE;
		return NULL;
	}
	*__err = FFLY_SUCCESS;
#ifndef __fflib
	printf("%s\n", mode_str(__flags));
	file->libc_fp = fdopen(dup(file->fd), mode_str(__flags));
	fchmod(fileno(file->libc_fp), __mode);
#endif
	file->drain = -1;
	file->bufdst = 0;
	file->off = 0;
	file->flags = 0x0;
	file->ob_p = file->obuf;
	file->lock = FFLY_MUTEX_INIT;
	return file;
}

_f_bool_t static valid_fd(int __fd) {
/*
	workout diffrent way, i think file desc work from 0 to ...
	so putting this in a map of _64_u where if bit is high then 
	its valid, issue doing this would be gaps of wasted memory.
	i dont know
*/

	if (fcntl(__fd, F_GETFD, 0) == -1)
		return 0;
	return 1;
}

void f_fsopen(struct f_file_struc *__struc, _int_u __n) {
	struct f_file_struc *s;
	_int_u i;
	for(;i != __n;i++) {
		s = __struc+i;
		*s->f = ffly_fopen(s->path, s->flags, s->mode, &s->error);
	}
}

_f_err_t ffly_fstat(char const *__path, struct ffly_stat *__stat) {
	struct stat st;
	mem_set(&st, 0, sizeof(struct stat));
	if (stat(__path, &st) == -1) {
		ffly_fprintfs(ffly_err, "file, failed to stat file, errno: %d, %s\n", errno, strerror(errno));
		return FFLY_FAILURE;
	}

	*__stat = (struct ffly_stat) {
		.size = st.st_size
	};
	return FFLY_SUCCESS;
}

_f_off_t ffly_fseek(struct ffly_file *__f, _f_off_t __off, int __whence) {
	_8_s err;
	__linux_off_t off = fslseek(__f->fn, __off, __whence, &err);
	if (off == (__linux_off_t)-1) {
		ffly_fprintfs(ffly_err, "fseek failed.\n");
		return 0;
	}

	if (!__whence && __off != __f->off) {
		__f->off = __off;
		__f->drain = 0;
	}
	return (_f_off_t)off;
}

_f_err_t ffly_fcreat(char const *__path, _32_u __mode) {
	_8_s err;
	if (fsaccess(__path, F_OK, &err)) {
		ffly_fprintfs(ffly_err, "file at '%s' allready exists.\n");
		return FFLY_FAILURE;
	}

	if (fscreat(__path, __mode, &err) < 0) {
		ffly_fprintfs(ffly_err, "file, failed to create file, errno: %d, %s\n", errno, strerror(errno));
		return FFLY_FAILURE;
	}
	return FFLY_SUCCESS;
}

# define bufput(__f, __p, __size) \
	mem_cpy(__f->ob_p, __p, __size); \
	__f->ob_p+=__size

// solid write will skip buffer
_f_err_t ffly_fwrites(struct ffly_file *__f, void *__p, _int_u __bc) {
	_8_s err;
	mt_lock(&__f->lock);
	if (__f->ob_p>__f->obuf)
		drain(__f);
	fswrite(__f->fn, __p, __bc, &err);
	mt_unlock(&__f->lock);
	return FFLY_SUCCESS;
}

_f_err_t ffly_fpread(struct ffly_file *__f, void *__p, _int_u __bc, _int_u __offset) {
	_8_s err;
	fspread(__f->fn, __p, __bc, __offset, &err);
}

_f_err_t ffly_fpwrite(struct ffly_file *__f, void *__p, _int_u __bc, _int_u __offset) {
	_8_s err;
	fspwrite(__f->fn, __p, __bc, __offset, &err);
}

_f_err_t f_fwrite(struct ffly_file *__f, void *__p, _int_u __bc, _32_u *__got) {
	mt_lock(&__f->lock);
	_8_s err;
	if (is_flag(__f->flags, FF_NOBUFF)) {
		fswrite(__f->fn, __p, __bc, &err);	
		// goto _end; <<- remove else
	} else {
	if (!__f->drain) {
		drain(__f);
		__f->bufdst = __f->off;
		__f->drain = -1;
	}
	mt_unlock(&__f->lock);

	_8_u *src = (_8_u*)__p;
	while(__bc>OBUFSZ) {
		ffly_fwrite(__f, src, OBUFSZ);
		src+=OBUFSZ;
		__bc-=OBUFSZ;
	}

	mt_lock(&__f->lock);
	_int_u left;
	_int overflow;

	if (!__bc) {
		goto _end;
	}

	left = OBUFSZ-(__f->ob_p-__f->obuf);
	if (!left) {
		drain(__f);
		left = OBUFSZ;
	}

	overflow = (_int)__bc-(_int)left;
	__f->off+=__bc;
	if (overflow>0) {
		bufput(__f, src, left);
		drain(__f);
		src+=left;
		__bc = overflow;
	}

	bufput(__f, src, __bc);
	}
_end:
	mt_unlock(&__f->lock);
	return FFLY_SUCCESS;
}

_f_err_t f_fread(struct ffly_file *__f, void *__p, _int_u __bc, _32_u *__got) {
	_8_s err;
	_32_s res;
	res = fsread(__f->fn, __p, __bc, &err);
	if (err<0) {
		ffly_fprintfs(ffly_err, "failed to read file, %s\n", __f->path);

		return FFLY_FAILURE;
	}
	if (__got != NULL)
		*__got = res;
	return FFLY_SUCCESS;
}

_f_err_t ffly_fclose(struct ffly_file *__f) {
	drain(__f);
	_8_s _err;
	if ((_err = fsclose(__f->fn)) == -1) {
		
	}
# ifndef __fflib
	fclose(__f->libc_fp);
# endif
	_f_err_t err;
	m_free((void*)__f->path);
//	if (_err(err = m_free(__f))) {
//ffly_fprintf(ffly_err, "failed to free file.\n");
//		return err;
//	}
	m_free(__f);
	return FFLY_SUCCESS;
}

void f_fxclose(struct f_file_struc *__struc, _int_u __n) {
	struct f_file_struc *str;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		str = __struc+i;
		str->error = ffly_fclose(str->f_in);
	}
}
