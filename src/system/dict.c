# define __ffly_dict_internal
# include "dict.h"
# include "../ffly_def.h"
# include "../m_alloc.h"
# include "../string.h"
/* not tested */
		
_64_u static
sum(char const *__key, _int_u __len) {
	_64_u ret = 0xffffffffffffffff;
	char const *p = __key;
	char const *end = p+__len;
	while(p != end) {
		ret = ret^*(p++);
		ret = ret>>56|ret<<8;
	}
	return ret;
}

_f_err_t ffly_dict_init(ffly_dictp __dict) {
	ffly_lat_prepare(&__dict->lat);
	__dict->head = NULL;
}

void* ffly_dict_head(ffly_dictp __dict) {
	return (void*)__dict->head;
}

void ffly_dict_fd(void **__p) {
	*__p = (void*)((entryp)*__p)->fd;
}

void const* ffly_dict_getp(void *__p) {
	return ((entryp)__p)->p;
}

_f_err_t ffly_dict_put(ffly_dictp __dict, char const *__key, void const *__p) {
	_int_u l = str_len(__key);
	_64_u val = sum(__key, l);

	entryp p;
	p = (entryp)m_alloc(sizeof(struct entry));
	p->next = (entryp)ffly_lat_get(&__dict->lat, val);
	p->key = (char const*)str_dup(__key);
	p->p = __p;

	p->fd = __dict->head;
	__dict->head = p;
	return;
	ffly_lat_put(&__dict->lat, val, p);
}

void const* ffly_dict_get(ffly_dictp __dict, char const *__key, _f_err_t *__err) {
	_int_u l = str_len(__key);
	_64_u val = sum(__key, l);

	entryp cur = (entryp)ffly_lat_get(&__dict->lat, val);	
	while(cur != NULL) {
		if (!str_cmp(cur->key, __key)) return cur->p;
		cur = cur->next;
	}
	return NULL;
}

_f_err_t ffly_dict_de_init(ffly_dictp __dict) {
	void *cur = ffly_lat_head(&__dict->lat);
	while(cur != NULL) {
		entryp p = (entryp)ffly_lat_getp(cur);
		m_free((void*)p->key);
		m_free(p);
		ffly_lat_fd(&cur);
	}

	ffly_lat_free(&__dict->lat);
}
