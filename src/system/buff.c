# include "buff.h"
# include "../io.h"
# include "err.h"
# include "errno.h"
# include "../m_alloc.h"
# include "../string.h"
# include "../mutex.h"
# include "../msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSBUFF)
#define __FSPS __buff
#ifndef FF_BUFF_SA
#define mem_alloc(__n)\
    m_alloc(__n)  
#define mem_free(__p)\
    m_free(__p)
#define mem_realloc(__p, __n)\
    m_realloc(__p, __n)
#else
#define mem_alloc(__n)\
	__FSPS->ad->alloc(__FSPS->ma_arg, __n)
#define mem_free(__p)\
	__FSPS->ad->free(__FSPS->ma_arg, __p)
#define mem_realloc(__p, __n)\
	__FSPS->ad->realloc(__FSPS->ma_arg, __p, __n)
#endif

/*
	we could align blksize but would be a wast

	remove *
*/
_f_err_t _ffly_buff_init(ffly_buffp __FSPS, _int_u __blk_c, _f_size_t __blk_size, struct ei_struc *__ei) {	
#ifdef FF_BUFF_SA
	if (__ei != NULL) {
		__FSPS->ad = __ei->ad;
		goto _sk;
	}
	__FSPS->ad = ffly_ad_entry(0);
_sk:
#endif
	if ((__FSPS->p = mem_alloc(__blk_c*__blk_size)) == NULL) {
		MSG(ERROR, "failed to allocate memory.\n")
		return FFLY_FAILURE;
	}
	__FSPS->lock = FFLY_MUTEX_INIT;
	__FSPS->off = 0;
	__FSPS->ofof = 0;
	__FSPS->blk_c = __blk_c;
	__FSPS->rs_blk_c = __blk_c;
	__FSPS->blk_size = __blk_size;
	return FFLY_SUCCESS;
}
// get pointer at the current offset
void* ffly_buff_getp(ffly_buffp __FSPS) {
	return (_8_u*)__FSPS->p+(__FSPS->off*__FSPS->blk_size);
}
// get pointer at specific offset
void* ffly_buff_at(ffly_buffp __FSPS, _int_u __off) {
	return (_8_u*)__FSPS->p+(__off*__FSPS->blk_size);
}

_f_err_t f_buf_xput(f_buffp __FSPS, void *__src, _int_u __x) {
	mt_lock(&__FSPS->lock);
	mem_cpy((_8_u*)__FSPS->p+(__FSPS->off*__FSPS->blk_size), __src, __x*__FSPS->blk_size);
	mt_unlock(&__FSPS->lock);
	return F_SUCCESS;
}

_f_err_t f_buf_xget(f_buffp __FSPS, void *__dst, _int_u __x) {
	mt_lock(&__FSPS->lock);
	mem_cpy(__dst, (_8_u*)__FSPS->p+(__FSPS->off*__FSPS->blk_size), __x*__FSPS->blk_size);
	mt_unlock(&__FSPS->lock);
	return F_SUCCESS;
}

_f_err_t ffly_buff_put(ffly_buffp __FSPS, void *__p) {
	_f_err_t err;
	mt_lock(&__FSPS->lock);
	mem_cpy((_8_u*)__FSPS->p+(__FSPS->off*__FSPS->blk_size), __p, __FSPS->blk_size);
	mt_unlock(&__FSPS->lock);
	return FFLY_SUCCESS;
}

_f_err_t ffly_buff_get(ffly_buffp __FSPS, void *__p) {
	_f_err_t err;
	mt_lock(&__FSPS->lock);
	mem_cpy(__p, (_8_u*)__FSPS->p+(__FSPS->off*__FSPS->blk_size), __FSPS->blk_size);
	mt_unlock(&__FSPS->lock);
	return FFLY_SUCCESS;
}

_f_err_t _f_buf_incr(ffly_buffp __FSPS, _32_u __by) {
	_f_err_t err = FFLY_SUCCESS;
	mt_lock(&__FSPS->lock);
	if (__FSPS->off == __FSPS->blk_c-1) {
		MSG(WARN, "can't incrment any further.\n")
		err = FFLY_FAILURE;
	} else {
		__FSPS->off+=__by;
	}
	mt_unlock(&__FSPS->lock);
	return err;
}

_f_err_t _f_buf_decr(ffly_buffp __FSPS, _32_u __by) {
	_f_err_t err = FFLY_SUCCESS;
	mt_lock(&__FSPS->lock);
	if (!__FSPS->off) {
		MSG(WARN, "can't de-incrment any further.\n")
		err =  FFLY_FAILURE;
	} else {
		__FSPS->off-=__by;
	}
	mt_unlock(&__FSPS->lock);
	return err;
}

_f_err_t ffly_buff_resize(ffly_buffp __FSPS, _int_u __blk_c) {
	if ((__FSPS->p = mem_realloc(__FSPS->p, (__FSPS->blk_c = __blk_c)*__FSPS->blk_size)) == NULL) {
		ffly_fprintf(ffly_err, "buff: failed to realloc memory.\n");
		return FFLY_FAILURE;
	}
	return FFLY_SUCCESS;
}

_f_err_t ffly_buff_reset(ffly_buffp __FSPS) {
	_f_err_t err;
	__FSPS->off = 0;
	if (_err(err = ffly_buff_resize(__FSPS, __FSPS->rs_blk_c))) {
		ffly_fprintf(ffly_err, "buff: failed to resize.\n");
		return FFLY_FAILURE;
	}
	return FFLY_SUCCESS;
}

_f_err_t ffly_buff_de_init(ffly_buffp __FSPS) {
	_f_err_t err;
	if (_err(err = mem_free(__FSPS->p))) {
		ffly_fprintf(ffly_err, "buff: failed to free.\n");
		return err;
	}
	return FFLY_SUCCESS;
}
