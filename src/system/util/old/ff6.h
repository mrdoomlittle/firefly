# ifndef __ffly__ff6__h
# define __ffly__ff6__h
# include "../../y_int.h"
_int_u ffly_ff6_enc(void const*, char*, _int_u);
_int_u ffly_ff6_dec(char const*, void*, _int_u);
# endif /*__ffly__ff6__h*/
