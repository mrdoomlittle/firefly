# ifndef __ffly__pipe__h
# define __ffly__pipe__h
# include "../y_int.h"
# include "../types.h"
#define FF_PIPE_CREAT 0x1
#define FF_PIPE_SHMM 0x2
#define FF_PIPE_ENC 0x3
/*
	todo remove numeral id base and replace with pointer
	or

	* ffly_pipe

	assign id (*)

	????
*/

_int_u ffly_pipe_get_shmid(_int_u);
_int_u ffly_pipe(_int_u, _8_u, _int_u, _f_err_t*);
_f_err_t ffly_pipe_write(void*, _int_u, _int_u);
_f_err_t ffly_pipe_read(void*, _int_u, _int_u);
void ffly_pipe_shutoff(_int_u);
void ffly_pipe_close(_int_u);
_f_err_t ffly_pipe_listen(_int_u);
_f_err_t ffly_pipe_connect(_int_u);
# define ffly_pipe_wr8l(__val, __pipe) \
    ffly_pipe_wrl(__val, 1, __pipe)
# define ffly_pipe_wr16l(__val, __pipe) \
    ffly_pipe_wrl(__val, 2, __pipe)
# define ffly_pipe_wr32l(__val, __pipe) \
    ffly_pipe_wrl(__val, 4, __pipe)
# define ffly_pipe_wr64l(__val, __pipe) \
    ffly_pipe_wrl(__val, 8, __pipe)

# define ffly_pipe_rd8l(__pipe, __err) \
    ffly_pipe_rdl(1, __pipe, __err)
# define ffly_pipe_rd16l(__pipe, __err) \
    ffly_pipe_rdl(2, __pipe, __err)
# define ffly_pipe_rd32l(__pipe, __err) \
    ffly_pipe_rdl(4, __pipe, __err)
# define ffly_pipe_rd64l(__pipe, __err) \
    ffly_pipe_rdl(8, __pipe, __err)

_f_err_t ffly_pipe_wrl(_64_u, _8_u, _int_u);
_64_u ffly_pipe_rdl(_8_u, _int_u, _f_err_t*);
# endif /*__ffly__pipe__h*/
