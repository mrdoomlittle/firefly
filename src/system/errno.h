# ifndef __ffly__errno__h
# define __ffly__errno__h
# include "../y_int.h"
# ifdef __fflib
# include "../linux/errno.h"
# else
# include <errno.h>
# endif
# ifdef __ffly_use_opencl
#   include <CL/cl.hpp>
#	define ffly_cl_success CL_SUCCESS
# endif
# ifdef __ffly_use_cuda
#   include <cuda_runtime.h>
#	define ffly_cl_success cudaSuccess
# endif

# ifndef FFLY_SUCCESS
#	define FFLY_SUCCESS 0
# endif

# ifndef FFLY_FAILURE
#	define FFLY_FAILURE -1
# endif

/*
	dident succeed or failed
*/
# ifndef FFLY_NOP
#	define FFLY_NOP 1
# endif

# ifndef FF_ERR_NULL
#	define FF_ERR_NULL 0
# endif

// non existent init procedure
# define FF_ERR_NEIP 1

# ifdef __cplusplus
extern "C" {
# endif
# ifdef __fflib
int extern errno;
char const* strerror(int);
# endif
_8_u extern ffly_errno;
# ifdef __cplusplus
}
# endif
# endif /*__ffly__errno__h*/
