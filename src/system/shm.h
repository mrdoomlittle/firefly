# ifndef __ffly__shm__h
# define __ffly__shm__h
# include "../y_int.h"
# include "../types.h"
# define FF_SHM_MCI 0x1
void* ffly_shmget(_int_u*, _int_u, int, _8_u);
_f_err_t ffly_shmdt(_int_u);
_f_err_t ffly_shm_free(_int_u);
void ffly_shm_cleanup(_int_u);
# endif /*__ffly__shm__h*/
