# ifndef __ffly__lat__h
# define __ffly__lat__h
# include "../y_int.h"

/*
	like hash table but cares more about equal distribution
	so 
	ffly_lat_get(NULL, 212999999...)
	takes the same time as
	ffly_lat_get(NULL, 0)
*/

# define ff_lat struct ffly_lat
# ifdef __ffly_lat_internal
typedef struct record {
	struct record *next, *fd;
	_64_u key;
	void *p;
} *recordp;

typedef struct pod {
	void *p;
} *podp;

# endif
typedef struct ffly_lat {
# ifdef __ffly_lat_internal
	podp p;
	recordp head;
# else
	_8_u pad[16];
# endif
} *ffly_latp;

void* ffly_lat_head(ffly_latp);
void ffly_lat_fd(void**);
void* ffly_lat_getp(void*);
void ffly_lat_prepare(ffly_latp);
void ffly_lat_put(ffly_latp, _64_u, void*);
void* ffly_lat_get(ffly_latp, _64_u);
void ffly_lat_free(ffly_latp);
# endif /*__ffly__lat__h*/
