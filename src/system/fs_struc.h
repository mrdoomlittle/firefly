# ifndef __f__system__fs__struc__h
# define __f__system__fs__struc__h
# include "../y_int.h"
# include "../fsus.h"
struct ffly_fsop {
	_8_s(*open)(f_fsus_nodep, char const*, _32_u, _32_u);
	_8_s(*close)(f_fsus_nodep);
	_32_s(*lseek)(f_fsus_nodep, _64_u, _32_u, _8_s*);
	_64_u(*read)(f_fsus_nodep, void*, _64_u, _8_s*);
	_64_u(*write)(f_fsus_nodep, void*, _64_u, _8_s*);
	_64_u(*pwrite)(f_fsus_nodep, void*, _64_u, _64_u, _8_s*);
	_64_u(*pread)(f_fsus_nodep, void*, _64_u, _64_u, _8_s*);
	_32_s(*access)(char const*, _32_u, _8_s*);
	_32_s(*creat)(char const*, _32_u, _8_s*);
	void(*mkdir)(char const*, _8_s*);
};

# endif /*__f__system__fs__struc__h*/
