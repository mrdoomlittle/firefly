# ifndef __ffly__system__config__h
# define __ffly__system__config__h
# include "../y_int.h"
# include "../types.h"

/*
	murger with main config
*/
#define offof(__p) (((_8_u*)(__p))-ffly_sysconf)
#define SCF_LC_EXIT	0x00
#define SCF_LC_PP	0x01
/*
	i dont know, i was going to use a struct
	but this is simpler, and it does not matter to much.


	rundown:

		sysconfig -> ../config -> sysconfig


		so we read sysconfig

		and dump it into the ../config.h
		and then reupdate sysconfig to any changes

		so sysconfig is a non hard config and can be changed
		from external sources.

		this is more of config storage then anything else
		thus the reasion im not using a struct
*/
#define SCF_SZ 102
#define SCF_version			0
#define SCF_max_threads 	8
#define SCF_root_dir 		12
#define SCF_moddir			20
#define SCF_inidir			28
#define SCF_modl			36
#define SCF_inil			44
#define SCF_alssize			52
#define SCF_db_loaded		56
#define SCF_db_ip_addr		57
#define SCF_db_port			65
#define SCF_db_enckey		67
#define SCF_db_user			75
#define SCF_db_passwd		83
#define SCF_loaded			91
#define SCF_bh_ip_addr		92
#define SCF_bh_port			100

#define SCF_TY_version			char const*
#define SCF_TY_max_threads 		_32_u
#define SCF_TY_root_dir 		char const*
#define SCF_TY_moddir			char const*
#define SCF_TY_inidir			char const*
#define SCF_TY_modl				char const**
#define SCF_TY_inil				char const**
#define SCF_TY_alssize			_32_u
#define SCF_TY_db_loaded		_8_i
#define SCF_TY_db_ip_addr		char const*
#define SCF_TY_db_port			_16_u
#define SCF_TY_db_enckey		char const*
#define SCF_TY_db_user			char const*
#define SCF_TY_db_passwd		char const*
#define SCF_TY_loaded			_8_i
#define SCF_TY_bh_ip_addr		char const*
#define SCF_TY_bh_port			_16_u
#define sysconf_get(__what) \
	((SCF_TY_ ## __what*) (ffly_sysconf+(SCF_ ## __what)))
#define SYSC_AT(__what) (*sysconf_get(__what))
extern _8_u ffly_sysconf[];
# define sysconf_loaded \
	(*sysconf_get(loaded))
# define sysconf_db_loaded \
	(*sysconf_get(db_loaded))
# ifdef __cplusplus
extern "C" {
# endif
_f_err_t ffly_ld_sysconf(char const*);
void ffly_ld_sysconf_def(void);
void ffly_free_sysconf(void);
void f_sysconf_moveup(void);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__system__config__h*/
