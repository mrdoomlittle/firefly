# include "fs.h"
# include "../m_alloc.h"
void posix_fsop(struct ffly_fsop*);
void fire_fsop(struct ffly_fsop*);
/*
	i know i could just load the structure and not call a function to load it for me but
	then on the other end if we wanted to setup some stuff we dont be able too.
*/
static void(*load[])(struct ffly_fsop*) = {
	[FF_POSIX_FS] = posix_fsop,
	[FF_FIRE_FS] = fire_fsop
};

static _8_u loaded = 0x00;
static struct ffly_fsop _sysfs[2];
void ffly_sysfs(_8_u __what) {
	struct ffly_fsop *fs;
	fs = _sysfs+__what;
	if (!(loaded&(1<<__what))) {
		load[__what](fs);
		loaded |= 1<<__what;
	}
	F_CRADLE(sys.fs) = fs;
}
f_fsus_nodep f_fs_open(char const *__path, _32_u __flags, _32_u __mode) {
	f_fsus_nodep n;
	n = (f_fsus_nodep)m_alloc(sizeof(struct f_fsus_node));

	n->ops = F_CRADLE(sys.fs);
	n->ops->open(n, __path, __flags, __mode);
	return n;
}

_8_s f_fs_close(f_fsus_nodep __n) {
	_8_s re;
	re =__n->ops->close(__n);
	m_free(__n);
	return re;
}
