#ifndef __fsb__h
#define __fsb__h

struct fsb_val {
	float f;
	char const *s;
	_int_u len;
};
struct fsb_vec {
	struct fsb_val xyzw[4*4];
	//number of components x+y+z+w
	_64_u comp;
};

#endif/*__fsb__h*/
