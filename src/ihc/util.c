#include "ihc.h"
#include "fsl/fsl.h"
#include "../error.h"
#include "../io.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../string.h"
#include "../assert.h"
void static
_open(struct ihc_file *f){
	printf("FILE-OPEN: %s.\n",f->file_params.buf);
	f->fd = open(f->file_params.buf,O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
}

void static
_close(struct ihc_file *f){
	printf("FILE-CLOSE: %s.\n",f->file_params.buf);
	close(f->fd);
}

_32_u static
_write(struct ihc_file *f, void *__buf, _int_u __n) {
 assert(f->fd != -1);
 printf("FILE-WRITE-%d: %u-bytes.\n",f->fd,__n);
 write(f->fd,__buf,__n);

}


void ihc_util_file(struct ihc_file *f){
	f->priv = f;
	f->fd = -1;
	f->open = _open;
	f->close = _close;
	f->write = _write;
}
