#include "fsl.h"
#include "../../strf.h"
#include "../../io.h"
#include "../../assert.h"
#include "../../m_alloc.h"
#include "../../string.h"
#include "../../flue/shader.h"
/*
	DESIGN:

	if we are dealing with an operation that outputs a value.
	then we deal with it like this.

	int x;
	int y;
	int z;

	where it is 'intertwined'

	it = x+y;
	z = it;

	'it' is treated as its own variable.

	somthing like this is a big NO!

	z = x+y;

	this way has issues where as defining properties of the output becomes difficult.

	for example x and y are 32-bit ints at their LIMIT values.
	the output of x+y becomes a 64-bit int.
*/
struct fsl_type types[] = {
	{_fsl_t_void,0},
	{_fsl_t_8_u,_ima_imm},
	{_fsl_t_vec4,_ima_vec,1},
	{_fsl_t_mat4,_ima_mat,4},
	{_fsl_t_float,_ima_float,1},
};

static char const *tytab[] = {
	"VOID",
	"UNSIGNED 8",
	"VEC4",
	"MAT4",
	"FLOAT"
};
static struct ima_il il;
void static
expr(struct fsl_wkstrc *__wk);
void static
_expr(struct fsl_wkstrc *__wk);
void decl_spec(struct yl_token *__tok, struct fsl_type **__ty) {
	*__ty = types+(__tok->id&~yltype_bit);
}
_16_u static
qualifier(void) {
	_16_u q = -1;
	if (yl_tok->kind == _yl_keywd) {
		if (yl_tok->id == _fsl_flat) {
			q = FLUE_INTERP_MODE_FLAT;	
		}else
		if(yl_tok->id == _fsl_smooth) {
			q = FLUE_INTERP_MODE_SMOOTH;
		}else{
			return -1;
		}
		goto _yes;
	}
	return -1;
_yes:
	yl_tok = yl_nexttok();
	return q;
}



void static decl(void) {
	_16_u qtyp;
	qtyp = qualifier();

	struct fsl_type *ty;
	decl_spec(yl_tok,&ty);

	struct fsl_symb *s;
	struct yl_token *name, *in, *out;
 _again:
 	in = NULL;
	out = NULL;
 	name = yl_nexttok();
	if (name->len>2) {
		if (name->s[0] == 'i' && name->s[1] == 'n' && name->s[2]>='0' && name->s[2]<='9') {
			in = name;
			name = yl_nexttok();
		}
	}
	if (name->len>3) {
		if (name->s[0] == 'o' && name->s[1] == 'u' && name->s[3]>='0' && name->s[3]<='9') {
			out = name;
			name = yl_nexttok();
		}
	}
 	assert(name->kind == _yl_ident);
	printf("DECL, %w(%s)\n",name->s,name->len,yl_comm.tytab[ty->kind]);
   s = fsl_symb(name->s,name->len);
	s->ty = ty;
	s->s.em.reg = NULL;
	s->s.em.ident = ty->val;
	s->s.em.cnt = ty->cnt;
	s->s.em.c.comp.comp = (1<<4)|(2<<8)|(3<<12)|(4<<16);
	s->s.em.ys = NULL;
	s->s.em.c.ys = &s->s.em;
	if(s->s.em.ident == _ima_float){
		s->s.em.c.comp.comp = 1<<16;
	}
	if (in != NULL) {
	
		/*
			input of right now we just assume a vec4
		*/
		s->s.ign = -1;
		s->s.em.offset = 0;
		s->s.em.exp = FLUE_EXPORT_PARAM0+_ffly_dsn(in->n,in->nlen);
		hg_pd->interp_mode[s->s.em.exp] = qtyp;
		s->s.em.id = 0;
		s->s.em.ident = _ima_intrp;
		printf("got a input: %u.\n",s->s.em.val);

	}else
	if (out != NULL) {
		s->s.ign = -1;
		s->s.em.id = _ima_param;
		s->s.em.exp = FLUE_EXPORT_PARAM0+_ffly_dsn(out->n,out->nlen);
		s->s.em.ident = _ima_exp;
	}else {
		hg_decl(&s->s.em);
	}
	printf("CNT_VALUE: %u.\n",ty->cnt);
	
	if (!yl_nexttokis(_yl_keychar,_yl_comma)){
		goto _again;
	}
   if (yl_expecttok(_yl_keychar,_yl_semicolon) == -1) {
		printf("no semicolon at end of declerator.\n");
	}
}

/*
	things to keep aware of.

	vec4 vector;
	'vector[0]' is equivalent 'vector.x';

	mat4 matrix;
	'matrix[0]' is equivalent 'matrix.x.x';

	also NOTE: 'vector[0]' can only state 
	a single component. where as 'vector.x'
	can be used like 'vector.xy'
	or

	vector.xy = vector.zw; <= this would be allowed
*/
void static primary_expr(struct fsl_wkstrc *__wk) {
	if (yl_tok->kind == _yl_ident) {
		printf("PRIMARY: '%w'.\n",yl_tok->s,yl_tok->len);
		struct fsl_symb *s = fsl_symb(yl_tok->s,yl_tok->len);
		__wk->s = s;
		
		__wk->em = &s->s.em;
		__wk->x = __wk->em->ident;
		printf("PRIMARY: %u.\n",__wk->x);
		__wk->bits = s->bits;
		__wk->comp = s->s.em.comp;
	}else
	if (yl_tok->kind == __yl_num) {
		printf("NUMBER: %w, - %u\n",__wk->ss = yl_tok->s,__wk->len = yl_tok->len,yl_tok->len);
		__wk->em = m_alloc(sizeof(struct ys_em));
		__wk->em->ident = _ima_const;
		__wk->em->c.ys = __wk->em;
		__wk->x = _ima_const;
		hg_pack_float_str(&__wk->em->vec.xyzw[0].pack,yl_tok->s);
		__wk->em->c.comp.comp = 1<<16;
	}else{
		goto _sk;
	}
	yl_tok = yl_nexttok();
_sk:
	return;
}

/*
	this is very confusing.

	
	vec4 v0;
	vec4 v1;

	[ALLOWED]
	v0.x = v1.x;
	...
	v0.z = v1.z;

	v0.# = v1.#;		[ALLOWED]

	v0.xy = v1.xy;	[ALLOWED]
	v0.xy = v1.zw;	[ALLOWED]

	v0.xy = v1.xyz;	[DENY]

	v0.xyz = v1.xy;	[ALLOWED] fill z with zero???

	float flt;

	this is equivalent to:

	vec4 flt;
	where only flt.x is used.

	'
		float flt;
		flt = 1.0;
	'

	is equivalent to:
	
	'
		vec4 flt;
		flt.x = 1.0;
	'
*/
void static assignment_expr(struct fsl_wkstrc *__wk) {
	iftok(_yl_keychar,_yl_equal) {								
		struct fsl_wkstrc wk;
		_expr(&wk);
		struct ys_em *n;
		n = hg_new_node();
		n->r = wk.em;
		n->l = __wk->em; 
		printf("ASSIGNMENT: LHS: %u, RHS: %u.\n",__wk->x,wk.x);
		n->f = hg_ploped(hg_asgtab,hg_asglhs[__wk->x],hg_asgrhs[wk.x]);
		}else{
		goto _sk;
	}
	yl_tok = yl_nexttok();
_sk:
	return;
}
static _int_u gathervalues(struct hg_vec *v) {
	_int_u i;
	i = 0;
	yl_expecttok(_yl_keychar,_yl_l_brace);
	while(yl_nexttokis(_yl_keychar,_yl_r_brace) == -1) {
		yl_tok = yl_nexttok();
		struct fsl_wkstrc wk;
		expr(&wk);
		hg_pack_float_str(&v->xyzw[i].pack,wk.ss);
		if (yl_tok->kind == _yl_keychar && yl_tok->id == _yl_comma) {

		} else
			yl_ulex(yl_tok);
		i++;
	}
	return i;
}
void static vec_expr(struct fsl_wkstrc *__wk) {
	if (yl_tok->kind == _yl_keywd && (yl_tok->id == _yl_e(0) || yl_tok->id == _yl_e(1))) {
		printf("GOT vec exprssion.\n");
		struct ys_em *em;
		_int_u i;
		em = m_alloc(sizeof(struct ys_em));
		em->c.ys = em;
		__wk->em = em;
		i = gathervalues(&__wk->em->vec);
		for(;i != 4;i++){
			hg_pack_float_str(&__wk->em->vec.xyzw[i].pack,"0.0");
		}
		em->c.comp.comp = (1<<4)|(2<<8)|(3<<12)|(4<<16);
		__wk->x = _ima_vconst;
	}else
		goto _sk;
	yl_tok = yl_nexttok();
	_sk:return;
}


static _32_u arth_h_table[] = {
	0,	0,	0,	0,
	0,	1,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0
};

static _32_u arth_comp[] = {
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0
};

#define GET_ARTH_COMP(__x,__y) arth_comp[arth_h_table[__x]|arth_h_table[__y]<<2]
void static
arithop(struct fsl_wkstrc *__wk) {
	if(yl_tok->bits&_fsl_arth){
		_16_u op;
		struct yl_token *opt = yl_tok;
		if(opt->kind == __yl_num){
			op = yl_comm.valmap[opt->s[0]];
			if(op == _ima_sub){
				/*
					if we get this.

					flt = flt-1.0;
					
					equivalent to 

					flt = flt+-1.0;
				*/
				op = _ima_add;
			}
		}else{
			yl_tok = yl_nexttok();
			op = opt->val;
		}
		struct ys_em *n;
		n = m_alloc(sizeof(struct ys_em));
		n->c.ys = n;
		n->ys = n;
	
		n->l = __wk->em;
		__wk->em = n;
		struct fsl_wkstrc wk;
		expr(&wk);
		n->r = wk.em;
		printf(">>>>>> %u,%u\n", __wk->x,wk.x);
		n->f = hg_ploped(hg_arthtab,__wk->x,wk.x);
		printf("ARTH FUNC: %p.\n",n->f);
	/*
		###############################
	*/
		n->c.comp.comp = GET_ARTH_COMP(__wk->x,wk.x);
		n->op = op;
		__wk->x = _ima_other;
	}
}

/*
	tex(texture,attribute/position/param);
*/

void static func(struct fsl_wkstrc *__wk) {
	struct ys_em *ys = m_alloc(sizeof(struct ys_em));
	
	struct fsl_wkstrc wk;
	_int_u i;
	i = 0;
	yl_expecttok(_yl_keychar,_yl_l_paren);
_again:
	_expr(&wk);
	ys->ems[i] = wk.em;

	if (!yl_nexttokis(_yl_keychar,_yl_comma)) {
		i++;
		goto _again;
	}
	yl_expecttok(_yl_keychar,_yl_r_paren);

	__wk->em = ys;
		
	ys->c.ys = ys;
	ys->ys = ys;
	ys->c.comp.comp = _ima_vec_comp_default;
	ys->cnt = 1;
	yl_tok = yl_nexttok();
}

/*
	name to inbuilt functions?
*/
void static
tex(struct fsl_wkstrc *__wk) {
	/*
		do somthing about this
	*/
	if (yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_tex) {
		func(__wk);
		if(__wk->em->ys->ems[0]->ident == _ima_intrp) {
			__wk->x = _ima_img;
		}else{
			__wk->x = _ima_imgv;
		}
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_normalize) {
		func(__wk);
		__wk->em->f = _hg_NORMALIZE;
		__wk->x = _ima_other;
		__wk->em->c.comp.comp = _ima_vec_comp_default;
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_dot_product) {
		func(__wk);
		__wk->em->ys->f = _hg_DOT_PRODUCT;
		__wk->x = _ima_float;
		__wk->em->c.comp.comp = 1<<16;
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_inverse) {
		func(__wk);
		__wk->em->f = _hg_INVERSE;
		__wk->x = _ima_other;
		__wk->em->c.comp.comp = (1<<4)|(2<<8)|(3<<12)|(4<<16);
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_clamp) {
		func(__wk);
		__wk->em->f = _hg_CLAMP;
		__wk->x = _ima_float;
		__wk->em->c.comp.comp = 1<<16;
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_max) {
		func(__wk);
		__wk->em->f = _hg_MAX;
		__wk->x = _ima_float;
		__wk->em->c.comp.comp = 1<<16;
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_min) {
		func(__wk);
		__wk->em->f = _hg_MIN;
		__wk->x = _ima_float;
		__wk->em->c.comp.comp = 1<<16;
	}else if(yl_tok->kind == _yl_keywd && yl_tok->id == _fsl_amalg) {
		func(__wk);
		__wk->em->f = _hg_AMALGAMATE;
		__wk->x = _ima_float;
		__wk->em->c.comp.comp = 1<<16;
	}
}

void
expr(struct fsl_wkstrc *__wk) {
	__wk->s = NULL;
	__wk->bits = 0x00;
	__wk->comp = 0xf;
	tex(__wk);
	vec_expr(__wk);
	primary_expr(__wk);
	if(yl_tok->kind == _yl_keychar && yl_tok->id == _yl_dot){
		printf(";;;;;;;;; DOT.\n");
		struct ys_em *em;
		em = m_alloc(sizeof(struct ys_em));
		em->ys = NULL;
		em->ident = 0;
		em->c.ys = __wk->em;
		__wk->em = em;
		struct yl_token *comp;
		comp = yl_nexttok();
		yl_tok = yl_nexttok();
		em->c.comp.comp = 0;
		char c;
		_int_u i;
		i = 0;
		for(;(c = comp->s[i]) != '\0';i++){
			switch(c){
				case 'x':
					em->c.comp.comp |= 0<<(i*4);
				break;
				case 'y':
					em->c.comp.comp |= 1<<(i*4);
				break;
				case 'z':
					em->c.comp.comp |= 2<<(i*4);
				break;
				case 'w':
					em->c.comp.comp |= 3<<(i*4);
				break;
			}
		}
		if(__wk->x == _ima_mat){
			__wk->x = _ima_vec;
			em->ident = _ima_matindex;
			em->c.comp.comp<<=20;
			em->c.comp.comp |= (1<<4)|(2<<8)|(3<<12)|(4<<16);
		}else{
			__wk->x = _ima_float;
			em->c.comp.comp |= (i<<16);
		}
		printf("####>>%u>>####.\n",__wk->x);
	}
	arithop(__wk);
	assignment_expr(__wk);
}

void
_expr(struct fsl_wkstrc *__wk) {
	yl_tok = yl_nexttok();
	expr(__wk);
	yl_ulex(yl_tok);
}

void static
config_def(void) {
	printf("CONFIG.\n");
	struct yl_token *name;
	name = yl_nexttok();
	yl_expecttok(_yl_keychar,_yl_l_brace);
	while(yl_nexttokis(_yl_keychar,_yl_r_brace) == -1) {
		struct yl_token *tk;
		tk = yl_nexttok();
		if (tk->kind == _yl_keychar && tk->id == _yl_dot) {
			struct yl_token *elem;
			elem = yl_nexttok();
			if (elem->len == 6) {
				if (!mem_cmp(elem->s,"offset",6)) {
					yl_nexttok();

					struct yl_token *val;
					val = yl_nexttok();
					printf("'%w'%u = '%w'%u.\n",elem->s,elem->len,elem->len,val->s,val->len,val->len);
					struct fsl_symb *s = fsl_symb(name->s,name->len);
					s->s.em.offset = _ffly_dsn(val->s,val->len);
				} else {
					printf("fail ident, %w.\n",elem->s,elem->len);
				}
			}else
				printf("fail size.\n");
		}
	}
}

_8_u static isqual(_64_u __id) {
	_8_u r = __id == _fsl_flat || __id == _fsl_smooth;
	return r;
}

void fsl_parser(void) {
	GET_ARTH_COMP(_ima_float,_ima_float) = 1<<16;
	GET_ARTH_COMP(_ima_vec,_ima_float) = _ima_vec_comp_default;
	GET_ARTH_COMP(_ima_float,_ima_vec) = _ima_vec_comp_default;
	GET_ARTH_COMP(_ima_vec,_ima_vec) = _ima_vec_comp_default;

	yl_comm.tytab = tytab;
	struct yl_token *tk;
	_int_u i;
	i = 0;
	while(1) {
		tk = yl_nexttok();
		if (!tk)break;
		yl_tok = tk;
		if (yl_istype(tk->id) || isqual(tk->id)) {
			decl();
		}else
		if (tk->kind == _yl_keywd && tk->id == _fsl_config){
			config_def();
		}else{
			il.nalloc = 0;


			struct fsl_wkstrc wk;
			expr(&wk);
			yl_ulex(yl_tok);

			if (yl_expecttok(_yl_keychar,_yl_semicolon) == -1) {
				printf("no semicolon at end of expression.\n");
			}
		}
	}

}
