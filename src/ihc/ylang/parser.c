#include "yl.h"
#include "../../assert.h"
#include "../evy/evy.h"
#include "../../io.h"
#include "../../string.h"
#include "../../m_alloc.h"
struct yl_type static types[] = {
	{0},
	{0},
	{0},
	{0}
};
static char const *tytab[] = {
	"_8_U",
	"_16_U",
	"_32_U",
	"_64_U"
};


void static _expr(struct yl_wkstrc*);
void parser_decl_spec(struct yl_token *__tok, struct yl_type **__ty) {
	*__ty = types+(__tok->id&~yltype_bit);
}
void static
expr(struct yl_wkstrc*);
struct yl_token *yl_tok;
void static
decl(struct yl_token *__tok) {
	struct yl_type *ty;
	parser_decl_spec(__tok,&ty);

	struct yl_token *name; 
	name = yl_nexttok();
	assert(name->kind == _yl_ident);
	if (yl_expecttok(_yl_keychar,_yl_semicolon) == -1) {
		printf("no semicolon at end of declerator.\n");
	}
	printf("DECL, %w(%s)\n",name->s,name->len,yl_comm.tytab[ty->kind]);
	struct yl_symb *s = yl_symb(name->s,name->len);
	s->ty = ty;
}

void static
primary_expr(struct yl_wkstrc *__wk) {
	if (yl_tok->kind == _yl_ident) {
		printf("PRIMARY: %w.\n",yl_tok->s,yl_tok->len);
		struct yl_symb *s = yl_symb(yl_tok->s,yl_tok->len);
		__wk->em = &s->em;
		__wk->x = yl_comm.x;
	}else
	if (yl_tok->kind == __yl_num) {
		printf("NUMBER: %w.\n",yl_tok->s,yl_tok->len);
	}else{
		goto _sk;
	}
	yl_tok = yl_nexttok();
_sk:
	return;
}

void static
compound_stmt(void) {
	if (yl_nexttokis(_yl_keychar,_yl_l_brace) == -1) {
		return;
	}
_again:
	yl_tok = yl_nexttok();
	if (yl_tok->kind == _yl_keychar && yl_tok->id == _yl_r_brace) {
		goto _exit;
	}
	
	if (yl_istype(yl_tok->id)) {
		decl(yl_tok);
	}else{
		struct yl_wkstrc wk;
		expr(&wk);

		if (yl_expecttok(_yl_keychar,_yl_semicolon) == -1) {
			printf("no semicolon at end of expression.\n");
		}
	}
	goto _again;
_exit:
	return;
}

void static
func_def(void) {
//	struct yl_type *ty;
//	parser_decl_spec(yl_tok,&ty);

	struct yl_token *name = yl_nexttok();
	yl_expecttok(_yl_keychar,_yl_l_paren);
	yl_expecttok(_yl_keychar,_yl_r_paren);

	struct evy_dwel *d = m_alloc(sizeof(struct evy_dwel));
	yl_comm.ur_cnt = 0;
	yl_comm.ur = NULL;
	struct f_lhash hm;
	yl_comm._env = &hm;
	f_lhash_init(&hm);
	struct evy_flock *f, *orig;
	f = evy_fknew();
	orig = evy_curf;
	evy_fkplace(f);
	compound_stmt();
	struct evy_w *vars;
	assert(yl_comm.ur_cnt<256);
	vars = m_alloc(sizeof(struct evy_w)*yl_comm.ur_cnt);
	struct yl_symb *s = yl_comm.ur;
	_int_u i = 0;
	while(s != NULL) {
		s->em.yl.w = vars+i++;
		s = s->next;
	}

	d->vars = vars;
	d->nvar = yl_comm.ur_cnt;
	evy_fkplace(orig);
	f_lhash_destroy(&hm);
	struct evy_node *n;
	n = evy_new_node();
	n->yl.d = d;
	n->f = evy_functab.func;
	n->fk = f;
	yl_tok = yl_nexttok();
	yl_comm._env = NULL;
}

_8_u static _asgn_rhs[8] = {__evy_imm,__evy_fsv,__evy_gv};
_8_u static _asgn_lhs[8] = {0,__evy_fsv0,__evy_gv0};
void static 
assignment_expr(struct yl_wkstrc *__wk) {
	iftok(_yl_keychar,_yl_equal) {		
		struct yl_wkstrc wk;
		_expr(&wk);
				
		struct evy_node *n;
		n = evy_new_node();
		n->f = evy_ploped(assign,_asgn_lhs[__wk->x],_asgn_rhs[wk.x]);
		printf("ASSIGNMENT.\n");
	}else{
		goto _sk;
	}
	yl_tok = yl_nexttok();
_sk:
	return;
}

void
expr(struct yl_wkstrc *__wk) {	
	primary_expr(__wk);
	assignment_expr(__wk);
	yl_ulex(yl_tok);
}

void 
_expr(struct yl_wkstrc *__wk) {
	yl_tok = yl_nexttok();
	expr(__wk);
}

_8_s static
isfunc(void) {
	struct yl_token *_0,*_1;
	_0 = yl_nexttok();
	_1 = yl_nexttok();
	if (!_0||!_1)
		return -1;
	_8_s res = -1;
	if (yl_tok->kind == _yl_keywd && _0->kind == _yl_ident && _1->kind == _yl_keychar) {
		res = 0;
	}

	yl_ulex(_1);
	yl_ulex(_0);
	if (!res)
		printf("func match.\n");
	return res;
}

void
yl_process(void) {
	evy_curf = evy_fknew();
	yl_comm.tytab = tytab;
	struct yl_token *tk;
	_int_u i;
	i = 0;
	while(1) {
		tk = yl_nexttok();
		if (!tk)break;
		yl_tok = tk;
		if (yl_istype(tk->id)) {
			decl(tk);
		}else
		if (!isfunc()) {
			func_def();
		}else
		if (tk->kind == _yl_keywd){

		}else{
			struct yl_wkstrc wk;
			expr(&wk);

			if (yl_expecttok(_yl_keychar,_yl_semicolon) == -1) {
				printf("no semicolon at end of expression.\n");
			}
		}
	}
	evy_process();
	evy_functab.fin();

}

