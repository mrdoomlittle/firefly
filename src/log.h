# ifndef __ffly__log__h
# define __ffly__log__h
# include "y_int.h"
# include <stdarg.h>
#define LOG_LEVEL(__level) __level
#define LOG_LEVEL_BITS 0xff
#define LOG_MAX 20
#include "io.h"
#include "rws.h"
#include "ioc_struc.h"
#include "system/file.h"
/*
	NOTE: f_ioc_struc containes its own rws struct
*/
#define LOG_MAIN (logs+0)
#define LOG_FLUE (logs+1)
struct log_struc {
    void(*out)(char*, _int_u);
    _int_u level;
	struct f_ioc_struc ioc_out;
	struct ffly_file *_out;
};
extern struct log_struc logs[LOG_MAX];
void log_printf(struct log_struc*,char const*,...);
void y_log_file(struct log_struc*,char const*);
void y_log_fileend(struct log_struc*);
void ffly_log_write(_16_u, void*, _int_u);
void _ff_log(_16_u, _64_u, char const*, ...);
void ffly_log_init(_16_u, void(*)(char*, _int_u));
//void _ff_logv(_16_u, _64_u, char const*, va_list);
#define FF_LOG(...)\
	_ff_log(0, 0, __VA_ARGS__);
# endif /*__ffly__log__h*/
