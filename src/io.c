# include "io.h"
# include <stdarg.h>
# include "mutex.h"
# include "errno.h"
# include "error.h"
# include "msg.h"
# ifdef __fflib
# include "linux/unistd.h"
# else
# include <unistd.h>
# endif
# include "linux/signal.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSIO)
struct f_io_common _f_io_comm;
#ifdef __fflib
struct f_rw_struc *ffly_out;
struct f_rw_struc *ffly_log;
struct f_rw_struc *ffly_err;
struct f_rw_struc *ffly_in;
#endif
FF_FILE *_FFLY_OUT = NULL;
FF_FILE *_FFLY_LOG = NULL;
FF_FILE *_FFLY_ERR = NULL;
FF_FILE *_FFLY_IN = NULL;
# ifdef __fflib
/*
	TODO:
	 buffer?
*/
void putchar(char __c) {
	ffly_fwrite(ffly_out, &__c, 1);
}

/*
	shoud print in chunks and not single bytes.
*/
void ppad(char __c, _int_u __n) {
	char buf[__n];
	while(__n-->0)
		buf[__n] = __c;
	ffly_fwrite(ffly_out, buf, __n);
}

_int_u ffly_rdline(void *__buf, _int_u __size, struct f_rw_struc *__rws) {
	_32_u res;
	_8_s error;
	error = 0;
_again:
	res = __rws->funcs->read(__rws->arg, __buf, __size, &error);
	if (error<0) {
		*(_8_u*)__buf = '\0';
		return 0;
	}

	*((_8_u*)__buf+res-1) = '\0';
	return res-1;
}
# endif
#ifdef __fflib
# include "ioc_struc.h"
_32_u static
_out(_ulonglong __arg, void *__buf, _int_u __size, _8_s *__error) {
	ffly_fwrite((FF_FILE*)__arg, __buf, __size);
}

_32_u static
_in(_ulonglong __arg, void *__buf, _int_u __size, _8_s *__error) {
	_32_u ret;
	f_fread((FF_FILE*)__arg, __buf, __size, &ret);
	return ret;
}

struct f_rw_funcs rws_func = {
	.write = _out,
	.read = _in
};
static struct f_ioc_struc ioc_f_out;
static struct f_ioc_struc ioc_f_in;
static struct f_ioc_struc ioc_f_log;
static struct f_ioc_struc ioc_f_err;
#endif

// data in
void f_io_inlet(struct f_rw_struc *__rws, void *__buf, _int_u __size) {	
	_8_s err;
	__rws->funcs->read(__rws->arg, __buf, __size, &err);
}
// data out
void f_io_outlet(struct f_rw_struc *__rws, void *__buf, _int_u __size) {
	_8_s err;
	__rws->funcs->write(__rws->arg, __buf, __size, &err);
}

_y_err io_init(void) {
	_y_err e0, e1, e2, e3;
	/*
		TODO:
			change to /dev/stdout????
	*/
	if (!(_FFLY_OUT = f_fopen("/dev/tty", FF_O_WRONLY, 0, &e0)))
		goto _upmalfunc;
	if (!(_FFLY_IN = f_fopen("/dev/tty", FF_O_RDONLY, 0, &e1)))
		goto _upmalfunc;

	if (!(_FFLY_LOG = f_fopen("log",
		FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
		FF_S_IRUSR|FF_S_IWUSR, &e2)))
	{
		goto _upmalfunc;
	}

	if (!(_FFLY_ERR = f_fopen("err",
		FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
		FF_S_IRUSR|FF_S_IWUSR, &e3)))
	{
		goto _upmalfunc;
	}

	if ((e0|e1|e2|e3) != 0) {
		// big fucking issue
	}

	ffly_fopt(_FFLY_OUT, FF_STREAM);
	ffly_fopt(_FFLY_LOG, FF_STREAM);
	ffly_fopt(_FFLY_ERR, FF_STREAM);
#ifdef __fflib
	ioc_f_out.rws.funcs = &rws_func;
	ioc_f_out.rws.arg = (_ulonglong)_FFLY_OUT;

	ioc_f_in.rws.funcs = &rws_func;
	ioc_f_in.rws.arg = (_ulonglong)_FFLY_IN;

	ioc_f_log.rws.funcs = &rws_func;
	ioc_f_log.rws.arg = (_ulonglong)_FFLY_LOG;

	ioc_f_err.rws.funcs = &rws_func;
	ioc_f_err.rws.arg = (_ulonglong)_FFLY_ERR;

	ffly_out = &ioc_f_out.rws;
	ffly_in = &ioc_f_in.rws;
	ffly_log = &ioc_f_log.rws;
	ffly_err = &ioc_f_err.rws;
	_ffly_out->rws = ffly_out;
	_ffly_in->rws = ffly_in;
	_ffly_log->rws = ffly_log;
	_ffly_err->rws  = ffly_err;
#endif
	return Y_SUCC;
_upmalfunc:
	/*
		if log failed use lld???? have log fall over to lld????
	*/
	MSG(ERROR, "tryed to open file but returned null???.\n")
	return Y_FAIL;
}

void io_closeup(void) {
	/*
		TODO:
			premake on init
	*/
	struct f_file_struc strc[] = {
		{.f_in = _FFLY_OUT},
		{.f_in = _FFLY_IN},
		{.f_in = _FFLY_LOG},
		{.f_in = _FFLY_ERR}
	};
	f_fxclose(&strc, 4);
#ifdef __fflib
#endif
}

/*
	in case we are using libc
*/
#ifndef __fflib
mlock static lock = FFLY_MUTEX_INIT;
void static
ffly_print(FF_FILE *__file, char const *__format, va_list __args) {
	mt_lock(&lock);
	vfprintf(__file->libc_fp, __format, __args);
	fflush(__file->libc_fp);
	mt_unlock(&lock);
}

void ffly_fprintf(FF_FILE *__file, char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	ffly_print(__file, __format, args);
	va_end(args);
}

void ffly_fprintfs(FF_FILE *__file, char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	ffly_print(__file, __format, args);
	va_end(args);
}

void ffly_vfprintf(FF_FILE *__file, char const *__format, va_list __args) {
	ffly_print(__file, __format, __args);
}

void printf(char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	ffly_print(ffly_out, __format, args);
	va_end(args);
}
#endif
