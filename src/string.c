#include "string.h"
//float to string standard composition
/*
	personally i perfer .0001 and not 0.0001
	only when X<1
*/
_int_u y_ftssc(char *__buf,double __no) {
	if(!__no){
		__buf[0] = '0'; 
		return 1;
	}
	_int_u len = 1;
	if(__no>=1.0) len = 0;
	len = ffly_floatts(__no,__buf+len);
	if(__no<1.) {
		if(__no<0.) {
			__buf[0] = '-';
			__buf[1] = '0';
		}else {
		__buf[0] = '0';
		}
		len++;
	}
	return len;
}
