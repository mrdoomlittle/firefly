# include <stdio.h>
void _y_printf(char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	vprintf(__format, args);
	va_end(args);
}
