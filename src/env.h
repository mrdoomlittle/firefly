# ifndef __ffly__env__h
# define __ffly__env__h
# include "y_int.h"
void envinit(void);
void envcleanup(void);
void envload(char const**);
_8_s envset(char const*, char const*);
char const *envget(char const*);
char const **getenv(void);
# endif /*__ffly__env__h*/
