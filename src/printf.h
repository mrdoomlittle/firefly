# ifndef __ffly__printf__h
# define __ffly__printf__h
# include <stdarg.h>
# include "y_int.h"
# include "types.h"
# include "rws.h"
#define f_printf    printf
#define f_fprintf   ffly_fprintf
#define f_vfprintf  ffly_vfprintf
#define f_vsfprintf ffly_vsfprintf
#define f_sprintf   ffly_sprintf
#define f_vsprintf  ffly_vsprintf
#define f_fprintfs  ffly_fprintfs
#define f_printin   ffly_printin
# ifdef __cplusplus
/*
	TODO:
		remove any trace of old 'ffly' and 'f_' suffix
*/
extern "C" {
# endif
_f_err_t printf(char const*, ...);
_f_err_t vprintf(char const*,va_list);
_f_err_t ffly_fprintf(struct f_rw_struc*, char const*, ...);
_f_err_t ffly_vfprintf(struct f_rw_struc*, char const*, va_list);
_f_err_t ffly_vsfprintf(struct f_rw_struc*, _f_size_t, char const*, va_list);
_int_u ffly_sprintf(char*, char const*, ...);
_int_u ffly_vsprintf(char*, char const*, va_list);
_int_u ffly_fprintfs(struct f_rw_struc*, char const*, ...);
void ffly_printin(char const*, va_list, void(*)(void*, _int_u), void(*)(_int_u));
# ifdef __cplusplus
}
# endif
# endif /*__ffly__printf__h*/
