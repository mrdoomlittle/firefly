    struct amdgpu_vam vam;
    amdgpu_vam_init(&vam,0x10000000,0);
#define A 1
#define B 40
    struct amdgpu_va *va[A*B];
    struct amdgpu_va *va0[A*B];
	mem_set(va,0,sizeof(struct amdgpu_va*)*A*B);
    while(1) {
    _int_u y;
    y = 0;
    _int_u i,k;
    for(;y != A;y++) {
    i = 0;
    for(;i != B;) {
        _int_u j;
        j = ffgen_rand8l()+(y*B);
        if (!va[j]) {
            struct amdgpu_va *v;
			//populate
            v = amdgpu_va_alloc(&vam,(ffgen_rand8l()+1)*(ffgen_rand8l()+1),0);
            _64_u v0,v1;
			v0 = v->addr;
			v1 = v->addr+v->size;
			_int_u u;
			u = 0;
			for(;u != i;u++) {
				if((v0 >= va0[u]->addr && v0 < (va0[u]->addr+va0[u]->size)) || (v1 > va0[u]->addr && v1 <= (va0[u]->addr+va0[u]->size))){
					assert(1 == 0);
				}
			}
			va[j] = v;
			va0[i] = v;
			i++;
        }
    }
    }

    y = 0;
    for(;y != A;y++) {
    k = 0;
    for(;k != B;) {
        _int_u j;
        j = ffgen_rand8l()+(y*B);
        if (va[j] != NULL) {
            //depopulate
            amdgpu_va_free(&vam,va[j]);
            va[j] = NULL;
            k++;
        }
    }
    }
    //amdgpu_va_tree(&vam);
    }
    return;

