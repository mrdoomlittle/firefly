# include "../amdgpu.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../string.h"
#include "../system/errno.h"
struct amdgpu_ctx*
amdgpu_ctx_create(struct amdgpu_dev *__dev, _32_u __priority) {
	union drm_amdgpu_ctx args;
	mem_set(&args, 0, sizeof(union drm_amdgpu_ctx));
	int r;
	args.in.op = AMDGPU_CTX_OP_ALLOC_CTX;
	args.in.priority = __priority;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_CTX, &args, sizeof(union drm_amdgpu_ctx));
	if (r == -1) {
		printf("failed to allocate context,error: %s\n",strerror(errno));
		return NULL;
	}

	struct amdgpu_ctx *ctx;
	ctx = (struct amdgpu_ctx*)m_alloc(sizeof(struct amdgpu_ctx));
	ctx->ctx_id = args.out.alloc.ctx_id;
	ctx->dev = __dev;

	return ctx;
}

void amdgpu_ctx_free(struct amdgpu_ctx *__ctx) {
	int r;
	union drm_amdgpu_ctx args;
	mem_set(&args, 0, sizeof(union drm_amdgpu_ctx));
	args.in.op = AMDGPU_CTX_OP_FREE_CTX;
	args.in.ctx_id = __ctx->ctx_id;
	r = drm_cmdreadwrite(__ctx->dev->fd, DRM_AMDGPU_CTX, &args, sizeof(union drm_amdgpu_ctx));
	if (r == -1) {
		printf("failed to free context.\n");
	}

	m_free(__ctx);
}
