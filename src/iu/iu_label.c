#include "iu.h"
#include "iu_label.h"
#include "../citric/ct_font.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../m/alloca.h"
void mem_setf(float *__f, float __val, _int_u __n);
void static move(_ulonglong __arg,struct iu_label *__lb,_int_u __x,_int_u __y) {
	__lb->_w.x = __x;
	__lb->_w.y = __y;
}

void static draw(struct iu_label *__lb) {
	float x,y, n;
	//number of glypths
	_int_u ng = __lb->len;
	_int_u sz;
	float *overlay = _alloca(sz = (sizeof(float)*4*ng));
	mem_setf(overlay,1,ng*4);
	struct ct_text_line line;

	struct ct_text_frag frag;
	frag.s = __lb->s;
	frag.len = __lb->len;
	frag.next = NULL;
	x = __lb->_w.x;
	y = __lb->_w.y;

	line.frags = &frag;

	ct_font_draw(ct_com.fnt,&line,1,ng,x,y,0,__lb->size,&__lb->c,&__lb->c,1./(n = (float)ng),overlay,1024);
}

void iu_label_init(struct iu_label *__lb) {
	iu_wginit(__lb);
	__lb->_w.move = move;
	__lb->_w.draw = draw;
	__lb->c.r = 0;
	__lb->c.g = 0;
	__lb->c.b = 0;
	__lb->c.a = 1;
}

void iu_label_text(struct iu_label *__lb, char const *__s) {
	__lb->len = str_len(__s);
	__lb->s = __s;
}
