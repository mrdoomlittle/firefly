#ifndef __iu__board__h
#define __iu__board__h
#include "iu_widget.h"
#include "../citric/citric.h"
#include "iu_struc.h"
//inputs
#define IU_BOARD_GRAB          0
#define IU_BOARD_HIDE_SHOW     1


//outputs
#define IU_BOARD_HIDE      0
#define IU_BOARD_SHOW      1

struct iu_board {
	struct iu_widget _w;
	_64_u userdata[16];
	struct iu_caplv capt;
	struct iu_composer comp, *parent;
	_8_s griped;
	_int_u x,y;
	struct iu_render *rn;
};
void iu_brdinit(struct iu_board *__brd);
void iu_brdupdate(struct iu_board *__brd, struct ct_msg *m);
void iu_brdhide(struct iu_board*);
void  iu_brdlink(struct iu_composer *__b, struct iu_board *__brd);
void iu_brdinput(struct iu_widget*,struct iu_board*, struct ct_msg*);

float iu_board_xalign(struct iu_board *__brd, struct iu_widget *__w);
void iu_board_load(struct iu_board *w);
void iu_board_init(struct iu_board*);
void iu_board_deinit(struct iu_board*);
#endif/*__iu__board__h*/
