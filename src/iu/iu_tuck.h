#ifndef __iu__tuck__h
#define __iu__tuck__h
#include "iu.h"

#include "iu_widget.h"
#include "iu_board.h"
#include "iu_btn.h"
/*
	tuck is a single gizmos for a board,
	like a window control(exit,min,etc) buttons
*/
struct iu_tuck {
	struct iu_widget _w;
	void(*draw)(struct iu_widget*);
	struct iu_btn *collapse;
    struct ct_texture *tex;
    struct iu_icon *icon0,*icon1;
};

void iu_tuck_init(struct iu_tuck*);
#endif/*__iu__tuck__h*/
