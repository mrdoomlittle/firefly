#ifndef __iu__shell__h
#define __iu__shell__h
#include "iu_widget.h"
struct iu_shell {
	struct iu_widget _w;
	struct iu_widget *w;
};

void iu_shell_init(struct iu_shell*);
void iu_shell_add(struct iu_shell*,struct iu_widget*);
#endif/*__iu__shell__h*/
