#ifndef __iu__scale__h
#define __iu__scale__h
#include "iu_widget.h"
#include "iu_label.h"
struct iu_scale {
	struct iu_widget _w;	
	float value;
	struct iu_label tx;
};
void iu_scale_init(struct iu_scale*);
void iu_scale_deinit(struct iu_scale*);
#endif/*__iu__scale__h*/
