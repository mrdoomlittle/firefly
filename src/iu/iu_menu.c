#include "iu.h"
#include "iu_menu.h"

void static update(struct iu_menu *__m, struct iu_msgdata *m) {
	struct iu_caplv *tmp = iu_com.capt;
	iu_com.capt = &__m->capt;
	m->id = __m->id;
	iu_message(0,m);
	iu_com.capt = tmp;
}

void static move(_ulonglong __arg, struct iu_menu *__r, _int_u __x, _int_u __y) {
	__r->_w.x = __x;
	__r->_w.y = __y;
}
void static draw(struct iu_menu *__r) {
	float vtx[4];
	float x,y;
	x = __r->_w.x;
	y = __r->_w.y;
	vtx[0] = x;
	vtx[1] = y;
	vtx[2] = x+__r->_w._w;
	vtx[3] = y+__r->_w._h;

//	ct_com.g->crect(vtx,0,1,0.73,0,.4);
}

void iu_menu_show(struct iu_menu *__m) {
	__m->_w.skim = -1;
	__m->_w.cap->sub.skim = -1;
}

void iu_menu_hide(struct iu_menu *__m) {
	__m->_w.skim = 0;
	__m->_w.cap->sub.skim = 0;
}


void static input(struct iu_widget*w,struct iu_menu *__m, struct ct_msg *m) {
	if (m->code == IU_MENU_EXTEND) {
		iu_menu_show(__m);
	}else if (m->code == IU_MENU_RETRACT) {
		__m->_w.skim = 0;
		__m->_w.cap->sub.skim = 0;
	}
}

void static sizer(struct iu_menu*,struct iu_widget*);

void iu_menu_init(struct iu_menu *__m) {
	iu_wginit(__m);
	__m->_w.skim = 0;
	__m->_w.tellof_size = sizer;
	__m->_w.cap->sub.skim = 0;
	__m->_w.input = input;
	__m->_w.update = update;
	__m->_w.move = move;
	__m->_w.draw = draw;
	__m->nc = 0;
	iu_captinit(&__m->capt);
	__m->_w.cap->sub.arg = __m;
	__m->_w.cap->sub.update = update;
	__m->id = -1;
}

void iu_menu_add(struct iu_menu *__m,struct iu_widget *__w) {
	__m->childs[__m->nc++] = __w;
}
/*
	ISSUE FIXME:
		iu_wgsize is for a specific class of widget(too specific)
*/

/*
	calculate picking size, when lay is called wgsizeing takes place.
*/
#include "iu_text.h"
void static calc_bb(struct iu_menu *__m) {
	_int_u i;
	i = 0;
	float _w = __m->min_width,h = 0;
	for(;i != __m->nc;i++){
		struct iu_widget *w;
		w = __m->childs[i];
		if(w->class == IU_WGCLASS_TEXT) {
			struct iu_text *tx = w;
			tx->max = __m->max_width;
			w->ascertain(w);	
		}

		if(w->_w>_w) {
			_w = w->_w;
		}
	
        if (w->_h>__m->min_height) {
            h+=w->_h;
        }else{
            h+=__m->min_height;
        }

	}
	__m->_w._w = _w;
	__m->_w._h = h;
}
#include "iu_text.h"
void iu_menu_lay(struct iu_menu *__m) {
	calc_bb(__m);
	_int_u i;
	float yd = 0;
	i = 0;
	for(;i != __m->nc;i++){
		struct iu_widget *w;
		w = __m->childs[i];
		w->yd-=yd;
		w->parent = __m;
		w->w = __m->_w._w;
		/*
			tell child to change its size accordingly
		*/
		iu_wgsize(w,1,1);
	
		/*attach child to are chains*/
		iu_wgchild(w,__m,__m->_w.draw_chain);
		iu_wgchild(w,__m,__m->_w.move_chain);
		if (w->_h>__m->min_height) {
			yd+=w->_h;
		}else{
			yd+=__m->min_height;
		}
	}
}

void sizer(struct iu_menu *__m, struct iu_widget *__w) {
	/*
		if the childs width is larger then the menu then
		resize the menu or restrict the child.
	*/
	if(__w->_w>__m->_w._w){
		__m->dictator = __w;
	_back:
		__m->_w._w = __w->_w;
	}else{
		if(__m->dictator == __w) {
			if(__w->_w>__m->min_width) {
				goto _back;
			}
		}else{
			//no addition actions are requred
			//the resize is ineffective and changes nothing
			return;
		}
	}
	iu_wgevaluate_size(__m);
	_int_u i;
	i = 0;
	for(;i != __m->nc;i++){
		struct iu_widget *w;
		w = __m->childs[i];
		/*
			i dont likw this, to get rid of this eyesore we need
			just to replace the childs placeholder with the last child 
			where nc-1

			so childs[last] = childs[me]
				childs[me] = childs[last]
				while(__m->nc-1);
		*/
		if(w == __w)continue;
		w->w = __m->_w._w;
		iu_wgsize(w,1,1);

	//	iu_wgsize(w,__m->_w._w,__m->min_height);
	}
}


