#include "iu.h"
#include "iu_text.h"
#include "../citric/ct_font.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../m/alloca.h"
#include "iu_signal.h"
#define CURSOR_POS(__tx) ((__tx)->cursor_pos+(__tx)->offset)
void iu_text_deposit(struct iu_text *__tx, char const *__str, _int_u __len) {
	char *s = m_alloc(__len);//needs to be freed to lazy to do so
	mem_cpy(s,__str,__len);
	__tx->frags->s = s;
	__tx->frags->len = __len;
	__tx->len = __len;
}

void mem_setf(float *__f, float __val, _int_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		__f[i] = __val;
	}
}
void iu_text_default(struct iu_text *__t) {
	__t->min_reduct = 100;
	__t->max = 10000;
}
void static worksize(struct iu_text *__tx) {
	float w,h;
	h = __tx->_w.h;
	w = __tx->_w.w;
	/*
		min reduction on the width
	*/
	if(w<=__tx->min_reduct){
		w = __tx->min_reduct;
	}
	/*
		max appendage to width
	*/
	else if(w>__tx->max){
		w = __tx->max;
	}
	/*
		the max height of text is well, the height of a single glyph
	*/
	float min_height = __tx->glyph_height;
	if(h<min_height){
		h = min_height;
	}

	iu_printf("#TEXT] %f, %f, %u, %u\n",w,h,__tx->w,__tx->h);
	__tx->w = w;
	__tx->h = h;
	__tx->_w._w = w;
	__tx->_w._h = h;	
}

void static sizechange(struct iu_text *__tx) {
	worksize(__tx);
	__tx->_w.bbhd = __tx->size;
	iu_wgevaluate_size(__tx);
}

/*
	this means we have to work out are own size,
	meaning a size wasent specified.
*/
void static _ascertain(struct iu_text *__t) {
    __t->_w.w = ((float)(__t->len))*__t->size;
    __t->_w.h = __t->glyph_height;
	worksize(__t);	
}


void static intern_sizechange(struct iu_text *__t) {
	iu_printf("text -internal sizechange.\n");
	__t->_w.w = ((float)(__t->len))*__t->size;
	__t->_w.h = __t->glyph_height;
	sizechange(__t);
	if(__t->_w.parent != NULL){
		__t->_w.parent->tellof_size(__t->_w.parent,__t);
	}
}
static struct iu_text_frag* findfragfor(struct iu_text *__t,_int_u __pos);
#include "../assert.h"
_8_s static build_fragment_list(struct iu_text *__tx, struct ct_text_frag *__frags) {
	struct iu_text_frag *ufrg = findfragfor(__tx,__tx->offset);
	if(!ufrg){
		return -1;
	}
	_int_u offset;
	offset = __tx->offset-ufrg->start;

	struct ct_text_frag *frg;
	frg = __frags;
	frg->s = ufrg->s+offset;
	frg->len = ufrg->len-offset;
	frg->next = __frags+1;

	if((ufrg = ufrg->next) != NULL) {
		_int_u i;
		i = 1;
		for(;ufrg != NULL;i++) {
			frg = __frags+i;
			frg->s		= ufrg->s;
			frg->len	= ufrg->len;
			if(ufrg->len>32 || !ufrg->len){
				iu_printf("text fragment with illegal length of %u.\n",ufrg->len);
				return -1;
			}
			frg->next = frg+1;
			ufrg = ufrg->next;
		}
	}
	
	frg->next = NULL;
	return 0;
}
void static draw(struct iu_text *__tx) {
	float x,y, n;
	float vtx[4];
	x=__tx->_w.x;
	y=__tx->_w.y;
	vtx[0] = x;
	vtx[1] = y;
	vtx[2] = x+__tx->_w._w;
	vtx[3] = y+__tx->_w._h;
	/*
		for debug
	*/

	//ct_com.g->crect(vtx,0,0,0,1,1);

	//number of glypths
	_int_u ng = __tx->len;
	_int_u sz;
	float *overlay;
	struct ct_text_frag *frags;
	if(!ng || !__tx->num_frgs || !__tx->frags)return;

	if(__tx->cursor_pos>ng) {
		iu_printf("for some odd reason the cursor pos is out of bounds.\n");
		goto _end;
	}

	overlay = _alloca(sz = (sizeof(float)*4*ng));
	mem_setf(overlay,1,ng*4);
	if (__tx->cursor_pos<64 && __tx->cursor_end<64) {
		_int_u l;
		l = __tx->cursor_end;

		if((l+__tx->cursor_pos)>ng) {
			iu_printf("space between cursor start and end is too great.\n");
		
		}else{
			/*ignore highlighting of selected text if we have issues*/
			//ignore for now
			mem_setf(overlay+__tx->cursor_pos*4,0.5,l*4);
		}
	}
	
	frags = _alloca(sizeof(struct ct_text_frag)*__tx->num_frgs);
	if(build_fragment_list(__tx,frags) == -1) {
		goto _end;
	}
	__tx->lines[__tx->ln].frags = frags;	
	ct_font_draw(ct_com.fnt,__tx->lines,__tx->ln+1,ng,x-__tx->xd,y-__tx->yd,0,__tx->size,&__tx->c,&__tx->c,1./(n = (float)ng),overlay,__tx->_w.x+__tx->max);
_end:
	/*
		draw cursor - just a coloured rectangle
	*/
	x-=__tx->xd;
	y-=__tx->yd;
	vtx[0] = x+__tx->size*__tx->cursor_pos;
	vtx[1] = y;
	vtx[2] = vtx[0]+2;
	vtx[3] = y+__tx->glyph_height;
	ct_com.g->crect(vtx,0,1,0,0,1);

}

void static move(_ulonglong __arg, struct iu_text *__tx,_int_u __x, _int_u __y) {
	__tx->_w.x = __x;
	__tx->_w.y = __y;
}

static _int_u codetab[][512] = {
	{
		[CY_BTN_RIGHT]		= IU_TEXT_PRESS,
		[CY_KEY_LEFT]		= IU_TEXT_SCOOT_LEFT,
		[CY_KEY_RIGHT]		= IU_TEXT_SCOOT_RIGHT,
		[CY_BTN_LEFT]		= IU_TEXT_SELECT_START,
		[CY_KEY_SPACE]		= IU_TEXT_INSERT,
		[CY_DOT]			= IU_TEXT_INSERT,
		[CY_SLASH]			= IU_TEXT_INSERT,
		[CY_KEY_BACKSPACE]	= IU_TEXT_DEINSERT
	},
	{
		[CY_BTN_RIGHT]		= ~0,
		[CY_KEY_LEFT]		= ~0,
		[CY_KEY_RIGHT]		= ~0,
		[CY_BTN_LEFT]		= IU_TEXT_SELECT_END,
		[CY_KEY_SPACE]		= ~0,
		[CY_DOT]			= ~0,
		[CY_SLASH]			= ~0,
		[CY_KEY_BACKSPACE]	= ~0
	}
};

/*
	ranges from CY_KEY_a->CY_KEY_Z are keyboard input
*/
static struct iu_sigrange ranges[][2] = {
	{
		{
			{(~(_64_u)0)>>12,	0,	0,	0},
			IU_TEXT_INSERT,
			"A-z"
		}
	},
	{
		{
			{(~(_64_u)0)>>12,   0,  0,  0},
			~0,
			"A-z"
		}
	}
};

void static scoot_right(struct iu_text *__t) {
	_int_u max_char = __t->max/__t->size;
	if(__t->cursor_pos == max_char) {
		__t->offset++;
	}else{
		__t->cursor_pos++;
	}

}

void static scoot_left(struct iu_text *__t) {
	if(!__t->cursor_pos) {
		if(__t->offset>0)
			__t->offset--;
	}else{
		__t->cursor_pos--;
	}
}
static _8_s held = -1;
void static update_selection(struct iu_text *__t, float __x) {
	float x = __x-(__t->_w.x-__t->xd);
	if(x<0)return;
	x/=__t->size;

	__t->cursor_end = x-__t->cursor_pos;
}

void static update(struct iu_text *__t, struct iu_msgdata *m) {
	struct iu_msgdata _m;
	if(!held) {
		update_selection(__t,((float)m->m.x)*iu_com.b->vpw);
	}
	if (m->m.value == CT_RELEASE || m->m.value == CT_PRESS){
		_m.m.x = m->m.x;
		_m.m.y = m->m.y;
		_m.x = m->x;
		_m.y = m->y;
		_int_u code = m->m.code;
		_int_u val = (m->m.value == CT_RELEASE?1:0);
		if (__t->_w.output != NULL) {
			struct iu_sigrange *r = NULL;
			/*
				some idot thought it would be a good idea to use a returning shift(round and round the bits fucking go)
			*/
			r = iu_sigrangefor(&ranges[val],1,(((_64_u)1)<<(code&0x3f)),code&~((_64_u)0x3f));
		
			if(r != NULL) {
				_m.m.code = r->code;
				iu_printf("got range match(%s), %lx\n",r->ident,((_64_u)1)<<code);
			}else{
				_m.m.code = codetab[val][code];
			}


			if(_m.m.code == IU_TEXT_INSERT) {
				_m.m.value = m->m.code;
			}
			iu_printf("TEXT output, (%u)->(%u), %u.\n",code,_m.m.code, m->m.code);
			__t->_w.output(__t,__t->_w.out,&_m);
		}
	}
	return;
}

void static sanity_check(struct iu_text *__t) {
	_int_u i = 0;
	struct iu_text_frag *frg = __t->frags;
	while(frg != NULL) {
		i+=frg->len;
		frg = frg->next;
	}

	if(i != __t->len) {
		iu_printf("fragment length accumulation doesent match total length of text.\n");
	}
	iu_assert(i == __t->len);
}
struct iu_text_frag* findfragfor(struct iu_text *__t,_int_u __pos) {
	_int_u i = 0;
	struct iu_text_frag *frg = __t->frags, *tail = NULL;
	while(frg != NULL) {
		frg->start = i;
		if(__pos >= i && __pos < i+frg->len) {
			return frg;
		}
		i+=frg->len;
		tail = frg;
		frg = frg->next;
	}

	return tail;
}

void static strip_from_frag(struct iu_text_frag *__f, _int_u __pos) {
	if(!__f->len)return;
	if(__pos>=__f->len) {
		iu_printf("an issue arose while trying to strip charicter from text fragment, charicter at %u is off limits and not within %u\n",__pos,__f->len);
		return;
	}
	_int_u i;
	i = __pos;
	while(i != __f->len-1) {
		__f->s[i] = __f->s[i+1];
		i++;
	}
	__f->len--;
}

void static dumpfrags(struct iu_text *__t) {
	struct iu_text_frag *f;
	f = __t->frags;
	_int_u i = 0;
	while(f != NULL) {
		iu_printf("FRAG-%u: len: %u.\n",i++,f->len);
		f = f->next;
	}

}

void iu_text_charstr_selection(struct iu_text *__t, char *__buf, _int_u __start, _int_u __len) {
	struct iu_text_frag *f;
	f = findfragfor(__t,__start);
	if(!f)return;
	
	_int_u offset = __start-f->start;
	_int_u len;
	mem_cpy(__buf,f->s+offset,len = (f->len-offset));

	f = f->next;
	while(f != NULL) {
		_int_u i = 0;
		for(;i != f->len;i++) {
			if(len+i>=__len)
				return;
			__buf[len+i] = f->s[i];
		}
		len+=f->len;
		f = f->next;
	}
}

void iu_text_charstr(struct iu_text *__t, char *__buf) {
	struct iu_text_frag *f;
	f = __t->frags;
	_int_u i = 0;
	while(f != NULL) {
		char *d = __buf+i;
		mem_cpy(d,f->s,f->len);
		i+=f->len;
		f = f->next;
	}
}

#include "../assert.h"
//decisive inputs
void static input(struct iu_widget*w,struct iu_text *__t, struct ct_msg *m) {
	if(m->code == IU_TEXT_SELECT_START) {
		float x = (((float)m->x)*iu_com.b->vpw)-(__t->_w.x-__t->xd);
		if(x<0)return;
		x/=__t->size; 
	
		if(x>__t->len){
			x = __t->len;
		}
		__t->cursor_pos = x;
		__t->cursor_end = 0;
		held = 0;
	}else if(m->code == IU_TEXT_SELECT_END) {
		update_selection(__t,((float)m->x)*iu_com.b->vpw);
		held = -1;
	}else if(m->code == IU_TEXT_INSERT) {
		struct iu_text_frag *b;
		_int_u pos;
		b = findfragfor(__t,pos = CURSOR_POS(__t));
		iu_printf("TEXT got insert, %p, at: %u\n",b,__t->cursor_pos);
		dumpfrags(__t);
		if(b != NULL || !__t->frags) {
			iu_printf("text can be inserted.\n");
			char c;
			if(m->value>=CY_KEY_A && m->value<=CY_KEY_Z) {
				c = 'a'+(m->value-CY_KEY_A);
			}else{
				switch(m->value){
					case CY_DOT:
						c = '.';
					break;
					case CY_SLASH:
						c = '/';
					break;
					case CY_KEY_SPACE:
						c = ' ';
					break;
					default:
						iu_printf("illegitimate character - '%c'.\n",c);
						c = '#';
				}
			}

			struct iu_text_frag *inj;
			inj = m_alloc(sizeof(struct iu_text_frag));			
			inj->s = m_alloc(1);/*should alloc larger region and attempt to make use of the free space*/
			inj->prev = NULL;
			inj->next = NULL;
			*(char*)inj->s = c;
			inj->len = 1;
			__t->num_frgs++;
				
			if(!__t->frags){
				__t->frags = inj;
			}else{
			_int_u len = pos-b->start;
			if(!len) {
				if(!b->prev) {
					assert(b == __t->frags);
					inj->next = b;
					b->prev = inj;
					__t->frags = inj;
					goto _goods;
				}
				b = b->prev;
				len = pos-b->start;
			}

			iu_printf("TEXT ===> %u | %u, -- '%c'\n",len,b->len,*(char*)inj->s);
			if (len == b->len) {
				/*
					here we can just append a new fragment after this one
				*/
				inj->prev = b;
				if(b->next != NULL)
					b->next->prev = inj;
				inj->next = b->next;
				b->next = inj;
			}else {
				/*
					here we split the fragment into three parts(TODO: two not three)
				
					b-> inj -> bb
				*/
				__t->num_frgs++;
				struct iu_text_frag *bb;
				bb = m_alloc(sizeof(struct iu_text_frag));
				bb->next = NULL;//only here to avoid issues/mistakes
				bb->prev = NULL;
				
				
				bb->next = b->next;
				if(b->next != NULL)
					b->next->prev = bb;

				bb->s = b->s+len;
				bb->len = b->len-len;
				iu_printf("bblen: %u, blen: %u, len: %u.\n",bb->len,b->len,len);
				iu_assert(b->len>len);
				iu_assert(b->len>0 && bb->len>0);
				b->len = len;
				inj->prev = b;
				b->next = inj;
				bb->prev = inj;
				inj->next = bb;
				iu_printf("SPLIT text frags (%u) | (%u), %u.\n",b->len,bb->len,__t->len);
			}
			}
		_goods:

			__t->len++;
			scoot_right(__t);
			intern_sizechange(__t);
		}
	}else if(m->code == IU_TEXT_DEINSERT) {
		struct iu_text_frag *b;
		_int_u pos;
		pos = CURSOR_POS(__t);
		if(!pos)
			return;
		pos--;
		b = findfragfor(__t,pos);
		if(b != NULL) {
			_int_u off;
			off = pos-b->start;
			strip_from_frag(b,off);
			if(!b->len) {
				if(b == __t->frags && !b->next) {
					m_free(__t->frags);
					__t->frags = NULL;
					__t->num_frgs = 0;
				}else{
					if(b->prev != NULL) {
						b->prev->next = b->next;
					}
					if(b->next != NULL) {
						b->next->prev = b->prev;
					}
					if(b == __t->frags) {
						__t->frags = b->next;
					}
					m_free(b);
					__t->num_frgs--;
				}
			}
			
			__t->len--;
			scoot_left(__t);
			intern_sizechange(__t);
		}
	}else if(m->code == IU_TEXT_SCOOT_LEFT){
		scoot_left(__t);
	}else if(m->code == IU_TEXT_SCOOT_RIGHT){
		scoot_right(__t);
	}else{
		if(m->code == ~0)return;
		iu_printf("malformed message code, of %u.\n",m->code);
	//	assert(1 == 0);

	}
	sanity_check(__t);
}
void iu_text_config(struct iu_text *__tx) {
	sizechange(__tx);
	float w;
	w = ((float)(__tx->len))*__tx->size;
	float adj;
	if (__tx->bits&TX_VALIGN) {
		adj = __tx->glyph_height-__tx->_w.hj;
		iu_printf("BUTTON CONFIG: %f.\n",adj);
		__tx->yd = adj*0.5;
	}
	if (__tx->bits&TX_HALIGN) {
		adj = w-__tx->_w.wj;
		__tx->xd = adj*0.5;
	}
}


void iu_text_determin(struct iu_text *__tx) {
	__tx->glyph_height = ct_com.fnt->ratio*__tx->size;
	sizechange(__tx);
	__tx->min_reduct = __tx->size*4;
	__tx->max = 1;
}

void static sizer(struct iu_text *__tx) {
	__tx->glyph_height = ct_com.fnt->ratio*__tx->size;
	iu_printf("TEXT_SIZER: %u, %u.\n",__tx->_w.wj,__tx->_w.hj);
	float ox = __tx->_w.xd,oy = __tx->_w.yd;
	iu_text_config(__tx);

	iu_wgmove(NULL,__tx,__tx->_w.x+ox,__tx->_w.y+oy);
}

void iu_text_init(struct iu_text *__tx) {
	iu_wginit(__tx);
	__tx->_w.class = IU_WGCLASS_TEXT;
	__tx->_w.codetab = codetab;
	__tx->_w.output = input;
	__tx->_w.input = input;
	__tx->_w.ascertain = _ascertain;
	__tx->_w.out = __tx;
	__tx->_w.size = sizer;
	__tx->_w.move = move;
	__tx->_w.draw = draw;	
	__tx->c.r = 0;
	__tx->cursor_pos = 0;
	__tx->c.g = 0;
	__tx->c.b = 0;
	__tx->c.a = 1;
	__tx->bits = 0;
	__tx->_w.cap->sub.arg = __tx;
	__tx->_w.cap->sub.update = update;
	__tx->frags = m_alloc(sizeof(struct iu_text_frag));
	__tx->frags->prev = NULL;
	__tx->frags->next = NULL;
	__tx->line = __tx->char_limit = 24;
	__tx->ln = 0;
	__tx->in_focus = -1;
	__tx->num_frgs = 1;
	__tx->offset = 0;
	__tx->xd = 0;
	__tx->yd = 0;
	__tx->w = __tx->h = 0;
}
