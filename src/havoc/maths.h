#ifndef __havoc__maths__h
#define __havoc__maths__h
#include "../y_int.h"
#include "maths/dot.h"
#define h_ident(__m) h_matcopY(__m,h_m_ident)
_8_s xyplane_intersect(float __xs, float __xe, float __ys, float __ye, float __z, float x, float y, float z, float dx, float dy, float dz);
#endif/*__havoc__maths__h*/
