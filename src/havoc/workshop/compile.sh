rm -f *.o hw/*.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../../ && . ./compile.sh && cd havoc/workshop
ffly_objs="$ffly_objs settings.o main_menu.o"
gcc -c $cc_flags main_menu.c
gcc -c $cc_flags settings.c
gcc $cc_flags -o a.out main.c $ffly_objs -nostdlib
