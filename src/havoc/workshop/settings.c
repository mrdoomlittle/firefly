# include "workshop.h"
#include "../font.h"
#include "../../clay.h"
#include "../../io.h"
#include "../common.h"
#include "../../y.h"
#include "../resource.h"
#include "../icon.h"

extern struct h_context hct;
static texp icon_tex;
static struct h_icon icons;

static struct ui_dropdown dropdown;
static struct ui_btn w_exit_bt;
static struct ui_checkbox debug_cb;
static struct ui_checkbox stats_cb;
static struct ui_text debug_cb_text;
static struct ui_text enable_stats_text;
static struct ui_rect w_hbar;
static struct ui_text fps_cntr;
static struct ui_text mem_usage;
static struct ui_text mem_aa;
void static _debug_cb(_ulonglong __arg, struct h_msg *m) {
	if (m->code == UI_CHECKBOX_CHECKED) {
		y_bits = Y_PRINTF_DISABLE;
	}else
	if (m->code == UI_CHECKBOX_UNCHECKED) {
		y_bits = 0;
	}
}
static _8_s draw_stats = -1;
void static _stats_cb(_ulonglong __arg, struct h_msg *m) {
	if (m->code == UI_CHECKBOX_CHECKED) {
		draw_stats = 0;
	}else
	if (m->code == UI_CHECKBOX_UNCHECKED) {
		draw_stats = -1;
	}
}
#define D(__x) ((1./255.)*__x)

extern _int_u transmap_btn_to_window[64];
extern _int_u transmap_btn_to_window0[64];
static _int_u whbar_code[64];
static _int_u transmap_window_to_hbar[64];
#define settings_w _ws.settings_w
void static move_decor(struct ui_window *_w) {
   	float x = _w->w.x, y = _w->w.y;
	float w = _w->w.w, h = _w->w.h;
   	w_hbar.vtx[0] = x;
    w_hbar.vtx[1] = y-0.02;
    w_hbar.vtx[2] = x+0.5;
    w_hbar.vtx[3] = y;
    w_hbar.w.x = x;
    w_hbar.w.y = y-0.02;
    w_hbar.w.w = w;
    w_hbar.w.h = 0.02;
	h_wg_update(w_hbar.w.b,&w_hbar.w);
}
void
ws_settings_init(void) {
	whbar_code[UI_BTN_PRESS]	=	UI_WINDOW_GRAB;
	whbar_code[UI_BTN_RELEASE]	=	UI_WINDOW_RELEASE;
	transmap_window_to_hbar[UI_WINDOW_HIDE] = UI_RECT_HIDE;
	transmap_window_to_hbar[UI_WINDOW_SHOW] = UI_RECT_SHOW;

	w_hbar.c = (struct h_colour){D(255),D(255),D(255),1};
	ui_rect_new(&w_hbar);
	
	fontp fnt = _ws.fnt;
	icon_tex = _ws._16_16;
	
	icons.tx = icon_tex;
	debug_cb.icon = &icons;
	stats_cb.icon = &icons;
	struct h_icon_data *icon = icons.icons;
	icon->uv[0] = 0;
	icon->uv[1] = 0;
	icon->uv[2] = 16;
	icon->uv[3] = 0;
	icon->uv[4] = 0;
	icon->uv[5] = 16;

	debug_cb.tick = icon;
	stats_cb.tick = icon;
	icon = icons.icons+1;
	icon->uv[0] = 16;
	icon->uv[1] = 0;
	icon->uv[2] = 32;
	icon->uv[3] = 0;
	icon->uv[4] = 16;
	icon->uv[5] = 16;
	debug_cb.cross = icon;
	stats_cb.cross = icon;
	ui_checkbox_new(&debug_cb);
	ui_checkbox_new(&stats_cb);
#define TEXT "file"
	struct ui_btn_strc btn_config = {
		.x = 0,
		.y = 0,
		.w = 0.08,
		.h = 0.017,
		.tw = 0.8,
		.th = 0.8,
		.text = TEXT,
		.len = sizeof(TEXT)-1,
		.bg = {D(255),D(255),D(255),1},
		.tx = {
			.colour = {D(0),D(0),D(0),1}
		}
	};
	ui_btn_new(&w_exit_bt);
	w_exit_bt.tx.bits = FNT_HALIGN|FNT_VALIGN;
	btn_config.w = 0.04;
	btn_config.h = 0.02;
	btn_config.text = "exit";
	btn_config.len = sizeof("exit")-1;
	w_exit_bt.tx.linelen = btn_config.len;
	ui_btn_load(&w_exit_bt,&btn_config,fnt);

	ui_window_new(&settings_w);
	ui_window_config(&settings_w,0.25,0.25,0.5,0.5);
	h_wg_link(&_h_is.comp,&settings_w);
	h_wg_link(&settings_w.comp,&w_exit_bt);
	float cbh = 0.016*settings_w.comp.asp;
	float cbm = (0.03*0.5)-(cbh*0.5);
	ui_checkbox_config(&debug_cb,0.09+0.01,0.01+cbm,0.016,cbh);
	h_wg_link(&settings_w.comp,&debug_cb);

	ui_checkbox_config(&stats_cb,0.09+0.01,0.05+cbm,0.016,cbh);
	h_wg_link(&settings_w.comp,&stats_cb);

	ui_btn_move(&w_exit_bt,1.0-0.04,0.0);

		//redirect messages from button to window
	w_exit_bt.w.output = settings_w.w.input;
	w_exit_bt.w.out = &settings_w;
	w_exit_bt.w.codetab = transmap_btn_to_window;

	ui_window_add(&settings_w,&w_exit_bt);
	
	ui_window_add(&settings_w,&debug_cb);
	debug_cb.w.output = _debug_cb;
	
	ui_window_add(&settings_w,&stats_cb);
	stats_cb.w.output = _stats_cb;

	text_creat(&debug_cb_text,0);
	debug_cb_text.f = fnt;
	debug_cb_text.x = 0.01;
	debug_cb_text.y = 0.01;
	debug_cb_text.w = 0.08;
	debug_cb_text.h = 0.03;
	debug_cb_text.bg = (struct h_colour){0,0,0,0};
	debug_cb_text.colour = (struct h_colour){1,1,1,1};
	debug_cb_text.text = "disable printf";
	debug_cb_text.len = sizeof("disable printf")-1;
	debug_cb_text.linelen = debug_cb_text.len;
	ui_window_add(&settings_w,&debug_cb_text);

	text_creat(&enable_stats_text,0);
	enable_stats_text.f = fnt;
	enable_stats_text.x = 0.01;
	enable_stats_text.y = 0.01+0.04;
	enable_stats_text.w = 0.08;
	enable_stats_text.h = 0.03;
	enable_stats_text.bg = (struct h_colour){0,0,0,0};
	enable_stats_text.colour = (struct h_colour){1,1,1,1};
	enable_stats_text.text = "show stats";
	enable_stats_text.len = sizeof("show stats")-1;
	enable_stats_text.linelen = sizeof("disable printf")-1;
	ui_window_add(&settings_w,&enable_stats_text);


	text_creat(&fps_cntr,0);
	fps_cntr.f = fnt;
	fps_cntr.y = 0;
	fps_cntr.w = 0.07;
	fps_cntr.h = 0.015;
	fps_cntr.x = 1-fps_cntr.w;
	fps_cntr.bg = (struct h_colour){0,0,0,0};
	fps_cntr.colour = (struct h_colour){1,1,1,1};
	fps_cntr.linelen = 19;
	text_creat(&mem_usage,0);
	mem_usage.f = fnt;
	mem_usage.y = 0.02;
	mem_usage.w = 0.07;
	mem_usage.h = 0.015;
	mem_usage.x = 1-mem_usage.w;
	mem_usage.bg = (struct h_colour){0,0,0,0};
	mem_usage.colour = (struct h_colour){1,1,1,1};
	mem_usage.linelen = 19;
	text_creat(&mem_aa,0);
	mem_aa.f = fnt;
	mem_aa.y = 0.04;
	mem_aa.w = 0.07;
	mem_aa.h = 0.015;
	mem_aa.x = 1-mem_aa.w;
	mem_aa.bg = (struct h_colour){0,0,0,0};
	mem_aa.colour = (struct h_colour){1,1,1,1};
	mem_aa.linelen = 19;
	w_hbar.w.output = settings_w.w.input;
	w_hbar.w.out = &settings_w;
	w_hbar.w.codetab = whbar_code;
	settings_w.move_decor = move_decor;
	h_wg_link(&_h_is.comp,&w_hbar);
	move_decor(&settings_w);
	settings_w.w.output = w_hbar.w.input;
	settings_w.w.out = &w_hbar;
	settings_w.w.codetab = transmap_window_to_hbar;
}

# include "../../m.h"

void
ws_settings_deinit(void) {


}
#include "../../maths.h"
#include "../../strf.h"
void
ws_settings_tick(void) {
	char buf[256];
	if (_h_is.cps>=0 && _h_is.cps<=100 && !draw_stats) {
		fps_cntr.text = buf;
		mem_usage.text = buf;
		mem_aa.text = buf;
		_int_u size;
		size = ffly_strf(buf,256,"FPS %u",(_64_u)_h_is.cps);
		if (size<64 && size>0) {
			fps_cntr.len = size;

			ui_text_draw(&fps_cntr);
		}
		
		struct meminfo mi;
		meminfo(&mi);
		size = ffly_strf(buf,256,"MEM %u.%u-kb",mi.used/1024,mi.used&(1024-1));
		if (size<64 && size>0) {
			mem_usage.len = size;
			ui_text_draw(&mem_usage);
		}

		size = ffly_strf(buf,256,"AA %u",mi.aa);
		if (size<64 && size>0) {
			mem_aa.len = size;
			ui_text_draw(&mem_aa);
		}
	}
	if (settings_w.hidden == -1) {
		ui_rect_draw(&w_hbar);
		ui_window_draw(&settings_w);
	}
}
