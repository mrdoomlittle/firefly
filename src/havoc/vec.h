# ifndef __havoc__vec__h
# define __havoc__vec__h
# include "../flue/types.h"
typedef struct h_vec3 {
	double x, y, z;
} hvec3;
typedef struct h_vec4 {
	union {
		struct {
			_flu_float x, y, z, w;
		};
		struct {
			_flu_float r, g, b, a;
		};
	};
} hvec4;


typedef struct h_vec3* hvec3p;
typedef struct h_vec4* hvec4p;


/*
	three component vector addition and subtraction
*/
void h_3vadd(hvec3p,hvec3p,hvec3p);
void h_3vsub(hvec3p,hvec3p,hvec3p);

void h_3vmulfloat(hvec3p,hvec3p,double);

void h_vadd(hvec4p,hvec4,hvec4);
void h_vsub(hvec4p,hvec4,hvec4);
void h_vmul(hvec4p,hvec4,hvec4);
void h_vdiv(hvec4p,hvec4,hvec4);
void h_vmulf(hvec4p,hvec4,float);
void h_vdivf(hvec4p,hvec4,float);

/*
	substitute - TODO: find real name for this vector operation
*/
void h_vsstmin(hvec3p,hvec3p);
void h_vsstmax(hvec3p,hvec3p);



# endif /*__havoc__vec__h*/
