#ifndef __render__h
#define __render__h

struct rdr_job{
  void(*func)(struct rdr_job*);
  void *data;
  struct rdr_job *next;
};

struct rdr_job* hv_rdr_job(void);
void hv_rjlink(struct rdr_job*);
void h_static_subsis_render(struct rdr_job*);
#endif/*__render__h*/
