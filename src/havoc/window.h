# ifndef __havoc__window__h
# define __havoc__window__h
# include "mare.h"

typedef struct h_wd {
	_8_u userdata[0x100];
	h_marep m;
	_int_u x, y;
	_int_u width, height;
	char title[128];
	_int_u tlen;
	void *render;
} *h_wdp;


h_wdp h_wd_new(void);
void h_wd_map(h_wdp);
# endif /*__havoc__window__h*/
