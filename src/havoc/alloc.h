#ifndef __hv__alloc__h
#define __hv__alloc__h
#define HV_MESH_STRUC       h_mesh
#define HV_MODEL_STRUC      h_model
#define HV_MBUFFER_STRUC    hv_mbuffer
#define hv_alloc_struc(__struc) m_alloc(sizeof(struct __struc));

#endif/*__hv__alloc__h*/
