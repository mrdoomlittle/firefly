# ifndef __havoc__scene__h
# define __havoc__scene__h
# include "common.h"
struct h_scene {
	void(*on_entry)(void);
	void(*on_exit)(void);
	_8_s(*tick)(void);
};

# endif /*__havoc__scene__h*/
