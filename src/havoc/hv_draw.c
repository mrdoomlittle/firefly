#include "renderer.h"
#include "resource.h"
#include "../io.h"
#include "../assert.h"
#include "hv_tex.h"
void h_lineto(struct h_context *__ct,float *__vtx,float __r, float __g, float __b, float __a) {
  return;
	float *consts = _h_is.perc_const;
  consts[0] = __r;
  consts[1] = __g;
  consts[2] = __b;
  consts[3] = __a;

  float vtx[8];
  vtx[0] = __vtx[0]+2;
  vtx[1] = __vtx[1];

  vtx[2] = __vtx[0];
  vtx[3] = __vtx[1];

  vtx[4] = __vtx[2]-2;
  vtx[5] = __vtx[3];

  vtx[6] = __vtx[2];
  vtx[7] = __vtx[3];

  //pack are data into appropriate format
  void *data;
  data = h_data(vtx,1,H_QUAD);
  h_quad(data,1,crect_ps.priv,crect_vs.priv,rect_link);
  h_buffer_destroy(data);
}

void h_rect_withbuf(struct h_context *__ct, void *__buf, _int_u __cnt, float __r, float __g, float __b, float __a) { 
	//set const data for the shaders
  float *consts = _h_is.perc_const;
  consts[0] = __r;
  consts[1] = __g;
  consts[2] = __b;
  consts[3] = __a;
  _h_is.rd->rect_withbuf(__buf,__cnt, crect_ps.priv,crect_vs.priv,rect_link);
}
void h_rect(struct h_context *__ct, float *__vtx, float __r, float __g, float __b, float __a) {
  //set const data for the shaders
  float *consts = _h_is.perc_const;
  consts[0] = __r;
  consts[1] = __g;
  consts[2] = __b;
  consts[3] = __a;

  _h_is.rd->rect(__vtx, crect_ps.priv,crect_vs.priv,rect_link);
}

void h_uirect(struct h_context *__ct, float *__vtx, _int_u __cnt, hv_texp __tx, float __r, float __g, float __b, float __a) {
  return;
	assert(__cnt>0);
  float *consts = _h_is.perc_const;
  consts[0] = __r;
  consts[1] = __g;
  consts[2] = __b;
  consts[3] = __a;

  h_matcopY(consts+4,h_m_ident);
  _h_is.rd->ui_rect(__vtx, __cnt,__tx, ui_ps.priv,ui_vs.priv,ui_link);
}

