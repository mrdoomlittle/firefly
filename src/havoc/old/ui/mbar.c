# include "mbar.h"
# include "../../m_alloc.h"
# include "../../io.h"
void static move(struct ui_mbar*, _flu_float, _flu_float);
void static update(_ulonglong __arg, _int_u __id, _int_u __code, _int_u __value) {
	printf("############## activity.\n");
}

struct ui_mbar* ui_mbar_new(void) {
	struct ui_mbar *b;
	b = m_alloc(sizeof(struct ui_mbar));
//	b->rct = ui_rect_new();
	b->w.draw = ui_mbar_draw;
	b->w.update = update;
	b->n = 0;
	b->x = 0;
	b->w.w = 1;
	b->w.h = 0.029;
	b->rct->c.r = 1; 
	b->rct->c.g = 0;
	b->rct->c.b = 0;
	b->rct->c.a = 1;
	move(b, 0, 0);
	return b;
}

void move(struct ui_mbar *__b, _flu_float __x, _flu_float __y) {
	__b->w.x = __x;
	__b->w.y = __y;
	__b->rct->vtx[0] = __x;
	__b->rct->vtx[1] = __y;
	__b->rct->vtx[2] = __x+__b->w.w;
	__b->rct->vtx[3] = __y+__b->w.h;
}

static struct h_hbox bx;
void ui_mbar_add(struct ui_mbar *__b, struct h_widget *__wg) {
/*	struct h_rbdesc *dsc;
	__wg->hb = &(dsc = ((__b->w.rb->dsc+1)+__b->w.rb->n++))->hb;
	__b->boxes[__b->n++] = __wg;
	__wg->_w = __b->w.w;
	__wg->_h = __b->w.h;
	__wg->hb->x = __wg->x*_h_is.width;
	__wg->hb->y = __wg->y*_h_is.height;
	__wg->hb->xfar = (__wg->x+__wg->w)*_h_is.width;
	__wg->hb->yfar = (__wg->y+__wg->h)*_h_is.height;
	dsc->update = __wg->update;
	dsc->arg = __wg;
*/
}
/*
	after enlock is preformed contents become uneditable
*/
void ui_mbar_enlock(struct ui_mbar *__b) {
	_int_u i;
	i = 0;
	_flu_float dis = 0;
	for(;i != __b->n;i++) {
		struct h_widget *w;
		w = __b->boxes[i];
		w->move(w, w->x+dis, 0);
		dis+=w->w;
		dis+=(0.02*__b->w.w);
	}
}

void ui_mbar_delock(struct ui_mbar *__b) {

}

# include "../renderer.h"
void ui_mbar_draw(struct ui_mbar *__b) {
	ui_rect_draw(__b->rct);
	_int_u i;
	i = 0;
	for(;i != __b->n;i++) {
		__b->boxes[i]->draw(__b->boxes[i]);
	}
}
