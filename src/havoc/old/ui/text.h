# ifndef __havoc__text__h
# define __havoc__text__h
#define TEXT_LIMIT 86
# include "../../y_int.h"
# include "../font.h"
#include "widget.h"
typedef struct ui_text {
	struct h_widget _w;

	fontp f;
	_int_u x,y;
	float size;
	_flue_float xoff,yoff;
	char *text;
	float linelen;
	_int_u len;
	_8_u bits;
	struct h_recvbox *b;
	_flu_float w, h;
	struct font_state fs;
	struct h_colour colour, bg;
} *ui_textp;
#define UI_TX_SETTEXT 0x01
#define UI_TX_SETFONT 0x02
struct ui_text_struc {
	struct {
		char const *p;
		_int_u len;
	} text;
	fontp f;
	_8_u bits;
};


void ui_text_draw(ui_textp);
void text_alter(ui_textp, struct ui_text_struc*);
void text_creat(ui_textp,_8_u);
void text_destroy(ui_textp);
# endif /*__havoc__text__h*/
