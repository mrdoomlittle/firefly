#ifndef __ui__window__h
#define __ui__window__h
#include "widget.h"
#define UI_WINDOW_HIDE 0
#define UI_WINDOW_SHOW 1
#define UI_WINDOW_GRAB	2
#define UI_WINDOW_RELEASE	3
struct ui_window {
	struct h_widget w;
	struct h_composer comp;
	struct h_widget *ws[16];
	void(*move_decor)(struct ui_window*);
	float xdis,ydis;
	_int_u n;
	_8_s hidden;
	_8_s griped;
};
void ui_window_add(struct ui_window*,struct h_widget*);
void ui_window_config(struct ui_window *w, float __x, float __y, float __w, float __h);
void ui_window_new(struct ui_window*);
void ui_window_draw(struct ui_window*);
#endif/*__ui__window__h*/
