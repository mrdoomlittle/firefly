# include "font.h"
# include "../m_alloc.h"
# include "../bitfont.h"
# include "../flue/common.h"
# include "../io.h"
# include "common.h"
#include "../assert.h"
# include "renderer.h"
#include "../msg.h"
#include "../domain.h"
#define MSG_BITS MSG_DOMAIN(_DOM_HAVOC)
// fixed char font
void static
fcf(fontp __f, _int_u __size) {
	_int_u i;
	_8_u c;
	struct font_char *cs;
	i = 0;
	_flu_float wn, hn;
	wn = 1./_h_is.maps_map[0].width;
	hn = 1./_h_is.maps_map[0].height;
	for(;i != __f->nac;i++) {
		cs = __f->cmap+__f->acarr[i];	
		_64_u loc;
		loc = H_8_8_ALLOC(&_h_is.maps_alloc[0]);

		double xx, yy;
		xx = FSB_X(loc);
		yy = FSB_Y(loc);
		cs->u0 = xx*wn;
		cs->v0 = yy*wn;
		cs->u1 = (xx+8)*wn;
		cs->v1 = (yy+8)*wn;
		MSG(INFO,"LOCATION ALOTMENT, %fx%f.\n",xx,yy)
		_int_u x, y;
		_8_u *s;
		float *d;
		y = 0;
		/*
			create tone map
		*/
		assert(__size>0);
		for(;y != __size;y++) {
			x = 0;
			for(;x != __size;x++) {
				s = cs->bm+x+(y*__size);
				d = ((float*)_h_is.maps_map[0].ptr)+(_64_u)((xx+x)+((yy+y)*_h_is.maps_map[0].width))*4;
				if (*s>0) {
					d[0] = 1;
					d[1] = 1;
					d[2] = 1;
					d[3] = 1;
				} else {
					d[0] = 0;
					d[1] = 0;
					d[2] = 0;
					d[3] = 0;
				}
			}
		}
	}
}

void _bfload(fontp __f) {
	MSG(INFO,"LOADING-bitfont.\n")
	MSG(INFO,"LOADMAP: %s{%ux%u}\n", _bf_info.lm,_bf_info.width,_bf_info.height)
	__f->width = _bf_info.width;
	__f->height = _bf_info.height;
	_int_u bms;
	bms = _bf_info.width*_bf_info.height;
	_8_u *md;
	md = (_8_u*)m_alloc(_bf_info.n*bms);
	struct font_char *cs;
	_int_u i;
	_8_u c;
	i = 0;
	for(;i != _bf_info.n;i++) {
		cs = __f->cmap+(c = _bf_info.lm[i]);
		cs->bm = md+(i*bms);
		_bf_info.bm = cs->bm;
		bfchar(c);
	}
	__f->acarr = _bf_info.lm;
	__f->nac = _bf_info.n;
	fcf(__f, 8);
}
struct font_driver fnt_drv[20] = {
	{_bfload}
};
/*
	there are 2 ways to load textures 
	from raw font data or thru prepreped font maps(bit maps with all fonts used + additional font loading data

*/
fontp font_load(void) {
	fontp f;
	f = (fontp)m_alloc(sizeof(struct font));
	f->d = fnt_drv;
	f->d->load(f);
	font_test(f,'k');
	return f;
}

void font_state_init(struct font_state *__state) {
	struct h_obj *j;
	j = obj_new();
	__state->j = j;
	__state->j->p = NULL;
}
#include "shader.h"
#include "resource.h"

extern struct h_shader ui_ps;
extern struct h_shader ui_vs;
extern _64_u ui_link[8];
#include "../assert.h"
void font_draw(fontp __f, char const *__str, _int_u __len, _int_u __x, _int_u __y, _8_u __bits, _flu_float __size, struct h_colour *__colour, struct h_colour *__bg) {
	assert(__str[__len-1] != '\0');
	struct font_char *cs;
	_flu_float buf[__len*12];
	_flu_float *p = buf;
	
	struct row {
		_flu_float *p;
		/*
			number of chars in the row
		*/
		_int_u w;
	};

	struct row r[24];
	_int_u cr = 0;
	char c;
	_int_u i;
	i = 0;
	_int_u xof = 0;
	r[cr].p = p;
	while(i != __len) {
		c = __str[i++];
		if (c == '\n') {
			r[cr].w = xof;
			cr++;
			r[cr].p = p;
			xof = 0;
			continue;
		}
		cs = __f->cmap+c;
		p[4] = cs->u0*_h_is.maps_map[0].width;
		p[5] = cs->v0*_h_is.maps_map[0].height;
		p[6] = cs->u1*_h_is.maps_map[0].width;
		p[7] = cs->v0*_h_is.maps_map[0].height;
		
		p[8] = cs->u0*_h_is.maps_map[0].width;
		p[9] = cs->v1*_h_is.maps_map[0].height;
		p[10] = cs->u1*_h_is.maps_map[0].width;
		p[11] = cs->v1*_h_is.maps_map[0].height;
		p+=12;
		xof+=8;
	}

	r[cr].w = xof;
	struct row *_r;
	cr++;
	_flu_float _xof;
	_flu_float gw, gh;
	gh = __size;
	gw = __size;
	i = 0;

	for(;i != cr;i++) {
		_r = r+i;
		_flu_float *p;
		p = _r->p;
		_int_u w;
		w = 0;
		_xof = 0;
		for(;w != _r->w/8;w++) {
			p[0] = __x+(w*gw);
			p[1] = __y;
			p[2] = p[0]+gw;
			p[3] = p[1]+gh;
			p+=12;
		}
		
		h_uirect(_h_is.ct,_r->p,_r->w/8,_h_is.maps_map->tex,__colour->r,__colour->g,__colour->b,__colour->a);
	}
}

void font_test(fontp __f, char __c) {
	_8_u *bm;
	bm = __f->cmap[__c].bm;
	char buffer[128*128];
	_int_u x, y;
	y = 0;
	for(;y != __f->height;) {
		x = 0;
		for(;x != __f->width;x++) {
			buffer[x+(y*(__f->width+1))] = *(bm+x+(y*__f->width))?'#':'.';
		}
		y++;
		buffer[(y*(__f->width+1))-1] = '\n';
	}
	buffer[(__f->width+1)*__f->height] = '\0';
	MSG(INFO,"\n%s",buffer)
}
# include "../bitfont.h"
_8_u *font_char(fontp __f, char __c) {
	return __f->cmap[__c].bm;
}
