#include "vec.h"
void h_3vadd(hvec3p __dst, hvec3p __a, hvec3p __b) {
	__dst->x = __a->x+__b->x;
	__dst->y = __a->y+__b->y;
	__dst->z = __a->z+__b->z;
}

void h_3vsub(hvec3p __dst, hvec3p __a, hvec3p __b) {
	__dst->x = __a->x-__b->x;
	__dst->y = __a->y-__b->y;
	__dst->z = __a->z-__b->z;
}

void h_vadd(hvec4p __dst, hvec4 __a, hvec4 __b) {
	__dst->x = __a.x+__b.x;
	__dst->y = __a.y+__b.y;
	__dst->z = __a.z+__b.z;
	__dst->w = __a.w+__b.w;
}

void h_vsub(hvec4p __dst, hvec4 __a, hvec4 __b) {
	__dst->x = __a.x-__b.x;
	__dst->y = __a.y-__b.y;
	__dst->z = __a.z-__b.z;
	__dst->w = __a.w-__b.w;
}

void h_vmul(hvec4p __dst, hvec4 __a, hvec4 __b) {
	__dst->x = __a.x*__b.x;
	__dst->y = __a.y*__b.y;
	__dst->z = __a.z*__b.z;
	__dst->w = __a.w*__b.w;
}

void h_vdiv(hvec4p __dst, hvec4 __a, hvec4 __b) {
	__dst->x = __a.x/__b.x;
	__dst->y = __a.y/__b.y;
	__dst->z = __a.z/__b.z;
	__dst->w = __a.w/__b.w;
}

void h_vmulf(hvec4p __dst, hvec4 __a, float __f) {
	__dst->x = __a.x*__f;
	__dst->y = __a.y*__f;
	__dst->z = __a.z*__f;
	__dst->w = __a.w*__f;
}

void h_vdivf(hvec4p __dst, hvec4 __a, float __f) {
	__dst->x = __a.x/__f;
	__dst->y = __a.y/__f;
	__dst->z = __a.z/__f;
	__dst->w = __a.w/__f;
}
void h_3vmulfloat(hvec3p __dst,hvec3p __src,double __float) {
	__dst->x = __src->x*__float;
	__dst->y = __src->y*__float;
	__dst->z = __src->z*__float;
}
/*
	get lowest and highest vectors
*/
void h_vsstmin(hvec3p __a,hvec3p __b){
	if(__b->x<__a->x){
		__a->x = __b->x;
	}
	if(__b->y<__a->y){
		__a->y = __b->y;
	}
	if(__b->z<__a->z){
		__a->z = __b->z;
	}
}

void h_vsstmax(hvec3p __a,hvec3p __b){
	if(__b->x>__a->x){
		__a->x = __b->x;
	}
	if(__b->y>__a->y){
		__a->y = __b->y;
	}
	if(__b->z>__a->z){
		__a->z = __b->z;
	}
}
