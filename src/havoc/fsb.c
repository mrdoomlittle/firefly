# include "../m_alloc.h"
# include "../y_int.h"
# include "../assert.h"
# include "../ffly_def.h"
# include "common.h"
static struct fsb_area a = {
	0, 0, 128, NULL
};
static struct fsb_area *arr[8] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&a
};

#define A1	0
#define A2	1
#define A4	2
#define A8	3
#define A16	4
#define A32	5
#define A64	6
struct fsb_area *fsb_alloc(_int_u __depth) {
	struct fsb_area **a;
	struct fsb_area *_a;
	a = arr+__depth;
	if (*a != NULL) {
		_a = *a;
		*a = _a->child;
		assert(_a != NULL);
		return _a;
	}
	while(!*a) {
		a++;
	}

	assert(*a != NULL);
	
	_a = *a;
	struct fsb_area *q;
	q = (struct fsb_area*)m_alloc(4*sizeof(struct fsb_area));
	_int_u x, y;
	x = _a->x;
	y = _a->y;
	q->x = x;
	q->y = y;
	_int_u of = _a->size>>1;
	q->size = of;
	q[1].x = x+of;
	q[1].y = y;
	q[1].size = of;
	q[2].x = x;
	q[2].y = y+of;
	q[2].size = of;
	q[3].x = x+of;
	q[3].y = y+of;
	q[3].size = of;
	q->child = q+1;
	q[1].child = q+2;
	q[2].child = q+3;
	q[3].child = NULL;
	*(a-1) = q;
	*a = _a->child;
	printf("split, %u to %u.\n", _a->size, of);
	// repeat
	return fsb_alloc(__depth);
}

_64_u H_8_8_ALLOC(struct fsb_alloc_8_8 *__a) {
	_64_u r;
	r = __a->at;
	__a->at+=64;
	return r;
}
/*
# include <string.h>
char static map[64*64];
void clear() {
	memset(map, '.', sizeof(map));
	_int_u i;
	i = 0;
	for(;i != 64;i++) {
		map[63+(i*64)] = '\n';
	}
	map[(64*64)-1] = '\0';
}
void test() {
	clear();
	alloc(A8);
	_int_u i = 0;
	for(;i != 24;i++) {
		struct fsb_area *a;
		a = alloc(A4);
		_int_u x, y;
		y = 0;
		for(;y != a->size;y++) {
			x = 0;
			for(;x != a->size;x++) {
				map[(x+a->x)+((y+a->y)*64)] = 'a'+i;
			}
		}
		printf("%s\n", map);
		usleep(500000);
		//printf("%u, %u, %u.\n", a->x, a->y, a->size);
	}
}

int main() {
	test();
}*/
