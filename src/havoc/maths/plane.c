#include "../maths.h"
/*
	NOTE:
		this routine doesent care about the Z axis, meaning the plane could be in front or behind
		and the result returned would be the same if the distance from and to the point and plane are the same along the z axis.
		its up the high routines to check the z axis.
*/
_8_s xyplane_intersect(float __xs, float __xe, float __ys, float __ye, float __z, float x, float y, float z, float dx, float dy, float dz) {
	//distance along the zaxis from point to plane
	float zdis = z-__z;

	float j,m;
	j = dx/dz;
	m = dy/dz;

	float k,h;
	k = zdis*j;//are x deviation from are point
	h = zdis*m;//are y deviation

	/*
			here we pretend that the z axis doesent exist, and all lies on a 2d plane.
			meaning the plane and point lie on the same 2d plane.
	*/

	float xp,yp;
	xp = x+k;
	yp = y+h;
	if(xp<__xs && xp>__xe){
		return -1;//out of bounds
	}

	if(yp<__ys && yp>__ye){
		return -1;
	}
	return 0;
}
