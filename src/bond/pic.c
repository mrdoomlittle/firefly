#include "bond.h"
#include "../blf.h"
#include "../io.h"
#include "../assert.h"
#include "../em_pic/core.h"
#include "../blf.h"
_16_u static jmp18_real[] = {
	/*
		movlp
		goto
	*/
	0x3180,
	0x2800
};
_16_u static jmp11_real[] = {
	0x2800	
};

_16_u static call18_real[] = {
	/*
		movlp
		goto
	*/
	0x3180,
	0x2000
};
_16_u static call11_real[] = {
	0x2000	
};



_32_u static jmp_em[] = {
	_em_pic_goto
};

_32_u static call_em[] = {
	_em_pic_call
};

/*
	jump18 include setting a movlp
*/
void static j18_real(_16_u *__buf, _64_u __addr) {
	__buf[0] |= __addr&0x7ff;
	__buf[1] |= __addr>>11;
}

void static j11_real(_16_u *__buf, _64_u __addr) {
	__buf[0] |= __addr&0x7ff;
}

void static j0_em(_32_u *__buf, _64_u __addr) {
	__buf[0] |= __addr<<8;
}

struct bn_patch pic_patch[2] = {
	{~0x7ff,~0xffff,1,2,2,4,j11_real,j18_real,jmp11_real,jmp18_real},
	{~0x7ff,~0xffff,1,2,2,4,j11_real,j18_real,call11_real,call18_real}
};

struct bn_patch pic_em_patch[2] = {
	{~0xffffffff,0,1,0,4,0,j0_em,NULL,jmp_em,NULL},
	{~0xffffffff,0,1,0,4,0,j0_em,NULL,call_em,NULL}
};

#include "../string.h"
void pic_fix(bn_tangp __t,struct bn_rel *rl) {
	struct bn_picfix *pf = __t;
	_32_u *outbuf = __t->buf;
	_int_u ndw = 0;
	_64_u buf = pf->buf;
	_64_u bts = pf->bts;
	_8_u ib[10];	
	_8_u ic = 0, i = 0;
	_64_u adr = __t->dis;
	printf("PICFIX: level: %u, adr: %u, dis: %d\n", __t->level, __t->adr, __t->dis);
	if (__t->level != 255) {
		assert(__t->level<2);
		assert(__t->fu.pt != NULL);
		void(*j)(_16_u*, _64_u) = (&__t->fu.pt->j0)[__t->level];
		_8_u *_ib = (&__t->fu.pt->ib0)[__t->level];
		_int_u wds = (&__t->fu.pt->w0)[__t->level];
		_int_u sz = (&__t->fu.pt->s0)[__t->level];
		printf("WORDS: %u, SIZE: %u.\n",wds, sz);
		mem_cpy(ib,_ib,sz);
		j(ib, adr);
		ic = wds;
		__t->words+=wds;
	}
_again:
	if (bts<=32) {
		_64_u w = buf>>bts;
		outbuf[ndw] = w&0xffffffff;
		printf("DW0: %x.\n", outbuf[ndw]);
		ndw++;
		bts+=32;
	}


	if (i != ic) {
		_64_u w = *(_16_u*)(ib+(i*2));
		buf>>=14;
		buf|=(w&0x3fff)<<(64-14);

		bts-=14;
		printf("WORD: %x\n", w);
		i++;
		goto _again;
	}

	outbuf[ndw] = buf>>bts;
	printf("DW: %x.\n", outbuf[ndw]);
	ndw++;
	__t->bsz = ndw*4;	
	printf("PIC_BSZ: %u.\n", __t->bsz);
}

void pic_em_fix(bn_tangp __t,struct bn_rel *rl) {
	if (!rl)return;
	printf("FIX: %u.\n",__t->bsz);
	if (rl->sm != NULL) {
		/*
			we anticipate that the data is at the end.
		*/
		_8_u *page = __t->_m->pages[__t->src>>F_BNM_PAGE_SHFT];
		_8_u *p = ((_8_u*)(page+(__t->src&F_BNM_PAGE_MASK)))+rl->where;
		
		_64_u adr;
		if (!rl->sm->w)
			adr = rl->sm->adr+rl->dis;
		else
			adr = rl->sm->w->adr+rl->dis;
		if (rl->flags&BLF_R_PART) {
			adr>>=8;
		}
		*p = adr&0xff;
		printf("####%x[ %x : %x ] - where: %x, dis: %x - %x / %s\n",*(p-1), *p,rl->sm->adr,rl->where,rl->dis,rl->flags,rl->flags&BLF_R_PART?"HIGH":"LOW");	
		return;
	}
	_32_u *outbuf = __t->buf;
	_64_u adr = __t->dis*4;
	if (__t->level != 255) {
		struct bn_patch *pt;
		pt = __t->fu.pt;
		assert(pt != NULL);
		mem_cpy(outbuf,pt->ib0,pt->s0);
		((void(*)(_32_u*,_64_u))pt->j0)(outbuf,adr);
		__t->words+=1;
		__t->bsz = 4;
		printf("DWORDOUT: %x.\n", *(_32_u*)outbuf);
	}
}

