#include "bond.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../string.h"
#include "../remf.h"
#define PAR _ulonglong PAR_NAME
#define PAR_NAME __ctx
#define _pread(__buf, __size, __offset)\
	F_bond_pread(PAR_NAME, __buf, __size, __offset)
#include "../tools.h"
_8_i static
hdrcheck(struct remf_hdr *__hdr) {
	_8_u *_0;
	_0 = __hdr->h.ident;
	_8_u im;
	if (*_0 != FH_MAG0) {
		goto _mm;
	}
	im = 1;
	if (_0[1] != FH_MAG1) {
		goto _mm;
	}
	im = 2;
	if (_0[2] != FH_MAG2) {
		goto _mm;
	}
	im = 3;
	if (_0[3] != FH_MAG3) {
		goto _mm;
	}
	im = 4;
	if (_0[4] != FH_MAG4) {
		goto _mm;
	}
	im = 5;
_mm:
	if (im != 5) {
		printf("FHEADER ident missmatch.\n");
		return -1;
	}
	if (__hdr->h.id != _f_remf) {
		printf("this file isent in remf format.\n");
		return -1;
	}
	if (__hdr->h.vers != FH_VERS) {
		printf("version missmatch.\n");
		return -1;
	}

	return 0;
}

static struct f_bond_cake *ck;
struct segment {
	_int_u size;
	_8_u *p;
	_64_u adr;
	struct segment *next;
};


static struct segment *pgs = NULL;
static _64_u pgb_sz = 0;
static struct bn_tang *ttbl;
static struct bn_wa *wtbl;
static struct bn_wa_block *wpt;
static _8_u *stt;

void static
absorb_symbs(struct remf_sy *__sy, _int_u __n, struct hash *__hm) {
	struct remf_sy *s;
	struct bn_symb *sm;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		s = __sy+i;
		struct remf_stent *e;
		e = (struct remf_stent*)(stt+s->name);
		_8_u *p = (_8_u*)(((_64_u)e)+F_REMF_STE);
		sm = bn_symbfor(p, e->l, __hm);
		char buf[128];
		mem_cpy(buf, p, e->l);
		buf[e->l] = '\0';
		printf("SYMB: %s.\n", buf);
		switch(s->type) {
			case F_SY_WA: {
				if (s->flags&F_SY_RS) {
					printf("wa needs to be resolved.\n");
				//	wpt[s->val].w = &sm->w;
				} else {
					printf("wa present.\n");
					struct bn_wa *w = wtbl+s->val;
				//	sm->w = w;
				//	sm->type = F_BN_REMF_ST_TANG;
				//	sm->t = t;
			}
			break;
			}
		}
	}
}

void static 
absorb_rel(struct remf_rel *__rel, _int_u __n) {
	struct bn_block *b;
	b = (struct bn_block*)m_alloc(sizeof(struct bn_block));
	struct bn_rel *_r, *r;
	_r = (struct bn_rel*)m_alloc(sizeof(struct bn_rel)*__n);
	struct remf_rel *rl;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		r = _r+i;
		rl = __rel+i;
		r->src = ttbl+rl->src;
		printf("RELOC' src: %u, dst: %u, type: %u, flags: %u\n", rl->src, rl->dst, rl->type, rl->flags);
		switch(rl->type) {
			case F_REMF_R_16:
				r->type = F_BN_R_16;
			break;
			case F_REMF_R_32:
				r->type = F_BN_R_32;
			break;
		}
		if (rl->flags&F_REMF_R_UDD) {
			bn_wa_lk(&r->dst, wpt+__rel->dst);
		} else {
			r->dst = wtbl+__rel->dst;
		}
	}
	b->n = __n;
	b->p = _r;
	b->next = f_bs.rl;
	f_bs.rl = b;
}


void static absorb_regions(PAR, struct remf_reg_hdr *__regs, _int_u __n) {
	struct remf_reg_hdr *r;
	_int_u i, ii;
	i = 0;
	for(;i != __n;i++) {
		r = __regs+i;
		void *p;
		p = m_alloc(r->size);
		_pread(p, r->size, r->offset);
		if (i == F_RG_RLT) {
			absorb_rel(p, r->n);
		} else if (i == F_RG_SYT) {
			/*
				default symbols, local
			*/
			absorb_symbs(p, r->_1, &ck->hm);
			struct remf_agg *a;
			a = ((_8_u*)p)+r->_0;
			_int_u i;
			i = 0;
			for(;i != F_REMF_AGG_SY_N;i++) {
				absorb_symbs(((_8_u*)p)+a->start, a->n, &f_bs.symbols);	
				a++;
			}
		} else if (i == F_RG_STT) {
			stt = (_8_u*)p;
			continue;
		} else {
			struct segment *ps;
			ps = (struct segment*)m_alloc(sizeof(struct segment));
			ps->p = p;
			ps->adr = r->adr;
			ps->size = r->size;
			ps->next = pgs;
			pgs = ps;
			pgb_sz+= r->size;
			continue;
		}

		m_free(p);
	}
}
static struct bn_wa *ep = NULL;
void static
proc_src(struct f_bond_sf *sf) {
	_ulonglong __ctx = sf->ctx;
	pgs = NULL;
	ck = (struct f_bond_cake*)m_alloc(sizeof(struct f_bond_cake));
	f_bond_hash_init(&ck->hm);
	struct remf_hdr hdr;
	_pread(&hdr, sizeof(struct remf_hdr), 0);
	if (hdrcheck(&hdr) == -1)
		return;
	struct remf_tt tt;
	_pread(&tt, sizeof(struct remf_tt), hdr.tt);

	printf("remf_HDR: rsts: %u, rn: %u, rg: %u.\n", hdr.rsts, hdr.rn, hdr.rg);
	printf("remf_TT: t_n: %u, t: %u.\n", tt.t_n, tt.t);
	bn_tgclumpp clm = bn_tgclump();

	ttbl = (struct bn_tang*)m_alloc(tt.t_n*sizeof(struct bn_tang));
	clm->tg = ttbl;
	clm->n = tt.t_n;
	struct remf_tang *t = (struct remf_tang*)m_alloc(tt.t_n*sizeof(struct remf_tang)), *ct;
	_pread(t, tt.t_n*sizeof(struct remf_tang), tt.t);

	wpt = (struct bn_wa_block*)m_alloc(tt.w_m*sizeof(struct bn_wa_block));
	wtbl = (struct bn_wa*)m_alloc(tt.w_n*sizeof(struct bn_wa));

	struct remf_wa *w = (struct remf_wa*)m_alloc(tt.w_n*sizeof(struct remf_wa)), *cw;
	_pread(w, tt.w_n*sizeof(struct remf_wa), tt.w);
	struct bn_wa *_w;
	_int_u i;
	i = 0;
	for(;i != tt.w_n;i++) {
		cw = w+i;
		_w = wtbl+i;
	/*
		sum of last files + my address
	*/
		_w->adr = cw->adr+f_bs.adr;
	}

	i = 0;
	for(;i != tt.w_m;i++) {
		wpt[i].l = NULL;
	}

	struct bn_link *l;
	l = (struct bn_link*)m_alloc(sizeof(struct bn_link));
	l->b = wpt;
	l->n = tt.w_m;
	l->next = f_bs.lk;
	f_bs.lk = l;

	if ((hdr.flags&F_REMF_EPDEG)>0) {
		ep = wtbl+hdr.routine;
	}

	struct remf_reg_hdr *rh, *r;
	rh = (struct remf_reg_hdr*)m_alloc(hdr.rsts);
	_pread(rh, hdr.rsts, hdr.rg);

	absorb_regions(PAR_NAME, rh, hdr.rn);
	struct f_bond_map *m;
	m = bn_map_new(0);
	f_bond_mapout(m, pgb_sz);
	if (!pgb_sz) {
		printf("no progbits present.\n");
	} else {
		struct segment *s;
		s = pgs;
		while(s != NULL) {
			f_bond_write(m, s->p, s->size, s->adr);
			printf("PROGRAM-BITS.\n");
			ffly_hexdump(s->p, s->size);
			s = s->next;
		}
	}
	clm->m = m;
	bn_tangp _t;
	_64_u lastsrc = 0;
	i = 0;
	for(;i != tt.t_n;i++){
		ct = t+i;
		_t = ttbl+i;
		_t->nb = 0;
		_t->src = ct->src;
		_t->adr = ct->adr+f_bs.adr;
	//	_t->d = NULL;
		_t->k = 0;
		_t->bsz = 0;
		_t->size = ct->size;
		if (ct->src<lastsrc) {
			printf("WARNING: tangs not in order.\n");
		}
		lastsrc = ct->src;
	}
	f_bs.adr+=hdr.adr;
}

void static
done(void) {
}

void 
bn_remf_output(_ulonglong __ctx) {
	struct remf_hdr hdr;
    *hdr.h.ident    = FH_MAG0;
    hdr.h.ident[1]  = FH_MAG1;
    hdr.h.ident[2]  = FH_MAG2;
    hdr.h.ident[3]  = FH_MAG3;
    hdr.h.ident[4]  = FH_MAG4;
    hdr.h.vers = FH_VERS;
    hdr.h.id = _f_remf;

    *hdr.ident      = FF_REMF_MAG0;
    hdr.ident[1]    = FF_REMF_MAG1;
    hdr.ident[2]    = FF_REMF_MAG2;
    hdr.ident[3]    = FF_REMF_MAG3;
   	hdr.id = REMF_ID_RES; 
	hdr.rn = 0;
	hdr.rsts = 0;
	hdr.rg = FF_REMF_NULL;
	hdr.tt = FF_REMF_NULL;
	if (ep != NULL) {
		printf("ENTRY POINT.\n");
		hdr.routine = ep->adr;
	}
	hdr.adr = f_bs.adr;
	hdr.sg = f_bs.out.offset;
	hdr.sn = 1;
	hdr.ssts = remf_seghdrsz;
	struct remf_seg_hdr seg;
	seg.type = 0x00;
	seg.offset = remf_hdrsz;
	seg.sz = f_bs.adr;
	seg.adr = 0;
	f_bs.out.write(__ctx, &seg, remf_seghdrsz);
	f_bs.out.pwrite(__ctx, &hdr, remf_hdrsz, 0);
}

void f_bond_remf(void) {
	f_bs.proc_src = proc_src;
	f_bs.done = done;
}
