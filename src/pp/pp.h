# ifndef __ffly__pp__h
# define __ffly__pp__h
# include "../y_int.h"
# include "../ffly_def.h"
_8_u extern *p;
_8_u extern *end;
int extern src;
int extern dst;

typedef struct bucket {
	_8_u sort, val;
	_8_u *p;
} *bucketp;

enum {
	_percent,
	_ident,
	_chr
};

enum {
	_define,
	_ifndef,
	_ifdef,
	_endif
};

_8_u at_eof();
bucketp lex();
void oust(_8_u);
void prossess();
void flushbuf();
void prep();
void finish();
void ulex(bucketp);
void sk_space();
void define(char*, _64_u, _8_u*, _8_u*);
# endif /*__ffly__pp__h*/
