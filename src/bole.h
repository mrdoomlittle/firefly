# ifndef __ffly__boleig__h
# define __ffly__boleig__h
# include "system/buff.h"
# include "system/vec.h"
# include "system/map.h"
# include "y_int.h"
# include "types.h"
#define ffly_bole_is_str(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_str)
#define ffly_bole_is_chr(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_chr)
#define ffly_bole_is_64l_u(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_64l_u)
#define ffly_bole_is_32l_u(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_32l_u)
#define ffly_bole_is_16l_u(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_16l_u)
#define ffly_bole_is_8l_u(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_8l_u)
#define ffly_bole_is_64l_s(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_64l_s)
#define ffly_bole_is_32l_s(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_32l_s)
#define ffly_bole_is_16l_s(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_16l_s)
#define ffly_bole_is_8l_s(__p) \
	(((struct ffly_bole_val*)__p)->kind == _ffly_bole_8l_s)

/*
	TODO:
		cleanup
		replace 'free' vec with linked list struct 

		changed name to bole i dont know if its a fitting name.
*/

enum {
	_ffly_bole_val,
	_ffly_bole_var,
	_ffly_bole_arr,
	_ffly_bole_struct
};

struct ffly_bole_val {
	_8_u kind;
	_f_byte_t *p;
};

struct ffly_bole_var {
	struct ffly_bole_val *val;
	char *name;
};

struct ffly_bole_arr {
	struct ffly_vec data;
	char *name;
	_int_u l;
};

struct ffly_bole_struct {
	struct ffly_map fields;
	char *name;
};

struct ffly_bole_entity {
	_8_u kind;
	void *p;
};

// boleig bare
// all in one
typedef struct {
	struct ffly_vec entities;
	struct ffly_vec free;
	struct ffly_map env;
} ffbole;

typedef ffbole* ffbolep;

struct ffly_bole {
	struct ffly_buff sbuf;
	_f_byte_t *p, *end;
	_f_off_t off;
	struct ffly_vec toks;
	// rename to tokbuf
	struct ffly_buff tokbuf;

	struct ffly_vec entities;
	struct ffly_vec free;
	struct ffly_map env;
};

enum {
	_ffly_bole_str,
	_ffly_bole_chr,
// signed int
	_ffly_bole_64l_s,
	_ffly_bole_32l_s,
	_ffly_bole_16l_s,
	_ffly_bole_8l_s,
// unsigned int
	_ffly_bole_64l_u,
	_ffly_bole_32l_u,
	_ffly_bole_16l_u,
	_ffly_bole_8l_u
};

void const* ffly_bole_get(ffbolep, char const*);
_int_u ffly_bole_arr_len(void const*);
void const* ffly_bole_arr_elem(void const*, _int_u);
_f_err_t ffly_bole_init(struct ffly_bole*);
_f_err_t ffly_bole_read(struct ffly_bole*);
_f_err_t ffly_bole_ld(struct ffly_bole*, char const*);
_f_err_t ffly_bole_free(struct ffly_bole*);
_f_err_t ffbole_free(ffbolep);
void ffly_bole_depos(struct ffly_bole*, ffbolep);
char const* ffly_bole_str(void const*);
char ffly_bole_chr(void const*);
_64_u ffly_bole_64l_u(void const*);
_32_u ffly_bole_32l_u(void const*);
_16_u ffly_bole_16l_u(void const*);
_8_u ffly_bole_8l_u(void const*);
_64_u ffly_bole_int_u(void const*);

_64_i ffly_bole_64l_s(void const*);
_32_i ffly_bole_32l_s(void const*);
_16_i ffly_bole_16l_s(void const*);
_8_i ffly_bole_8l_s(void const*);
_64_i ffly_bole_int_s(void const*);
void const* ffly_bole_struc_get(void const*, char const*);
# endif /*__ffly__boleig__h*/
