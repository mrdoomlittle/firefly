# include "../wharf.h"

void f_whf_send(f_whf_conp __con, void *__buf, _int_u __size) {
	send(__con->fd, __buf, __size, 0);
}

void f_whf_recv(f_whf_conp __con, void *__buf, _int_u __size) {
	recv(__con->fd, __buf, __size, 0);
}
