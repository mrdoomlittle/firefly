# include "pkg.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "dep/str_cpy.h"
# include "dep/mem_cpy.h"
# include "linux/limits.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "system/io.h"
char static dir[PATH_MAX];

char const *pkc_plan;
_int_u pkc_pfl;

#define is_space(__c) \
	(__c == ' ' || __c == '\n' || __c == '\t')
void ff_pkc_init(void) {

}

struct link {
	struct link *next;
	struct pkc_ingot *p;
};

static _8_u *text;
static _int_u cur;
static _int_u txsz;

_8_u static at_eof(void) {
	return cur>=txsz;
}

struct pkc_ingot static*
read_ingot(void) {
	_8_u eo;
	while(is_space(*(text+cur)) && !(eo = at_eof()))
		cur++;
	
	if (eo || *(text+cur) == '#')
		return NULL;
	_8_u *p;

	p = text+cur;
	char buf[PATH_MAX];
	char *bufp;

	bufp = buf;
_again:
	if (*p != ',') {
		*(bufp++) = *(p++);
		goto _again;
	}
	p++;
	*bufp = '\0';
	cur=(p-text);

	struct pkc_ingot *i;
	i = (struct pkc_ingot*)__f_mem_alloc(
		sizeof(struct pkc_ingot));

	i->plen = (bufp-buf);
	i->path = (char const*)__f_mem_alloc(i->plen+1);
	f_mem_cpy((void*)i->path, buf, i->plen+1);
	return i;
}

struct pkc_plan*
pkc_get_plan(void) {
	char file[PATH_MAX];
	char *p = file;
	p+=f_str_cpy(p, dir);
	f_mem_cpy(p, pkc_plan, pkc_pfl);
	*(p+pkc_pfl) = '\0';

	int fd;
	fd = open(file, O_RDONLY, 0);
	struct stat st;
	fstat(fd, &st);
	
	struct pkc_plan *plan;
	cur = 0;
	plan = (struct pkc_plan*)__f_mem_alloc(sizeof(struct pkc_plan));
	txsz = st.st_size;
	text = (_8_u*)__f_mem_alloc(txsz);
	read(fd, text, txsz);
	struct link *l, *top = NULL;
	struct pkc_ingot *i;
	_int_u n;

	n = 0;
_again:
	if (!(i = read_ingot())) {
		goto _end;
	}

	l = (struct link*)__f_mem_alloc(sizeof(struct link));

	l->p = i;
	l->next = top;
	top = l;
	n++;
	goto _again;
_end:
	plan->n = n;
	printf("N: %u\n", n);
	if (n>0) {
		plan->i = (struct pkc_ingot**)__f_mem_alloc(n*sizeof(struct pkc_ingot*));
	
		struct link *b;
		struct pkc_ingot **ip;
		ip = plan->i+n;
		l = top;
		while(l != NULL) {
			b = l;
			l = l->next;

			*(--ip) = (i = b->p);

			unsigned int mode;
			stat(i->path, &st);
			switch(st.st_mode&S_IFMT) {
				case S_IFREG:
					i->type = IG_FILE;
				break;
				case S_IFDIR:
					i->type = IG_DIR;
				break;
				default:
					i->type = IG_UNKNOWN;
			}

			__f_mem_free(b);

			printf("ingot: %s, type: %s\n", i->path, i->type == IG_FILE?"file":"directory");
		}
	} else
		plan->i = NULL;
	close(fd);
	return plan;
}

void ff_pkc_de_init(void) {

}

# include "dep/str_cmp.h"
void ff_pkc_construct(char const*, char const*);
_f_err_t ffmain(int __argc, char const *__argv[]) {
	if (__argc<3){
		return 0;
	}

	char dc;
	char const *a0, *a1;
	char const **av, **e;
	av = __argv+1;
	e = av+__argc;
	while(av != e) {
		if (!f_str_cmp(*av, "-c")) {
			dc = 'c';
		} else if (!f_str_cmp(*av, "-d")) {
			dc = 'd';
		} else if (!f_str_cmp(*av, "-p")) {
			a0 = *(++av);
		} else if (!f_str_cmp(*av, "-dat")) {
			a1 = *(++av);
		} else if (!f_str_cmp(*av, "-dir")) {
			a0 = *(++av);
		}
		av++;
	}

	printf("%c : %s:%s\n", dc, a0, a1);
	if (dc == 'c') {
		ff_pkc_construct(a0, a1);
	} else if (dc == 'd') {
		ff_pkc_deconstruct(a0, a1);
	}
}
