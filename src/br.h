# ifndef __ffly__br__h
# define __ffly__br__h
# include "y_int.h"
# include "net.h"
# include "brick.h"
typedef struct ffly_br {
	FF_SOCKET *sock;
	_32_u *bricks;
	_int_u brick_c;
	_8_u bricksize;
} *ffly_brp;

void ffly_br_retrieve(_8_u, _int_u, void*);
void ffly_br_put(ffly_brp, _32_u, _int_u);
void ffly_br_free(ffly_brp);
void ffly_br_prep(ffly_brp, _8_u, _int_u);
void ffly_br_shutdown(ffly_brp);
void ffly_br_open(ffly_brp);
void ffly_br_close(ffly_brp);
void ffly_br_start(ffly_brp);
# endif /*__ffly__br__h*/
