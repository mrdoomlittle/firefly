# ifndef __ffly__atomic__h
# define __ffly__atomic__h
# include "y_int.h"
# ifdef __cplusplus
extern "C" {
# endif
#define f_atomic_add ffly_atomic_add
#define f_atomic_sub ffly_atomic_sub
#define f_atomic_set ffly_atomic_set
#define f_atomic_get ffly_atomic_get
#define f_atomic_incr ffly_atomic_incr
#define f_atomic_decr ffly_atomic_decr
#define atomic_incr ffly_atomic_incr
#define atomic_decr ffly_atomic_decr
#define ffly_atomic_add(__v, __by)\
	__asm__("lock addq %1, (%0)\n" : : "r"(__v), "r"((_64_u)__by));
#define ffly_atomic_sub(__v, __by)\
    __asm__("lock subq %1, (%0)\n" : : "r"(__v), "r"((_64_u)__by));
#define atmq_add(__v, __by)\
	__asm__("lock addq %1, (%0)\n" : : "r"(__v), "r"((_64_u)__by));
#define atmq_sub(__v, __by)\
    __asm__("lock subq %1, (%0)\n" : : "r"(__v), "r"((_64_u)__by));

#define ffly_atomic_set(__v, __to)\
		*(__v) = (__to);
#define ffly_atomic_get(__v)\
	*(__v)
#define ffly_atomic_incr(__v)\
	__asm__("lock incq (%0)\n" : : "r"(__v));
#define ffly_atomic_decr(__v)\
	__asm__("lock decq (%0)\n" : : "r"(__v));
#define atmq_inc(__v) __asm__("lock incq (%0)\n" : : "r"(__v));
#define atmq_dec(__v) __asm__("lock decq (%0)\n" : : "r"(__v));

#define atmd_inc(__v) __asm__("lock incl (%0)\n" : : "r"(__v));
#define atmd_dec(__v) __asm__("lock decl (%0)\n" : : "r"(__v));

#define atmw_inc(__v) __asm__("lock incw (%0)\n" : : "r"(__v));
#define atmw_dec(__v) __asm__("lock decw (%0)\n" : : "r"(__v));

//halflig
#define atmh_inc(__v) __asm__("lock incb (%0)\n" : : "r"(__v));
#define atmh_dec(__v) __asm__("lock decb (%0)\n" : : "r"(__v));
#include "ffly_def.h"
_16_u __FORCE_INLINE__ xwadd(_16_u *__dst, _16_u __val){
  _16_u result;
  __asm__ __volatile__(
    "lock xadd %2,(%1)\n"
    "movw %2,%0\n" : "=m"(result) : "r"(__dst), "r"(__val));
  return result;
}
_32_u __FORCE_INLINE__ xdadd(_32_u *__dst, _32_u __val){
  _32_u result;
  __asm__ __volatile__(
    "lock xaddl %2,(%1)\n"
    "movl %2,%0\n" : "=m"(result) : "r"(__dst), "r"(__val));
  return result;
}


#define atmq_and(__dst,__val) __asm__("lock andq %1,(%0)\n" : : "r"(__dst), "r"(__val));
#define atmd_or(__dst,__val) __asm__("lock orq %1,(%0)\n" : : "r"(__dst), "r"(__val));


# ifdef __cplusplus
}
namespace mdl {
namespace firefly {
namespace system {
typedef struct {
	ff::u64_t operator++(int) {
		ff::u64_t ret = this->d;
		ffly_atomic_incr(&this->d);
		return ret;
	}

	ff::u64_t operator--(int) {
		ff::u64_t ret = this->d;
		ffly_atomic_decr(&this->d);
		return ret;
	}

	ff::u64_t operator=(ff::u64_t __d) {
		this->d = __d;
		return __d;
	}

	ff::u64_t val() {return this->d;}
	ff::u64_t d;
} atomic_t;
}
}
}
# endif
# endif /*__ffly__atomic__h*/
