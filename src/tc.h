# ifndef __ffly__tc__h
# define __ffly__tc__h
# include "y_int.h"

struct tc_spec {
	_64_u sec, nsec;
};

void ff_tc_gettime(struct tc_spec*);
# endif /*__ffly__tc__h*/
