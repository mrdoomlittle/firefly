#include "billet.h"
#include "m_alloc.h"
#include "string.h"
#include "sched.h"
#include "clock.h"
#include "system/flags.h"
#include "hard.h"
#include "msg.h"
/*
	billets are to be use in conjunction with crypto 
	say encrypting a large amount of data.
	theres no point of it staying in memory say dump
	it to disc for the meantime.
*/
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_BILLET)
#define DECAY_TIME TIME_FMS(2000)// 4 seconds
#define PAGE_SHIFT 8
#define PAGE_SIZE (1<<PAGE_SHIFT)
#define PAGE_MASK (PAGE_SIZE-1)
#define PAGE_LOADED 0x01
F_DEF_MUTEX(static lock)
static y_bltp top = NULL;
void static _page_load(struct blt_page *__page) {
	__page->p = m_alloc(PAGE_SIZE);
	hard_read(__page->h, __page->p, PAGE_SIZE, 0);
	__page->flags |= PAGE_LOADED;
	__page->decay = clockv;
}

void static _page_unload(struct blt_page *__page) {
	hard_write(__page->h, __page->p, PAGE_SIZE, 0);
	m_free(__page->p);
	__page->flags ^= PAGE_LOADED;
}

void static _update_page(struct blt_page *__page) {
	_8_s res;

	res = mt_trylock(&__page->lock);
	if (res == -1) {
		return;
	}

	if ((clockv-__page->decay)>=DECAY_TIME) {
		_page_unload(__page);
	}
	mt_unlock(&__page->lock);
}

void static _update_pages(y_bltp __blt) {
	char buf[17*16+1];
	mt_lock(&__blt->lock);
	_int_u i = 0;
	struct blt_page **pp, **end, *pg;
	pp = __blt->p;
	end = pp+__blt->page_c;
	while(pp != end) {
		pg = *pp++;
		char c;
		if ((i>>4)>3) {
			buf[(4*17)-1] = '\0';
			while(i != (4*17)-1) {
				buf[i++] = '!';
			}
			i = 0;
			while(i != 3*17) {
				buf[i+16] = '\n';
				i+=17;
			}

			i = 0;
			MSG(INFO, "BLT-PAGES\n%s\n", buf)
		}
		if (f_is_flag(pg->flags, PAGE_LOADED)) {
			c = '#';
			_update_page(pg);
		} else {
			c = '.';
		}
		buf[i+(i>>4)] = c;
		i++;
	}

	mt_unlock(&__blt->lock);
}


_8_i _tick(long long __arg) {
	MSG(INFO, "updading billet pages.\n")
	y_bltp blt;
	mt_lock(&lock);
	blt = top;
	while(blt != NULL) {
		MSG(INFO, "billet with %u-pages.\n", blt->page_c);
		_update_pages(blt);
		blt = blt->next;
	}
	mt_unlock(&lock);
}

void blt_init(void) {
	sched_entityp ent;
	ent = sched_new();
	ent->type = _sched_time_bound;
	ent->iv = TIME_FMS(1000);
	ent->arg = 0;
	ent->func = _tick;

	sched_submit(ent);
}

struct blt_page* _page_alloc(void) {
	struct blt_page *pg;
	pg = (struct blt_page*)m_alloc(sizeof(struct blt_page));

	return pg;
}

void static _init_page(struct blt_page *__p) {
	__p->flags = PAGE_LOADED;
	__p->p = m_alloc(PAGE_SIZE);

	__p->h = hard_alloc(PAGE_SIZE);
	__p->lock = MUTEX_INIT;
}

y_bltp blt_new(void) {
	y_bltp blt;
	blt = (y_bltp)m_alloc(sizeof(struct y_blt));
	blt->size = 0;
	blt->page_c = 1;
	blt->lock = MUTEX_INIT;
	struct blt_page *pg;
	blt->p = (struct blt_page**)m_alloc(sizeof(struct blt_page*));
	_init_page(pg = (*blt->p = _page_alloc()));
	pg->num = 0;

	mt_lock(&lock);
	if (top != NULL)
		top->bk = &blt->next;
	blt->next = top;
	blt->bk = &top;
	top = blt;
	mt_unlock(&lock);

	return blt;
}

void static _page_acquire(struct blt_page *__page) {
	mt_lock(&__page->lock);
	/*if (!f_is_flag(__page->flags, PAGE_LOADED)) {
		_page_load(__page);
		return;
	}
	__page->decay = clockv;
*/
}

void static _page_release(struct blt_page *__page) {
	mt_unlock(&__page->lock);
}

void blt_destroy(y_bltp __blt) {

}

void static _page_read(struct blt_page *__p, void *__dst, _int_u __size, _int_u __offset) {
	_page_acquire(__p);
	mem_cpy(__dst, ((_8_u*)__p->p)+__offset, __size);
	_page_release(__p);
}

void static _page_write(struct blt_page *__p, void *__src, _int_u __size, _int_u __offset) {
	_page_acquire(__p);
	mem_cpy(((_8_u*)__p->p)+__offset, __src, __size);
	_page_release(__p);
}

void blt_read(y_bltp __blt, void *__dst, _int_u __size, _32_u __offset) {
	_int_u end, pe; 
	end = __offset+__size;
	pe = (end+(PAGE_SIZE-1))>>PAGE_SHIFT;

	_8_u *dst;
	dst = (_8_u*)__dst;
	_int_u page, pg_off, left;
	page = __offset>>PAGE_SHIFT;
	pg_off = __offset-(page<<PAGE_SHIFT);
	struct blt_page **pp;
	pp = __blt->p;

	if (__size<=(left = (PAGE_SIZE-pg_off))) {
		_page_read(*(pp+page), dst, left, pg_off);
		return;
	} {
	if (pg_off>0) {
		_page_read(*(pp+page), dst, left, pg_off);
		pg_off = 0;
		page++;
		dst+=left;
	}
	}
	while(page < (__offset+__size)>>PAGE_SHIFT) {
		_page_read(*(pp+page), dst, PAGE_SIZE, 0);
		page++;
		dst+=PAGE_SIZE;
	}

	left = __size-(dst-(_8_u*)__dst);
	if (left>0) {
		_page_read(*(pp+page), dst, left, 0);
	}
}

void blt_write(y_bltp __blt, void *__src, _int_u __size, _32_u __offset) {
	mt_lock(&__blt->lock);
	_int_u ps, pe;
	ps = __blt->page_c;
	_int_u end;
	struct blt_page *pg;
	end = __offset+__size;

	pe = (end+(PAGE_SIZE-1))>>PAGE_SHIFT;
	
	if (pe>ps) {
		__blt->p = (struct blt_page**)m_realloc(__blt->p, pe*sizeof(struct blt_page*));
		_int_u i;
		i = ps;
		for(;i != pe;i++) {
			_init_page(pg = (*(__blt->p+i) = _page_alloc()));
			pg->num = i;
		}
		__blt->page_c = pe;
	}

	mt_unlock(&__blt->lock);

	_8_u *src;
	src = (_8_u*)__src;
	_int_u page, pg_off, left;
	page = __offset>>PAGE_SHIFT;
	pg_off = __offset&PAGE_MASK;
	struct blt_page **pp;
	pp = __blt->p;
	if (__size<=(left = (PAGE_SIZE-pg_off))) {
		_page_write(*(pp+page), src, left, pg_off);
		return;
	} else { 
	/*
		if theres enough then page can be filled
	*/
	if (pg_off>0) {
		_page_write(*(pp+page), src, left, pg_off);
		pg_off = 0;
		page++;
		src+=left;
	}
	}

	
	while(page<(__offset+__size)>>PAGE_SHIFT) {
		_page_write(*(pp+page), src, PAGE_SIZE, 0);
		page++;
		src+=PAGE_SIZE;
	}

	left = __size-(src-(_8_u*)__src);
	if (left>0) {
		_page_write(*(pp+page), src, left, 0);
	}
}
