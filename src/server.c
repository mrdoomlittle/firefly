# include "net.h"
# include "system/io.h"
# include "dep/str_len.h"
# include "dep/str_cpy.h"
# include "dep/bzero.h"
# include "dep/mem_set.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
/*
	_f_err_t err;
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(10198);
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_TCP);
	ff_net_bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	ff_net_listen(sock);
	FF_SOCKET *peer = ff_net_accept(sock, &err);

	char buf[128];
	f_str_cpy(buf, "abd");
	ff_net_send(peer, buf, 128, 0, &err);
	ff_net_close(peer);
	ff_net_close(sock);
*/
/*
	_f_err_t err;
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(10198);
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_UDP);
	ff_net_bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));

	char buf[128];
	f_bzero(buf, 128);
	ff_net_recv(sock, buf, 128, 0, &err);
	printf("got: %s\n", buf);
	ff_net_close(sock);

*/
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_TCP);
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(10198);

	struct f_netinfo ni = {
		.adr = &addr,
		.len = sizeof(struct sockaddr_in)
	};
	ff_net_bind(sock, &ni);
	FF_SOCKET *sk;
	sk = ff_net_minc(sock, &err);
	char buf[21299];
	f_mem_set(buf, 'F', 21299);
//	sock->prot.send(sk->ctx.priv_ctx, buf, 64, 0, &err);
	ff_net_send(sk, buf, 21299, 0, &err);
	ff_net_close(sk);

	ff_net_close(sock);
/*
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(21299);
	ff_net_bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)); 

	struct sockaddr from;
	f_bzero(&from, sizeof(struct sockaddr));
	socklen_t len;

	char buf[200];
	ff_net_recvfrom(sock, (struct sockaddr*)&from, &len, buf, 200, &err);
	printf("%s\n", buf);

	ff_net_close(sock);
*/
}
