# include "carriage.h"
# include "ffly_def.h"
# include "memory/mem_alloc.h"
# include "memory/mem_realloc.h"
# include "memory/mem_free.h"
# include "system/io.h"
struct carriage {
	_16_u i, n;
	_8_u bits;
	_16_u done;

	_int_u ibs;
	_64_u *ib;

	_16_u dud;
};

#define N 1
#define BUSY 0x01
static struct carriage cr[] = {
	{0, 0, 0x02, 0, 0, NULL, 0}
};

void ffly_carriage_put(_int_u __c) {
	struct carriage *c = cr+__c;
	c->bits |= BUSY;
}

_8_i ffly_carriage_turn(_int_u __c, _16_u __n) {
	struct carriage *c = cr+__c;

	_8_u off;
	_16_u b;

	off = 63-(__n-((b = (__n>>6))*64));
	return 0-(_8_i)((*(c->ib+b)>>off)&0x01);
}

_8_i ffly_carriage_ready(_int_u __c) {
	struct carriage *c = cr+__c;
	return -1+(_8_i)(c->bits&BUSY);
}

_16_u ffly_carriage_add(_int_u __c) {
	struct carriage *c = cr+__c;
	_16_u r;
	if (c->n == 0xffff) {
		printf("error maxed charriage.\n");
		while(1);
	}
	r = c->n++;
	if (!c->ib) {
		c->ib = (_64_u*)__f_mem_alloc(sizeof(_64_u));
		c->ibs++;
	} else {
		if ((r>>6) >= c->ibs)
			c->ib = (_64_u*)__f_mem_realloc(c->ib, (++c->ibs)*sizeof(_64_u));
		else
			goto _sk;
	}
	*(c->ib+r) = 0;
_sk:
	return r;
}

void ffly_carriage_rm(_int_u __c, _16_u __n) {

}

void ffly_carriage_done(_int_u __c, _16_u __n) {
	struct carriage *c = cr+__c;

	_8_u res;
	__asm__("lock incw %0" : "=m"(c->i));
_again:
	res = 0;
	__asm__("lock btrw $1, (%1)\n"
			"setcb %0"
	
	: "=m"(res) : "r"(&c->bits));

	if (!res && (c->bits&BUSY)>0) {
		goto _again;
	}

	if (res) {
		if (c->i != c->n-c->dud)
			/*
				reset
			*/
			c->bits |= 0x02;
		else
			/*
				remove busy bit
			*/
			c->bits ^= BUSY;
	}

	_8_u off;
	_16_u b;
    off = 63-(__n-((b = (__n>>6))*64));
	__asm__("movq $1, %%rax\n"
			"movb %1, %%cl\n"
			"shlq %%cl, %%rax\n"
			"lock orq %%rax, (%0)\n"
	: : "r"(c->ib+b), "m"(off) : "rax", "rcx");
	__asm__("lock incw %0" : "=m"(c->done));
}

void ffly_carriage_dud(_int_u __c) {
	cr[__c].dud++;
}

void ffly_carriage_udud(_int_u __c) {
	cr[__c].dud--;
}

void ffly_carriage_wait(_int_u __c) {
	struct carriage *c = cr+__c;
	if (!(c->n-c->dud))
		return;
	printf("waiting for bit to be removed.\n");
	while((c->bits&BUSY)>0) {
		if ((c->bits&BUSY)>0 && c->done == c->n-c->dud) {
			printf("error.\n");
		}
	}
	printf("waiting for all to finish.\n");
	while(c->done != c->n-c->dud);

	_64_u *p;
	_64_u *end;
	p = c->ib;
	end = p+c->ibs;
	while(p != end) {
		*p ^= *p;
		p++;
	}
}

void ffly_carriage_reset(_int_u __c) {
	struct carriage *c = cr+__c;
	c->i = 0;
	c->bits = 0x02;
	c->done = 0;
}

void ffly_carriage_cleanup(void) {
	struct carriage *c, *end;
	c = cr;
	end = c+N;
	while(c != end) {
		if (c->ib != NULL)
			__f_mem_free(c->ib);
		c++;
	}
}
