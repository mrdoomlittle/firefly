# ifndef __f__nail__h
# define __f__nail__h
# include "y_int.h"
/*
	will we want to keep track of things like maps, vecs, buffers
	etc we could have one list for each or do it this way
*/
struct f_nail {
	void *ptr;
	struct f_nail *next, **bk;
};

#define F_NW_VEC 0
#define F_NW_MAP 1

struct f_nail *f_nailat(_int_u);
struct f_nail *f_nail_attach(_int_u, void*);
void f_nail_detach(struct f_nail*);
# endif /*__f__nail__h*/
