# include "strf.h"
# include "io.h"
# include "string.h"
/*
	for low level stuff
*/
#define ishex(__c)\
	((__c >= '0' && __c <= '9') || (__c >= 'a' && __c <= 'f'))
_int_u static ithx(char *__buf, _64_u __int, _int_u __pad) {
	_8_u bit;
	_int_u i;
	i = 0;
	char *p;
	p = __buf;
	_int_u pad;
	pad = __pad<<2;
	while(i != pad) {
		bit = (__int>>((pad-4)-i))&0x0f;
		char c;
		if (bit>=0 && bit<=9) {
			c = '0'+bit;
		} else {
			c = 'a'+(bit-10);
		}
		*(p++) = c;
		i+=4;
	}
	return p-__buf;
}

static _int_u nds(char *__buf, _64_u __num) {
	char *p;
	p = __buf;
	if (!__num) {
		*__buf = '0';
		return 1;
	}

	_64_u pl, n = 0;
	pl = 1;
	for(;pl*10<=__num;pl*=10,n++);

	_64_u i;
	i = 0;
	while(i != n+1) {
		_64_u val;
		val = __num/pl;
		*(p++) = '0'+val;
		__num-=val*pl;
		pl/=10;
		i++;
	}
	return i;
}

_64_u _ffly_hxti(char *__buf, _int_u __size) {
	_64_u bit;

	char *p;
	p = __buf;
	_64_u i;
	i = __size;
	char c;
	_64_u out;
	out = 0;
	while(i != 0) {
		c = *(p++);
		if (c>='A'&&c<='Z') {
			c = 'a'+(c-'A');
			goto _j0;
		}
		if (c >= '0' && c <= '9') {
			bit = c-'0';
		} else if (c >= 'a' && c <= 'f') {
		_j0:
			bit = 10+(c-'a');
		} else {
			// error
			printf("error hxti got: %c.\n", c);
		}
		i--;
		out |= bit<<(i<<2);
	}
	return out;
}
/*
int main() {
	char const *str = "0000000000000001";
	printf("%u\n", _ffly_hxti((char*)str, 16));
}
*/
_int_u _ffly_nds(char *__buf, _64_u __num) {
	return nds(__buf, __num);
}

_int_u _ffly_ithx(char *__buf, _64_u __int, _int_u __pad) {
	return ithx(__buf, __int, __pad);
}

static _64_u pow10_tbl[] = {
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000,
	10000000,
	100000000,
	1000000000,
	10000000000,
	100000000000,
	1000000000000,
	10000000000000
};

_64_u _ffly_dsn(char *__buf, _int_u __len) {
	_int_u i;
	_64_u *pow;
	pow = pow10_tbl+__len-1;
	char c;
	_64_u r = 0;
	i = 0;
	while(i != __len) {
		r+= (__buf[i]-'0')**pow;
		pow--;
		i++;
	}
	return r;
}
/*
//indirect
_64_u _ffly_idsn(char *__buf, _16_u *__table,_int_u __len) {
	_int_u i;
	_64_u *pow;
	pow = pow10_tbl+__len-1;
	char c;
	_64_u r = 0;
	i = 0;
	while(i != __len) {
		r+= (__buf[__table[i]]-'0')**pow;
		pow--;
		i++;
	}
	return r;
}



*/

void ffly_scnf(_int_u __n, char *__buf, struct scn_block *__blks) {
	struct scn_block *b, *e;
	b = __blks;
	e = b+__n;

	while(b != e) {
		void *dst;
		char *s;
		dst = b->dst;
		s = __buf+b->start;
		switch(b->type) {
			case _scnb_h2:
				*(_int_u*)dst = _ffly_hxti(s, 2);
			break;
			case _scnb_h4:
				*(_int_u*)dst = _ffly_hxti(s, 4);
			break;
			case _scnb_i:
				*(_int_u*)dst = _ffly_dsn(s, b->len);
			break;
		}
		b++;
	}
}


//# include "dep/str_len.h"
//# include "dep/mem_cpy.h"

_int_u ffly_strf(char *__buf, _int_u __bfsize, char const *__format, ...) {
	_int_u r;
	va_list args;
	va_start(args, __format);
	r = ffly_strfa(__buf, __bfsize, __format, args);
	va_end(args);
	return r;
}

_int_u ffly_strfa(char *__buf, _int_u __bfsize, char const *__format, va_list __args) {
	char const *p;

	p = __format;
	char *d;
	d = __buf;
	_int_u pad;
	pad = 0;
	char c;
	while((c = *p) != '\0') {
		//printf("'%c'\n", c);
		if (c == '%') {
			c = *(++p);
			if (c == 's') {
				char *s = va_arg(__args, char*);
				_int_u len;
				len = str_len(s);
				if (len>0) { 
					mem_cpy(d, s, len);
					d+=len;
				}
			} else if (c == 'd') {
				_32_s v = va_arg(__args, _32_s);
				if (v<0) {
					v = -v;
					*(d++) = '-';
				}
				d+=nds(d, v);
			} else if (c == 'u') {
				d+=nds(d, va_arg(__args, _32_u));
			} else if (c == 'x') {
			_hex:
				d+=ithx(d, va_arg(__args, _32_u), pad);
				pad = 0;				
			} else if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f')) {
				char buf[4];
				char *dst;
				dst = buf;
	
				for(;ishex(c);c = *(++p)) {
					*(dst++) = c;
				}

				_64_u out;
				out = 0;
				while(--dst != buf) {
					c = *dst;
					if (c >= '0' && c <= '9')
						out = (out<<4)|((c-'0')&0x0f);
					else if (c >= 'a' && c <= 'f')
						out = (out<<4)|(((c-'a')+10)&0x0f);
				}
				pad = out;
				if (*p == 'x')
					goto _hex;
			}
			p++;
		} else {
			*(d++) = c;
			p++;
		}
	}
	*d = '\0';
	return d-__buf;
}
/*
int main() {
	char const *str = "0001:01:02";
	_int_u a, b, c;
	a = b = c = 0;
	struct scn_block blks[] = {
		{_scnb_h4, 0, 4, &a},
		{_scnb_h2, 5, 2, &b},
		{_scnb_h2, 8, 2, &c}
	};
	ffly_scnf(3, (char*)str, blks);
	printf("%u, %u, %u.\n", a, b, c);
}*/
/*
int main() {
	char path[256];
	memset(path, 0, 256);
	ffly_strf(path, 255, "rs.cis%u", 2);
	printf("'%s'\n", path);
}*/
