# include "chunkbuf.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "dep/mem_cpy.h"
#define newchunk ((struct f_cb_chunk*)__f_mem_alloc(sizeof(struct f_cb_chunk)))

#define attachchunk(__cb, __c)\
	(__cb)->c->next = __c;\
	(__cb)->c = __c;
void
f_cnkbuf_pushout(struct f_chunkbuf *__cb, void *__buf, _int_u __size) {
	_int_u coff = __cb->off&F_CB_CHUNK_MASK;
	__cb->off+=__size;
	_8_u *p = (_8_u*)__buf;
	_int_u am = 0;
	if (coff>0) {
		if (__size>(am = (F_CB_CHUNK_SIZE-coff))) {
			f_mem_cpy(__cb->c->buf+coff, p, am);
			p+=am;
			newchunk;
			coff = 0;
		} else {
			f_mem_cpy(__cb->c->buf+coff, p, __size);
			return;
		}
	}

	_int_u cs = __size-am;
	_int_u cc;
	cc = ((cs+(F_CB_CHUNK_SIZE-1))>>F_CB_CHUNK_SHFT);
	
	struct f_cb_chunk *_cc = __cb->c;
	if (cc>0) {
		struct f_cb_chunk **c, **cp;
		c = (struct f_cb_chunk**)__f_mem_alloc(cc*sizeof(struct f_cb_chunk*));

		_int_u i;
		i = 1;
		*c = _cc;
		for(;i != cc;i++) {
			c[i] = newchunk;
			attachchunk(__cb, c[i]);
		}
	
		cp = c;
		struct f_cb_chunk **e;
		e = cp+(cs>>F_CB_CHUNK_SHFT);
		while(cp<e) {
			f_mem_cpy((*cp)->buf, p, F_CB_CHUNK_SIZE);
			p+=F_CB_CHUNK_SIZE;
			cp++;
		}
		_cc = *cp;
		__f_mem_free(c);
	}
	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		f_mem_cpy(_cc->buf, p, F_CB_CHUNK_SIZE);
	}
	
	if (!(__cb->off&F_CB_CHUNK_MASK)) {
		newchunk;
	}
}

void f_cnkbuf_init(struct f_chunkbuf *__cb) {
	struct f_cb_chunk *c;
	c = newchunk;
	__cb->c = c;
	__cb->c_top = c;
	__cb->off = 0;
}

void f_cnkbuf_clean(struct f_chunkbuf *__cb) {
	struct f_cb_chunk *c, *bk;
	c = __cb->c_top;
	while(c != NULL) {
		c = (bk = c)->next;
		__f_mem_free(bk);
	}
}
