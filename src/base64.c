#include "base64.h"

char const static *table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
_int_u b64_encode(char *__dst, void *__src, _int_u __len) {
    char *p = (_8_u*)__dst;
    _8_u *src = (_8_u*)__src;
    _8_u *end = src+__len;
    _64_u buf = 0;
    _8_u n = 0;
    while (src != end) {
        buf = buf<<8|*src;
        n+=8;
       _bk:
        if (n>=6) {
            *(p++) = table[(buf>>(n-6)&0x3f)];
            n-=6;         
            goto _bk;
        }
        src++;
    }
    if (n>0)
        *(p++) = table[(buf<<(6-n)&0x3f)];
    return p-__dst;
}

_int_u b64_enc_len(_int_u __len) {
    _int_u ret = (((__len*8)>>1)/3);
    ret = ret+((__len-(ret*6))>0);
    return ret;
}

void b64_decode(void *__dst, void *__src, _int_u __len) {

}
/*
# include <string.h>
int main() {
    char buf[200];
    memset(buf, '\0', 200);
    char const *s = "Heiu8u28u8du8uhj";
    _int_u sl = strlen(s);
    _int_u len = ffly_b64_encode(buf, (void*)s, sl);
    printf("%u, %u\n", len, ffly_b64_enc_len(sl));
 
}*/
