#include "slab_dealer.h"
#include "m_alloc.h"
#include "ffly_def.h"
#include "io.h"
void sd_init(struct slab_dealer *__sd){
	__sd->offset = 0;
	__sd->sb_bin = NULL;
	__sd->n_inbin = 0;
}

_64_u static sb_alloc(struct slab_dealer *__sd){
	_64_u rt;
	if(__sd->sb_bin != NULL){
		struct sd_slab *sb;
		sb = __sd->sb_bin;
		__sd->sb_bin = sb->next;
		rt = sb->offset;
		m_free(sb);
		__sd->n_inbin--;
		goto _rt;
	}

	rt = __sd->offset;

	__sd->offset+=SD_SSIZE;
_rt:
//	printf("allocated new slab at: %x.\n",rt);
	return rt;
}

void sb_free(struct slab_dealer *__sd, _64_u __offset){
	struct sd_slab*
	sb = m_alloc(sizeof(struct sd_slab));

	sb->offset = __offset;

	sb->next = __sd->sb_bin;
	__sd->sb_bin = sb;
	__sd->n_inbin++;
}

void sd_realloc(struct sd_area *__a,_64_u __size){
	_int_u nsb;
	nsb = (__size+SD_SMASK)>>SD_SSHFT;
	printf("reloc from %u to %u slabs.\n",__a->n_slabs,nsb);
	if(nsb<__a->n_slabs){
		_int_u i;
		i = nsb;
		for(;i != __a->n_slabs;i++){
			sb_free(__a->dealer,__a->slabs[i]);
		}
		__a->n_slabs = nsb;
		__a->slabs = m_realloc(__a->slabs,nsb*sizeof(_64_u));
	}else if(nsb>__a->n_slabs){
		_int_u i;
		i = __a->n_slabs;
		__a->n_slabs = nsb;
		__a->slabs = m_realloc(__a->slabs,nsb*sizeof(_64_u));
		for(;i != nsb;i++){
			__a->slabs[i] = sb_alloc(__a->dealer);
		}
	}
}

struct sd_area* sd_alloc(struct slab_dealer *__sd,_64_u __size){
	struct sd_area *area;
	area = m_alloc(sizeof(struct sd_area));
	_int_u nsb;
	nsb = (__size+SD_SMASK)>>SD_SSHFT;
	area->slabs = m_alloc(nsb*sizeof(_64_u));
	area->n_slabs = nsb;
	_int_u i;
	i = 0;
	for(;i != nsb;i++){
		area->slabs[i] = sb_alloc(__sd);
	}
	area->dealer = __sd;
}
void sd_free(struct sd_area *__area){
	_int_u i;
	i = 0;
	for(;i != __area->n_slabs;i++){
		sb_free(__area->dealer,__area->slabs[i]);
	}
	m_free(__area);
}

void static _read_write(struct sd_area *__area,_8_u *__buf,_64_u __size,_64_u __offset,void(*read_write)(_ulonglong,void*,_64_u,_64_u)){
	_64_u sbof;
	sbof = __offset&SD_SMASK;
	_64_u ss;
	ss = __offset>>SD_SSHFT;
	_64_u off = 0;
	_ulonglong arg;
	arg = __area->dealer->arg;
	_int_s left;
	left = SD_SSIZE-sbof;
	/*
		align to slabs by correcting sbof
	*/
	if(sbof>0 && __size>=left){
//		printf("check, %u,%u\n",left,sbof);
		read_write(arg,__buf,left,__area->slabs[ss]+sbof);
		off = left;
		__size-=left;
		sbof = 0;
		ss++;
	}

	_int_u ns;
	ns = __size>>SD_SSHFT;
	_int_u i;
	i = 0;
	for(;i != ns;i++){
		read_write(arg,__buf+off,SD_SSIZE,__area->slabs[ss]);
		off+=SD_SSIZE;
		ss++;
	}

	left = __size-off;
	if(left>0){
		read_write(arg,__buf+off,left,__area->slabs[ss]+sbof);
	}
}

void sd_read(struct sd_area *__area,void *__buf,_64_u __size,_64_u __offset){
	_read_write(__area,__buf,__size,__offset,__area->dealer->read);	
}

void sd_write(struct sd_area *__area,void *__buf,_64_u __size,_64_u __offset){
	_read_write(__area,__buf,__size,__offset,__area->dealer->write);
}
_8_u static buffer[512*64];
void static _write_data(_ulonglong arg,_8_u *__buf,_64_u __size,_64_u __offset){
  mem_cpy(buffer+__offset,__buf,__size);
}
void static _read_data(_ulonglong arg,_8_u *__buf,_64_u __size,_64_u __offset){
  mem_cpy(__buf,buffer+__offset,__size);
}

void sd_test(void) {
  struct slab_dealer dealer;
  dealer.read = _read_data;
  dealer.write = _write_data;
  sd_init(&dealer);
  struct sd_area *area;

  char buf[4096];
  _64_u offset;
  offset = 0;
  for(;offset != 4096-32;offset+=32){
    area = sd_alloc(&dealer,4096);
    sd_write(area,"hello",6,offset);

    sd_read(area,buf,32,offset);
    if(str_cmp(buf,"hello") == -1){
      printf("error, %u.\n",offset);
    }
    printf("%s.\n",buf);
    sd_free(area);
  }
}
