# ifndef __ffly__carriage__h
# define __ffly__carriage__h
# include "y_int.h"
/*
	send eveyone a letter and wait for everyone to send a reply saying they got the letter
*/
enum {
	_ff_carr0
};

_8_i ffly_carriage_ready(_int_u);
_8_i ffly_carriage_turn(_int_u, _16_u);
_16_u ffly_carriage_add(_int_u);
void ffly_carriage_put(_int_u);
void ffly_carriage_done(_int_u, _16_u);
void ffly_carriage_wait(_int_u);
void ffly_carriage_reset(_int_u);
void ffly_carriage_dud(_int_u);
void ffly_carriage_udud(_int_u);
void ffly_carriage_cleanup(void);
# endif /*__ffly__carriage__h*/
