#ifndef __slam__h
#define __slam__h

/*
	simple loading model
	

*/
#include "header.h"
struct slm_segment {
	QWORD off;
	QWORD bytes;
	/*
		address is not importent as its expected segments to be inorder
	*/
	QWORD adr;
	QWORD words;
};

struct slam_hdr {
	/*
		TODO:
		other types of segments to be included,

		instructions/program-bits,
		memory for global variables
	*/
	QWORD sg;
	QWORD ns;
};

#endif/*__slam__h*/
