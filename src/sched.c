#include "sched.h"
#include "msg.h"
#include "m_alloc.h"
#include "piston.h"
#include "cradle.h"
#include "mutex.h"
#include "ffly_def.h"
#include "clock.h"
#include "lib/cellar.h"
static struct cellar cel;
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSSCHED)
#define sched_lock mt_lock(&lock);
#define sched_unlock mt_unlock(&lock);
#define BLKSIZE (sizeof(struct sched_entity)<<PAGE_SHFT)
#define PAGE_SHFT 6
mlock static lock = MUTEX_INIT;
static sched_entityp dead = NULL;
sched_entityp sched_new(void){
	sched_lock
	sched_entityp ent;
	if(dead != NULL) {
		ent = dead;
		dead = dead->fd;
		goto _dead;
	}

	_int_u off;
	off = cel.off++;
	celr_overflow(&cel,PAGE_SHFT){
		celr_more(&cel,BLKSIZE);
	}

	ent = celr_at(&cel,off,PAGE_SHFT,struct sched_entity);
	ent->off = off;
_dead:
	MSG(INFO, "sched, %u, %p\n", off,ent)
	ent->last_time = clockval.val;
	ent->id = 0;
	ent->flags = 0;
	sched_unlock
	return ent;
}

void sched_submit(sched_entityp __ent){
	sched_entity_attach(__ent,sched_preferred());
}
void entity_detach_nolock(sched_entityp);
void static
remove(sched_entityp __ent) {
	entity_detach_nolock(__ent);
	sched_lock
	__ent->fd = dead;
	dead = __ent;
	sched_unlock
}

void sched_rm(sched_entityp __ent) {
	remove(__ent);
	sched_entity_detach(__ent);
}

#define setlock(__bit)\
	__asm__("lock xorq %%rax, combits(%%rip)\n" : : "a"(__bit));
_64_u static combits = -1;
_64_u static ident = -1;
_8_s static lockup = -1;
#include "clock.h"
void static attempt_to_level(sched_entityp __ent,struct pcore *to,_64_u delt){
	if(to->delay+delt >__ent->core->delay){
		return;//nothing accomplished
	}

	sched_migrate(__ent,to);
}
#include "cradle.h"

void static just_run(sched_entityp __ent){
	struct y_clockval now,end;
	now = clockval;
	if(!__ent->func(__ent)){
		remove(__ent);
		return;
	}

	end = clockval;
	_64_u delt;
	delt = now.val-end.val;
	if(!__ent->core->id){		
		attempt_to_level(__ent,&_f_cradle.pcores[_f_cradle.npc-1],delt);
	}else{
		attempt_to_level(__ent,__ent->core-1,delt);
	}
}

void static time_bound(sched_entityp __ent){
	if ((clockval.val-__ent->last_time)>=__ent->iv) {
		__ent->last_time = clockval.val;
		just_run(__ent);
	}
}

void static runthrough(struct sched_list *__ls, void(*exec_job)(sched_entityp)){
	mt_lock(&__ls->lock);
	

	sched_entityp cur, ent;
	cur = (sched_entityp)__ls->head.next;
	while(cur != NULL) {	
		cur = (ent = cur)->link.next;
		exec_job(ent);
	}

	mt_unlock(&__ls->lock);
}

void sched_tick(struct pcore *__c) {
	if (!lockup) {
		if (!(combits&(1<<__c->id))) {
			return;
		}
		setlock(1<<__c->id);
		return;
	}

	struct sched_core *s;
	s = &__c->sched;
	runthrough(s->mann[0].list+0,time_bound);
	runthrough(s->mann[0].list+1,time_bound);
	runthrough(s->mann[1].list+0,just_run);
	runthrough(s->mann[1].list+1,just_run);
}

void static mann_init(struct sched_mann *mn){
	y_iwlist_init(&mn->list[0].head);
	y_iwlist_init(&mn->list[1].head);
	mn->list[0].cur = &mn->list[0].head;
	mn->list[1].cur = &mn->list[1].head;
	mn->list[0].lock = MUTEX_INIT;
	mn->list[1].lock = MUTEX_INIT;
	mn->ent_n = 0;
}

void sched_init(void) {
	struct pcore *p;
	struct sched_core *s;
	_int_u i;
	i = 0;
	for(;i != _f_cradle.npc;i++) {
		s = &(p = _f_cradle.pcores+i)->sched;
		mann_init(s->mann+0);
		mann_init(s->mann+1);
		ident ^= p->sig;
	}

	celr_init(&cel,BLKSIZE);
}

void sched_de_init(void) {
	struct y_timespec ts0, ts1;
	lockup = 0;
	MSG(INFO, "finishing up waiting on sig-%x.\n", ident)
	y_clock_gettime(&ts0);
	_64_u start = clockval.val;
	while(combits != ident) {
		if (clockval.val>=(start+TIME_INSEC(10))) {
			MSG(ERROR, "some cores are unresponsive.\n")
			break;
		}
	}
	y_clock_gettime(&ts1);
	MSG(INFO, "took %u.%u-clicks for core lockup.\n", ts1.higher-ts0.higher, ts1.lower-ts0.lower)
}
