s_ldw,4 16,%s#12,%s#0
waitcnt 0
;LOAD
load,xyzw'32f4 0,%v#0'idx,%v#8,%s#3,0
waitcnt 0
expd %par#1,%v#8,%v#9,%v#10,%v#11
waitcnt 0
;VOP1
mov32v %v#12,%v#8
;VOP1
mov32v %v#13,%v#10
;VOP1
mov32v %v#14,0.0
;VOP1
mov32v %v#15,0.0
expd %par#2,%v#12,%v#13,%v#14,%v#15
waitcnt 0
;LOAD16
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 16,0,%v#92,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 32,0,%v#96,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 48,0,%v#100,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 64,0,%v#104,%s#3,0
waitcnt 0
;VOP2
mul32f %v#8,%v#92,%v#12
;VOP2
mac32f %v#9,%v#93,%v#12
;VOP2
mac32f %v#10,%v#94,%v#12
;VOP2
mac32f %v#11,%v#95,%v#12
;VOP2
mul32f %v#8,%v#96,%v#13
;VOP2
mac32f %v#9,%v#97,%v#13
;VOP2
mac32f %v#10,%v#98,%v#13
;VOP2
mac32f %v#11,%v#99,%v#13
;VOP2
mul32f %v#8,%v#100,%v#14
;VOP2
mac32f %v#9,%v#101,%v#14
;VOP2
mac32f %v#10,%v#102,%v#14
;VOP2
mac32f %v#11,%v#103,%v#14
;VOP2
mul32f %v#8,%v#104,%v#15
;VOP2
mac32f %v#9,%v#105,%v#15
;VOP2
mac32f %v#10,%v#106,%v#15
;VOP2
mac32f %v#11,%v#107,%v#15
mov32v %v#8,%v#12
mov32v %v#9,%v#13
mov32v %v#10,%v#14
mov32v %v#11,%v#15
;LOAD16
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 208,0,%v#92,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 224,0,%v#96,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 240,0,%v#100,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 256,0,%v#104,%s#3,0
waitcnt 0
;VOP2
mul32f %v#8,%v#92,%v#12
;VOP2
mac32f %v#9,%v#93,%v#12
;VOP2
mac32f %v#10,%v#94,%v#12
;VOP2
mac32f %v#11,%v#95,%v#12
;VOP2
mul32f %v#8,%v#96,%v#13
;VOP2
mac32f %v#9,%v#97,%v#13
;VOP2
mac32f %v#10,%v#98,%v#13
;VOP2
mac32f %v#11,%v#99,%v#13
;VOP2
mul32f %v#8,%v#100,%v#14
;VOP2
mac32f %v#9,%v#101,%v#14
;VOP2
mac32f %v#10,%v#102,%v#14
;VOP2
mac32f %v#11,%v#103,%v#14
;VOP2
mul32f %v#8,%v#104,%v#15
;VOP2
mac32f %v#9,%v#105,%v#15
;VOP2
mac32f %v#10,%v#106,%v#15
;VOP2
mac32f %v#11,%v#107,%v#15
mov32v %v#8,%v#12
mov32v %v#9,%v#13
mov32v %v#10,%v#14
mov32v %v#11,%v#15
;LOAD16
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 80,0,%v#92,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 96,0,%v#96,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 112,0,%v#100,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 128,0,%v#104,%s#3,0
waitcnt 0
;VOP2
mul32f %v#8,%v#92,%v#12
;VOP2
mac32f %v#9,%v#93,%v#12
;VOP2
mac32f %v#10,%v#94,%v#12
;VOP2
mac32f %v#11,%v#95,%v#12
;VOP2
mul32f %v#8,%v#96,%v#13
;VOP2
mac32f %v#9,%v#97,%v#13
;VOP2
mac32f %v#10,%v#98,%v#13
;VOP2
mac32f %v#11,%v#99,%v#13
;VOP2
mul32f %v#8,%v#100,%v#14
;VOP2
mac32f %v#9,%v#101,%v#14
;VOP2
mac32f %v#10,%v#102,%v#14
;VOP2
mac32f %v#11,%v#103,%v#14
;VOP2
mul32f %v#8,%v#104,%v#15
;VOP2
mac32f %v#9,%v#105,%v#15
;VOP2
mac32f %v#10,%v#106,%v#15
;VOP2
mac32f %v#11,%v#107,%v#15
mov32v %v#8,%v#12
mov32v %v#9,%v#13
mov32v %v#10,%v#14
mov32v %v#11,%v#15
expd %pos#0,%v#8,%v#9,%v#10,%v#11`
waitcnt 0
;LOAD16
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 144,0,%v#92,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 160,0,%v#96,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 176,0,%v#100,%s#3,0
waitcnt 0
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 192,0,%v#104,%s#3,0
waitcnt 0
s_ldw,4 32,%s#12,%s#0
waitcnt 0
;LOAD
load,xyzw'32f4 0,%v#2'idx,%v#8,%s#3,0
waitcnt 0
expd %par#0,%v#8,%v#9,%v#10,%v#11
waitcnt 0
end 0
